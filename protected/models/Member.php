<?php

/**
 * This is the model class for table "member".
 *
 * The followings are the available columns in table 'member':
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $nama
 * @property string $alamat
 * @property string $telepon
 * @property string $nama_instansi
 * @property string $alamat_instansi
 * @property string $telepon_instansi
 * @property string $login_terakhir
 * @property string $waktu_dibuat
 */
class Member extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'member';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email,password,nama,telepon,alamat','required','message'=>'{attribute} tidak boleh kosong.'),
			array('email, password, nama, telepon, nama_instansi, telepon_instansi', 'length', 'max'=>255),
			array('email','unique','on'=>'signup','message'=>'Email sudah digunakan oleh member lain. Silahkan gunakan email yang lain.'),
			array('alamat, alamat_instansi, aktif, login_terakhir, waktu_dibuat', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, email, password, nama, alamat, telepon, nama_instansi, alamat_instansi, telepon_instansi, login_terakhir, waktu_dibuat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'password' => 'Password',
			'nama' => 'Nama',
			'alamat' => 'Alamat',
			'telepon' => 'Telepon',
			'nama_instansi' => 'Nama Instansi',
			'alamat_instansi' => 'Alamat Instansi',
			'telepon_instansi' => 'Telepon Instansi',
			'login_terakhir' => 'Login Terakhir',
			'waktu_dibuat' => 'Waktu Dibuat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('aktif',$this->aktif);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('telepon',$this->telepon,true);
		$criteria->compare('nama_instansi',$this->nama_instansi,true);
		$criteria->compare('alamat_instansi',$this->alamat_instansi,true);
		$criteria->compare('telepon_instansi',$this->telepon_instansi,true);
		$criteria->compare('login_terakhir',$this->login_terakhir,true);
		$criteria->compare('waktu_dibuat',$this->waktu_dibuat,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Member the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getDataInovasi()
	{
		$model = Inovasi::model()->findAllByAttributes(array('member_id'=>$this->id),array('order'=>'waktu_dibuat DESC'));
		if($model!==null)
			return $model;
		else	
			return false;
	}
	
	public function sendSignupEmail()
	{
		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

		// Additional headers
		$headers .= 'To: '.$this->nama.' <'.$this->email.'>' . "\r\n";
		$headers .= 'From: Inovasi Administrasi Negara <noreply@inovasi.lan.go.id>' . "\r\n";
		$headers .= 'Cc: p2ipk@lan.go.id, p2ipk.inovasi@gmail.com' . "\r\n";
		//$headers .= 'Bcc: birthdaycheck@example.com' . "\r\n";
		
		$subject = 'Konfirmasi Pendaftaran Member Website Inovasi Administrasi Negara';
		
		$message = '<p>Terima kasih sudah melakukan pendaftaran di website Inovasi Administrasi Negara</p>';
		$message .= '<p>Berikut ini data informasi member yang Anda isi:</p>';
		
		$message .= '<p><b>Informasi Login</b> <br>';
		$message .= 'Email: '.$this->email.'<br>';
		$message .= 'Password: Tidak ditampilkan </p>';
		
		$message .= '<p><b>Data Member</b> <br>';
		$message .= 'Nama: '.$this->nama.'<br>';
		$message .= 'Alamat: '.$this->alamat.'<br>';
		$message .= 'Telepon: '.$this->telepon.'</p>';
		
		$message .= '<p><b>Data Instansi</b> <br>';
		$message .= 'Nama Instansi: '.$this->nama_instansi.'<br>';
		$message .= 'Alamat Instansi: '.$this->alamat_instansi.'<br>';
		$message .= 'Telepon Instansi: '.$this->telepon_instansi.'</p>';
		
		$message .= '<p>Akun member anda harus diaktifkan terlebih dahulu oleh admin website sebelum 
		dapat digunakan untuk login. Anda akan menerima email konfirmasi jika akun member Anda sudah diaktifkan.</p>';
		
		
		//mail($this->email, $subject, $message, $headers);
		//mail('inovasi@lan.go.id', $subject, $message, $headers);
		mail($this->email,$subject,$message,$headers);
	}
	
	public function sendActivationEmail()
	{
		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

		// Additional headers
		$headers .= 'To: '.$this->nama.' <'.$this->email.'>' . "\r\n";
		$headers .= 'From: Inovasi Administrasi Negara <noreply@inovasi.lan.go.id>' . "\r\n";
		$headers .= 'Cc: p2ipk@lan.go.id, p2ipk.inovasi@gmail.com' . "\r\n";
		//$headers .= 'Bcc: birthdaycheck@example.com' . "\r\n";
		
		$subject = 'Aktivasi Member Website Inovasi Administrasi Negara';
		
		$message .= '<p>Selamat. Member dengan data berikut ini:</p>';
		
		$message .= '<p><b>Informasi Login</b> <br>';
		$message .= 'Email: '.$this->email.'<br>';
		$message .= 'Password: Tidak ditampilkan </p>';
		
		$message .= '<p><b>Data Member</b> <br>';
		$message .= 'Nama: '.$this->nama.'<br>';
		$message .= 'Alamat: '.$this->alamat.'<br>';
		$message .= 'Telepon: '.$this->telepon.'</p>';
		
		$message .= '<p><b>Data Instansi</b> <br>';
		$message .= 'Nama Instansi: '.$this->nama_instansi.'<br>';
		$message .= 'Alamat Instansi: '.$this->alamat_instansi.'<br>';
		$message .= 'Telepon Instansi: '.$this->telepon_instansi.'</p>';
		
		$message .= '<p>Telah <b>AKTIF</b>. Member dapat login untuk mulai mengirimkan data inovasi.</p>';
		
		//mail($this->email, $subject, $message, $headers);
		//mail('inovasi@lan.go.id', $subject, $message, $headers);
		mail($this->email,$subject,$message,$headers);
	}
}
