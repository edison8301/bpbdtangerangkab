<?php

/**
 * This is the model class for table "page_video".
 *
 * The followings are the available columns in table 'page_video':
 * @property integer $id
 * @property integer $page_id
 * @property string $mp4
 * @property string $ogv
 * @property string $webm
 */
class PageVideo extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PageVideo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'page_video';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('page_id', 'required'),
			array('page_id', 'numerical', 'integerOnly'=>true),
			array('mp4, ogv, webm', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, page_id, mp4, ogv, webm', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'page_id' => 'Page',
			'mp4' => 'Mp4',
			'ogv' => 'Ogv',
			'webm' => 'Webm',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('page_id',$this->page_id);
		$criteria->compare('mp4',$this->mp4,true);
		$criteria->compare('ogv',$this->ogv,true);
		$criteria->compare('webm',$this->webm,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}