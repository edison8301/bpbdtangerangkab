<?php

/**
 * This is the model class for table "event".
 *
 * The followings are the available columns in table 'event':
 * @property integer $id
 * @property string $title
 * @property string $date
 * @property string $time
 * @property string $place
 * @property integer $event_category_id
 */
class Event extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'event';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('event_category_id', 'numerical', 'integerOnly'=>true),
			array('title, place', 'length', 'max'=>255),
			array('date, time, description, time_created, thumbnail', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, date, time, place, event_category_id, thumbnail, time_created, skpd_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'EventCategory'=>array(self::BELONGS_TO,'EventCategory','event_category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'date' => 'Date',
			'time' => 'Time',
			'place' => 'Place',
			'event_category_id' => 'Category',
			'thumbnail' => 'Thumbnail',
			'created_time' => 'Created Time',
			'skpd_id' => 'SKPD',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('time',$this->time,true);
		$criteria->compare('place',$this->place,true);
		$criteria->compare('event_category_id',$this->event_category_id);
		$criteria->compare('thumbnail',$this->thumbnail,true);
		$criteria->compare('time_created',$this->time_created,true);
		
		$criteria->order = 'time_created DESC';
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Event the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getEventLang($lang=null)
	{
	
		if($lang==null)
		{
			$lang = Helper::getLang();
		}
	
		if($this->isNewRecord)
		{
			$model = new EventLang;
		} else {
			$model = EventLang::model()->findByAttributes(array('event_id'=>$this->id,'lang'=>$lang));
			
			if(empty($model))
			{
				$model = new EventLang;
				$model->lang = $lang;
				$model->event_id = $this->id;
				$model->save();
			}
			
		}
		return $model;
	}
	
	public function getLatestEvent($limit=null)
	{
		if($limit==null) $limit = 5;
		$criteria = new CDbCriteria;
		$criteria->order = 'date DESC';
		$criteria->limit = $limit;

		return Event::model()->findAll($criteria);			
	}
	
	public function getThumbnailImage()
	{
		if($this->thumbnail!='')
			return CHtml::image(Yii::app()->theme->baseUrl.'/uploads/event/'.$this->thumbnail,'',array('class'=>'img-responsive center-block'));
		else
			return CHtml::image(Yii::app()->theme->baseUrl.'/uploads/post/default.png','',array('class'=>'img-responsive center-block'));
	}
	
	public function getThumbnail()
	{
		if($this->thumbnail!='')
			return CHtml::image(Yii::app()->theme->baseUrl.'/uploads/event/'.$this->thumbnail,'',array('class'=>'img-responsive'));
		else
			return CHtml::image(Yii::app()->theme->baseUrl.'/uploads/post/default.png','',array('class'=>'img-responsive'));
	}
	
	public function getBackendThumbnail($data,$class)
	{
		if($data!='')
			return CHtml::image(Yii::app()->request->baseUrl.'/uploads/event/'.$data,'',array('class'=>$class));
		else
			return CHtml::image(Yii::app()->request->baseUrl.'/images/image_not_found.gif','',array('class'=>$class));
	}
	
	public function getEventForCalendar()
	{
		$event = array();
		foreach(EventLang::model()->findAllByAttributes(array('lang'=>Helper::getLang()),array('order'=>'id DESC')) as $data)
		{
			$event[] = array('title'=>$data->title,'start'=>$data->Event->date,'color'=>'#08659d','allDay'=>true,'url'=>Yii::app()->createUrl('event/read',array('id'=>$data->event_id)));
		}
		return $event;
	}
	
	public function getEventChart($type)
	{
		$criteria = new CDbCriteria;
		$criteria->addCondition('YEAR(created_time) = :year');
		$criteria->params[':year']=date('Y');
		
		$list = array();
		$model = EventCategory::model()->findAll();
		
		if($type=='bar')
			foreach($model as $data)
			{
				$count = count(Event::model()->findAllByAttributes(array('event_category_id'=>$data->id),$criteria));
				$list[] = array('type'=>'column','name'=>$data->title,'data'=>array((int)$count));
			}
		if($type=='pie')
			foreach($model as $data)
			{
				$count = count(Event::model()->findAllByAttributes(array('event_category_id'=>$data->id),$criteria));
				$list[] = array($data->title,(int)$count);
			}
		if($type=='stack')
			foreach($model as $data)
			{
				$count = count(Event::model()->findAllByAttributes(array('event_category_id'=>$data->id),$criteria));
				$list[] = array('type'=>'column','name'=>$data->title,'data'=>array(
					$data->getEventReport($data->id,01,date('Y')),
					$data->getEventReport($data->id,02,date('Y')),
					$data->getEventReport($data->id,03,date('Y')),
					$data->getEventReport($data->id,04,date('Y')),
					$data->getEventReport($data->id,05,date('Y')),
					$data->getEventReport($data->id,06,date('Y')),
					$data->getEventReport($data->id,07,date('Y')),
					$data->getEventReport($data->id,08,date('Y')),
					$data->getEventReport($data->id,09,date('Y')),
					$data->getEventReport($data->id,10,date('Y')),
					$data->getEventReport($data->id,11,date('Y')),
					$data->getEventReport($data->id,12,date('Y')),
				));
			}
			
		return $list;
	}
	
	
	
	public function getEventReport()
	{
		$list = array();
		$model = EventCategory::model()->findAll();
		foreach($model as $data)
		{
			$count = count(Event::model()->findAllByAttributes(array('event_category_id'=>$data->id)));
			$list[] = array('type'=>'column','name'=>$data->title,'data'=>array((int)$count));
		}
		return $list;
	}
	
	public function getEventforRead($id)
	{	
		return EventLang::model()->findByAttributes(array('lang'=>Helper::getLang(),'event_id'=>$id));;
	}
	
	public function getOtherEvent()
	{
		$criteria = new CDbCriteria;
		$criteria->limit = 5;
		
		$model = Event::model()->findAll($criteria);
		
		if($model!==null)
			return $model;
		else
			return false;
	}
	
	public function getCategory($category_id=null)
	{
		if($category_id!=null)
			return Event::model()->findByAttributes(array('event_category_id'=>$category_id));
		if($category_id==null)
			return EventCategory::model()->findAll();
	}
	
	public function getSkpd($skpd_id=null)
	{
		if($skpd_id!=null)
			return Post::model()->findByAttributes(array('skpd_id'=>$skpd_id));
		if($skpd_id==null)
			return Skpd::model()->findAll();
	}
	
	public function getRelationField($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
	}
	
	public function getEventYearly()
	{
		$year = date('Y');
		$criteria = new CDbCriteria;
		$criteria->addCondition('YEAR(date) = :year');
		$criteria->params[':year']=date($year);
		$criteria->order = 'date ASC';
		
		$list = Event::model()->findAll($criteria);
		
		return $list;
	}
	
	
}