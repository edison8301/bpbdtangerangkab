<?php

/**
 * This is the model class for table "page".
 *
 * The followings are the available columns in table 'page':
 * @property integer $id
 * @property string $title
 * @property string $content
 */
class Page extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Page the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'page';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('title, image', 'length', 'max'=>255),
			//array('cover_image','file','allowEmpty'=>true,'types'=>'jpg,JPG,gif,GIF,png,PNG','maxSize'=>1024*500,'tooLarge'=>'File maksimal 500 KB'),
			array('content,time_created', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, content', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'video'=>array(self::HAS_ONE,'PageVideo','page_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'content' => 'Content',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('content',$this->content,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getDataDownload()
	{
		$model = Download::model()->findAllByAttributes(array('model'=>'Page','model_id'=>$this->id));
		
		if($model!==null)
			return $model;
		else
			return false;
	}
	
	public function countDataDownload()
	{
		return Download::model()->countByAttributes(array('model'=>'Page','model_id'=>$this->id));
	}
	
	public function getDataPhoto()
	{
		$model = Photo::model()->findAllByAttributes(array('model'=>'Page','model_id'=>$this->id));
		
		if($model!==null)
			return $model;
		else
			return false;
	}
	
	public function countDataPhoto()
	{
		return Photo::model()->countByAttributes(array('model'=>'Page','model_id'=>$this->id));
	}
	
	public function getDataVideo()
	{
		$model = Video::model()->findAllByAttributes(array('model'=>'Page','model_id'=>$this->id));
		
		if($model!==null)
			return $model;
		else
			return false;
	}
	
	public function countDataVideo()
	{
		return Video::model()->countByAttributes(array('model'=>'Page','model_id'=>$this->id));
	}

	public function getImage($htmlOptions=array())
	{
		if($this->image!='')
			return CHtml::image(Yii::app()->theme->baseUrl.'/uploads/page/'.$this->image,'',$htmlOptions);
		else
			return CHtml::image(Yii::app()->theme->baseUrl.'/images/no-image.jpg','',$htmlOptions);
	}
}