<?php

/**
 * This is the model class for table "block".
 *
 * The followings are the available columns in table 'block':
 * @property integer $id
 * @property string $title
 * @property string $content
 */
class Block extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Block the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'block';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('order, block_status_id','numerical','integerOnly'=>true),
			array('title', 'length', 'max'=>255),
			array('content, block_position_id, block_type_id', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, content', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'active'=>array(self::BELONGS_TO,'Active','active_id'),
			'block_mode'=>array(self::BELONGS_TO,'BlockMode','block_mode_id'),
			'block_position'=>array(self::BELONGS_TO,'BlockPosition','block_position_id'),
			'block_type'=>array(self::BELONGS_TO,'BlockType','block_type_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'block_type_id'=>'Type',
			'block_position_id'=>'Position',
			'block_status_id'=>'Status',
			'content' => 'Content',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('block_type_id',$this->block_type_id,true);
		$criteria->compare('block_position_id',$this->block_position_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function saveBlockAttribute($array)
	{
		foreach($array as $key => $value)
		{
			$this->saveBlockAttributeByKey($key,$value);
		}
		
		return true;
	}
	
	public function saveBlockAttributeByKey($key,$value)
	{
		$model = $this->findBlockAttributeByKey($key);
		if($model!==null)
		{
			$model->value=$value;
		} else {
			$model = new BlockAttribute;
			$model->block_id = $this->id;
			$model->key = $key;
			$model->value = $value;	
		}
		
		$model->save();
		
		return true;
	}
	
	public function findBlockAttributeByKey($key)
	{
		$model = BlockAttribute::model()->findByAttributes(array('block_id'=>$this->id,'key'=>$key));
		
		if($model!==null)
			return $model;
		else
			return null;
	}
	
	public function getBlockAttributeValueByKey($key)
	{
		$model = $this->findBlockAttributeByKey($key);
		if($model!=null)
			return $model->value;
		else
			return null;
	}
	
	public function displayBlock()
	{
		$output = '<div class="box">';
		$output .= '<div class="box-header">';
		$output .= $this->title;
		$output .= '</div>';
		$output .= '<div class="box-content">';
		$output .= $this->getContent();
		$output .= '</div>';
		$output .= '</div>';
		
		return $output;
	}
	
	public function getContent()
	{
		$output = '';
		
		//text
		if($this->block_type_id == 'text')
		{
			$output = $this->getBlockAttributeValueByKey('text');
		}
		
		//html
		if($this->block_type_id == 'html')
		{
			$output = $this->getBlockAttributeValueByKey('html');
		}
		
		//page
		if($this->block_type_id == 3)
		{
			$page_id = $this->getBlockAttributeValueByKey('page_id');
			
			$model = Page::model()->findByPk($page_id);
			
			$output .= $model->content;
		}
		
		//post
		if($this->block_type_id == 4)
		{
			$category_id = $this->getBlockAttributeValueByKey('post_category_id');
			
			
			foreach(Post::model()->findAllByAttributes(array('post_category_id'=>$category_id),array('order'=>'date_created DESC','limit'=>5)) as $post)
			{
				$output .='<div class="block-post">';
				$output .= '<div class="block-post-title">'.CHtml::link($post->title,array('post/read','id'=>$post->id)).'</div>';
				$output .= '<div class="block-post-date_created">'.$post->date_created.'</div>';
				$output .= '<div class="row-fluid">';
				if($post->thumbnail!='') {
				$output .= '<div class="span3">'.CHtml::image(Yii::app()->request->baseUrl.'/uploads/post/'.$post->thumbnail).'</div>';
				$output .= '<div class="span9">'.$post->content.'</div>';
				} else {
				$output .= '<div class="span12">'.$post->content.'</div>';
				}
				$output .= '</div>';
				$output .= '</div>';
			}

		}
		
		//Video Single
		if($this->block_type_id == 5)
		{
			$video_id = $this->getBlockAttributeValueByKey('video_id');
			
			$model = Video::model()->findByPk($video_id);
			
			if($model->video_type_id == 1)
			{
				$video_code = $model->getVideoAttributeValueByKey('youtube_url');
				$video_height = $model->getVideoAttributeValueByKey('youtube_height');
				$video_width = $model->getVideoAttributeValueByKey('youtube_width');
				$video_code = str_replace('http://www.youtube.com/watch?v=','',$video_code);
				$video_code = str_replace('http://youtube.com/watch?v=','',$video_code);
				$video_code = str_replace('www.youtube.com/watch?v=','',$video_code);
				$video_code = str_replace('youtube.com/watch?v=','',$video_code);
				
				$output .= 	'<iframe width="'.$video_width.'" height="'.$video_height.'" src="//www.youtube.com/embed/'.$video_code.'" frameborder="0" allowfullscreen></iframe>';
			
			}

		}
		
		if($this->block_type_id == 6)
		{
			$menu_id = $this->getBlockAttributeValueByKey('menu_id');
			
			$model = Menu::model()->findByPk($menu_id);
			
			$output .= '<div class="block-menu">';
			
			foreach($model->getDataMenuItem() as $data)
			{
				$output .= '<div class="block-menu-item">'.CHtml::link($data->title,$data->getUrl()).'</div>';
			}
			
			$output .= '</div>';
		}
		
		return $output;
	
	}
	
	public function getRelationField($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
	}
	
	public function getRenderPartial()
	{
		return $this->block_type_id;
	}

	public static function findAllByPosition($position)
	{
		return Block::model()->findAllByAttributes(array('block_position_id'=>$position,'block_status_id'=>1),array('order'=>'t.order ASC'));
	}
	
	public function getBlockDisplay()
	{
		$output = null;
		
		if($this->block_type_id == 'carousel_inovasi_home')
		{
			$output  = Yii::app()->controller->renderPartial('//layouts/block/carousel_inovasi_home',array('block'=>$this),true);
		}
		
		if($this->block_type_id == 'carousel_post_home')
		{
			$post_category_id = $this->getBlockAttributeValueByKey('carousel_post_home_post_category_id');
			$output  = Yii::app()->controller->renderPartial('//layouts/block/carousel_post_home',array('block'=>$this,'post_category_id'=>$post_category_id),true);
		}
		
		if($this->block_type_id == 'post_carousel')
		{
			$post_category_id = $this->getBlockAttributeValueByKey('post_carousel_post_category_id');
			$output  = Yii::app()->controller->renderPartial('//layouts/block/post_carousel',array('block'=>$this,'post_category_id'=>$post_category_id),true);
		}
		
		if($this->block_type_id == 'post_list')
		{
			$output  = Yii::app()->controller->renderPartial('//layouts/block/post-list',array(
				'block'=>$this,
			),true);
		}

		if($this->block_type_id == 'post_home')
		{
			$output  = Yii::app()->controller->renderPartial('//layouts/block/post_home',array(
				'block'=>$this,
			),true);
		}
		
		
		if($this->block_type_id == 'list_recent_post')
		{
			$post_category_id = $this->getBlockAttributeValueByKey('list_recent_post_post_category_id');
			$display_thumbnail = $this->getBlockAttributeValueByKey('list_recent_post_display_thumbnail');
			
			$output  = Yii::app()->controller->renderPartial('//layouts/block/post-list-recent',array(
				'block'=>$this,
				'post_category_id'=>$post_category_id,
				'display_thumbnail'=>$display_thumbnail
			),true);
		}
		
		if($this->block_type_id == 'list_recent_event')
		{
			$event_category_id = $this->getBlockAttributeValueByKey('list_recent_post_post_category_id');
			$output  = Yii::app()->controller->renderPartial('//layouts/block/event-list-recent',array('block'=>$this,'event_category_id'=>$event_category_id),true);
		}
		
		if($this->block_type_id == 'search')
		{
			$output  = Yii::app()->controller->renderPartial('//layouts/block/search',array('block'=>$this),true);
		}
		
		if($this->block_type_id == 'e_directory')
		{
			$output  = Yii::app()->controller->renderPartial('//layouts/block/e_directory',array('block'=>$this),true);
		}
		
		if($this->block_type_id == 'kategorisasi_inovasi')
		{
			$output  = Yii::app()->controller->renderPartial('//layouts/block/kategorisasi_inovasi',array('block'=>$this),true);
		}
		
		if($this->block_type_id == 'sitemap')
		{
			$output  = Yii::app()->controller->renderPartial('//layouts/block/sitemap',array('block'=>$this),true);
		}
		
		if($this->block_type_id == 'recent_popular')
		{
			$output  = Yii::app()->controller->renderPartial('//layouts/block/recent_popular',array('block'=>$this),true);
		}
		
		if($this->block_type_id == 'page')
		{
			$output  = Yii::app()->controller->renderPartial('//layouts/block/page',array('block'=>$this),true);
		}
		
		if($this->block_type_id == 'submit_inovasi')
		{
			$output  = Yii::app()->controller->renderPartial('//layouts/block/submit_inovasi',array('block'=>$this),true);
		}
		
		if($this->block_type_id == 'forum_link')
		{
			$output  = Yii::app()->controller->renderPartial('//layouts/block/forum_link',array('block'=>$this),true);
		}
		
		if($this->block_type_id == 'visitor_counter')
		{
			$output  = Yii::app()->controller->renderPartial('//layouts/block/visitor_counter',array('block'=>$this),true);
		}
		
		if($this->block_type_id == 'statistik_inovasi')
		{
			$output  = Yii::app()->controller->renderPartial('//layouts/block/statistik_inovasi',array('block'=>$this),true);
		}
		
		if($this->block_type_id == 'link_list')
		{
			$output  = Yii::app()->controller->renderPartial('//layouts/block/link-list',array('block'=>$this),true);
		}
		
		if($this->block_type_id == 'text')
		{
			$output  = Yii::app()->controller->renderPartial('//layouts/block/text',array('block'=>$this),true);
		}
		
		if($this->block_type_id == 'html')
		{
			$output  = Yii::app()->controller->renderPartial('//layouts/block/html',array('block'=>$this),true);
		}
		
		if($this->block_type_id == 'login')
		{
			$output  = Yii::app()->controller->renderPartial('//layouts/block/login',array('block'=>$this),true);
		}
		
		if($this->block_type_id == 'weather')
		{
			$output  = Yii::app()->controller->renderPartial('//layouts/block/weather',array('block'=>$this),true);
		}
		
		if($this->block_type_id == 'chart')
		{
			$output  = Yii::app()->controller->renderPartial('//layouts/block/chart',array('block'=>$this),true);
		}

		return $output;
	}
	
	public function aturUrutan() 
	{
		$i = 1;
		foreach(Block::model()->findAllByAttributes(array('block_position_id'=>$this->block_position_id),array('order'=>'t.order ASC')) as $data)
		{
			$data->order = $i;
			$data->save();
			$i++;
		}
		
		return true;
	}
	
	protected function beforeDelete()
	{
		foreach(BlockAttribute::model()->findAllByAttributes(array('block_id'=>$this->id)) as $data)
		{
			$data->delete();
		}
		
		return true;
	}
}