<?php

/**
 * This is the model class for table "menu".
 *
 * The followings are the available columns in table 'menu':
 * @property integer $id
 * @property integer $parent_id
 * @property string $title
 */
class Menu extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Menu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'menu';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('title', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, parent_id, title', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'parent_id' => 'Parent',
			'title' => 'Title',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getParentList($menu_id)
	{
		$list = array();
		
		$level=0;
		
		foreach(MenuItem::model()->findAllByAttributes(array('parent_id'=>0)) as $data)
		{
			$level=0;
			$list[$data->id]=$data->title;
			$list=$list + $data->getSubparentList($level);
		}
		
		return $list;
		
		//return CHtml::listData(MenuItem::model()->findAllByAttributes(array('menu_id'=>$menu_id,'parent_id'=>0)),'id','title');
	}
	
	public function getDataMenuItem()
	{
		$model = MenuItem::model()->findAllByAttributes(array('menu_id'=>$this->id,'parent_id'=>'0'),array('order'=>'t.order ASC'));
		
		if($model!==null)
			return $model;
		else
			return false;
	}
	
	public function getMenuItemById($menu_id)
	{
		$list = array();
		
		foreach(MenuItem::model()->findAllByAttributes(array('menu_id'=>$menu_id,'parent_id'=>0),array('order'=>'t.order ASC')) as $data)
		{
			$list[] = array('label'=>$data->title,'url'=>$data->getUrl(),'items'=>$data->getSubmenuList());
		}		
		
		return $list;
		
	}
	
	public function getDataTreeItem()
	{	
		$output = array();
		$model = MenuItem::model()->findAllByAttributes(array('menu_id'=>1,'parent_id'=>'0'),array('order'=>'t.order ASC'));
		
		foreach($model as $menu)
		{
			$output[] = array('text'=>CHtml::link($menu->title,$menu->getUrl()),'url'=>$menu->getUrl(),'children'=>$menu->getDataChildrenTree());
		}

		$tree[] = array('text'=>'PETA SITUS','children'=>$output);
		
		return $tree;
	}
	
	public function getDataChildrenTree()
	{
		
		$model = MenuItem::model()->findAllByAttributes(array('menu_id'=>1,'parent_id'=>$this->id),array('order'=>'t.order ASC'));
		
		foreach($model as $menu)
		{
			$children[] = array('text'=>CHtml::link($menu->title,$menu->getUrl()));
		}
		
		
		return $output;
	}

	
	protected function beforeDelete()
	{
		foreach($this->getDataMenuItem() as $data)
		{
			$data->delete();
		}
		
		return true;
	}
}