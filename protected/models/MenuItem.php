<?php

/**
 * This is the model class for table "menu_item".
 *
 * The followings are the available columns in table 'menu_item':
 * @property integer $id
 * @property integer $menu_id
 * @property integer $parent_id
 * @property integer $urutan
 * @property string $title
 */
class MenuItem extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MenuItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'menu_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('menu_id, title', 'required'),
			array('menu_id, parent_id, order', 'numerical', 'integerOnly'=>true),
			array('title, menu_item_type_id', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, menu_id, parent_id, order, title', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'menu_item_type'=>array(self::BELONGS_TO,'MenuItemType','menu_item_type_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'menu_id' => 'Menu',
			'menu_item_type_id' => 'Type',
			'parent_id' => 'Parent',
			'order' => 'Order',
			'title' => 'Title',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('menu_id',$this->menu_id);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('order',$this->order);
		$criteria->compare('title',$this->title,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function saveMenuAttributeByKey($key,$value)
	{
		$model = $this->getMenuAttributeByKey($key);
		if($model!==null)
		{
			$model->value=$value;
			$model->save();
		} else {
			$model = new MenuItemAttribute;
			$model->menu_item_id = $this->id;
			$model->key = $key;
			$model->value = $value;
			$model->save();
		}
	}
	
	public function saveMenuAttribute($array)
	{
		foreach($array as $key => $value)
		{
			$this->saveMenuAttributeByKey($key,$value);
		}
		
		return true;
	}
	
	public function getMenuAttributeByKey($key)
	{
		$model = MenuItemAttribute::model()->findByAttributes(array('menu_item_id'=>$this->id,'key'=>$key));
		
		if($model!==null)
			return $model;
		else
			return null;
	}
	
	public function getMenuAttributeValueByKey($key)
	{
		$model = $this->getMenuAttributeByKey($key);
		if($model!==null)
			return $model->value;
		else
			return null;
	}
	
	public function getUrl()
	{
		$url = '';
		
		if($this->menu_item_type_id=='link')
		{
			$url = $this->getMenuAttributeValueByKey('url');
			$url = str_replace('<base_url>',Yii::app()->request->baseUrl,$url);
		}
		
		if($this->menu_item_type_id=='page')
		{
			$page_id = $this->getMenuAttributeValueByKey('page_id');
			$url = Yii::app()->controller->createUrl('page/read',array('id'=>$page_id));
		}
		
		if($this->menu_item_type_id=='post')
		{
			$post_category_id = $this->getMenuAttributeValueByKey('post_category_id');
			$url = Yii::app()->controller->createUrl('postCategory/read',array('id'=>$post_category_id));
		}
		
		if($this->menu_item_type_id=='contact')
		{
			$url = Yii::app()->controller->createUrl('site/contact',array('id'=>$this->id));
		}
		
		if($this->menu_item_type_id=='photo_album')
		{
			$url = Yii::app()->controller->createUrl('photoAlbum/index');
		}
		
		return $url;
	
	}
	
	public function countSubmenu()
	{
		return MenuItem::model()->countByAttributes(array('parent_id'=>$this->id));
	}
	
	public function getSubmenuList()
	{
		$list = array();
		
		foreach(MenuItem::model()->findAllByAttributes(array('parent_id'=>$this->id),array('order'=>'t.order ASC')) as $data)
		{
			$list[] = array('label'=>$data->title,'url'=>$data->getUrl(),'items'=>$data->getSubmenuList());
		}
		
		return $list;
		
	}
	
	public function getDataMenuItemSub()
	{
		$model = MenuItem::model()->findAllByAttributes(array('parent_id'=>$this->id),array('order'=>'t.order ASC'));
		
		if($model!==null)
			return $model;
		else
			return false;
	}
	
	public function getDataChildrenTree()
	{
		$output = array();
		$model = MenuItem::model()->findAllByAttributes(array('menu_id'=>1,'parent_id'=>$this->id),array('order'=>'t.order ASC'));
		
		if($model!==null)
		{
			foreach($model as $menu)
			{
				$output[] = array('text'=>CHtml::link($menu->title,$menu->getUrl()));
			}
		}
		
		return $output;
	}
	
	protected function beforeDelete()
	{
		$model = MenuItemAttribute::model()->findAllByAttributes(array('menu_item_id'=>$this->id));
		if($model!==null)
		{
			foreach($model as $data)
				$data->delete();
		} 
		
		return true;
	}
	
	public function getSubparentList($level)
	{
		$list = array();
		
		$level++;
		$bantu = '';
		
		for($i=1;$i<=$level;$i++)
			$bantu .= '==';
				
		foreach(MenuItem::model()->findAllByAttributes(array('parent_id'=>$this->id)) as $data)
		{
			
				
			$list[$data->id]=$bantu.' '.$data->title;
			$list=$list + $data->getSubparentList($level);
		}
		
		return $list;
	}
	
	public function getRelationField($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else	
			return null;
	}
	
	public function refreshOrder()
	{
		$i = 1;
		foreach(MenuItem::model()->findAllByAttributes(array('parent_id'=>$this->parent_id),array('order'=>'t.order ASC')) as $data)
		{
			$data->order = $i;
			$data->save();
			$i++;
		}
		
		return true;
	}
}