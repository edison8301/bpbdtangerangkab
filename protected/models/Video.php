<?php



/**

 * This is the model class for table "video".
 *
 * The followings are the available columns in table 'video':
 * @property integer $id
 * @property integer $video_category_id
 * @property string $title
 * @property string $file
 */

class Video extends CActiveRecord

{

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Video the static model class
	 */

	public static function model($className=__CLASS__)

	{

		return parent::model($className);

	}



	/**
	 * @return string the associated database table name
	 */

	public function tableName()

	{

		return 'video';

	}



	/**
	 * @return array validation rules for model attributes.
	 */

	public function rules()

	{

		// NOTE: you should only define rules for those attributes that

		// will receive user inputs.

		return array(

			array('title', 'required'),

			array('video_category_id, video_type_id, model_id', 'numerical', 'integerOnly'=>true),

			array('title, file, model, youtube_url, mp4_file,ogv_file,webm_file', 'length', 'max'=>255),

			array('description, time_created','safe'),

			// The following rule is used by search().

			// Please remove those attributes that should not be searched.

			array('id, video_category_id, title, file', 'safe', 'on'=>'search'),

		);

	}



	/**
	 * @return array relational rules.
	 */

	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.

		return array(

			'video_category'=>array(self::BELONGS_TO,'VideoCategory','video_category_id'),
			'video_type'=>array(self::BELONGS_TO,'VideoType','video_type_id')

		);

	}



	/**
	 * @return array customized attribute labels (name=>label)
	 */

	public function attributeLabels()

	{

		return array(

			'id' => 'ID',

			'video_category_id' => 'Category',

			'video_type_id' => 'Type',

			'title' => 'Title',

			'file' => 'File',

		);

	}



	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */

	public function search()

	{

		// Warning: Please modify the following code to remove attributes that

		// should not be searched.



		$criteria=new CDbCriteria;



		$criteria->compare('id',$this->id);

		$criteria->compare('video_category_id',$this->video_category_id);

		$criteria->compare('title',$this->title,true);

		$criteria->compare('file',$this->file,true);

		

		$criteria->order = 'time_created DESC';

		

		return new CActiveDataProvider($this, array(

			'criteria'=>$criteria,

		));

	}

	

	public function saveVideoAttributeByKey($key,$value)

	{

		$model = $this->getVideoAttributeByKey($key);

		if($model!==null)

		{

			$model->value=$value;

			$model->save();

		} else {

			$model = new VideoAttribute;

			$model->video_id = $this->id;

			$model->key = $key;

			$model->value = $value;

			$model->save();

		}

	}

	

	public function saveVideoAttribute($array)

	{

		foreach($array as $key => $value)

		{

			$this->saveVideoAttributeByKey($key,$value);

		}

		

		return true;

	}

	

	public function getVideoAttributeByKey($key)

	{

		$model = VideoAttribute::model()->findByAttributes(array('video_id'=>$this->id,'key'=>$key));

		

		if($model!==null)

			return $model;

		else

			return null;

	}

	

	public function getVideoAttributeValueByKey($key)

	{

		$model = $this->getVideoAttributeByKey($key);

		if($model!==null)

			return $model->value;

		else

			return null;

	}

	

	public function displayVideo()

	{

		$video_code = $this->getVideoAttributeValueByKey('youtube_url');

		$video_width = '460px';

		$video_height = '350px';

		

		$video_code = str_replace('http://www.youtube.com/watch?v=','',$video_code);

		$video_code = str_replace('http://youtube.com/watch?v=','',$video_code);

		$video_code = str_replace('www.youtube.com/watch?v=','',$video_code);

		$video_code = str_replace('youtube.com/watch?v=','',$video_code);

				

		$output = 	'<iframe width="'.$video_width.'" height="'.$video_height.'" src="//www.youtube.com/embed/'.$video_code.'" frameborder="0" allowfullscreen></iframe>';

		

		return $output;

	

	}

	

	public function getVideo($style=null)

	{

		$video_code = $this->youtube_url;

		$video_width = '460px';

		$video_height = '350px';

		

		$video_code = str_replace('https://www.youtube.com/watch?v=','',$video_code);

		$video_code = str_replace('https://youtube.com/watch?v=','',$video_code);

		$video_code = str_replace('http://www.youtube.com/watch?v=','',$video_code);

		$video_code = str_replace('http://youtube.com/watch?v=','',$video_code);

		$video_code = str_replace('www.youtube.com/watch?v=','',$video_code);

		$video_code = str_replace('youtube.com/watch?v=','',$video_code);

				

		$output = 	'<iframe style="'.$style.'" width="'.$video_width.'" height="'.$video_height.'" src="//www.youtube.com/embed/'.$video_code.'" frameborder="0" allowfullscreen></iframe>';

		

		return $output;

	

	}

	

	public function getThumbnail($htmlOptions=array())

	{

		$video_code = $this->youtube_url;

		

		$video_code = str_replace('https://www.youtube.com/watch?v=','',$video_code);

		$video_code = str_replace('http://www.youtube.com/watch?v=','',$video_code);

		$video_code = str_replace('https://youtube.com/watch?v=','',$video_code);

		$video_code = str_replace('http://youtube.com/watch?v=','',$video_code);

		$video_code = str_replace('www.youtube.com/watch?v=','',$video_code);

		$video_code = str_replace('youtube.com/watch?v=','',$video_code);

		

		$img_url = "http://img.youtube.com/vi/".$video_code."/0.jpg";			

		

		return CHtml::image($img_url,'',$htmlOptions);

	

	}

	

	public function getRelationField($relation,$field)

	{

		if(!empty($this->$relation->$field))

			return $this->$relation->$field;

		else

			return null;

	}	

}