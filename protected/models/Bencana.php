<?php

/**
 * This is the model class for table "bencana".
 *
 * The followings are the available columns in table 'bencana':
 * @property integer $id
 * @property string $tanggal
 * @property integer $id_bencana_jenis
 * @property string $lokasi
 * @property integer $id_bencana_desa
 * @property integer $id_bencana_kecamatan
 * @property string $nama_korban
 * @property string $penyebab
 * @property integer $korban_materi
 * @property integer $korban_kk
 * @property integer $korban_jiwa
 * @property integer $korban_meninggal
 * @property integer $korban_luka
 * @property string $keterangan
 * @property string $waktu_dibuat
 */
class Bencana extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bencana';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_bencana_jenis, id_bencana_desa, id_bencana_kecamatan, korban_materi, korban_kk, korban_jiwa, korban_meninggal, korban_luka', 'numerical', 'integerOnly'=>true),
			array('lokasi, nama_korban, penyebab', 'length', 'max'=>255),
			array('tanggal, keterangan, waktu_dibuat', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tanggal, id_bencana_jenis, lokasi, id_bencana_desa, id_bencana_kecamatan, nama_korban, penyebab, korban_materi, korban_kk, korban_jiwa, korban_meninggal, korban_luka, keterangan, waktu_dibuat', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tanggal' => 'Tanggal',
			'id_bencana_jenis' => 'Id Bencana Jenis',
			'lokasi' => 'Lokasi',
			'id_bencana_desa' => 'Id Bencana Desa',
			'id_bencana_kecamatan' => 'Id Bencana Kecamatan',
			'nama_korban' => 'Nama Korban',
			'penyebab' => 'Penyebab',
			'korban_materi' => 'Korban Materi',
			'korban_kk' => 'Korban Kk',
			'korban_jiwa' => 'Korban Jiwa',
			'korban_meninggal' => 'Korban Meninggal',
			'korban_luka' => 'Korban Luka',
			'keterangan' => 'Keterangan',
			'waktu_dibuat' => 'Waktu Dibuat',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tanggal',$this->tanggal,true);
		$criteria->compare('id_bencana_jenis',$this->id_bencana_jenis);
		$criteria->compare('lokasi',$this->lokasi,true);
		$criteria->compare('id_bencana_desa',$this->id_bencana_desa);
		$criteria->compare('id_bencana_kecamatan',$this->id_bencana_kecamatan);
		$criteria->compare('nama_korban',$this->nama_korban,true);
		$criteria->compare('penyebab',$this->penyebab,true);
		$criteria->compare('korban_materi',$this->korban_materi);
		$criteria->compare('korban_kk',$this->korban_kk);
		$criteria->compare('korban_jiwa',$this->korban_jiwa);
		$criteria->compare('korban_meninggal',$this->korban_meninggal);
		$criteria->compare('korban_luka',$this->korban_luka);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('waktu_dibuat',$this->waktu_dibuat,true);

		$criteria->order = 'tanggal DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Bencana the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getBencanaJenis()
	{
		$model = BencanaJenis::model()->findByPk($this->id_bencana_jenis);
		if ($model!==null)
			return $model->nama;
		else
			return null;
	}

	public function getBencanaDesa()
	{
		$model = BencanaDesa::model()->findByPk($this->id_bencana_desa);
		if ($model!==null)
			return $model->nama;
		else
			return null;
	}

	public function getBencanaKecamatan()
	{
		$model = BencanaKecamatan::model()->findByPk($this->id_bencana_kecamatan);
		if ($model!==null)
			return $model->nama;
		else
			return null;
	}

	

	public static function getdataChartByJenis($year)
	{
	$dataChartJenis = '';
 		foreach(BencanaJenis::model()->findAll() as $data) 
  		{ 
			$dataChartJenis .= '{"label":"'.$data->nama.'","value":"'.$data->getCountData().'"},';
  		}
  		return $dataChartJenis;
	}

	public static function getChartDataByYear($year)
	{
		$dataChartBulan = '';
		$tahun = '2014';

		for($i=1;$i<=12;$i++)
		{
   		 	$criteria = new CDbCriteria;

    		$bulan = $i;
    		if($i<=10) $bulan = '0'.$i;

   			$awal = $year.'-'.$bulan.'-01';
    		$akhir = $year.'-'.$bulan.'-31';
    
	    	$criteria->condition = 'tanggal >= :awal AND tanggal <= :akhir';
	    	$criteria->params = array(':awal'=>$awal,':akhir'=>$akhir);
    
	    	$jumlah_kejadian = Bencana::model()->count($criteria);

	    	$nama_bulan = '';
	    	if($i==1) $nama_bulan = 'Jan';
	    	if($i==2) $nama_bulan = 'Feb';
	    	if($i==3) $nama_bulan = 'Mar';
	    	if($i==4) $nama_bulan = 'Apr';
	    	if($i==5) $nama_bulan = 'Mei';
	    	if($i==6) $nama_bulan = 'Jun';
	    	if($i==7) $nama_bulan = 'Jul';
	    	if($i==8) $nama_bulan = 'Aug';
	    	if($i==9) $nama_bulan = 'Sep';
	    	if($i==10) $nama_bulan = 'Oct';
	    	if($i==11) $nama_bulan = 'Nov';
	    	if($i==12) $nama_bulan = 'Des';

    		$dataChartBulan .= '{"label":"'.$nama_bulan.'","value":"'.$jumlah_kejadian.'"},';
		}
		
		return $dataChartBulan;
	}


}
