<?php

/**
 * This is the model class for table "post".
 *
 * The followings are the available columns in table 'post':
 * @property integer $id
 * @property integer $post_category_id
 * @property string $title
 * @property string $content
 */
class Post extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Post the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'post';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('post_category_id, total_views', 'numerical', 'integerOnly'=>true),
			array('title, image', 'length', 'max'=>255),
			//array('thumbnail','file','allowEmpty'=>true,'types'=>'jpg,JPG,gif,GIF,png,PNG','maxSize'=>1024*500,'tooLarge'=>'File maksimal 500 KB','safe'=>true),
			array('image','safe'),
			array('content, tags, time_created', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, post_category_id, title, content', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'post_category'=>array(self::BELONGS_TO,'PostCategory','post_category_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'post_category_id' => 'Category',
			'title' => 'Title',
			'content' => 'Content',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('post_category_id',$this->post_category_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('content',$this->content,true);
		
		$criteria->order = 'time_created DESC';
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function beforeDelete()
	{
		PostCategoryMap::model()->deleteAllByAttributes(array('post_id'=>$this->id));
		
		return true;
		
	}
	
	public function getTimeCreated()
	{
		return date('d-m-Y H:i:s',strtotime($this->time_created));
	}
	
	public function getImage($htmlOptions=array())
	{
		if($this->image!='')
			return CHtml::image(Yii::app()->theme->baseUrl.'/uploads/post/'.$this->image,'',$htmlOptions);
		else
			return CHtml::image(Yii::app()->theme->baseUrl.'/images/no-image.jpg','',$htmlOptions);
	}
	
	public function getDataDownload()
	{
		$model = Download::model()->findAllByAttributes(array('model'=>'Post','model_id'=>$this->id));
		
		if($model!==null)
			return $model;
		else
			return false;
	}
	
	public function countDataDownload()
	{
		return Download::model()->countByAttributes(array('model'=>'Post','model_id'=>$this->id));
	}
	
	public function getOtherPost()
	{
		$criteria = new CDbCriteria;
		$criteria->limit = 5;
		$criteria->order = 'time_created DESC';
		
		$model = Post::model()->findAll($criteria);
		
		if($model!==null)
			return $model;
		else
			return false;
	}
	
	public function getLatestPostByCategoryId($category_id=null,$limit=null)
	{
		if($limit==null)
			$limit = 3;
			
		if($category_id!=null)
		{
			$criteria = new CDbCriteria;
			$criteria->condition = 'id IN (SELECT post_id FROM post_category_map WHERE post_category_id = :category_id)';
			$criteria->params = array(':category_id'=>$category_id);
			$criteria->limit = $limit;
			$criteria->order = 'time_created DESC';
			$model = Post::model()->findAll($criteria);
			
		} else {
			$model = Post::model()->findAll(array('order'=>'time_created DESC','limit'=>$limit));
		}
		
		if($model!==null)
			return $model;
		else
			return false;
		
	}
	
	public function updatePostCategoryMap($post_category_id=null)
	{
		
		if($post_category_id == null)
		{
			foreach(PostCategoryMap::model()->findAllByAttributes(array('post_id'=>$this->id)) as $data)
			{	
				$data->active=0;
				$data->save();
			}
			
			$post_category_id = $this->post_category_id;
		}
			
		$postCategory = PostCategory::model()->findByPk($post_category_id);
		
		$parent_id = 0;
		
		if($postCategory !== null)
			$parent_id = $postCategory->parent_id;
		
		$postCategoryMap = PostCategoryMap::model()->findByAttributes(array('post_id'=>$this->id,'post_category_id'=>$post_category_id));
		
		if($postCategoryMap === null)
			$postCategoryMap = new PostCategoryMap;
		
		$postCategoryMap->post_id = $this->id;
		
		$postCategoryMap->post_category_id = $post_category_id;
		
		$postCategoryMap->active = 1;
		
		$postCategoryMap->save();
			
		if($parent_id != 0)
		{
			$this->updatePostCategoryMap($parent_id);
		}
		
		return true;
	}
	
	public function getDataPhoto()
	{
		$model = Photo::model()->findAllByAttributes(array('model'=>'Post','model_id'=>$this->id));
		
		if($model!==null)
			return $model;
		else
			return false;
	}
	
	public function countDataPhoto()
	{
		return Photo::model()->countByAttributes(array('model'=>'Post','model_id'=>$this->id));
	}
	
	public function getDataVideo()
	{
		$model = Video::model()->findAllByAttributes(array('model'=>'Post','model_id'=>$this->id));
		
		if($model!==null)
			return $model;
		else
			return false;
	}
	
	public function countDataVideo()
	{
		return Video::model()->countByAttributes(array('model'=>'Post','model_id'=>$this->id));
	}
	
	public function getExcerpt($maxLength=125) 
	{
		$str = $this->content;
		$startPos=0; 
		
		if(strlen($str) > $maxLength) 
		{
			$excerpt   = substr($str, $startPos, $maxLength-3);
			$lastSpace = strrpos($excerpt, ' ');
			$excerpt   = substr($excerpt, 0, $lastSpace);
			$excerpt  .= '...';
		} else {
			$excerpt = $str;
		}
	
		return $excerpt;
	}
	
	public function getRelationField($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
	}
}