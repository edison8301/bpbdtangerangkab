<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginMember extends CFormModel
{
	public $email;
	public $password;
	public $rememberMe;
	public $verifyCode;
	private $_identity;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// username and password are required
			array('email, password', 'required'),
			// rememberMe needs to be a boolean
			array('rememberMe', 'boolean'),
			// password needs to be authenticated
			array('password', 'authenticate'),			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(			'verifyCode'=>'Kode Verifikasi',
			'rememberMe'=>'Remember me next time',
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate()
	{
		$model = Member::model()->findByAttributes(array('email'=>$this->email,'password'=>$this->password));
		if($model!==null)
			return true;
		else
			return false;
	}
	
	public function isAktif()
	{
		$model = Member::model()->findByAttributes(array('email'=>$this->email,'password'=>$this->password));
		if($model===null)
			return false;
		else if($model->aktif == 1)
			return true;
		else	
			return false;
	}
	
	public function getMemberId()
	{
		return Member::model()->findByAttributes(array('email'=>$this->email,'password'=>$this->password))->id;
	}
	
	public function updateLoginTerkahir()
	{
		$model = Member::model()->findByAttributes(array('email'=>$this->email,'password'=>$this->password));
		
		date_default_timezone_set('Asia/Jakarta');
		$model->login_terakhir = date('Y-m-d H:i:s');
		$model->save();
		
		return true;
	}
	
}
