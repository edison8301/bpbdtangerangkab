<?php



/**
 * This is the model class for table "link".
 *
 * The followings are the available columns in table 'link':
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property integer $link_category_id
 * @property string $image
 * @property integer $order
 * @property string $time_created
 */

class Link extends CActiveRecord

{

	/**
	 * @return string the associated database table name
	 */

	public function tableName()

	{

		return 'link';

	}



	/**
	 * @return array validation rules for model attributes.
	 */

	public function rules()

	{

		// NOTE: you should only define rules for those attributes that

		// will receive user inputs.

		return array(

			array('title', 'required'),

			array('link_category_id, order, new_tab', 'numerical', 'integerOnly'=>true),

			array('title, url, image', 'length', 'max'=>255),

			array('time_created', 'safe'),

			// The following rule is used by search().

			// @todo Please remove those attributes that should not be searched.

			array('id, title, url, link_category_id, image, order, time_created', 'safe', 'on'=>'search'),

		);

	}



	/**
	 * @return array relational rules.
	 */

	public function relations()

	{

		// NOTE: you may need to adjust the relation name and the related

		// class name for the relations automatically generated below.

		return array(

			'LinkCategory'=>array(self::BELONGS_TO,'LinkCategory','link_category_id'),

		);

	}



	/**
	 * @return array customized attribute labels (name=>label)
	 */

	public function attributeLabels()

	{

		return array(

			'id' => 'ID',

			'title' => 'Title',

			'url' => 'Url',

			'link_category_id' => 'Category',

			'image' => 'Image',

			'order' => 'Order',

			'time_created' => 'Time Created',

			'new_tab' => 'Open in New Tab'

		);

	}



	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */

	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('link_category_id',$this->link_category_id);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('order',$this->order);
		$criteria->compare('time_created',$this->time_created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));

	}



	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Link the static model class
	 */

	public static function model($className=__CLASS__)

	{

		return parent::model($className);

	}

	

	public function getLink($link_category_id)

	{	

		return Link::model()->findAllByAttributes(array('link_category_id'=>$link_category_id),array('order'=>'t.order ASC'));			

	}

	

	public function getBackendThumbnail($data,$class)

	{

		if($data!='')

			return CHtml::image(Yii::app()->theme->baseUrl.'/uploads/link/'.$data,'',array('class'=>$class));

		else

			return CHtml::image(Yii::app()->theme->baseUrl.'/images/image_not_found.gif','',array('class'=>'admin-photo'));

	}

	

	public function getImage($htmlOptions=array())

	{

		return CHtml::image(Yii::app()->theme->baseUrl.'/uploads/link/'.$this->image,'',$htmlOptions);

	}

	

	public function getUrl()

	{

		return $this->url;

	}

	

	protected function beforeDelete()

	{

		$path = Yii::app()->theme->basePath.'/uploads/link/';

		if(file_exists($path.$this->image) AND $this->image!= '')

			unlink($path.$this->image);

			

		return true;

	}

	

	public function getRelationField($relation,$field)

	{

		if(!empty($this->$relation->$field))

			return $this->$relation->$field;

		else

			return null;

	}

}

