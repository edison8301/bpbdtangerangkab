<?php

/**
 * This is the model class for table "photo".
 *
 * The followings are the available columns in table 'photo':
 * @property integer $id
 * @property integer $photo_category_id
 * @property string $title
 * @property string $file
 * @property string $date_created
 */
class Photo extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Photo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'photo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('photo_category_id, photo_album_id, model_id', 'numerical', 'integerOnly'=>true),
			array('title, file, model', 'length', 'max'=>255),
			//array('file','file','types'=>'jpg,JPG,png,PNG,gif,GIF','maxSize'=>1024*200,'tooLarge'=>'Gambar terlalu besar (maks: 200 KB). Silahkan resize terlebih dahulu','allowEmpty'=>true),
			array('time_created', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, photo_category_id, title, file, time_created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'photo_category'=>array(self::BELONGS_TO,'PhotoCategory','photo_category_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'photo_category_id' => 'Photo Category',
			'photo_album_id' => 'Photo Album',
			'title' => 'Title',
			'file' => 'File',
			'time_created' => 'Time Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('photo_category_id',$this->photo_category_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('file',$this->file,true);
		$criteria->compare('time_created',$this->time_created,true);
		
		$criteria->order = 'time_created DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function beforeDelete()
	{
		$path = Yii::app()->basePath.'/../uploads/photo/';
		if($this->file!='' AND file_exists($path.$this->file))
			unlink($path.$this->file);
		
		return true;
	}
	
	public function getRelationField($relation,$field)
	{
		if(!empty($this->$relation->$field))
			return $this->$relation->$field;
		else
			return null;
	}		
	
	public function getImage($htmlOptions=array())
	{
		return CHtml::image(Yii::app()->request->baseUrl."/uploads/photo/".$this->file,'',$htmlOptions);
	}
}