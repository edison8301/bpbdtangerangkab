<?php

/**
 * This is the model class for table "slide".
 *
 * The followings are the available columns in table 'slide':
 * @property integer $id
 * @property string $title
 * @property string $file
 */
class Slide extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Slide the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'slide';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('title, file', 'length', 'max'=>255),
			//array('file','file','types'=>'jpg,JPG,png,PNG,gif,GIF','allowEmpty'=>true,'maxSize'=>1024*200,'tooLarge'=>'Gambar terlalu besar (maks: 200 KB). Silahkan resize terlebih dahulu'),
			array('time_created','safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, file', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'file' => 'File',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('file',$this->file,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function getSlideList()
	{
		$models = Slide::model()->findAll();
		
		$slides = array();
									
		foreach($models as $data)
		{
			$slide = array('src'=>Yii::app()->theme->baseUrl.'/uploads/slide/'.$data->file,'caption'=>$data->title);
			array_push($slides,$slide);
		}
		
		return $slides;
	}
	
	public function getSlideImages()
	{
		$models = Slide::model()->findAll();
		
		$slides = array();
									
		foreach($models as $data)
		{
			$slide = $data->file;
			array_push($slides,$slide);
		}
		
		return $slides;
	}
	
	public function getSlideCarousel($limit=null)
	{
		if($limit==null)
			$limit = 3;
			
		$carousel = array();
		foreach(Slide::model()->findAll(array('order'=>'id DESC','limit'=>$limit)) as $data)
		{			
			$carousel[] = array('image'=>(Yii::app()->theme->baseUrl.'/uploads/slide/'.$data->file),'caption'=>CHtml::link($data->title));
		}
		return $carousel;
	}
	
	public function getSlideAlts()
	{
		$models = Slide::model()->findAll();
		
		$alts = array();
									
		foreach($models as $data)
		{
			$alt = $data->title;
			array_push($alts,$alt);
		}
		
		return $alts;
	}
	
	public function getImage()
	{
		return CHtml::image(Yii::app()->theme->baseUrl."/uploads/slide/".$this->file,'',array('class'=>'img-responsive'));
	}	
	
	protected function beforeDelete()
	{
		$path = Yii::app()->theme->basePath.'/uploads/slide/';
		if($this->file!='' AND file_exists($path.$this->file))
			unlink($path.$this->file);
		
		return true;
	}
}