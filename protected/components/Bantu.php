<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Bantu 
{
	public function tanggal($tgl)
	{
		if($tgl!='0000-00-00')
		{
			$tgl=explode('-',$tgl);
			$tgl=$tgl['2']."-".$tgl['1']."-".$tgl['0'];
	
			$bantu = date('j',strtotime($tgl))." ".Bantu::getBulan($tgl)." ".date('Y',strtotime($tgl));
		} else {
			$bantu = '';
		}
		
		return $bantu;
		
	}
	
	public function getTanggal($waktu)
	{
		if($waktu != '0000-00-00') {
		$time = strtotime($waktu);
		
		$tgl = date('j',$time);
		$h = date('n',$time);
		
		if($h == '1') $bulan = 'Januari';
		if($h == '2') $bulan = 'Februari';
		if($h == '3') $bulan = 'Maret';
		if($h == '4') $bulan = 'April';
		if($h == '5') $bulan = 'Mei';
		if($h == '6') $bulan = 'Juni';
		if($h == '7') $bulan = 'Juli';
		if($h == '8') $bulan = 'Agustus';
		if($h == '9') $bulan = 'September';
		if($h == '10') $bulan = 'Oktober';
		if($h == '11') $bulan = 'November';
		if($h == '12') $bulan = 'Desember';
		
		$tahun  = date('Y',$time);
		
		return $tgl." ".$bulan." ".$tahun;
		
		} else {
		
		return "";
		
		}
	}
	
	public function getTgl($waktu)
	{
		$time = strtotime($waktu);

		return date('d-m-Y',$time);
	
	}
	
	public function getWaktuDibuat($waktu)
	{
		
		$time = strtotime($waktu);
		
		$h = date('N',$time);
		
		if($h == '1') $hari = 'Senin';
		if($h == '2') $hari = 'Selasa';
		if($h == '3') $hari = 'Rabu';
		if($h == '4') $hari = 'Kamis';
		if($h == '5') $hari = 'Jumat';
		if($h == '6') $hari = 'Sabtu';
		if($h == '7') $hari = 'Minggu';
		
		
		$tgl = date('j',$time);
		
		$h = date('n',$time);
		
		if($h == '1') $bulan = 'Januari';
		if($h == '2') $bulan = 'Februari';
		if($h == '3') $bulan = 'Maret';
		if($h == '4') $bulan = 'April';
		if($h == '5') $bulan = 'Mei';
		if($h == '6') $bulan = 'Juni';
		if($h == '7') $bulan = 'Juli';
		if($h == '8') $bulan = 'Agustus';
		if($h == '9') $bulan = 'September';
		if($h == '10') $bulan = 'Oktober';
		if($h == '11') $bulan = 'November';
		if($h == '12') $bulan = 'Desember';
		
		$tahun  = date('Y',$time);
		
		$pukul = date('h:i:s',$time);
		
		$output = $hari.', '.$tgl.' '.$bulan.' '.$tahun.' | '.$pukul.' WIB';
		
		return $output;
		
	}
	
	public function getTanggalDibuat($waktu)
	{
		
		$time = strtotime($waktu);
		
		$h = date('N',$time);
		
		if($h == '1') $hari = 'Senin';
		if($h == '2') $hari = 'Selasa';
		if($h == '3') $hari = 'Rabu';
		if($h == '4') $hari = 'Kamis';
		if($h == '5') $hari = 'Jumat';
		if($h == '6') $hari = 'Sabtu';
		if($h == '7') $hari = 'Minggu';
		
		
		$tgl = date('j',$time);
		
		$h = date('n',$time);
		
		if($h == '1') $bulan = 'Januari';
		if($h == '2') $bulan = 'Februari';
		if($h == '3') $bulan = 'Maret';
		if($h == '4') $bulan = 'April';
		if($h == '5') $bulan = 'Mei';
		if($h == '6') $bulan = 'Juni';
		if($h == '7') $bulan = 'Juli';
		if($h == '8') $bulan = 'Agustus';
		if($h == '9') $bulan = 'September';
		if($h == '10') $bulan = 'Oktober';
		if($h == '11') $bulan = 'November';
		if($h == '12') $bulan = 'Desember';
		
		$tahun  = date('Y',$time);
		
		$pukul = date('h:i:s',$time);
		
		$output = $hari.', '.$tgl.' '.$bulan.' '.$tahun;
		
		return $output;
		
	}
	
	public function getHariTanggal($waktu)
	{
		
		$time = strtotime($waktu);
		
		$h = date('N',$time);
		
		if($h == '1') $hari = 'Senin';
		if($h == '2') $hari = 'Selasa';
		if($h == '3') $hari = 'Rabu';
		if($h == '4') $hari = 'Kamis';
		if($h == '5') $hari = 'Jumat';
		if($h == '6') $hari = 'Sabtu';
		if($h == '7') $hari = 'Minggu';
		
		
		$tgl = date('j',$time);
		
		$h = date('n',$time);
		
		if($h == '1') $bulan = 'Januari';
		if($h == '2') $bulan = 'Februari';
		if($h == '3') $bulan = 'Maret';
		if($h == '4') $bulan = 'April';
		if($h == '5') $bulan = 'Mei';
		if($h == '6') $bulan = 'Juni';
		if($h == '7') $bulan = 'Juli';
		if($h == '8') $bulan = 'Agustus';
		if($h == '9') $bulan = 'September';
		if($h == '10') $bulan = 'Oktober';
		if($h == '11') $bulan = 'November';
		if($h == '12') $bulan = 'Desember';
		
		$tahun  = date('Y',$time);
		
		$output = $hari.', '.$tgl.' '.$bulan.' '.$tahun;
		
		return $output;
		
	}
	
	public function getKodePin()
	{
		//removed number 0, capital o, number 1 and small L
		//Total: keys = 32, elements = 33
		$characters = array(
			"A","B","C","D","E","F","G","H","J","K","L","M",
			"N","P","Q","R","S","T","U","V","W","X","Y","Z",
			"1","2","3","4","5","6","7","8","9");

		//make an "empty container" or array for our keys
		$keys = array();

		//first count of $keys is empty so "1", remaining count is 1-6 = total 7 times
		while(count($keys) < 6) {
		//"0" because we use this to FIND ARRAY KEYS which has a 0 value
		//"-1" because were only concerned of number of keys which is 32 not 33
		//count($characters) = 33
			$x = mt_rand(0, count($characters)-1);
			if(!in_array($x, $keys)) {
				$keys[] = $x;
			}
		}
		
		$random_chars='';
	
		foreach($keys as $key){
			$random_chars .= $characters[$key];
		}
		
		return $random_chars;	
	
	}
}