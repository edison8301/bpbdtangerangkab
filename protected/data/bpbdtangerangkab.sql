-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 02 Okt 2015 pada 13.22
-- Versi Server: 5.5.44
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bpbdtangerangkab`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `active`
--

CREATE TABLE IF NOT EXISTS `active` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `active`
--

INSERT INTO `active` (`id`, `title`) VALUES
(1, 'Ya'),
(2, 'Tidak');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bencana`
--

CREATE TABLE IF NOT EXISTS `bencana` (
  `id` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `id_bencana_jenis` int(11) DEFAULT NULL,
  `lokasi` varchar(255) DEFAULT NULL,
  `id_bencana_desa` int(11) DEFAULT NULL,
  `id_bencana_kecamatan` int(11) DEFAULT NULL,
  `nama_korban` varchar(255) DEFAULT NULL,
  `penyebab` varchar(255) DEFAULT NULL,
  `korban_materi` int(11) DEFAULT NULL,
  `korban_kk` int(11) DEFAULT NULL,
  `korban_jiwa` int(11) DEFAULT NULL,
  `korban_meninggal` int(11) DEFAULT NULL,
  `korban_luka` int(11) DEFAULT NULL,
  `keterangan` text,
  `waktu_dibuat` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bencana`
--

INSERT INTO `bencana` (`id`, `tanggal`, `id_bencana_jenis`, `lokasi`, `id_bencana_desa`, `id_bencana_kecamatan`, `nama_korban`, `penyebab`, `korban_materi`, `korban_kk`, `korban_jiwa`, `korban_meninggal`, `korban_luka`, `keterangan`, `waktu_dibuat`) VALUES
(2, '2015-04-02', 2, 'Desa Palasari, Ds. Caringin, Ds. Kemuning Kec. Legok Kabupaten Tangerang', 2, 2, '-', 'Angin Puting Beliung', NULL, NULL, NULL, NULL, NULL, '2 Unit', '2015-09-23 12:46:23'),
(3, '2015-09-10', 1, 'Jl Utama No. 11 Kawasan Industri Pasar Kemis Kec. Pasar Kemis Kab. Tangerang', 2, 2, 'PT Global Piberindo. an Andi', 'Mesin Kompresor', NULL, NULL, NULL, NULL, NULL, '8 unit', '2015-09-23 17:15:27'),
(4, '2014-01-01', 3, 'Jalan Kawasan Industri Mekar Jaya Rt 1/2 Kec.Sepatan Kab.Tangerang', 2, 14, 'Bpk. Hambali', 'Konsleting Listrik', NULL, NULL, NULL, NULL, NULL, '4 Unit Damkar\r\n', '2015-09-29 12:53:15'),
(5, '2014-02-27', 3, 'Jl. Binong Raya No.9 Kel. Binong Kec. Cururg Kab.Tangerang', 70, 13, '', 'Kebocoran mesin heater sauna', 50000000, NULL, NULL, NULL, NULL, '4 Unit Damkar\r\n', '2015-09-29 12:56:41');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bencana_desa`
--

CREATE TABLE IF NOT EXISTS `bencana_desa` (
  `id` int(11) NOT NULL,
  `id_bencana_kecamatan` int(11) DEFAULT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bencana_desa`
--

INSERT INTO `bencana_desa` (`id`, `id_bencana_kecamatan`, `nama`) VALUES
(2, 2, 'Desa Mekar Sari'),
(3, 3, 'Desa Jenggot '),
(4, 3, 'Desa Keduang'),
(5, 3, 'Desa Klutuk'),
(6, 3, 'Desa Waliwis'),
(7, 3, 'Desa Cijeruk'),
(8, 3, 'Desa Mekar Baru'),
(9, 4, 'Desa Buni Ayu'),
(10, 4, 'Desa Suka Mulya'),
(11, 4, 'Desa Kaliasin'),
(12, 4, 'Desa Perahu'),
(13, 4, 'Desa Buna'),
(14, 4, 'Desa Benda'),
(15, 5, 'Desa Gelam Jaya'),
(16, 6, 'Desa Onyam'),
(17, 6, 'Desa Kandawati'),
(18, 6, 'Desa Kedung'),
(19, 6, 'Desa Gunung Kaler'),
(20, 7, 'Desa Gempol Sari'),
(21, 7, 'Desa Jati Mulya'),
(22, 7, 'Desa Lebak Wangi'),
(23, 8, 'Desa Pasilian'),
(24, 8, 'Desa Pengedangan Udik'),
(25, 8, 'Desa Muncang'),
(26, 8, 'Desa Pasir'),
(27, 8, 'Desa Cirupak'),
(28, 8, 'Desa Kronjo'),
(29, 8, 'Desa Bakuang'),
(30, 8, 'Desa Pengedangan Ilir'),
(31, 9, 'Kelurahan Dadap'),
(32, 10, 'Desa Klebet'),
(33, 10, 'Desa Karang Anyar'),
(34, 10, 'Desa Patramanggala'),
(35, 10, 'Desa Kemiri'),
(36, 10, 'Desa Legok Sukamaju'),
(37, 10, 'Desa Lontar'),
(38, 10, 'Desa Ranca Labuh'),
(39, 11, 'Desa Mauk Barat'),
(40, 11, 'Desa Gunung Sari'),
(41, 11, 'Desa Mauk Timur'),
(42, 11, 'Desa Marga Mulya'),
(43, 11, 'Desa Jati Waringin'),
(44, 11, 'Desa Ketapang'),
(45, 11, 'Desa Kedung Dalem'),
(46, 12, 'Desa Cisereh'),
(47, 12, 'Kelurahan Kadu Agung'),
(48, 12, 'Desa Pasir Nangka'),
(49, 12, 'Desa Pasir Bolang'),
(50, 13, 'Desa Curug Wetan'),
(51, 14, 'Kelurahan Sepatan'),
(52, 14, 'Desa Karet'),
(53, 14, 'Desa Pondok Jaya'),
(54, 14, 'Desa Pisang Jaya'),
(55, 14, 'Desa Sarakan'),
(56, 14, 'Desa Mekar Jaya'),
(57, 14, 'Desa Kayu Agung'),
(58, 15, 'Kecamatan'),
(59, 16, 'Desa Suka Wali'),
(60, 16, 'Desa Kali Baru'),
(61, 16, 'Desa Buaran Mangga'),
(62, 16, 'Desa Kiara Payung'),
(63, 16, 'Desa Laksana'),
(64, 16, 'Desa Kramat'),
(65, 16, 'Kelurahan Paku Haji'),
(66, 16, 'Desa Gaga'),
(67, 16, 'Desa Surya Bahari'),
(68, 16, 'Desa Boni Sari'),
(69, 16, 'Desa Buruan Bambu'),
(70, 16, 'Desa Kohod'),
(71, 16, 'Desa Rawa Boni'),
(72, 17, 'Desa Gintung'),
(73, 17, 'Desa Rawa Kidang'),
(74, 17, 'Desa Karang Serang'),
(75, 17, 'Desa Pekayon'),
(76, 17, 'Desa Sukadiri'),
(77, 18, 'Desa Sindang Sono'),
(78, 18, 'Desa Badak Anom'),
(79, 18, 'Desa Sindang Panon'),
(80, 19, 'Desa Cibago'),
(81, 20, 'Kelurahan Sukamulya'),
(82, 20, 'Kelurahan Bunder'),
(83, 20, 'Desa Bitung Jaya'),
(84, 20, 'Desa Cikupa'),
(85, 20, 'Desa Pasir Jaya'),
(86, 20, 'Desa Bojong'),
(87, 20, 'Desa Dukuh'),
(88, 20, 'Desa Budi Mulya'),
(89, 20, 'Desa Suka Damai'),
(90, 20, 'Desa Cibadak'),
(91, 20, 'Desa Telaga Sari');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bencana_jenis`
--

CREATE TABLE IF NOT EXISTS `bencana_jenis` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bencana_jenis`
--

INSERT INTO `bencana_jenis` (`id`, `nama`) VALUES
(1, 'Kabut Asap'),
(2, 'Angin Puting Beliung/Alam'),
(3, 'Kebakaran');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bencana_kecamatan`
--

CREATE TABLE IF NOT EXISTS `bencana_kecamatan` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bencana_kecamatan`
--

INSERT INTO `bencana_kecamatan` (`id`, `nama`) VALUES
(2, 'KECAMATAN RAJEG'),
(3, 'KECAMATAN MEKAR BARU'),
(4, 'KECAMATAN SUKA MULYA'),
(5, 'KECAMATAN PASAR KEMIS'),
(6, 'KECAMATAN GUNUNG KALER'),
(7, 'KECAMATAN SEPETAN TIMUR'),
(8, 'KECAMATAN KRONJO'),
(9, 'KECAMATAN KOSAMBI'),
(10, 'KECAMATAN KEMIRI'),
(11, 'KECAMATAN MAUK'),
(12, 'KECAMATAN TIGARAKSA'),
(13, 'KECAMATAN CURUG'),
(14, 'KECAMATAN SEPATAN'),
(15, 'KECAMATAN PANONGAN'),
(16, 'KECAMATAN PAKU HAJI'),
(17, 'KECAMATAN SUKADIRI'),
(18, 'KECAMATAN SINDANG JAYA'),
(19, 'KECAMATAN CISAUK'),
(20, 'KECAMATAN CIKUPA'),
(21, 'KECAMATAN JAYANTI'),
(22, 'KECAMATAN TELUK NAGA'),
(23, 'KECAMATAN KRESEK');

-- --------------------------------------------------------

--
-- Struktur dari tabel `block`
--

CREATE TABLE IF NOT EXISTS `block` (
  `id` int(11) NOT NULL,
  `order` int(11) DEFAULT '1',
  `block_position_id` varchar(255) DEFAULT NULL,
  `block_type_id` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `block_status_id` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `block`
--

INSERT INTO `block` (`id`, `order`, `block_position_id`, `block_type_id`, `title`, `block_status_id`) VALUES
(2, 1, 'rightbar', 'post_list', 'Info Bencana', 1),
(22, 1, 'home_center', 'post_home', 'Info Bencana', 1),
(17, 4, 'rightbar', 'search', 'Cari', 1),
(23, 3, 'rightbar', 'link_list', 'Link Terkait', 1),
(19, 2, 'home_center', 'chart', 'Grafik Kejadian Bencana', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `block_attribute`
--

CREATE TABLE IF NOT EXISTS `block_attribute` (
  `id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` text
) ENGINE=MyISAM AUTO_INCREMENT=291 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `block_attribute`
--

INSERT INTO `block_attribute` (`id`, `block_id`, `key`, `value`) VALUES
(6, 2, 'text', ''),
(7, 2, 'html', ''),
(8, 2, 'carousel_post_home_post_category_id', '1'),
(9, 2, 'carousel_post_bottom_post_category_id', '1'),
(10, 2, 'list_recent_post_post_category_id', '1'),
(30, 2, 'list_recent_post_display_thumbnail', 'ya'),
(31, 2, 'list_recent_event_event_category_id', ''),
(50, 2, 'link_list_link_category_id', '1'),
(98, 2, 'link_list_display_mode', 'text'),
(99, 2, 'page_page_id', '1'),
(262, 22, 'link_list_display_mode', 'text'),
(261, 22, 'list_recent_event_event_category_id', ''),
(260, 22, 'post_list_limit', ''),
(259, 22, 'post_list_display_mode', 'title'),
(258, 22, 'post_list_post_category_id', '1'),
(257, 22, 'carousel_post_bottom_post_category_id', '1'),
(256, 22, 'carousel_post_home_post_category_id', '1'),
(255, 22, 'html_display_title', 'ya'),
(254, 22, 'html', ''),
(253, 22, 'text_display_title', 'ya'),
(252, 22, 'text', ''),
(131, 2, 'text_display_title', 'ya'),
(132, 2, 'html_display_title', 'ya'),
(133, 2, 'post_list_post_category_id', '2'),
(134, 2, 'post_list_display_mode', 'title_image'),
(135, 2, 'post_list_limit', '3'),
(192, 17, 'text', ''),
(193, 17, 'text_display_title', 'ya'),
(194, 17, 'html', ''),
(195, 17, 'html_display_title', 'ya'),
(196, 17, 'carousel_post_home_post_category_id', '1'),
(197, 17, 'carousel_post_bottom_post_category_id', '1'),
(198, 17, 'post_list_post_category_id', '1'),
(199, 17, 'post_list_display_mode', 'title'),
(200, 17, 'post_list_limit', ''),
(201, 17, 'list_recent_event_event_category_id', ''),
(202, 17, 'link_list_display_mode', 'text'),
(203, 17, 'page_page_id', '1'),
(269, 23, 'carousel_post_home_post_category_id', '1'),
(270, 23, 'post_carousel_post_category_id', '1'),
(271, 23, 'post_list_post_category_id', '1'),
(272, 23, 'post_list_display_mode', 'title'),
(273, 23, 'post_list_limit', ''),
(274, 23, 'list_recent_event_event_category_id', ''),
(275, 23, 'link_list_link_category_id', '1'),
(267, 23, 'html', ''),
(216, 19, 'text', ''),
(217, 19, 'text_display_title', 'ya'),
(218, 19, 'html', ''),
(219, 19, 'html_display_title', 'ya'),
(220, 19, 'carousel_post_home_post_category_id', '1'),
(221, 19, 'carousel_post_bottom_post_category_id', '1'),
(222, 19, 'post_list_post_category_id', '1'),
(223, 19, 'post_list_display_mode', 'title'),
(224, 19, 'post_list_limit', ''),
(225, 19, 'list_recent_event_event_category_id', ''),
(226, 19, 'link_list_display_mode', 'text'),
(227, 19, 'page_page_id', '1'),
(276, 23, 'link_list_display_mode', 'image'),
(268, 23, 'html_display_title', 'ya'),
(266, 23, 'text_display_title', 'ya'),
(265, 23, 'text', ''),
(264, 19, 'post_carousel_post_category_id', '1'),
(263, 22, 'page_page_id', '1'),
(277, 23, 'page_page_id', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `block_position`
--

CREATE TABLE IF NOT EXISTS `block_position` (
  `id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `block_position`
--

INSERT INTO `block_position` (`id`, `title`) VALUES
('home_center', 'Home Center'),
('rightbar', 'Rightbar'),
('home_leftbar', 'Home Leftbar'),
('bottom_1', 'Bottom 1'),
('bottom_2', 'Bottom 2'),
('bottom_3', 'Bottom 3'),
('bottom_4', 'Bottom 4');

-- --------------------------------------------------------

--
-- Struktur dari tabel `block_status`
--

CREATE TABLE IF NOT EXISTS `block_status` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `block_status`
--

INSERT INTO `block_status` (`id`, `title`) VALUES
(1, 'Aktif'),
(2, 'Tidak Aktif');

-- --------------------------------------------------------

--
-- Struktur dari tabel `block_type`
--

CREATE TABLE IF NOT EXISTS `block_type` (
  `id` varchar(266) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `block_type`
--

INSERT INTO `block_type` (`id`, `title`) VALUES
('script', 'Script'),
('html', 'HTML'),
('post_carousel', 'Post Carousel'),
('post_list', 'Post List'),
('search', 'Search'),
('visitor_counter', 'Visitor Counter'),
('event_list', 'Event List'),
('link_list', 'Link List'),
('login', 'Login'),
('chart', 'Chart'),
('post_home', 'Post Home');

-- --------------------------------------------------------

--
-- Struktur dari tabel `download`
--

CREATE TABLE IF NOT EXISTS `download` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `download_access_id` int(11) DEFAULT '1',
  `total_download` int(11) DEFAULT '0',
  `time_created` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `download_access`
--

CREATE TABLE IF NOT EXISTS `download_access` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `event`
--

CREATE TABLE IF NOT EXISTS `event` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `date` date DEFAULT NULL,
  `time` varchar(20) DEFAULT NULL,
  `place` varchar(255) DEFAULT NULL,
  `event_category_id` int(11) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `description` text,
  `time_created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `event_category`
--

CREATE TABLE IF NOT EXISTS `event_category` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `event_category`
--

INSERT INTO `event_category` (`id`, `title`) VALUES
(1, 'Kategori A');

-- --------------------------------------------------------

--
-- Struktur dari tabel `link`
--

CREATE TABLE IF NOT EXISTS `link` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `link_category_id` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `new_tab` tinyint(4) DEFAULT '0',
  `time_created` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `link`
--

INSERT INTO `link` (`id`, `title`, `url`, `link_category_id`, `image`, `order`, `new_tab`, `time_created`) VALUES
(1, 'BNPB', 'http://bnpb.go.id/', 1, '1443768725_link_bnpb.png', 1, 1, '2015-10-02 13:46:13'),
(2, 'BMKG', 'http://www.bmkg.go.id/BMKG_Pusat/', 1, '1443768873_link_bmkg.jpg', NULL, 1, '2015-10-02 13:54:33'),
(3, 'PVMBG', 'http://pvmbg.bgl.esdm.go.id/', 1, '1443769116_link_badan_geologi.jpg', NULL, 1, '2015-10-02 13:58:36'),
(4, 'Badan Statistik Kab Tangerang', 'http://tangerangkab.bps.go.id', 1, '1443769321_link_bstatistik.jpg', NULL, 0, '2015-10-02 14:02:01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `link_category`
--

CREATE TABLE IF NOT EXISTS `link_category` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `link_category`
--

INSERT INTO `link_category` (`id`, `title`) VALUES
(1, 'Link Terkait');

-- --------------------------------------------------------

--
-- Struktur dari tabel `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `id` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `alamat` text,
  `telepon` varchar(255) DEFAULT NULL,
  `nama_instansi` varchar(255) DEFAULT NULL,
  `alamat_instansi` text,
  `telepon_instansi` varchar(255) DEFAULT NULL,
  `login_terakhir` datetime DEFAULT NULL,
  `aktif` int(11) DEFAULT '0',
  `waktu_dibuat` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `menu`
--

INSERT INTO `menu` (`id`, `title`) VALUES
(1, 'Main Menu');

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu_attribute`
--

CREATE TABLE IF NOT EXISTS `menu_attribute` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu_item`
--

CREATE TABLE IF NOT EXISTS `menu_item` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `menu_item_type_id` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '1',
  `title` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `menu_item`
--

INSERT INTO `menu_item` (`id`, `menu_id`, `menu_item_type_id`, `parent_id`, `order`, `title`) VALUES
(1, 1, 'link', 0, 1, 'Home'),
(2, 1, '2', 0, 2, 'Profil'),
(3, 1, 'link', 0, 3, 'Info Bencana'),
(4, 1, 'link', 0, 6, 'Web Mail'),
(5, 1, 'page', 2, 1, 'Tujuan'),
(6, 1, 'page', 2, 2, 'Visi dan Misi'),
(39, 1, 'link', 0, 7, 'PPID'),
(38, 1, 'contact', 0, 5, 'Hubungi kami'),
(13, 1, 'post', 0, 4, 'Berita'),
(14, 1, 'page', 2, 3, 'Fungsi Dan Tugas'),
(17, 1, 'page', 2, 5, 'Daftar Nama Pejabat'),
(40, 1, 'page', 2, 4, 'Struktur Organisasi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu_item_attribute`
--

CREATE TABLE IF NOT EXISTS `menu_item_attribute` (
  `id` int(11) NOT NULL,
  `menu_item_id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` text
) ENGINE=MyISAM AUTO_INCREMENT=284 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `menu_item_attribute`
--

INSERT INTO `menu_item_attribute` (`id`, `menu_item_id`, `key`, `value`) VALUES
(1, 1, 'url', '<base_url>'),
(2, 1, 'page_id', '1'),
(3, 1, 'post_category_id', '1'),
(4, 1, 'photo_id', '6'),
(5, 1, 'photo_category_id', '1'),
(6, 1, 'video_id', '7'),
(7, 1, 'video_category_id', '1'),
(8, 1, 'form_contact_email', ''),
(9, 1, 'kategori_id', '1'),
(10, 2, 'url', '#'),
(11, 2, 'page_id', '1'),
(12, 2, 'post_category_id', '1'),
(13, 2, 'photo_id', '6'),
(14, 2, 'photo_category_id', '1'),
(15, 2, 'video_id', '7'),
(16, 2, 'video_category_id', '1'),
(17, 2, 'form_contact_email', ''),
(18, 2, 'kategori_id', '1'),
(19, 3, 'url', '<base_url>/index.php?r=bencana/index'),
(20, 3, 'page_id', '1'),
(21, 3, 'post_category_id', '1'),
(22, 3, 'photo_id', '6'),
(23, 3, 'photo_category_id', '1'),
(24, 3, 'video_id', '1'),
(25, 3, 'video_category_id', '1'),
(26, 3, 'form_contact_email', ''),
(27, 3, 'kategori_id', '1'),
(28, 4, 'url', '#'),
(29, 4, 'page_id', '1'),
(30, 4, 'post_category_id', '1'),
(31, 4, 'photo_id', '6'),
(32, 4, 'photo_category_id', '1'),
(33, 4, 'video_id', '7'),
(34, 4, 'video_category_id', '1'),
(35, 4, 'form_contact_email', 'info@bandung.lan.go.id'),
(36, 4, 'kategori_id', '1'),
(37, 5, 'url', '#'),
(38, 5, 'page_id', '1'),
(39, 5, 'post_category_id', '1'),
(40, 5, 'photo_id', '6'),
(41, 5, 'photo_category_id', '1'),
(42, 5, 'video_id', '1'),
(43, 5, 'video_category_id', '1'),
(44, 5, 'form_contact_email', ''),
(45, 5, 'kategori_id', '1'),
(46, 6, 'url', ''),
(47, 6, 'page_id', '2'),
(48, 6, 'post_category_id', '1'),
(49, 6, 'photo_id', '6'),
(50, 6, 'photo_category_id', '1'),
(51, 6, 'video_id', '1'),
(52, 6, 'video_category_id', '1'),
(53, 6, 'form_contact_email', ''),
(54, 6, 'kategori_id', '1'),
(275, 40, 'page_id', '4'),
(274, 40, 'url', ''),
(273, 14, 'form_contact_html', ''),
(272, 14, 'post_category_id', '1'),
(271, 6, 'form_contact_html', ''),
(270, 13, 'form_contact_html', ''),
(64, 8, 'url', 'http://diklat.bandung.lan.go.id/v2'),
(65, 8, 'page_id', '1'),
(66, 8, 'post_category_id', '1'),
(67, 8, 'photo_id', '6'),
(68, 8, 'photo_category_id', '1'),
(69, 8, 'video_id', '1'),
(70, 8, 'video_category_id', '1'),
(71, 8, 'form_contact_email', ''),
(72, 8, 'kategori_id', '1'),
(73, 9, 'url', 'http://litbang.bandung.lan.go.id/v2'),
(74, 9, 'page_id', '1'),
(75, 9, 'photo_id', '6'),
(76, 9, 'photo_category_id', '1'),
(77, 9, 'video_id', '1'),
(78, 9, 'video_category_id', '1'),
(79, 9, 'form_contact_email', ''),
(80, 9, 'kategori_id', '1'),
(94, 11, 'video_category_id', '1'),
(93, 11, 'video_id', '1'),
(92, 11, 'photo_category_id', '1'),
(91, 11, 'photo_id', '6'),
(90, 11, 'page_id', '1'),
(89, 11, 'url', 'http://asesmen.bandung.lan.go.id/v2'),
(95, 11, 'form_contact_email', ''),
(96, 11, 'kategori_id', '1'),
(97, 12, 'url', 'http://administrasi.bandung.lan.go.id'),
(98, 12, 'page_id', '1'),
(99, 12, 'photo_id', '6'),
(100, 12, 'photo_category_id', '1'),
(101, 12, 'video_id', '1'),
(102, 12, 'video_category_id', '1'),
(103, 12, 'form_contact_email', ''),
(104, 12, 'kategori_id', '1'),
(105, 13, 'url', ''),
(106, 13, 'page_id', '1'),
(107, 13, 'photo_id', '6'),
(108, 13, 'photo_category_id', '1'),
(109, 13, 'video_id', '1'),
(110, 13, 'video_category_id', '1'),
(111, 13, 'form_contact_email', ''),
(112, 13, 'kategori_id', '1'),
(113, 14, 'url', ''),
(114, 14, 'page_id', '3'),
(115, 14, 'photo_id', '6'),
(116, 14, 'photo_category_id', '1'),
(117, 14, 'video_id', '1'),
(118, 14, 'video_category_id', '1'),
(119, 14, 'form_contact_email', ''),
(120, 15, 'url', ''),
(121, 15, 'page_id', '4'),
(122, 15, 'photo_id', '6'),
(123, 15, 'photo_category_id', '1'),
(124, 15, 'video_id', '1'),
(125, 15, 'video_category_id', '1'),
(126, 15, 'form_contact_email', ''),
(127, 16, 'url', ''),
(128, 16, 'page_id', '8'),
(129, 16, 'photo_id', '6'),
(130, 16, 'photo_category_id', '1'),
(131, 16, 'video_id', '1'),
(132, 16, 'video_category_id', '1'),
(133, 16, 'form_contact_email', ''),
(134, 17, 'url', ''),
(135, 17, 'page_id', '5'),
(136, 17, 'photo_id', '6'),
(137, 17, 'photo_category_id', '1'),
(138, 17, 'video_id', '1'),
(139, 17, 'video_category_id', '1'),
(140, 17, 'form_contact_email', ''),
(141, 18, 'url', ''),
(142, 18, 'page_id', '5'),
(143, 18, 'photo_id', '6'),
(144, 18, 'photo_category_id', '1'),
(145, 18, 'video_id', '1'),
(146, 18, 'video_category_id', '1'),
(147, 18, 'form_contact_email', ''),
(148, 19, 'url', ''),
(149, 19, 'page_id', '6'),
(150, 19, 'photo_id', '6'),
(151, 19, 'photo_category_id', '1'),
(152, 19, 'video_id', '1'),
(153, 19, 'video_category_id', '1'),
(154, 19, 'form_contact_email', ''),
(283, 3, 'form_contact_html', ''),
(282, 13, 'post_category_id', '1'),
(162, 21, 'url', 'http://diklat.bandung.lan.go.id/v2/index.php?r=page/read&id=6'),
(163, 21, 'page_id', '10'),
(164, 21, 'photo_id', '6'),
(165, 21, 'photo_category_id', '1'),
(166, 21, 'video_id', '7'),
(167, 21, 'video_category_id', '1'),
(168, 21, 'form_contact_email', ''),
(169, 22, 'url', 'http://diklat.bandung.lan.go.id/v2/index.php?r=page/read&id=10'),
(170, 22, 'page_id', '13'),
(171, 22, 'photo_id', '6'),
(172, 22, 'photo_category_id', '1'),
(173, 22, 'video_id', '7'),
(174, 22, 'video_category_id', '1'),
(175, 22, 'form_contact_email', ''),
(176, 23, 'url', ''),
(177, 23, 'page_id', '14'),
(178, 23, 'photo_id', '6'),
(179, 23, 'photo_category_id', '1'),
(180, 23, 'video_id', '1'),
(181, 23, 'video_category_id', '1'),
(182, 23, 'form_contact_email', ''),
(183, 24, 'url', 'http://litbang.bandung.lan.go.id/v2/index.php?r=postCategory/read&id=1'),
(184, 24, 'page_id', '11'),
(185, 24, 'photo_id', '6'),
(186, 24, 'photo_category_id', '1'),
(187, 24, 'video_id', '1'),
(188, 24, 'video_category_id', '1'),
(189, 24, 'form_contact_email', ''),
(190, 25, 'url', 'http://jurnal.bandung.lan.go.id'),
(191, 25, 'page_id', '12'),
(192, 25, 'photo_id', '6'),
(193, 25, 'photo_category_id', '1'),
(194, 25, 'video_id', '1'),
(195, 25, 'video_category_id', '1'),
(196, 25, 'form_contact_email', ''),
(197, 26, 'url', 'http://litbang.bandung.lan.go.id/v2/index.php?r=postCategory/read&id=3'),
(198, 26, 'page_id', '15'),
(199, 26, 'photo_id', '6'),
(200, 26, 'photo_category_id', '1'),
(201, 26, 'video_id', '7'),
(202, 26, 'video_category_id', '1'),
(203, 26, 'form_contact_email', ''),
(204, 27, 'url', 'http://administrasi.bandung.lan.go.id/index.php?r=page/read&id=6'),
(205, 27, 'page_id', '16'),
(206, 27, 'photo_id', '6'),
(207, 27, 'photo_category_id', '1'),
(208, 27, 'video_id', '1'),
(209, 27, 'video_category_id', '1'),
(210, 27, 'form_contact_email', ''),
(211, 28, 'url', 'http://administrasi.bandung.lan.go.id/index.php?r=page/read&id=4'),
(212, 28, 'page_id', '17'),
(213, 28, 'photo_id', '6'),
(214, 28, 'photo_category_id', '1'),
(215, 28, 'video_id', '7'),
(216, 28, 'video_category_id', '1'),
(217, 28, 'form_contact_email', ''),
(218, 9, 'post_category_id', '1'),
(219, 11, 'post_category_id', '1'),
(220, 4, 'form_contact_html', '<p>\r\n	<strong>Pusat Kajian dan Pendidikan Pelatihan Aparatur I LAN</strong></p>\r\n<p>\r\n	<strong>Alamat.</strong> Jl. Kiara Payung km. 4,7 Bumi Perkemahan Jatinangor Sumedang, Jawa Barat<br />\r\n	<strong>Telp.</strong> (022) 7790048, 7782041<br />\r\n	<strong>Fax.</strong> (022) 7790055, 7790044<br />\r\n	<strong>Email.</strong> info@bandung.lan.go.id</p>\r\n'),
(221, 29, 'url', 'http://bandung.lan.go.id/index.php?r=photoAlbum/index'),
(222, 29, 'page_id', '1'),
(223, 29, 'post_category_id', '1'),
(224, 29, 'photo_id', '6'),
(225, 29, 'photo_category_id', '1'),
(226, 29, 'video_id', '7'),
(227, 29, 'video_category_id', '1'),
(228, 29, 'form_contact_email', ''),
(229, 29, 'form_contact_html', ''),
(230, 30, 'url', ''),
(231, 30, 'page_id', '18'),
(232, 30, 'post_category_id', '1'),
(233, 30, 'photo_id', '6'),
(234, 30, 'photo_category_id', '1'),
(235, 30, 'video_id', '7'),
(236, 30, 'video_category_id', '1'),
(237, 30, 'form_contact_email', ''),
(238, 30, 'form_contact_html', ''),
(239, 24, 'post_category_id', '1'),
(240, 24, 'form_contact_html', ''),
(241, 21, 'post_category_id', '1'),
(242, 21, 'form_contact_html', ''),
(243, 25, 'post_category_id', '1'),
(244, 25, 'form_contact_html', ''),
(245, 26, 'post_category_id', '1'),
(246, 26, 'form_contact_html', ''),
(247, 22, 'post_category_id', '1'),
(248, 22, 'form_contact_html', ''),
(249, 23, 'post_category_id', '1'),
(250, 23, 'form_contact_html', ''),
(251, 27, 'post_category_id', '1'),
(252, 27, 'form_contact_html', ''),
(253, 28, 'post_category_id', '1'),
(254, 28, 'form_contact_html', ''),
(255, 1, 'form_contact_html', ''),
(256, 2, 'form_contact_html', ''),
(257, 5, 'form_contact_html', ''),
(258, 38, 'url', ''),
(259, 38, 'page_id', '1'),
(260, 38, 'post_category_id', '1'),
(261, 38, 'photo_id', '6'),
(262, 38, 'form_contact_email', 'thomas.alfa.edison@gmail.com'),
(263, 38, 'form_contact_html', '<p>\r\n	<strong>Badan Penanggulangan Bencana Daerah Kabupaten Tangerang</strong></p>\r\n<p>\r\n	Jalan Raya Curug Km. 2 Curug Tangerang<br />\r\n	Telepon: 021-5984343<br />\r\n	Email: info@bpbd.tangerangkab.go.id</p>\r\n'),
(264, 39, 'url', '#'),
(265, 39, 'page_id', '1'),
(266, 39, 'post_category_id', '1'),
(267, 39, 'photo_id', '6'),
(268, 39, 'form_contact_email', ''),
(269, 39, 'form_contact_html', ''),
(276, 40, 'post_category_id', '1'),
(277, 40, 'photo_id', '6'),
(278, 40, 'form_contact_email', ''),
(279, 40, 'form_contact_html', ''),
(280, 17, 'post_category_id', '1'),
(281, 17, 'form_contact_html', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu_item_type`
--

CREATE TABLE IF NOT EXISTS `menu_item_type` (
  `id` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` int(11) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `menu_item_type`
--

INSERT INTO `menu_item_type` (`id`, `title`, `status`) VALUES
('link', 'Link', 1),
('page', 'Page', 1),
('contact', 'Contact', 1),
('post', 'Post', 1),
('photo_album', 'Photo Album', 1),
('video_category', 'Video Category', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text,
  `image` varchar(255) DEFAULT NULL,
  `time_created` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `page`
--

INSERT INTO `page` (`id`, `title`, `content`, `image`, `time_created`) VALUES
(1, 'Tujuan', '<p>Konten Tujuan</p>\r\n', '', '2015-09-22 08:30:17'),
(2, 'Visi dan Misi', '<p>Konten Visi dan Misi</p>\r\n', '', '2015-09-22 08:52:19'),
(3, 'Fungsi dan Tugas', '<p>Konten Fungsi dan Tugas</p>\r\n', '', '2015-09-22 08:52:44'),
(4, 'Struktur Organisasi', '<p>Struktur Organisasi</p>\r\n', '', '2015-09-22 08:53:23'),
(5, 'Daftar Nama Pejabat', '<p>Konten Daftar Nama Pejabat</p>\r\n', '', '2015-09-22 08:53:49'),
(6, 'Bencana', '<p>sdfsdff</p>\r\n', '', '2015-09-23 12:51:36');

-- --------------------------------------------------------

--
-- Struktur dari tabel `photo`
--

CREATE TABLE IF NOT EXISTS `photo` (
  `id` int(11) NOT NULL,
  `photo_category_id` int(11) DEFAULT NULL,
  `photo_album_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `time_created` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `photo`
--

INSERT INTO `photo` (`id`, `photo_category_id`, `photo_album_id`, `title`, `file`, `model`, `model_id`, `time_created`) VALUES
(6, 1, NULL, 'dokter', '1401094210_1395134211_3.jpg', NULL, NULL, '2014-05-26 15:50:10'),
(4, 2, NULL, 'Inovasi Kemasyarakatan', '1401245654_1395134193_2.jpg', NULL, NULL, '2014-05-26 14:05:57'),
(5, 2, NULL, 'Test', '1401245712_1395134388_11.jpg', NULL, NULL, '2014-05-26 14:07:55'),
(7, 1, NULL, 'Photo Pinguin', '1432702812_Chrysanthemum-light.jpg', NULL, NULL, '2015-05-27 12:00:12'),
(14, 2, NULL, 'Rapat Diskusi Fokkasi', '1438889672_IMG_7525.JPG', '', NULL, '2015-08-07 02:34:32'),
(10, 1, NULL, 'tes', '1438889998_IMG_7542.JPG', 'Post', 1, '2015-07-06 15:11:13'),
(11, 1, NULL, 'Assesment Bontang, 16 Juni 2014', '1438889898_IMG_7538.JPG', 'Post', 1, '2015-07-06 15:12:19'),
(15, 2, NULL, 'Rapat Diskusi Fokkasi', '1438889692_IMG_7526.JPG', '', NULL, '2015-08-07 02:34:52'),
(16, 2, NULL, 'Rapat Diskusi Fokkasi', '1438889706_IMG_7527.JPG', '', NULL, '2015-08-07 02:35:06'),
(17, 2, NULL, 'Rapat Diskusi Fokkasi', '1438889726_IMG_7528.JPG', '', NULL, '2015-08-07 02:35:26'),
(18, 2, NULL, 'Rapat Diskusi Fokkasi', '1438889741_IMG_7529.JPG', '', NULL, '2015-08-07 02:35:41'),
(19, 2, NULL, 'Rapat Diskusi Fokkasi', '1438889759_IMG_7530.JPG', '', NULL, '2015-08-07 02:35:59'),
(20, 1, NULL, 'Pertemuan Fokkasi', '1438890084_IMG_7533.JPG', '', NULL, '2015-08-07 02:41:24'),
(26, 1, NULL, 'Grafik Jumlah Pegawai Jenis Kelamin', '1439184257_Grafik-Jumlah-Pegawai-Sebaran-Jenis-Kelamin1.png', '', NULL, '2015-08-10 12:24:17'),
(27, 1, NULL, 'Grafik Jumlah Pegawai Golongan', '1439184535_Grafik-Jumlah-Pegawai-Golongan1.png', '', NULL, '2015-08-10 12:28:55'),
(28, 1, NULL, 'Grafik Jumlah Pegawai Usia', '1439184727_Grafik-Jumlah-Pegawai-Usia1.png', '', NULL, '2015-08-10 12:32:07'),
(29, 1, NULL, 'Grafik Jumlah Pegawai Jabatan', '1439184871_Grafik-Jumlah-Pegawai-Jabatan1.png', '', NULL, '2015-08-10 12:34:31'),
(25, 1, NULL, 'Grafik Jumlah Pegawai Pendidikan', '1439183198_Grafik-Jumlah-Pegawai-Pendidikan1.png', '', NULL, '2015-08-10 12:06:38'),
(30, 1, NULL, 'Inovasi KKIAN 1', '1439187474_Slide3.JPG', '', NULL, '2015-08-10 13:17:21'),
(31, 1, NULL, 'Inovasi KKIAN 2', '1439187524_Slide4.JPG', '', NULL, '2015-08-10 13:18:44'),
(32, 1, NULL, 'Inovasi KKIAN 3', '1439187579_Slide5.JPG', '', NULL, '2015-08-10 13:19:39'),
(33, 1, NULL, 'Inovasi PKKA 1', '1439187710_Slide7.JPG', '', NULL, '2015-08-10 13:21:50'),
(34, 1, NULL, 'Inovasi PKKA 2', '1439187785_Slide8.JPG', '', NULL, '2015-08-10 13:23:05'),
(35, 1, NULL, 'Inovasi Diklat 1', '1439187836_Slide10.JPG', '', NULL, '2015-08-10 13:23:56'),
(36, 1, NULL, 'Inovasi Diklat 2', '1439187875_Slide11.JPG', '', NULL, '2015-08-10 13:24:35'),
(37, 1, NULL, 'Inovasi Diklat 3', '1439187918_Slide12.JPG', '', NULL, '2015-08-10 13:25:18'),
(38, 1, NULL, 'Inovasi Diklat 4', '1439187956_Slide13.JPG', '', NULL, '2015-08-10 13:25:56'),
(39, 1, NULL, 'Inovasi Admin 1', '1439188010_Slide15.JPG', '', NULL, '2015-08-10 13:26:50'),
(40, 1, NULL, 'Inovasi Admin 1', '1439188045_Slide16.JPG', '', NULL, '2015-08-10 13:27:25'),
(41, 1, NULL, 'Inovasi Admin 3', '1439188085_Slide17.JPG', '', NULL, '2015-08-10 13:28:05'),
(42, 1, NULL, 'Inovasi Admin 4', '1439188123_Slide18.JPG', '', NULL, '2015-08-10 13:28:43'),
(44, NULL, 2, 'Diskusi Pembentukan Forum Kemitraan Untuk Kajian Kebijakan Dan Inovasi Administrasi', '1439294321_IMG_7530.JPG', 'photoAlbum', 2, '2015-08-11 18:58:41'),
(45, NULL, 2, 'Diskusi Pembentukan Forum Kemitraan Untuk Kajian Kebijakan Dan Inovasi Administrasi', '1439294349_IMG_7528.JPG', 'photoAlbum', 2, '2015-08-11 18:59:09'),
(46, NULL, 2, 'Diskusi Pembentukan Forum Kemitraan Untuk Kajian Kebijakan Dan Inovasi Administrasi', '1439294380_IMG_7540.JPG', 'photoAlbum', 2, '2015-08-11 18:59:40'),
(47, NULL, 2, 'Diskusi Pembentukan Forum Kemitraan Untuk Kajian Kebijakan Dan Inovasi Administrasi', '1439294407_IMG_7542.JPG', 'photoAlbum', 2, '2015-08-11 19:00:07'),
(48, NULL, 2, 'Diskusi Pembentukan Forum Kemitraan Untuk Kajian Kebijakan Dan Inovasi Administrasi', '1439294434_IMG_7544.JPG', 'photoAlbum', 2, '2015-08-11 19:00:34'),
(49, NULL, 2, 'Diskusi Pembentukan Forum Kemitraan Untuk Kajian Kebijakan Dan Inovasi Administrasi', '1439294470_IMG_7557.JPG', 'photoAlbum', 2, '2015-08-11 19:01:10'),
(50, NULL, NULL, 'header jurnal', '1439349226_Web-Jurnal-Header.png', '', NULL, '2015-08-12 10:13:46'),
(51, NULL, 3, 'Peresmian Counter Kejujuran, Dusun Pembibitan dan Dusun Kelinci', '1439403725_11223752_522793737873930_3642485205921268071_n.jpg', 'photoAlbum', 3, '2015-08-13 01:22:05'),
(52, NULL, 3, 'Peresmian Counter Kejujuran, Dusun Pembibitan dan Dusun Kelinci', '1439403740_11826055_522794084540562_7325211062167046685_n.jpg', 'photoAlbum', 3, '2015-08-13 01:22:20'),
(53, NULL, 3, 'Peresmian Counter Kejujuran, Dusun Pembibitan dan Dusun Kelinci', '1439403757_11828581_522793787873925_2245953311333063714_n.jpg', 'photoAlbum', 3, '2015-08-13 01:22:37'),
(54, NULL, 3, 'Peresmian Counter Kejujuran, Dusun Pembibitan dan Dusun Kelinci', '1439403771_11836659_522793927873911_7857107259585545213_n.jpg', 'photoAlbum', 3, '2015-08-13 01:22:51'),
(55, NULL, 3, 'Peresmian Counter Kejujuran, Dusun Pembibitan dan Dusun Kelinci', '1439403787_11863461_522793994540571_1719197345108361115_n.jpg', 'photoAlbum', 3, '2015-08-13 01:23:07'),
(56, NULL, 3, 'Peresmian Counter Kejujuran, Dusun Pembibitan dan Dusun Kelinci', '1439403804_11887830_522793681207269_2135896794957021374_n.jpg', 'photoAlbum', 3, '2015-08-13 01:23:24'),
(57, NULL, 3, 'Peresmian Counter Kejujuran, Dusun Pembibitan dan Dusun Kelinci', '1439403821_11889408_522793814540589_5768024023418656896_n.jpg', 'photoAlbum', 3, '2015-08-13 01:23:41'),
(58, NULL, 3, 'Peresmian Counter Kejujuran, Dusun Pembibitan dan Dusun Kelinci', '1439403836_11889604_522794044540566_6031759989072758959_n.jpg', 'photoAlbum', 3, '2015-08-13 01:23:56'),
(59, NULL, 3, 'Peresmian Counter Kejujuran, Dusun Pembibitan dan Dusun Kelinci', '1439403848_11889652_522794024540568_2511699064039034279_n.jpg', 'photoAlbum', 3, '2015-08-13 01:24:08'),
(60, 3, NULL, 'background image', '1442808944_Background-Aplikasi-Internal.png', '', NULL, '2015-09-21 10:12:18');

-- --------------------------------------------------------

--
-- Struktur dari tabel `photo_album`
--

CREATE TABLE IF NOT EXISTS `photo_album` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `time_created` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `photo_album`
--

INSERT INTO `photo_album` (`id`, `title`, `description`, `time_created`) VALUES
(2, 'Diskusi Pembentukan Forum Kemitraan Untuk Kajian Kebijakan Dan Inovasi Administrasi', '<p>Album ini merupakan kumpulan dokumentasi mengenai kegiatan Diskusi Pembentukan Forum Kemitraan Untuk Kajian Kebijakan dan Inovasi Administrasi</p>\r\n', '2015-08-11 06:30:17'),
(3, 'Peresmian Counter Kejujuran, Dusun Pembibitan dan Dusun Kelinci', '<p>Dokumentasi Peresmian Counter Kejujuran, Dusun Pembibitan dan Dusun Kelinci Oleh Plt. Kepala LAN, Bapak Adi Suryanto dan Kepala Pusat PKP2A I LAN, Bapak Joni Dawud. Kegiatan ini merupakan salah satu kepedulian PKP2A I LAN terhadap lingkungan sekitar</p>\r\n', '2016-08-12 13:21:04');

-- --------------------------------------------------------

--
-- Struktur dari tabel `photo_category`
--

CREATE TABLE IF NOT EXISTS `photo_category` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `photo_category`
--

INSERT INTO `photo_category` (`id`, `title`) VALUES
(1, 'Kategori A'),
(2, 'PNPM 2014'),
(3, 'Icon');

-- --------------------------------------------------------

--
-- Struktur dari tabel `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `post_category_id` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `total_views` int(11) DEFAULT '0',
  `time_created` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `post`
--

INSERT INTO `post` (`id`, `title`, `content`, `post_category_id`, `image`, `tags`, `total_views`, `time_created`) VALUES
(1, 'PENURUNAN STATUS GUNUNG RAUNG DARI SIAGA MENJADI WASPADA', '<p>Status siaga Gn Raung sejak 29 Juni 2015 dinaikan dari Waspada (level ll) ke Siaga (level lll). Kondisi ini ditandai dengan aktivitas yang meningkat dan fluktuatif. Tremor terus menerus terpantau oleh mesin seismograf di Pos Pantau Gunung Api (PPGA) Gn. Raung di Kecamatan Songgon Banyuwangi. Pengamatan terus dilakukan terkait pergerakan tremor menerus yang fluktuatif antara 2-32 mm pada setiap harinya. Tindakan kesiapsiagaan warga pun dilakukan untuk mengantisipasi kemungkinan terburuk yaitu gunung tersebut akan meletus. Beberapa bandara pun sempat di tutup dikarenakan abu vulkanik yang mengganggu jalur penerbangan. Meski demikian, aktivitas gunung dgn ketinggian 3332mdpl ini berangsur surut sejak seminggu belakangan. Sehingga pada Senin (24/08/2015) Pusat Vulkanologi Mitigasi Benacana Geologi (PVMBG) melalui surat resmi dengan nomor 2651/45/BGL.V/2015 Menurunkan status Gn. Raung dari Siaga (level lll) menjadi Waspada (level ll). &lt;<a href="http://bpbd.jatimprov.go.id/images/Penurunan_staus_raung_dari_siaga_ke_waspada.pdf">surat PVMBG klik disini</a>&gt;</p>\r\n', 1, '1442998547_5.jpg', '', 1, '2015-09-23 12:10:53'),
(2, 'Info Bencana', '<p>sdf</p>\r\n', 2, '', '', 2, '2015-09-23 16:42:59'),
(5, 'BANTEN DARURAT KEKERINGAN', '<p>dkfjdsjf</p>\r\n', 1, '', '', 0, '2015-09-23 22:05:07'),
(6, 'KEKERINGAN DI BEBERAPA WILAYAH INDONESIA', '<p>jlkjdljf</p>\r\n', 1, '', '', 0, '2015-09-24 13:21:36'),
(8, 'Grafik', '', 3, '', '', 0, '2015-09-30 13:52:54'),
(9, 'PABRIK PEMBUAT SEPATU DI TANGERANG TERBAKAR', '<p>Terjadi kebakaran di sebuah pabrik pembuat sepatu yakni PT KMK Global Sport, yang berada di kawasan Industri Telesonic, Kabupaten Tangerang, Senin (27/7) sore</p>\r\n\r\n<p><em>Kontributor Elshinta, Mus Mulyadi</em>, melaporkan sebanyak 17 mobil pemadam kebakaran di turunkan dari Kota dan Kabupaten Tangerang dan Tangerang Selatan untuk memadamkan api tersebut.</p>\r\n\r\n<p>Menurut informasi, ruangan yang terbakar hanya ruang produksi dan ruang office.</p>\r\n\r\n<p>Nampak petugas pemadam kebakaran berususah payah mencegah agar api tidak menyebar ke bangunan lainnya.</p>\r\n\r\n<p>Salah satu petugas pemadam kebakaran dari Kabupaten Tangerang, mengatakan, belum diketahui apa penyebab kebakaran tersebut dan belum dapat ditaksir seberapa besar kerugiannya.</p>\r\n\r\n<p>Beberapa mobil ambulans nampak disiagakan di lokasi kebakaran tersebut karena ada beberapa karyawan yang pingsan karena kaget dan mengalami sesak nafas akibat menghirup asap kebakaran di area tersebut.</p>\r\n\r\n<p>Sementara itu Polisi nampak melakukan penjagaan di lokasi kebakaran tersebut guna mencegah hal-hal yang tidak diingikan.</p>\r\n', 1, '1443597984_gudang-sepatu-sport-terbakar-2-orang-luka-luka-mG3.jpg', '', 0, '2015-09-30 14:26:24'),
(10, 'KEKERINGAN MELANDA PROPINSI BANTEN', '<p><strong>TEMPO.CO</strong>, <strong>Serang </strong>- Ribuan hektare lahan pertanian di Provinsi Banten mengalami gagal panen akibat kekeringan yang melanda seluruh Banten sejak beberapa bulan terakhir. Berdasarkan catatan Dinas Pertanian dan Peternakan Provinsi Banten, saat ini lebih dari 4.000 hektare lahan pertanian mengalami gagal panen atau puso.<br />\r\n<br />\r\n&ldquo;Data terakhir tercatat, 25.000 hektare lahan pertanian terdampak kekeringan, dan sekarang sudah 4.000 hektare lebih mengalami puso,&quot; kata Kepala Distanak Provinsi Banten Eneng Nurcahyati, Senin, 28 September 2015.<br />\r\n<br />\r\nMeski ribuan hektare sawah mengalami puso, ucap Eneng, kondisi kekeringan tidak berpengaruh signifikan pada peningkatan produksi padi. Sebab, ujar dia, pihaknya bersama kabupaten/kota telah melakukan upaya khusus (upsus) dalam rangka swasembada beras.<br />\r\n<br />\r\n&ldquo;Kalau dilihat dari target luas tanam, <em>alhamdulillah</em> Pandeglang sudah melampaui target pada tahun ini. Memang Pandeglang dan Lebak sebagai daerah yang diharapkan mampu berkontribusi besar terhadap pemenuhan target swasembada nasional. Mudah-mudahan tidak berpengaruh signifikan pada peningkatan produksi,&quot; tutur Eneng.<br />\r\n<br />\r\nSebelumnya, Badan Penanggulangan Bencana Daerah Banten memperpanjang masa tanggap darurat bencana kekeringan mulai 15 hingga 30 September 2015. Perpanjangan dilakukan karena titik kekeringan kian meluas hingga 80 kecamatan di Banten.<br />\r\n<br />\r\n&quot;Penetapan status tanggap darurat kekeringan semula 1-15 September lalu. Namun, karena tidak menunjukkan pengurangan titik kemarau, malah cenderung bertambah, tanggap darurat kekeringan diperpanjang dari 15 sampai 30 September,&quot; ujar Kepala BPBD Banten Komari.<br />\r\n<br />\r\nKomari mengatakan data per 14 September 2015, jumlah kecamatan yang dilanda kekeringan sebanyak 80 kecamatan dari sebelumnya hanya 74 kecamatan. &quot;Kemudian jumlah desa juga bertambah, dari semula 148 desa yang dilanda kekeringan menjadi 160 desa,&quot; ucapnya.<br />\r\n<br />\r\nPerpanjangan tanggap darurat juga dilakukan karena prediksi BMKG menyebut puncak musim kemarau masih akan terjadi hingga Oktober mendatang. Menurut dia, saat ini warga setiap hari hanya mengandalkan distribusi air bersih dari bantuan-bantuan pemerintah.<br />\r\n<br />\r\n&quot;Kondisi di lapangan masih butuh bantuan. Krisis air kian meluas. Pendistribusian air dalam rangka perpanjangan tanggap darurat itu akan dilakukan selama 15 hari. Selain itu, kami sudah memberikan bantuan pembuatan pompa di 37 titik,&quot; ujarnya.</p>\r\n', 1, '1443598115_20141019135710642.jpg', '', 1, '2015-09-30 14:28:35'),
(11, 'DAMPAK EL NINO 2015', '<p>Pekan lalu, Kamis (30/7/2015) Kepala Badan Meteorologi, Klimatologi dan Geofisika (BMKG), Andi Eka Sakya, merilis peringatan dini dampak fenomena El Nino 2015 di Indonesia. Tahun ini, El Nino yang semula diprediksi berskala moderate, berpotensi menguat. Akibatnya, kemarau tahun ini akan berlangsung lebih lama. Yang penting kita ketahui ialah bahwa kemarau panjang bisa berdampak luas.</p>\r\n\r\n<p><strong>Sekilas tentang El Nino</strong><br />\r\nEl nino adalah sebutan dalam bahasa Spanyol yang artinya Si Buyung Kecil (The Little Boy) atau Anak Kristus (Christ Child). El Nino adalah nama yang diberikan oleh para nelayan lepas pantai Amerika Selatan pada tahun 1600-an untuk menyebut fenomena menghangatnya air laut di Samudra Pasifik yang tidak lazim pada sekitar bulan Desember (perayaan Natal).</p>\r\n\r\n<p>Belakangan diketahui bahwa fenomena tersebut terjadi akibat melemahnya angin pasat yang biasanya (dalam kondisi normal) bersirkulasi di Samudra Pasifik. Keadaan ini menyebabkan air hangat di bagian barat Pasifik tertarik ke timur.<br />\r\nKarena air hangat di bagian barat Pasifik (Australia, Papua Nugini, dan Indonesia) berkurang maka penguapan (evaporasi) juga menurun.</p>\r\n\r\n<p>Berkurangnya evaporasi menyebabkan atmosfer di kawasan tersebut miskin uap air. Karena miskin uap air, curah hujan di kawasan ini juga menurun sehingga terjadilah kekeringan (kemarau). Semakin kuat dan masif perpindahan masa air laut itu, akan semakin lama pula musim kemarau yang ditimbulkannya.</p>\r\n\r\n<p><strong>Kemarau Panjang dan Dampaknya</strong><br />\r\nDalam rilis itu, Kepala BMKG menyatakan bahwa berdasarkan berbagai data klimatologis yang ada, fenomena El Nino tahun 2015 ini berkecenderungan menguat. Oleh karena itu pihaknya memprediksi kemarau tahun ini akan berlangsung lama. Paling cepat, hujan baru turun pada bulan November atau Desember. Daerah yang diperkirakan bakal terkena dampak serius El Nino 2015 adalah Sumatera Selatan, Lampung, Jawa, Bali, Nusa Tenggara, Kalimantan Selatan dan Sulawesi Selatan.</p>\r\n\r\n<p>Mengingat kemarau sudah berlangsung sejak Mei, bahkan ada yang sudah mengalaminya sejak Maret, maka sulit dibayangkan apa akibatnya jika hujan baru turun di bulan November atau Desember. Sungai, situ, sawah, lahan-lahan pertanian, dan sumur-sumur dangkal bakal mengering. Bila itu terjadi, inilah dampak yang akan kita hadapi:</p>\r\n\r\n<p><strong>a. Dampak langsung</strong><br />\r\n1) Anjloknya produksi pertanian dan perkebunan<br />\r\nBerkurangnya produksi pertanian ini bisa memicu melambungnya harga-harga bahan makanan: beras, sayur mayur, dan buah-buahan. Selain bisa menurunkan tingkat kesehatan akibat kurangnya asupan gizi, kelangkaan bahan makanan pokok pada tingkatan ekstrem bisa menimbulkan bencana kelaparan.</p>\r\n\r\n<p>2) Krisis air bersih<br />\r\nKetika sungai, situ, dan sumur dangkal mengering banyak masyarakat yang akan kesulitan mendapatkan air bersih. Kondisi ini bisa mendorong timbulnya wabah penyakit menular karena mayarakat terpaksa menggunakan/mengkonsumsi air yang tidak higenis.</p>\r\n\r\n<p>3) Kebakaran<br />\r\nDalam kondisi normal saja kebakaran hutan/lahan dan properti lainnya bisa terjadi dengan mudah dan sulit mengendalikannya, apatah lagi pada musim kemarau ketika pepohonan mengering dan meranggas, ketika di lingkungan pemukiman kekurangan air.</p>\r\n\r\n<p>4) Berhentinya PLTA<br />\r\nSeperti diberitakan oleh salah satu TV nasional (Selasa, 4/8/2015), kemarau belum lagi genap 3 bulan, PLTA Cirata (Jawa Barat) terpaksa mengistirahatkan 80% turbinnya karena debit bendungan Cirata menurun tajam. Jika seluruh turbin PLTA Cirata ini berhenti dapat dipastikan pasokan listrik Jawa-Bali akan berkurang. Pada akhirnya, kondisi ini akan berdampak pada banyak sektor , khusunya dunia industri.</p>\r\n\r\n<p><strong>b. Dampak tidak langsung</strong><br />\r\nMengeringnya lahan-lahan pertanian bisa berakibat berhentinya usaha pertanian. Keadaan ini dapat menyebabkan tingginya tingkat pengangguran di pedesaan. Pengangguran di satu sisi, ditambah tingginya harga-harga kebutuhan pokok di sisi lain, berpotensi menimbulkan masalah sosial tersendiri. Urbanisasi mungkin meningkat, jumlah gelandangan dan pengemis bertambah, dan angka kriminalitas boleh jadi akan tinggi.</p>\r\n', 1, '1443598408_el-nino-55c1dc44b2927317214461c4.jpg', '', 1, '2015-09-30 14:33:28'),
(12, 'SIMULASI PENANGGULANGAN BENCANA KEBAKARAN', '<p>&nbsp; Badan Penanggulangan Bencana Daerah Kabupaten Tangerang melalui Subbid Penanganan Kebakaran bersama Masrakat Perum Puri Permai Tigaraksa Blok D3 RW 3 meliputi 16 Rt &nbsp;melaksanakan simulasi Penanggulangan Bencana Kebakaran .</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Tujuan dari simulasi tersebut adalah untuk mensosialisasikan Penanggulangan Bahaya Kebakaran secara dini ,dan diharapkan dimana terjadi kebakaran masrakat bisa mengambil tindakan pemadaman secara dini dengan tepat dan benar sehinga kebakaran bisa dipadamkan sebelum api membesar dan meluas.</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Pada sesi pelatihan para petugas Pemadam Kebakaran memberikan contoh bagaimana cara memadamkan kebakaran pada kompor,karena mengingat hal tersebut sering sekali terjadi pada masyarakat maka penanggulanga secara dini sangat perlu .</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Selain itu Masyarakat Puri Permai juga di berikan pembekalan cara cara melakukan pemadaman secara tradisional ( menggunakan Karung basah) dan cara menggunakan Apar ( Alat Pemadam Api Ringan) tentu saja hal ini tujuan nya adalah penanggulangan secara dini. Masyarakat juga di minta untuk sesegera mungkin dan tidak perlu ada keraguan untuk segera melapor pada petugas Pemadam Kebakaran.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Masyarakat pun secara Serius mengikuti acara tersebut, terlihat begitu semangat mengikuti sesi demi sesi latihan.</p>\r\n\r\n<p>Kami sangat berterimakasih kepada Petugas Pemadam Kebakaran dimana kami sudah diberi pengetahuan tentang cara cara penanggulangan kebakaran secara dini, ini sangat penting sekali bagi masyarakat sehingga kami jadi tahu dan paham apa saja tindakan kami saat terjadi kebakaran, kata sala satu warga yg menjadi peserta simulasi tersebut.</p>\r\n', 1, '1443598639_simulasi.jpg', '', 0, '2015-09-30 14:37:19'),
(13, 'REKAPITULASI DISTRIBUSI AIR BERSIH BPBD KAB TANGERANG', '<p>Terhitung 33 tengki /157.500 sudah di distribusikan ke 4600 KK di wilayah pantura Kabupaten Tangerang. Bantuan air bersih ini di distribusikan dengan menggunakan&nbsp;</p>\r\n\r\n<p>1. Truk Tangki air dari BPBD Prov.Banten , 9 tangki / 45.000 Liter</p>\r\n\r\n<p>2. BPBD Kabupaten Tangerang, 13 Tangki / 65.000 Liter</p>\r\n\r\n<p>3. UPT Pemadam Kebakaran Dan &nbsp;Penanggulangan Bencana Kab.Tangerang 11 Tangki / 4750 Liter</p>\r\n\r\n<p>Sementa Jumlah Daerah yang terkena dampak &nbsp;kekeringan sebanyak 24 Kecamatan dari 29 Kecamatan di kabupaten Tangerang.</p>\r\n', 1, '1443598750_REKAPITULASI.jpg', '', 0, '2015-09-30 14:39:10'),
(14, 'BANTUAN AIR BERSIH BPBD KAB TANGERANG', '<p>i tahun 2015 kekeringan di Kabupaten Tangerang terjadi di beberapa wilayah . di antaranya di Kecamatan Kresek, Kec, Mekar baru, Kec Kronjo, Kec. Suka Mulya ,Kec. Gunung Kaler, Kec. Legok.Dampak dari kekeringan ini warga kesulitan untuk mendapatkan Air bersih ,tidak berfungsinya MCK dan kekeringan lahan pertanian.</p>\r\n\r\n<p>&nbsp; Badan Penanggulangan Bencana Daerah (BPBD) Kab.Tangerang Telah memberikan Bantuan Air Bersih Di wilayah Kecamatan Legok yaitu di Kp. Cirarab, Kp. Cipari Desa Cirarab.(Selasa,4-8-2015).Kp.Bojong Kamal, Kp Pulo (Rabu,5-8-2015) Desa Bojong Kamal.</p>\r\n\r\n<p>&nbsp; Mereka (Warga) sangat Membutuhkan Air Bersih karena sumber Air yang ada sudah kering, sungai kering, dan Lahan Pertanian kering.</p>\r\n', 1, '1443598855_BANTUAN.jpg', '', 1, '2015-09-30 14:40:55');

-- --------------------------------------------------------

--
-- Struktur dari tabel `post_category`
--

CREATE TABLE IF NOT EXISTS `post_category` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `post_category`
--

INSERT INTO `post_category` (`id`, `parent_id`, `title`) VALUES
(1, 0, 'Berita'),
(2, 0, 'Info Bencana'),
(3, 0, 'Grafik');

-- --------------------------------------------------------

--
-- Struktur dari tabel `post_category_map`
--

CREATE TABLE IF NOT EXISTS `post_category_map` (
  `id` int(11) NOT NULL,
  `post_id` int(11) DEFAULT NULL,
  `post_category_id` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `post_category_map`
--

INSERT INTO `post_category_map` (`id`, `post_id`, `post_category_id`, `active`) VALUES
(1, 1, 1, 1),
(2, 2, 2, 1),
(5, 5, 1, 1),
(6, 6, 1, 1),
(8, 8, 3, 1),
(9, 9, 1, 1),
(10, 10, 1, 1),
(11, 11, 1, 1),
(12, 12, 1, 1),
(13, 13, 1, 1),
(14, 14, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `role`
--

INSERT INTO `role` (`id`, `nama`) VALUES
(1, 'Admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_akses`
--

CREATE TABLE IF NOT EXISTS `role_akses` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `akses_id` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=88 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `role_akses`
--

INSERT INTO `role_akses` (`id`, `role_id`, `akses_id`, `status`) VALUES
(1, 2, 1, 0),
(3, 1, 1, 1),
(4, 1, 2, 1),
(5, 1, 4, 1),
(6, 1, 3, 1),
(7, 1, 5, 1),
(8, 1, 6, 1),
(9, 1, 8, 1),
(10, 1, 7, 1),
(11, 1, 9, 1),
(12, 1, 10, 1),
(13, 1, 11, 1),
(14, 1, 13, 1),
(15, 1, 12, 1),
(16, 1, 14, 1),
(17, 1, 25, 1),
(18, 1, 26, 1),
(19, 1, 28, 1),
(20, 1, 27, 1),
(21, 1, 29, 1),
(22, 1, 15, 1),
(23, 1, 16, 1),
(24, 1, 18, 1),
(25, 1, 17, 1),
(26, 1, 19, 1),
(27, 1, 35, 1),
(28, 1, 36, 1),
(29, 1, 38, 1),
(30, 1, 37, 1),
(31, 1, 39, 1),
(32, 1, 20, 1),
(33, 1, 21, 1),
(34, 1, 23, 1),
(35, 1, 22, 1),
(36, 1, 24, 1),
(37, 1, 30, 1),
(38, 1, 31, 1),
(39, 1, 33, 1),
(40, 1, 32, 1),
(41, 1, 34, 1),
(42, 2, 10, 1),
(43, 2, 11, 1),
(44, 2, 14, 1),
(45, 2, 25, 1),
(46, 2, 26, 1),
(47, 2, 28, 1),
(48, 2, 27, 1),
(49, 2, 29, 1),
(50, 2, 15, 1),
(51, 2, 16, 1),
(52, 2, 19, 1),
(53, 2, 20, 1),
(54, 2, 21, 1),
(55, 2, 23, 1),
(56, 2, 22, 1),
(57, 2, 24, 1),
(58, 2, 40, 1),
(59, 2, 41, 1),
(60, 2, 43, 1),
(61, 2, 42, 1),
(62, 2, 44, 1),
(63, 3, 5, 1),
(64, 3, 6, 1),
(65, 3, 8, 1),
(66, 3, 7, 1),
(67, 3, 9, 1),
(68, 3, 10, 1),
(69, 3, 11, 1),
(70, 3, 13, 1),
(71, 3, 12, 1),
(72, 3, 14, 1),
(73, 3, 15, 1),
(74, 3, 16, 1),
(75, 3, 18, 1),
(76, 3, 17, 1),
(77, 3, 19, 1),
(78, 3, 40, 1),
(79, 3, 41, 1),
(80, 3, 43, 1),
(81, 3, 42, 1),
(82, 3, 44, 1),
(83, 3, 20, 1),
(84, 3, 21, 1),
(85, 3, 23, 1),
(86, 3, 22, 1),
(87, 3, 24, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `slide`
--

CREATE TABLE IF NOT EXISTS `slide` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `time_created` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `slide`
--

INSERT INTO `slide` (`id`, `title`, `file`, `time_created`) VALUES
(2, 'Slide 2', '1442886184_slide1.jpg', '2014-05-11 13:07:18'),
(8, 'Slide 3', '1442886301_slide2.jpg', '2015-09-07 14:41:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `theme`
--

CREATE TABLE IF NOT EXISTS `theme` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `theme`
--

INSERT INTO `theme` (`id`, `title`) VALUES
(1, 'Default');

-- --------------------------------------------------------

--
-- Struktur dari tabel `theme_attribute`
--

CREATE TABLE IF NOT EXISTS `theme_attribute` (
  `id` int(11) NOT NULL,
  `theme_id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` text
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `theme_attribute`
--

INSERT INTO `theme_attribute` (`id`, `theme_id`, `key`, `value`) VALUES
(1, 1, 'header_height', '190px'),
(2, 1, 'logo_file', '1384513966_jangkar-asam-70.png'),
(3, 1, 'logo_margin_left', '50px'),
(4, 1, 'logo_margin_top', '20px'),
(5, 1, 'logo_margin_right', ''),
(6, 1, 'logo_margin_bottom', ''),
(7, 1, 'background_color', '#2967D8'),
(8, 1, 'background_repeat', 'repeat-x'),
(9, 1, 'background_position', 'center top'),
(10, 1, 'background_attachment', 'scroll'),
(11, 1, 'header_background_color', ''),
(12, 1, 'header_background_repeat', '0'),
(13, 1, 'header_background_position', 'left top'),
(14, 1, 'header_background_attachment', 'scroll'),
(16, 1, 'upload_file_type', 'background'),
(15, 1, 'background_image', '1384513837_headerbg.png'),
(17, 1, 'header_background_image', ''),
(24, 1, 'box_header_background_image', NULL),
(18, 1, 'mainmenu_background_color', '#cccccc'),
(19, 1, 'mainmenu_background_repeat', 'no-repeat'),
(20, 1, 'mainmenu_background_position', 'center top'),
(21, 1, 'mainmenu_background_attachment', 'scroll'),
(22, 1, 'mainmenu_background_image', ''),
(23, 1, 'box_background_image', ''),
(25, 1, 'box_header_background_color', '#cccccc'),
(26, 1, 'box_content_background_image', NULL),
(27, 1, 'box_content_background_color', '#efefef'),
(28, 1, 'mainmenu_link_color', '#00000'),
(29, 1, 'mainmenu_link_color_hover', '#666666'),
(30, 1, 'mainmenu_background_color_hover', '#efefef'),
(31, 1, 'mainmenu_link_background_color_hover', '#efefef'),
(32, 1, 'running_font_color', '#000000'),
(33, 1, 'running_background_color', '#efefef');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `role_id`) VALUES
(1, 'admin', 'bisabisa', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_attribute`
--

CREATE TABLE IF NOT EXISTS `user_attribute` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_attribute`
--

INSERT INTO `user_attribute` (`id`, `user_id`, `key`, `value`) VALUES
(1, 2, 'kategori_id', '2'),
(2, 5, 'kategori_id', '1'),
(3, 6, 'kategori_id', '1'),
(4, 7, 'kategori_id', '1'),
(5, 8, 'kategori_id', '1'),
(6, 9, 'kategori_id', '1'),
(7, 10, 'kategori_id', '1'),
(8, 1, 'kategori_id', '1'),
(9, 11, 'kategori_id', '1'),
(10, 4, 'kategori_id', '1'),
(11, 12, 'kategori_id', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `validasi`
--

CREATE TABLE IF NOT EXISTS `validasi` (
  `id` int(11) NOT NULL,
  `kategori_validasi_id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `validasi`
--

INSERT INTO `validasi` (`id`, `kategori_validasi_id`, `nama`) VALUES
(1, 1, 'Baru'),
(2, 1, 'Modifikasi'),
(3, 2, 'Output'),
(4, 2, 'Perbaikan kondisi setelah inovasi'),
(5, 2, 'Terukur'),
(6, 3, 'Masuk dalam keputusan formal'),
(7, 3, 'Masuk dalam perencanaan'),
(8, 3, 'Evaluasi berkala'),
(9, 3, 'Alokasi sumberdaya dan anggaran'),
(10, 4, 'Dapat dikembangkan lebih lanjut'),
(11, 4, 'Potensi untuk diimplementasikan di tempat lain');

-- --------------------------------------------------------

--
-- Struktur dari tabel `video`
--

CREATE TABLE IF NOT EXISTS `video` (
  `id` int(11) NOT NULL,
  `video_category_id` int(11) DEFAULT NULL,
  `video_type_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `file` varchar(255) DEFAULT NULL,
  `youtube_url` varchar(255) DEFAULT NULL,
  `mp4_file` varchar(255) DEFAULT NULL,
  `ogv_file` varchar(255) DEFAULT NULL,
  `webm_file` varchar(255) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `time_created` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `video`
--

INSERT INTO `video` (`id`, `video_category_id`, `video_type_id`, `title`, `description`, `file`, `youtube_url`, `mp4_file`, `ogv_file`, `webm_file`, `model`, `model_id`, `time_created`) VALUES
(7, 1, 1, 'Pemberlakuan Standar Kompetensi PNS Masih Temui Banyak Tantangan', '<p>https://www.youtube.com/watch?v=l65-AD-GQTY</p>\r\n', NULL, 'https://www.youtube.com/watch?v=l65-AD-GQTY', '', '', '', '', NULL, '2015-08-10 19:53:15'),
(6, 1, 1, 'Seminar Proyek Perubahan Dr. Septiana Dwi Putrianti', '<p>https://www.youtube.com/watch?v=cfffslGkxOk</p>\r\n', NULL, 'https://www.youtube.com/watch?v=cfffslGkxOk', '', '', '', '', NULL, '2015-08-22 02:45:06');

-- --------------------------------------------------------

--
-- Struktur dari tabel `video_attribute`
--

CREATE TABLE IF NOT EXISTS `video_attribute` (
  `id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` text
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `video_attribute`
--

INSERT INTO `video_attribute` (`id`, `video_id`, `key`, `value`) VALUES
(1, 1, 'video_youtube_url', 'http://www.youtube.com/watch?v=A8a0dJAaVfE'),
(2, 1, 'video_upload_youtube_height', '20px'),
(3, 1, 'video_upload_youtube_width', '50px'),
(4, 1, 'video_youtube_height', '20px'),
(5, 1, 'video_youtube_width', '50px'),
(6, 1, 'youtube_url', 'http://www.youtube.com/watch?v=A8a0dJAaVfE'),
(7, 1, 'youtube_height', '250px'),
(8, 1, 'youtube_width', '200px'),
(9, 2, 'youtube_url', 'https://www.youtube.com/watch?v=PQtYfVkvMr4'),
(10, 2, 'youtube_height', ''),
(11, 2, 'youtube_width', ''),
(12, 3, 'youtube_url', 'https://www.youtube.com/watch?v=MV_euZ_lKH8'),
(13, 3, 'youtube_height', ''),
(14, 3, 'youtube_width', ''),
(15, 4, 'youtube_url', 'https://www.youtube.com/watch?v=5Nt8unATZ-8'),
(16, 4, 'youtube_height', ''),
(17, 4, 'youtube_width', ''),
(18, 5, 'youtube_url', 'https://www.youtube.com/watch?v=Lh0_MrxX1FA'),
(19, 5, 'youtube_height', ''),
(20, 5, 'youtube_width', ''),
(21, 6, 'youtube_url', ''),
(22, 6, 'youtube_height', ''),
(23, 6, 'youtube_width', ''),
(24, 7, 'youtube_url', ''),
(25, 7, 'youtube_height', ''),
(26, 7, 'youtube_width', ''),
(27, 8, 'youtube_url', ''),
(28, 8, 'youtube_height', ''),
(29, 8, 'youtube_width', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `video_category`
--

CREATE TABLE IF NOT EXISTS `video_category` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `video_category`
--

INSERT INTO `video_category` (`id`, `title`) VALUES
(1, 'Kategori A'),
(2, 'tes');

-- --------------------------------------------------------

--
-- Struktur dari tabel `video_type`
--

CREATE TABLE IF NOT EXISTS `video_type` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `video_type`
--

INSERT INTO `video_type` (`id`, `title`) VALUES
(1, 'Youtube'),
(2, 'Upload Video');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `active`
--
ALTER TABLE `active`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bencana`
--
ALTER TABLE `bencana`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bencana_desa`
--
ALTER TABLE `bencana_desa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bencana_jenis`
--
ALTER TABLE `bencana_jenis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bencana_kecamatan`
--
ALTER TABLE `bencana_kecamatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `block`
--
ALTER TABLE `block`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `block_attribute`
--
ALTER TABLE `block_attribute`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `block_position`
--
ALTER TABLE `block_position`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `block_status`
--
ALTER TABLE `block_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `block_type`
--
ALTER TABLE `block_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `download`
--
ALTER TABLE `download`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `download_access`
--
ALTER TABLE `download_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_category`
--
ALTER TABLE `event_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `link`
--
ALTER TABLE `link`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `link_category`
--
ALTER TABLE `link_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_attribute`
--
ALTER TABLE `menu_attribute`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_item`
--
ALTER TABLE `menu_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_item_attribute`
--
ALTER TABLE `menu_item_attribute`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_item_type`
--
ALTER TABLE `menu_item_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photo`
--
ALTER TABLE `photo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photo_album`
--
ALTER TABLE `photo_album`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photo_category`
--
ALTER TABLE `photo_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_category`
--
ALTER TABLE `post_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_category_map`
--
ALTER TABLE `post_category_map`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_akses`
--
ALTER TABLE `role_akses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `theme`
--
ALTER TABLE `theme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `theme_attribute`
--
ALTER TABLE `theme_attribute`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_attribute`
--
ALTER TABLE `user_attribute`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `validasi`
--
ALTER TABLE `validasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video_attribute`
--
ALTER TABLE `video_attribute`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video_category`
--
ALTER TABLE `video_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video_type`
--
ALTER TABLE `video_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `active`
--
ALTER TABLE `active`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `bencana`
--
ALTER TABLE `bencana`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `bencana_desa`
--
ALTER TABLE `bencana_desa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT for table `bencana_jenis`
--
ALTER TABLE `bencana_jenis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `bencana_kecamatan`
--
ALTER TABLE `bencana_kecamatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `block`
--
ALTER TABLE `block`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `block_attribute`
--
ALTER TABLE `block_attribute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=291;
--
-- AUTO_INCREMENT for table `block_status`
--
ALTER TABLE `block_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `download`
--
ALTER TABLE `download`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `download_access`
--
ALTER TABLE `download_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `event_category`
--
ALTER TABLE `event_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `link`
--
ALTER TABLE `link`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `link_category`
--
ALTER TABLE `link_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `menu_attribute`
--
ALTER TABLE `menu_attribute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menu_item`
--
ALTER TABLE `menu_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `menu_item_attribute`
--
ALTER TABLE `menu_item_attribute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=284;
--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `photo`
--
ALTER TABLE `photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `photo_album`
--
ALTER TABLE `photo_album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `photo_category`
--
ALTER TABLE `photo_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `post_category`
--
ALTER TABLE `post_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `post_category_map`
--
ALTER TABLE `post_category_map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `role_akses`
--
ALTER TABLE `role_akses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `slide`
--
ALTER TABLE `slide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `theme`
--
ALTER TABLE `theme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `theme_attribute`
--
ALTER TABLE `theme_attribute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user_attribute`
--
ALTER TABLE `user_attribute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `validasi`
--
ALTER TABLE `validasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `video_attribute`
--
ALTER TABLE `video_attribute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `video_category`
--
ALTER TABLE `video_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `video_type`
--
ALTER TABLE `video_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
