<?php



class VideoController extends Controller

{

	/**

	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning

	* using two-column layout. See 'protected/views/layouts/column2.php'.

	*/

	public $layout='//layouts/admin/column2';



	/**

	* @return array action filters

	*/

	public function filters()

	{

		return array(

			'accessControl', // perform access control for CRUD operations

		);

	}



	/**

	* Specifies the access control rules.

	* This method is used by the 'accessControl' filter.

	* @return array access control rules

	*/

	public function accessRules()

	{

		return array(

			array('allow',  // allow all users to perform 'index' and 'view' actions

				'actions'=>array('detail','index'),

				'users'=>array('*'),

			),

			array('allow', // allow authenticated user to perform 'create' and 'update' actions

				'actions'=>array('create','update','view'),

				'users'=>array('@'),

			),

			array('allow', // allow admin user to perform 'admin' and 'delete' actions

				'actions'=>array('admin','delete','hapus'),

				'users'=>array('@'),

				//'expression'=>'Yii::app()->user->isAdmin()',

			),

			array('deny',  // deny all users

				'users'=>array('*'),

			),

		);

	}



	/**

	* Displays a particular model.

	* @param integer $id the ID of the model to be displayed

	*/



	public function actionDetail($id)

	{

		$this->layout = '//layouts/column2';

		

		$this->render('detail',array(

			'model'=>$this->loadModel($id),

		));

	}

	

	public function actionView($id)

	{	

		$this->render('view',array(

			'model'=>$this->loadModel($id),

		));

	}



/**

* Creates a new model.

* If creation is successful, the browser will be redirected to the 'view' page.

*/

	public function actionCreate()

	{

		

		$model=new Video;



		// Uncomment the following line if AJAX validation is needed

		// $this->performAjaxValidation($model);

		

		if(isset($_GET['model']))

			$model->model = $_GET['model'];

			

		if(isset($_GET['model_id']))

			$model->model_id = $_GET['model_id'];



		if(isset($_POST['Video']))

		{

			$model->attributes=$_POST['Video'];

			
			if(empty($model->time_created))
			{
				date_default_timezone_set('Asia/Jakarta');

				$model->time_created = date('Y-m-d H:i:s');
			}



			

			if($model->save())

			{

				

				Yii::app()->user->setFlash('success','Data berhasil disimpan');

				

				if($model->model=='Post')

					$this->redirect(array('post/view','id'=>$model->model_id));

					

				if($model->model=='Page')

					$this->redirect(array('page/view','id'=>$model->model_id));

					

				$this->redirect(array('view','id'=>$model->id));

			}

		}



		$this->render('create',array(

			'model'=>$model,

		));

	}



/**

* Updates a particular model.

* If update is successful, the browser will be redirected to the 'view' page.

* @param integer $id the ID of the model to be updated

*/

	public function actionUpdate($id)

	{

		$model=$this->loadModel($id);



		// Uncomment the following line if AJAX validation is needed

		// $this->performAjaxValidation($model);



		if(isset($_POST['Video']))

		{

			$model->attributes=$_POST['Video'];

			

			if($model->save())

			{

				if(!empty($_POST['VideoAttribute']))

					$model->saveVideoAttribute($_POST['VideoAttribute']);

				

				Yii::app()->user->setFlash('success','Data berhasil disimpan');

				

				if($model->model=='Post')

					$this->redirect(array('post/view','id'=>$model->model_id));

					

				if($model->model=='Page')

					$this->redirect(array('page/view','id'=>$model->model_id));

				

				$this->redirect(array('view','id'=>$model->id));

			}

		}

		

		



		$this->render('update',array(

			'model'=>$model,

		));

	}



/**

* Deletes a particular model.

* If deletion is successful, the browser will be redirected to the 'admin' page.

* @param integer $id the ID of the model to be deleted

*/

public function actionDelete($id)

{

if(Yii::app()->request->isPostRequest)

{

// we only allow deletion via POST request

$this->loadModel($id)->delete();



// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser

if(!isset($_GET['ajax']))

$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));

}

else

throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');

}



/**

* Lists all models.

*/

	public function actionIndex()

	{

		$this->layout = '//layouts/column2';

		

		$criteria = new CDbCriteria;

		$criteria->order = 'time_created DESC';

		

		if(isset($_GET['id']))

			$criteria->compare('video_category_id',$_GET['id']);

		

		$dataProvider=new CActiveDataProvider('Video',array(

			'criteria'=>$criteria,

			'pagination'=>array(

				'pageSize'=>5,

			),

			

		));

		

		$this->render('index',array(

			'dataProvider'=>$dataProvider,

		));

	}



/**

* Manages all models.

*/

	public function actionAdmin()

	{

		$model=new Video('search');

		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['Video']))

			$model->attributes=$_GET['Video'];



		$this->render('admin',array(

			'model'=>$model,

		));

	}

	

	public function actionHapus($id)

	{

		$video = $this->loadModel($id);

		$model = $video->model;

		$model_id = $video->model_id;

		$file = $video->file;

		

		$video->delete();

		

		Yii::app()->user->setFlash('success','Data berhasil dihapus');

				

		if($model == 'Post')

			$this->redirect(array('post/view','id'=>$model_id));

				

		if($model == 'Page')

			$this->redirect(array('page/view','id'=>$model_id));

		

	}



/**

* Returns the data model based on the primary key given in the GET variable.

* If the data model is not found, an HTTP exception will be raised.

* @param integer the ID of the model to be loaded

*/

public function loadModel($id)

{

$model=Video::model()->findByPk($id);

if($model===null)

throw new CHttpException(404,'The requested page does not exist.');

return $model;

}



/**

* Performs the AJAX validation.

* @param CModel the model to be validated

*/

protected function performAjaxValidation($model)

{

if(isset($_POST['ajax']) && $_POST['ajax']==='video-form')

{

echo CActiveForm::validate($model);

Yii::app()->end();

}

}

}

