<?php

class ThemeController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
	public $layout='//layouts/admin/column2';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),		
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','updateAttribute','updateLogo'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'expression'=>'*',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionUpdateAttribute($id)
	{
		$model = $this->loadModel($id);
		
		if($_POST['ThemeAttribute']['upload_file_type']=='logo')
		{
			$file = CUploadedFile::getInstanceByName('ThemeAttribute[logo_file]');
			$filename = $model->getAttr('logo_file');
		}
		
		if($_POST['ThemeAttribute']['upload_file_type']=='background')
		{
			$file = CUploadedFile::getInstanceByName('ThemeAttribute[background_image]');
			$filename = $model->getAttr('background_image');
		}
		
		if($_POST['ThemeAttribute']['upload_file_type']=='header')
		{
			$file = CUploadedFile::getInstanceByName('ThemeAttribute[header_background_image]');
			$filename = $model->getAttr('header_background_image');
		}
		
		if($_POST['ThemeAttribute']['upload_file_type']=='mainmenu')
		{
			$file = CUploadedFile::getInstanceByName('ThemeAttribute[mainmenu_background_image]');
			$filename = $model->getAttr('mainmenu_background_image');
		}
		
		if($_POST['ThemeAttribute']['upload_file_type']=='box_header')
		{
			$file = CUploadedFile::getInstanceByName('ThemeAttribute[box_header_background_image]');
			$filename = $model->getAttr('box_header_background_image');
		}
		
		if($_POST['ThemeAttribute']['upload_file_type']=='box_content')
		{
			$file = CUploadedFile::getInstanceByName('ThemeAttribute[box_content_background_image]');
			$filename = $model->getAttr('box_content_background_image');
		}
		
		
		if($_POST['ThemeAttribute']['upload_file_type']=='box')
		{
			$file = CUploadedFile::getInstanceByName('ThemeAttribute[box_background_image]');
			$filename = $model->getAttr('box_background_image');
		}
		
		$path = Yii::app()->basePath.'/../uploads/theme/';
		$old_filename = $filename;
		
		if($file!==null)
		{	
			$path = Yii::app()->basePath.'/../uploads/theme/';
			$filename = str_replace(' ','-',time().'_'.$file->name);
			$file->saveAs($path.$filename);
			
			if($old_filename!='' AND file_exists($path.$old_filename))
				unlink($path.$old_filename);
		}
		
		if($_POST['clear_image']=='1')
		{
			if($old_filename!='' AND file_exists($path.$old_filename))
				unlink($path.$old_filename);
			$filename = '';
		}
		
		if(!empty($_POST['ThemeAttribute']))
		{
			$model->saveThemeAttribute($_POST['ThemeAttribute']);
		}
		
		if($_POST['ThemeAttribute']['upload_file_type']=='logo')
		{
			$model->saveThemeAttributeByKey('logo_file',$filename);
		}
		
		if($_POST['ThemeAttribute']['upload_file_type']=='background')
		{
			$model->saveThemeAttributeByKey('background_image',$filename);
		}
		
		if($_POST['ThemeAttribute']['upload_file_type']=='header')
		{
			$model->saveThemeAttributeByKey('header_background_image',$filename);
		}
		
		if($_POST['ThemeAttribute']['upload_file_type']=='mainmenu')
		{
			$model->saveThemeAttributeByKey('mainmenu_background_image',$filename);
		}
		
		if($_POST['ThemeAttribute']['upload_file_type']=='box_header')
		{
			$model->saveThemeAttributeByKey('box_header_background_image',$filename);
		}
		
		if($_POST['ThemeAttribute']['upload_file_type']=='box_content')
		{
			$model->saveThemeAttributeByKey('box_content_background_image',$filename);
		}
		
		
		if($_POST['ThemeAttribute']['upload_file_type']=='box')
		{
			$model->saveThemeAttributeByKey('box_background_image',$filename);
		}
		
		Yii::app()->user->setFlash('success','Data berhasil disimpan');
		$this->redirect(array('theme/view','id'=>$id));
	}
	
	public function actionUpdateLogo($id)
	{
		$model = $this->loadModel($id);
			
		$file = CUploadedFile::getInstanceByName('ThemeAttribute[logo_file]');
			
		if($file!==null)
		{
			$path = Yii::app()->basePath.'/../uploads/theme/';
			$logo_file = str_replace(' ','-',time().'_'.$file->name);
			
			$file->saveAs($path.$logo_file);
		} else {
			$logo_file = $model->getAttr('logo_file');
		}
		
		if(!empty($_POST['ThemeAttribute']))
			$model->saveThemeAttribute($_POST['ThemeAttribute']);
		
		$model->saveThemeAttributeByKey('logo_file',$logo_file);
		
		Yii::app()->user->setFlash('success','Data berhasil disimpan');
		$this->redirect(array('theme/view','id'=>$id));
	}
	
	public function actionUpdateBackground($id)
	{
		$model = $this->loadModel($id);
			
		$file = CUploadedFile::getInstanceByName('ThemeAttribute[background_image]');
			
		if($file!==null)
		{
			$path = Yii::app()->basePath.'/../uploads/theme/';
			$logo_file = str_replace(' ','-',time().'_'.$file->name);
			
			$file->saveAs($path.$logo_file);
		} else {
			$background_image = $model->getAttr('background_image');
		}
		
		if(!empty($_POST['ThemeAttribute']))
			$model->saveThemeAttribute($_POST['ThemeAttribute']);
		
		$model->saveThemeAttributeByKey('background_image',$background_image);
		
		Yii::app()->user->setFlash('success','Data berhasil disimpan');
		$this->redirect(array('theme/view','id'=>$id));
	}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
public function actionCreate()
{
$model=new Theme;

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Theme']))
{
$model->attributes=$_POST['Theme'];
if($model->save())
$this->redirect(array('view','id'=>$model->id));
}

$this->render('create',array(
'model'=>$model,
));
}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
$model=$this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Theme']))
{
$model->attributes=$_POST['Theme'];
if($model->save())
$this->redirect(array('view','id'=>$model->id));
}

$this->render('update',array(
'model'=>$model,
));
}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Theme');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
	public function actionAdmin()
	{
		$model=new Theme('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Theme']))
			$model->attributes=$_GET['Theme'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}	

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Theme::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='theme-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
