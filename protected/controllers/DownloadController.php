<?php

class DownloadController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
	public $layout='//layouts/column2';

	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
	public function accessRules()
	{		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','getFile','loginFirst'),
				'users'=>array('*'),
			),			
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','hapus'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionLoginFirst()
	{
		$this->layout = '//layouts/column1';
		
		$this->render('loginFirst');
	}
	
	public function actionGetFile($id)
	{
		$model = $this->loadModel($id);
		
		if($model->download_access_id == 1)
		{
			$model->total_download++;
			$model->save();
			
			$this->redirect(Yii::app()->request->baseUrl.'/uploads/download/'.$model->file);
		}
		
		if($model->download_access_id == 2)
		{
			if(Yii::app()->user->getState('member')==1)
			{
				$model->total_download++;
				$model->save();
				
				$this->redirect(Yii::app()->request->baseUrl.'/uploads/download/'.$model->file);
			} else {
				$this->redirect(array('download/loginFirst'));
			}
		}
		
	}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
	public function actionCreate()
	{
		$model=new Download;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Download']))
		{
			$model->attributes=$_POST['Download'];
			
			$file = CUploadedFile::getInstance($model,'file');
			
			if($file!==null)
			{
				$model->file = str_replace(' ','-',time().'_'.$file->name);
			}			
		
			if($model->save())
			{
				if($file!==null)
				{
					$path = Yii::app()->basePath.'/../uploads/download/';
					$file->saveAs($path.$model->file);
				}
				
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				
				if($model->model == 'Post')
					$this->redirect(array('post/view','id'=>$model->model_id));
					
				if($model->model == 'Page')
					$this->redirect(array('page/view','id'=>$model->model_id));
				
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
		$oldFile = $model->file;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Download']))
		{
			$model->attributes=$_POST['Download'];
			
			$file = CUploadedFile::getInstance($model,'file');
			
			if($file!==null)
				$model->file = str_replace(' ','-',time().'_'.$file->name);
			else
				$model->file = $oldFile;
				
				
			if($model->save())
			{
				if($file!==null)
				{
					$path = Yii::app()->basePath.'/../uploads/download/';
					$file->saveAs($path.$model->file);
				
					if(file_exists($path.$oldFile) AND $oldFile!='')
						unlink($path.$oldFile);
				}
				
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				
				if($model->model == 'Post')
					$this->redirect(array('post/view','id'=>$model->model_id));
				
				if($model->model == 'Page')
					$this->redirect(array('page/view','id'=>$model->model_id));
				
				$this->redirect(array('view','id'=>$model->id));
			}
		}

$this->render('update',array(
'model'=>$model,
));
}

	/**
	* Deletes a particular model.
	* If deletion is successful, the browser will be redirected to the 'admin' page.
	* @param integer $id the ID of the model to be deleted
	*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
	
	public function actionHapus($id)
	{
		$download = $this->loadModel($id);
		$model = $download->model;
		$model_id = $download->model_id;
		$file = $download->file;
		
		if($download->delete())
		{
			$path = Yii::app()->basePath.'/../uploads/download/';	
			
			if(file_exists($path.$file) AND $file!='')
				unlink($path.$file);
		}
		
		Yii::app()->user->setFlash('success','Data berhasil dihapus');
				
		if($model == 'Post')
			$this->redirect(array('post/view','id'=>$model_id));
				
		if($model == 'Page')
			$this->redirect(array('page/view','id'=>$model_id));
		
	}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Download');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
	public function actionAdmin()
	{
		$this->layout = '//layouts/admin/column2';
		
		$model=new Download('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Download']))
			$model->attributes=$_GET['Download'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Download::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='download-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
