<?php

class PostCategoryController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
	public $layout='//layouts/admin/column2';

/**
* @return array action filters
*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('read'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','view','index','chartByPostCategory'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	
	public function actionRead($id)
	{
		$this->layout = '//layouts/column2';
		$model = $this->loadModel($id);
		
		$criteria=Yii::app()->advancedFilters->createCriteria();
		$postCategory=PostCategory::model()->findAllByAttributes(array('parent_id'=>$id));
		$list = '"'.$id.'"';
		foreach($postCategory as $category)
		{
			$list .= '|"'.$category->id.'"|';
		}
		$criteria->addAdvancedFilterCondition('post_category_id',$list);
		$criteria->addAdvancedFilterCondition('active',1);
		$criteria->group = 'post_id ASC';
		
		$dataProvider=new CActiveDataProvider('PostCategoryMap',array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>5,
			),
		));
		$this->render('read',array(
			'dataProvider'=>$dataProvider,
			'model'=>$model,
		));
	}

	public function actionView($id)
	{	
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}


/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
	public function actionCreate()
	{
		$model=new PostCategory;

		if(isset($_GET['parent_id']))
			$model->parent_id = $_GET['parent_id'];

		if(isset($_POST['PostCategory']))
		{
			$model->attributes=$_POST['PostCategory'];
			if($model->save())
			{
				Yii::app()->user->setFlash('success','Data berhasil ditambahkan');
				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PostCategory']))
		{
			$model->attributes=$_POST['PostCategory'];
			if($model->save())
			{
				$model->updatePostCategoryMap();
				Yii::app()->user->setFlash('success','Data berhasil diperbarui');
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$model = $this->loadModel($id);
			
			if($model->hasSubcategory())
			{
				Yii::app()->user->setFlash('error','GAGAL: Kategori yang memiliki subkategori tidak dapat dihapus. Silahkan hapus subkategori terlebih dahulu.');
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
			
			if($model->hasPost())
			{
				Yii::app()->user->setFlash('error','GAGAL: Kategori yang memiliki post tidak dapat dihapus. Silahkan pindahkan post ke kategori yang lain terlebih dahulu.');
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
			{
				$model->delete();
				Yii::app()->user->setFlash('success','Data berhasil dihapus');
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
			}
		} else {
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
		}
	}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('PostCategory');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

	/**
	* Manages all models.
	*/
	
	public function actionAdmin()
	{
		$model=new PostCategory('search');
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['PostCategory']))
			$model->attributes=$_GET['PostCategory'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function actionChartByPostCategory()
	{
		Yii::app()->fusioncharts->setChartOptions(array(
			'caption'=>'Jumlah Post Berdasarkan Kategori', 
		));
		
		foreach(PostCategory::model()->findAll() as $data) 
		{
			Yii::app()->fusioncharts->addSet(array('label'=>$data->title, 'value'=>$data->countPost()));
		}
		
		Yii::app()->fusioncharts->getXMLData(true);
	}
	
	

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=PostCategory::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='post-category-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
