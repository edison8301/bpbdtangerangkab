<?php

class PostController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/admin/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('read'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','create','update','view','chartByPostCategory',
					'removeThumbnail'
				),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
				//'expression'=>'Yii::app()->user->isAdmin()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	
	public function actionRead($id)
	{
		$this->layout = '//layouts/column2';
		
		$model = $this->loadModel($id);
		
		$model->total_views++;
		$model->save();
		
		$this->render('read',array(
			'model'=>$model,
		));
	}
	
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Post;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Post']))
		{
			$model->attributes=$_POST['Post'];
			
			$image = CUploadedFile::getInstance($model,'image');
			
			if($image!==null)
				$model->image = str_replace(' ','-',time().'_'.$image->name);
			
			date_default_timezone_set('Asia/Jakarta');
			$model->time_created = date('Y-m-d H:i:s');
			
			if($model->save())
			{
				$model->updatePostCategoryMap();
				if($image!==null)
				{
					$path = Yii::app()->theme->basePath.'/uploads/post/';
					$image->saveAs($path.$model->image);
				}
				
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	public function savePostCategory($model,$post_category_id=null)
	{
		if($post_category_id == null)
			$parent = PostCategory::model()->findByPk($model->post_category_id)->parent_id;
		else
			$parent = PostCategory::model()->findByPk($post_category_id)->parent_id;
		
		if($post_category_id == null)
			$cek = PostCategoryMap::model()->findByAttributes(array('post_id'=>$model->id,'post_category_id'=>$model->post_category_id));
		else
			$cek = PostCategoryMap::model()->findByAttributes(array('post_id'=>$model->id,'post_category_id'=>$post_category_id));
		
		if($cek === null)
			$postCategoryMap = new PostCategoryMap;
		else
			$postCategoryMap = $cek;
		
		$postCategoryMap->post_id = $model->id;
		if($post_category_id == null)
			$postCategoryMap->post_category_id = $model->post_category_id;
		else
			$postCategoryMap->post_category_id = $post_category_id;
		$postCategoryMap->active = 1;
		$postCategoryMap->save();
			
		if($parent != 0)
		{
			$postCategory = PostCategory::model()->findByPk($parent);
			$this->savePostCategory($model,$postCategory->id);
		}
	}
	
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
		$old_image = $model->image;
	
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Post']))
		{
			
			$model->attributes=$_POST['Post'];
			
			$image = CUploadedFile::getInstance($model,'image');
			
			if(is_object($image))
			{
				$model->image = str_replace(' ','-',time().'_'.$image->name);
				
			} else {
				$model->image = $old_image;
			}
		
			if($model->save())
			{
				$model->updatePostCategoryMap();
				if($image!==null)
				{
					$path = Yii::app()->theme->basePath.'/uploads/post/';
					$image->saveAs($path.$model->image);
					
					if(file_exists($path.$old_image) AND $old_image!='')
						unlink($path.$old_image);
				}
				
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	public function saveUpdateCategory($model)
	{
		$postCategory = PostCategoryMap::model()->findAllByAttributes(array('post_id'=>$model->id));
		foreach($postCategory as $category)
		{
			$category->active = 0;
			$category->save();
		}
		$this->savePostCategory($model);
	}
	
	public function actionRemoveThumbnail($id)
	{
		$model = $this->loadModel($id);
		
		if($model->thumbnail != '')
		{
			$path = Yii::app()->basePath.'/../uploads/post/';
			if(file_exists($path.$model->thumbnail))
				unlink($path.$model->thumbnail);
			
			$model->thumbnail = '';
			$model->save();
			
			Yii::app()->user->setFlash('success','Thumbnail berhasil dihapus');
			$this->redirect(array('post/update','id'=>$id));
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Post');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Post('search');
		
		$model->unsetAttributes();  // clear any default values
		
		if(isset($_GET['Post']))
			$model->attributes=$_GET['Post'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function actionChartByPostCategory()
	{
		Yii::app()->fusioncharts->setChartOptions(array(
			'caption'=>'Jumlah Post Berdasarkan Kategori', 
		));
		
		foreach(PostCategory::model()->findAll() as $data) 
		{
			Yii::app()->fusioncharts->addSet(array('label'=>$data->title, 'value'=>$data->countPost()));
		}
		
		Yii::app()->fusioncharts->getXMLData(true);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Post::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='post-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
