<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{	
		$this->layout = '//layouts/home';
		$this->render('index');
	}
	
	public function actionPencarian()
	{
		$this->layout = '//layouts/column2';
		$this->render('pencarian');
	}

	public function actionAplikasi()
	{
		$this->layout = '//layouts/aplikasi';
		$this->render('aplikasi');
	}	
	
	public function actionEmail()	
	{		
		$subject = 'test';		
		$content = 'tes';		
		$headers = 'From: noreply@inovasi.lan.go.id' . "\r\n" .			'Reply-To: info@inovasi.lan.go.id' . "\r\n" .				'MIME-Version: 1.0\r\n'.			'Content-Type: text/plain; charset=UTF-8';				mail('thomas.alfa.edison@gmail.com',$subject,$content,$headers);				print "OK";		
		
	}

	public function actiondashboardAdministrator()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'

		$this->redirect(array('site/dashboardAdministrator'));
	}

	public function actionBerita()
	{	
		$this->render('pages/post');
	}

	public function actionInovasi()
	{	
		$this->render('pages/inovasi');
	}

	public function actionPostPage($id)
	{
		$model = Post::model()->findByPk($id);
		
		$this->render('pages/postPage', array('model'=>$model));
	}

	public function actionInovasiPage($id)
	{
		$model = Inovasi::model()->findByPk($id);
		
		$this->render('pages/inovasiPage', array('model'=>$model));
	}

	public function actionPhotoCategory()
	{	
		$this->render('pages/photoCategory');
	}

	public function actionVideoCategory()
	{	
		$this->render('pages/videoCategory');
	}

	public function actionPhoto()
	{	
		$this->render('/photo/pagePhoto');
	}
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		$this->layout='//layouts/column2';
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$this->layout = '//layouts/column2';
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: Form Kontak PKP2A I LAN <noreply@bandung.lan.go.id>\r\n".
					"Reply-To: $name <{$model->email}> \r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/html; charset=UTF-8";								
				$content = '<b>Name:</b> '.$model->name.'<br>';				
				$content .= '<b>Email: </b>'.$model->email.'<br>';				
				$content .= '<b>Subject: </b>'.$model->subject.'<br>';				
				$content .= '<b>Message: </b><br>';				
				$content .= '<br>';				
				$content .= $model->body;										
				
				mail('thomas.alfa.edison@gmail.com',$subject,$content,$headers);
				//mail('p2ipk.inovasi@gmail.com',$subject,$content,$headers);
				//mail('p2ipk@lan.go.id',$subject,$content,$headers);
				
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		if(!Yii::app()->user->isGuest)
			$this->redirect(array('admin/index'));
		
		$this->layout ='//layouts/admin/login';
		$model=new LoginForm;
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			
			if($model->validate() && $model->login())
			{
				$this->redirect(array('admin/index'));
			}
		}
		
		$this->render('login',array(
			'model'=>$model
		));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		unset($_COOKIE['login']);
		unset($_COOKIE['username']);
		
		setcookie("login", "", time()-3600,'/','.bandung.lan.go.id');
		setcookie("username", "", time()-3600,'/','.bandung.lan.go.id');
		
		Yii::app()->user->logout();
		Yii::app()->user->setFlash('success','User berhasil logout');
		$this->redirect(array('site/index'));
	}
}