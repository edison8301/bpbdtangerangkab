<?php

class ApplicationController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
	public $layout='//layouts/admin/column2';

/**
* @return array action filters
*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'AccessRole',
		);
	}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','view'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
	public function actionCreate()
	{
		$model=new Application;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Application']))
		{
			$model->attributes=$_POST['Application'];
			date_default_timezone_set('Asia/Jakarta');
			$model->created_time = date('Y-m-d H:i:s');
			$image = CUploadedFile::getInstance($model,'image');
			
			if($image!==null)
				$model->image = str_replace(' ','-',time().'_'.$image->name);
			
			if($model->save())
			{
				if($image!==null)
				{
					$path = Yii::app()->theme->basePath.'/uploads/application/';
					$image->saveAs($path.$model->image);
				}
				
				Yii::app()->user->setFlash('success','Data berhasil ditambahkan');
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$oldFile = $model->image;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Application']))
		{
			$model->attributes=$_POST['Application'];
			
			$image = CUploadedFile::getInstance($model,'image');
			
			if($image!==null)
				$model->image = str_replace(' ','-',time().'_'.$image->name);
			else
				$model->image = $oldFile;
			
			if($model->save())
			{
				if($image!==null)
				{
					$path = Yii::app()->theme->basePath.'/uploads/application/';
					$image->saveAs($path.$model->image);
				
					if(file_exists($path.$oldFile) AND $oldFile!='')
						unlink($path.$oldFile);
				}
				
				Yii::app()->user->setFlash('success','Data berhasil ditambahkan');
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
	public function actionDelete($id)
	{
		$model=$this->loadModel($id);
		if(Yii::app()->request->isPostRequest)
		{
			$this->loadModel($id)->delete();
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) 
                ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,
                        'Invalid request. Please do not repeat this request again.');
	}

/**
* Lists all models.
*/
	public function actionIndex()
	{
		if(isset($_COOKIE['login']) AND $_COOKIE['login']==1)
		{
			$username = $_COOKIE['username'];
			$userIdentity = new UserIdentity($username,'bandunglan');		
			Yii::app()->user->login($userIdentity,3600*24*30);		
		}
		
		$this->layout = '//layouts/aplikasi';
		$this->render('index');

		/*
		$dataProvider=new CActiveDataProvider('Application');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
		*/
	}

/**
* Manages all models.
*/
	public function actionAdmin()
	{
		$model=new Application('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Link']))
			$model->attributes=$_GET['Link'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
	public function loadModel($id)
	{
		$model=Application::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='link-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
