<?php

class BencanaController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/admin/column2';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
return array(
array('allow',  // allow all users to perform 'index' and 'view' actions
'actions'=>array('index','view','read'),
'users'=>array('*'),
),
array('allow', // allow authenticated user to perform 'create' and 'update' actions
'actions'=>array('create','update','report','export','ExportExcel','ReportDesa','ReportDesaProcess','ExportExcelDesa'),
'users'=>array('@'),
),
array('allow', // allow admin user to perform 'admin' and 'delete' actions
'actions'=>array('admin','delete'),
'users'=>array('@'),
),
array('deny',  // deny all users
'users'=>array('*'),
),
);
}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
$this->render('view',array(
'model'=>$this->loadModel($id),
));
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
public function actionCreate()
{
$model=new Bencana;

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Bencana']))
{
$model->attributes=$_POST['Bencana'];
			date_default_timezone_set('Asia/Jakarta');
			$model->waktu_dibuat = date('Y-m-d H:i:s');
if($model->save())
$this->redirect(array('view','id'=>$model->id));
}

$this->render('create',array(
'model'=>$model,
));
}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
$model=$this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Bencana']))
{
$model->attributes=$_POST['Bencana'];
if($model->save())
$this->redirect(array('view','id'=>$model->id));
}

$this->render('update',array(
'model'=>$model,
));
}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
	public function actionIndex()
	{
		$this->layout = '//layouts/column1';
		$dataProvider=new CActiveDataProvider('Bencana');
		$this->render('_view',array(
			'dataProvider'=>$dataProvider,
		));
	}

/**
* Manages all models.
*/
public function actionAdmin()
{
$model=new Bencana('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['Bencana']))
$model->attributes=$_GET['Bencana'];

$this->render('admin',array(
'model'=>$model,
));
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Bencana::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='bencana-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}

	public function actionRead($id)
		{
		$this->layout = '//layouts/column2';
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

		public function actionReport()
	{
		$this->layout = '//layouts/admin/column4';
		if(!empty($_POST['tanggal_awal']) AND !empty($_POST['tanggal_akhir']))
			$this->redirect(array(
				'Bencana/ExportExcel',
				'tanggal_awal'=>$_POST['tanggal_awal'],
				'tanggal_akhir'=>$_POST['tanggal_akhir'],
			));

		$this->render('report');
	}

		public function actionReportProcess()
	{
		$this->layout = 'excel';
		$this->render('reportProcess',array(
			'tanggal_awal'=>$_GET['tanggal_awal'],
			'tanggal_akhir'=>$_GET['tanggal_akhir'],
		));
	}

		public function actionReportDesa()
	{
		$this->layout = '//layouts/admin/column4';
		if(!empty($_POST['wat']))
			$this->redirect(array(
				'Bencana/jjExportExcelDesa',
				'Bencana'=>$_POST['Bencana'],
			));

		$this->render('report_desa');
	}

		public function actionReportDesaProcess()
	{
		$this->layout = 'excel';
		$this->render('reportProcess',array(
			'Bencana'=>$_GET['Bencana'],
		));
	}

	public function actionExportExcel($tanggal_awal, $tanggal_akhir)
	{
			$criteria = new CDbCriteria;
			$criteria->condition = 'tanggal >= :waktu_awal AND tanggal <= :waktu_akhir';
			$criteria->params = array(':waktu_awal'=>$tanggal_awal.' 00:00:00',':waktu_akhir'=>$tanggal_akhir.'23:59:59');
			$criteria->order = 'tanggal ASC';

			spl_autoload_unregister(array('YiiBase','autoload'));
		
			Yii::import('application.vendors.PHPExcel',true);
		
			spl_autoload_register(array('YiiBase', 'autoload'));

			$PHPExcel = new PHPExcel();
			
			//Masih Berantakan :v 
			//style font
			$PHPExcel->getActiveSheet()->getStyle("A1:L1")->getFont()->setSize(14);
			$PHPExcel->getActiveSheet()->getStyle("A2:L6")->getFont()->setSize(10);
			//$PHPExcel->getActiveSheet()->getStyle("A1:G2")->getFont()->getColor()->setRGB('FFFFFF');
			$PHPExcel->getActiveSheet()->getStyle('A6:L6')->getFont()->setBold(true);
			$PHPExcel->getActiveSheet()->getStyle('A6:L6')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);//border header surat	
			$PHPExcel->getActiveSheet()->getStyle('A1:L1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//merge and center
			$PHPExcel->getActiveSheet()->getStyle('A2:L2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//merge and center
			$PHPExcel->getActiveSheet()->getStyle('A4:L4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//merge and center
			$PHPExcel->getActiveSheet()->getStyle('A3:L3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//merge and center
			$PHPExcel->getActiveSheet()->getStyle('A6:L6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//merge and center
			$PHPExcel->getActiveSheet()->getStyle('A6:L6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);//merge and center
			$PHPExcel->getActiveSheet()->mergeCells('A1:L1');//sama jLga
			$PHPExcel->getActiveSheet()->mergeCells('A2:L2');//sama jLga
			$PHPExcel->getActiveSheet()->mergeCells('A3:L3');//sama jLga
			$PHPExcel->getActiveSheet()->mergeCells('A4:L4');//sama jLga
			$PHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, "DATA KEJADIAN DAN KEBAKARAN");
			$PHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, "BADAN PENANGGULANGAN BENCANA DAERAH");
			$PHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 3, "KABUPATEN TANGERANG");
			$PHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 4, "BULAN :"); //value di merge and center
			//$PHPExcel->getActiveSheet()->getStyle('A3:G3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('D6EDC5');//background color header surat
			//$PHPExcel->getActiveSheet()->getStyle('A1:G2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('56BF48');//background color judul yg di merge and center
		
			
			//header surat
			$PHPExcel->getActiveSheet()->setCellValue('A6', 'NO');
			$PHPExcel->getActiveSheet()->setCellValue('B6', 'HARI/TANGGAL');
			$PHPExcel->getActiveSheet()->setCellValue('C6', 'JENIS BENCANA / KEBAKARAN');
			$PHPExcel->getActiveSheet()->setCellValue('D6', 'LOKASI BENCANA / KEBAKARAN');
			$PHPExcel->getActiveSheet()->setCellValue('E6', 'BADAN HUKUM/NAMA PEMILIK / KORBAN');
			$PHPExcel->getActiveSheet()->setCellValue('F6', 'PENYEBAB BENCANA / KEBAKARAN');
			$PHPExcel->getActiveSheet()->setCellValue('G6', 'MATERI');
			$PHPExcel->getActiveSheet()->setCellValue('H6', 'KK');
			$PHPExcel->getActiveSheet()->setCellValue('I6', 'JIWA');
			$PHPExcel->getActiveSheet()->setCellValue('J6', 'MENINGGAL');
			$PHPExcel->getActiveSheet()->setCellValue('K6', 'LUKA-LUKA');
			$PHPExcel->getActiveSheet()->setCellValue('L6', 'KET');


			$i = 1;
			$kolom = 7;
			//value nya
			foreach(Bencana::model()->findAll($criteria) as $data)
			{	
				$PHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6);
				$PHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
				$PHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(24);
				$PHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
				$PHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
				$PHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
				$PHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
				$PHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
				$PHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
				$PHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
				$PHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
				$PHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);

				$PHPExcel->getActiveSheet()->getStyle('A3:L'.$kolom)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//merge and center
				$PHPExcel->getActiveSheet()->getStyle('A2:L'.$kolom)->getFont()->setSize(9);
				$PHPExcel->getActiveSheet()->getStyle('A7:L'.$kolom)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);//border header surat	


				$PHPExcel->getActiveSheet()->setCellValue('A'.$kolom, $i);
				$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, Helper::tanggal($data->tanggal));
				$PHPExcel->getActiveSheet()->setCellValue('C'.$kolom, $data->getBencanaJenis());
				$PHPExcel->getActiveSheet()->setCellValue('D'.$kolom, $data->lokasi);
				$PHPExcel->getActiveSheet()->setCellValue('E'.$kolom, $data->nama_korban);
				$PHPExcel->getActiveSheet()->setCellValue('F'.$kolom, $data->penyebab);
				$PHPExcel->getActiveSheet()->setCellValue('G'.$kolom, $data->korban_materi);
				$PHPExcel->getActiveSheet()->setCellValue('H'.$kolom, $data->korban_kk);
				$PHPExcel->getActiveSheet()->setCellValue('I'.$kolom, $data->korban_jiwa);
				$PHPExcel->getActiveSheet()->setCellValue('J'.$kolom, $data->korban_meninggal);
				$PHPExcel->getActiveSheet()->setCellValue('K'.$kolom, $data->korban_luka);
				$PHPExcel->getActiveSheet()->setCellValue('L'.$kolom, $data->keterangan);

				$i++; $kolom++;
			}
					$PHPExcel->getActiveSheet()->getStyle('A6:L'.$kolom)->getAlignment()->setWrapText(true);
		$PHPExcel->getActiveSheet()->getStyle('A6:L'.$kolom)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

			$filename = time().'_Bencana.xlsx';

			$path = Yii::app()->basePath.'/../uploads/export/';
			$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
			$objWriter->save($path.$filename);	
			$this->redirect(Yii::app()->request->baseUrl.'/uploads/export/'.$filename);
	}

		public function actionExportExcelDesa($Bencanaid_bencana_desa)
	{
		

				$criteria = new CDbCriteria;
				$criteria->condition = 'id_bencana_desa = :Bencana';
				$criteria->params = array(':Bencana'=>$Bencana);
				$criteria->order = 'id_bencana_desa ASC';

			spl_autoload_unregister(array('YiiBase','autoload'));
		
			Yii::import('application.vendors.PHPExcel',true);
		
			spl_autoload_register(array('YiiBase', 'autoload'));

			$PHPExcel = new PHPExcel();
			
			//Masih Berantakan :v 
			//style font
			$PHPExcel->getActiveSheet()->getStyle("A1:L1")->getFont()->setSize(14);
			$PHPExcel->getActiveSheet()->getStyle("A2:L6")->getFont()->setSize(10);
			//$PHPExcel->getActiveSheet()->getStyle("A1:G2")->getFont()->getColor()->setRGB('FFFFFF');
			$PHPExcel->getActiveSheet()->getStyle('A6:L6')->getFont()->setBold(true);
			$PHPExcel->getActiveSheet()->getStyle('A6:L6')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);//border header surat	
			$PHPExcel->getActiveSheet()->getStyle('A1:L1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//merge and center
			$PHPExcel->getActiveSheet()->getStyle('A2:L2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//merge and center
			$PHPExcel->getActiveSheet()->getStyle('A4:L4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//merge and center
			$PHPExcel->getActiveSheet()->getStyle('A3:L3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//merge and center
			$PHPExcel->getActiveSheet()->getStyle('A6:L6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//merge and center
			$PHPExcel->getActiveSheet()->getStyle('A6:L6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::VERTICAL_CENTER);//merge and center
			$PHPExcel->getActiveSheet()->mergeCells('A1:L1');//sama jLga
			$PHPExcel->getActiveSheet()->mergeCells('A2:L2');//sama jLga
			$PHPExcel->getActiveSheet()->mergeCells('A3:L3');//sama jLga
			$PHPExcel->getActiveSheet()->mergeCells('A4:L4');//sama jLga
			$PHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, "DATA KEJADIAN DAN KEBAKARAN");
			$PHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2, "BADAN PENANGGULANGAN BENCANA DAERAH");
			$PHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 3, "KABUPATEN TANGERANG");
			$PHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 4, "BULAN :"); //value di merge and center
			//$PHPExcel->getActiveSheet()->getStyle('A3:G3')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('D6EDC5');//background color header surat
			//$PHPExcel->getActiveSheet()->getStyle('A1:G2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('56BF48');//background color judul yg di merge and center
		
			
			//header surat
			$PHPExcel->getActiveSheet()->setCellValue('A6', 'NO');
			$PHPExcel->getActiveSheet()->setCellValue('B6', 'HARI/TANGGAL');
			$PHPExcel->getActiveSheet()->setCellValue('C6', 'JENIS BENCANA / KEBAKARAN');
			$PHPExcel->getActiveSheet()->setCellValue('D6', 'LOKASI BENCANA / KEBAKARAN');
			$PHPExcel->getActiveSheet()->setCellValue('E6', 'BADAN HUKUM/NAMA PEMILIK / KORBAN');
			$PHPExcel->getActiveSheet()->setCellValue('F6', 'PENYEBAB BENCANA / KEBAKARAN');
			$PHPExcel->getActiveSheet()->setCellValue('G6', 'MATERI');
			$PHPExcel->getActiveSheet()->setCellValue('H6', 'KK');
			$PHPExcel->getActiveSheet()->setCellValue('I6', 'JIWA');
			$PHPExcel->getActiveSheet()->setCellValue('J6', 'MENINGGAL');
			$PHPExcel->getActiveSheet()->setCellValue('K6', 'LUKA-LUKA');
			$PHPExcel->getActiveSheet()->setCellValue('L6', 'KET');


			$i = 1;
			$kolom = 7;
			//value nya
			foreach(Bencana::model()->findAll($criteria) as $data)
			{	
				$PHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6);
				$PHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
				$PHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(24);
				$PHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
				$PHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(18);
				$PHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
				$PHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
				$PHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
				$PHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
				$PHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
				$PHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
				$PHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);

				$PHPExcel->getActiveSheet()->getStyle('A3:L'.$kolom)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//merge and center
				$PHPExcel->getActiveSheet()->getStyle('A2:L'.$kolom)->getFont()->setSize(9);
				$PHPExcel->getActiveSheet()->getStyle('A7:L'.$kolom)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);//border header surat	


				$PHPExcel->getActiveSheet()->setCellValue('A'.$kolom, $i);
				$PHPExcel->getActiveSheet()->setCellValue('B'.$kolom, Helper::tanggal($data->tanggal));
				$PHPExcel->getActiveSheet()->setCellValue('C'.$kolom, $data->getBencanaJenis());
				$PHPExcel->getActiveSheet()->setCellValue('D'.$kolom, $data->lokasi);
				$PHPExcel->getActiveSheet()->setCellValue('E'.$kolom, $data->nama_korban);
				$PHPExcel->getActiveSheet()->setCellValue('F'.$kolom, $data->penyebab);
				$PHPExcel->getActiveSheet()->setCellValue('G'.$kolom, $data->korban_materi);
				$PHPExcel->getActiveSheet()->setCellValue('H'.$kolom, $data->korban_kk);
				$PHPExcel->getActiveSheet()->setCellValue('I'.$kolom, $data->korban_jiwa);
				$PHPExcel->getActiveSheet()->setCellValue('J'.$kolom, $data->korban_meninggal);
				$PHPExcel->getActiveSheet()->setCellValue('K'.$kolom, $data->korban_luka);
				$PHPExcel->getActiveSheet()->setCellValue('L'.$kolom, $data->keterangan);


									
				$i++; $kolom++;
			}
				$PHPExcel->getActiveSheet()->getStyle('A6:L'.$kolom)->getAlignment()->setWrapText(true);
				$PHPExcel->getActiveSheet()->getStyle('A6:L'.$kolom)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

			$filename = time().'_Bencana.xlsx';

			$path = Yii::app()->basePath.'/../uploads/export/';
			$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
			$objWriter->save($path.$filename);	
			$this->redirect(Yii::app()->request->baseUrl.'/uploads/export/'.$filename);
	}

}
