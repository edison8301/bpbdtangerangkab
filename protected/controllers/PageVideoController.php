<?php

class PageVideoController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/column2';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
return array(
array('allow',  // allow all users to perform 'index' and 'view' actions
'actions'=>array('index','view'),
'users'=>array('*'),
),
array('allow', // allow authenticated user to perform 'create' and 'update' actions
'actions'=>array('create','update'),
'users'=>array('@'),
),
array('allow', // allow admin user to perform 'admin' and 'delete' actions
'actions'=>array('admin','delete'),
'users'=>array('@'),
),
array('deny',  // deny all users
'users'=>array('*'),
),
);
}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
	public function actionView($id)
	{
		$this->render('view',array(
		'model'=>$this->loadModel($id),
		));
	}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
	public function actionCreate()
	{
		$model=new PageVideo;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PageVideo']))
		{
			$model->attributes=$_POST['PageVideo'];
			$mp4 = CUploadedFile::getInstance($model,'mp4');
			$ogv = CUploadedFile::getInstance($model,'ogv');
			$webm = CUploadedFile::getInstance($model,'webm');
			
			if($mp4!==null)
				$model->mp4 = str_replace(' ','-',time().'_'.$mp4->name);
			
			if($ogv!==null)
				$model->ogv = str_replace(' ','-',time().'_'.$ogv->name);
			
			if($webm!==null)
				$model->webm = str_replace(' ','-',time().'_'.$ogv->webm);
			
			if($model->save())
			{
				$path = Yii::app()->basePath.'/../uploads/page/';
				
				if($mp4!==null)
				{
					$mp4->saveAs($path.$model->mp4);
				}
				
				if($mp4!==null)
				{
					$ogv->saveAs($path.$model->ogv);
				}
				
				if($webm!==null)
				{
					$webm->saveAs($path.$model->webm);
				}
				
				Yii::app()->user->setFlash('success','Foto berhasil ditambahkan');
				$this->redirect(array('pekerjaan/view','id'=>$model->page_id));
			}

		$this->render('create',array(
			'model'=>$model,
		));
		}
	}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
	public function actionUpdate($id)
	{
		$this->layout = 'admin/column2';
		
		$model=$this->loadModel($id);
		
		$mp4_old = $model->mp4;
		$ogv_old = $model->ogv;
		$webm_old = $model->webm;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PageVideo']))
		{
			$model->attributes=$_POST['PageVideo'];
			
			$mp4 = CUploadedFile::getInstance($model,'mp4');
			$ogv = CUploadedFile::getInstance($model,'ogv');
			$webm = CUploadedFile::getInstance($model,'webm');
			
			if($mp4!==null)
				$model->mp4 = str_replace(' ','-',time().'_'.$mp4->name);
			else 
				$model->mp4 = $mp4_old;
			
			if($ogv!==null)
				$model->ogv = str_replace(' ','-',time().'_'.$ogv->name);
			else 
				$model->ogv = $ogv_old;
			
			if($webm!==null)
				$model->webm = str_replace(' ','-',time().'_'.$ogv->webm);
			else 
				$model->webm = $webm_old;
			
			if($model->save())
			{
				$path = Yii::app()->basePath.'/../uploads/pekerjaan/';
				
				if($mp4!==null)
				{
					$mp4->saveAs($path.$model->mp4);
					
					if(file_exists($path.$mp4_old) AND $mp4_old!='')
						unlink($path.$mp4_old);
				}
				
				if($ogv!==null)
				{
					$ogv->saveAs($path.$model->ogv);
					
					if(file_exists($path.$ogv_old) AND $ogv_old!='')
						unlink($path.$ogv_old);
				}
				
				if($webm!==null)
				{
					$webm->saveAs($path.$model->webm);
					
					if(file_exists($path.$mp4_old) AND $mp4_old!='')
						unlink($path.$mp4_old);
				}
				
				Yii::app()->user->setFlash('success','Video berhasil diperbarui');
				$this->redirect(array('page/view','id'=>$model->page_id));
			}
		}
	
		$this->render('update',array(
			'model'=>$model,
		));
}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('PageVideo');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
$model=new PageVideo('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['PageVideo']))
$model->attributes=$_GET['PageVideo'];

$this->render('admin',array(
'model'=>$model,
));
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=PageVideo::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='page-video-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
