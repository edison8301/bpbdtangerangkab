<?php

class MemberController extends Controller
{
	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/
	public $layout='//layouts/column2';
	
	public $verifyCode;
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			
		);
	}
	/**
	* @return array action filters
	*/
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('signup','login','logout','captcha','email'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','view','index'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	* Displays a particular model.
	* @param integer $id the ID of the model to be displayed
	*/
	public function actionView($id)
	{
		$this->layout = '//layouts/admin/column2';
		
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionEmail() 
	{
		$to      = 'thomas.alfa.edison@gmail.com';
		$subject = 'the subject';
		$message = 'hello';
		$headers = 'From: noreply@inovasi.lan.go.id' . "\r\n" .
			'Reply-To: noreply@inovasi.lan.og.id' . "\r\n" .
			'X-Mailer: PHP/' . phpversion();

		mail($to, $subject, $message, $headers);
		
		print "OK";
	}

	/**
	* Creates a new model.
	* If creation is successful, the browser will be redirected to the 'view' page.
	*/
	public function actionCreate()
	{
		$model=new Member;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Member']))
		{
			$model->attributes=$_POST['Member'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	* Updates a particular model.
	* If update is successful, the browser will be redirected to the 'view' page.
	* @param integer $id the ID of the model to be updated
	*/
	public function actionUpdate($id)
	{
		$this->layout = '//layouts/administrator/column2';
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Member']))
		{
			$model->attributes=$_POST['Member'];
			if($model->save())
			{
				if($model->aktif == 1 AND $model->login_terakhir == '')
					$model->sendActivationEmail();
					
				Yii::app()->user->setFlash('success','Data berhasil disimpan');
				$this->redirect(array('view','id'=>$model->id));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
	public function actionSignup()
	{
		$this->layout = '//layouts/column1';
		
		$model=new Member;
		$model->scenario = 'signup';

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Member']))
		{
			$model->attributes=$_POST['Member'];
			
			date_default_timezone_set('Asia/Jakarta');
			$model->waktu_dibuat = date('Y-m-d H:i:s');
			
			if($model->save())
			{
				$model->sendSignupEmail();
				
				Yii::app()->user->setFlash('success','Registrasi member baru berhasil. Anda dapat login setelah kami mengaktifkan akun member Anda. Anda akan mendapatkan pemberitahuan via email jika akun anda sudah diaktifkan. Terima kasih sudah mendaftar.'); 
				$this->redirect(array('/member/login'));
			}
		}

		$this->render('signup',array(
			'model'=>$model,
		));
	}
	
	public function actionLogin()
	{
		$this->layout = '//layouts/column1';
		
		$model=new LoginMember;
		
		if(!empty($_POST['LoginMember']))
		{
			$model->attributes = $_POST['LoginMember'];
			
			if(!$model->validate())
			{
				Yii::app()->user->setFlash('error','Kode verifikasi tidak cocok atau kolom username dan password kosong');
				$this->redirect(array('member/login'));
			}
			
			if(!$model->isAktif())
			{
				Yii::app()->user->setFlash('error','Akun anda belum diaktifkan oleh admin. Silahkan hubungi admin website.');
				$this->redirect(array('member/login'));
			}
			
			if($model->authenticate())
			{
				Yii::app()->user->setState('member',1);
				Yii::app()->user->setState('member_id',$model->getMemberId());
				
				$model->updateLoginTerkahir();
				
				Yii::app()->user->setFlash('success','Login berhasil. Anda dapat mengirimkan inovasi');
				$this->redirect(array('inovasi/submit'));
				
			} 
			
			Yii::app()->user->setFlash('error','Login gagal. Email atau password yang Anda inputkan salah');
			$this->redirect(array('member/login'));
			
		}
		
		$this->render('login',array(
			'model'=>$model
		));
	}
	
	public function actionLogout()
	{
		Yii::app()->user->clearStates();
		Yii::app()->user->setFlash('success','Logout berhasil. Silahkan login kembali untuk mengirimkan data inovasi');
		$this->redirect(array('member/login'));
	}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Member');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

	/**
	* Manages all models.
	*/
	public function actionAdmin()
	{
		$this->layout = '//layouts/admin/column2';
		
		$model=new Member('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Member']))
			$model->attributes=$_GET['Member'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}


/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Member::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='member-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
}
