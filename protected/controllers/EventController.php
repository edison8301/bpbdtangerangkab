<?php



class EventController extends Controller

{

	/**
	* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	* using two-column layout. See 'protected/views/layouts/column2.php'.
	*/

	public $layout='//layouts/admin/column2';



	/**
	* @return array action filters
	*/

	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}



	/**
	* Specifies the access control rules.
	* This method is used by the 'accessControl' filter.
	* @return array access control rules
	*/

	public function accessRules()

	{

		return array(

			array('allow',  // allow all users to perform 'index' and 'view' actions

				'actions'=>array('index','sitemap','read','report','calendar','exportProccess','ExportSkpdProccess'),

				'users'=>array('*'),

			),

			array('allow', // allow authenticated user to perform 'create' and 'update' actions

				'actions'=>array('create','update','view','removeThumbnail'),

				'users'=>array('@'),

			),

			array('allow', // allow admin user to perform 'admin' and 'delete' actions

				'actions'=>array('admin','delete'),

				'users'=>array('@'),

			),

			array('deny',  // deny all users

				'users'=>array('*'),

			),

		);

	}

	

	public function actionRemoveThumbnail($id)

	{

		$model = $this->loadModel($id);

		$oldFile = $model->thumbnail;

		

		$path = Yii::app()->theme->basePath.'/uploads/event/';

		if(file_exists($path.$oldFile) AND $oldFile!='')

			unlink($path.$oldFile);

		$model->thumbnail = Null;

		

		if($model->save())

			Yii::app()->user->setFlash('success','Thumbnail berhasil dihapus');

			$this->redirect(array('view','id'=>$model->id));

	}

	

	public function actionExportProccess()

	{

		if($_POST['export_type']=='pdf')

		{

			$pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'A4', true, 'UTF-8');

			// create new PDF document

			$pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

			// set document information

			$pdf->SetCreator(PDF_CREATOR);

			$pdf->SetAuthor('Pemerintah Kabupaten Serang');

			$pdf->SetTitle('Laporan Jumlah Agenda Tahun'.date('Y').'');

			$pdf->SetSubject('Laporan Jumlah Agenda');

			//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	

			//set auto page breaks

			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

			// add a page

			$pdf->AddPage();

			$pdf->SetFont('times', '', 10);

		

			$html  = '<h1 style="text-align:center;">Laporan Jumlah Agenda Tahun '.date('Y').'</h1>';

			$html .= '<table border="1">';

			$html .= '<tr style="text-align:center;">';

			$html .= '<td>Agenda</td>';

			$html .= '<td>Jan</td>';

			$html .= '<td>Feb</td>';

			$html .= '<td>Mar</td>';

			$html .= '<td>Apr</td>';

			$html .= '<td>Mei</td>';

			$html .= '<td>Jun</td>';

			$html .= '<td>Jul</td>';

			$html .= '<td>Agu</td>';

			$html .= '<td>Sep</td>';

			$html .= '<td>Okt</td>';

			$html .= '<td>Nov</td>';

			$html .= '<td>Des</td>';

			$html .= '</tr>';

			

			$categories = Event::model()->getCategory();

			$year = date('Y');

			foreach($categories as $category) 

			{

				$januari[] = $category->getEventReport($category->id,'01',$year)['0'];

				$februari[] = $category->getEventReport($category->id,'02',$year)['0'];

				$maret[] = $category->getEventReport($category->id,'03',$year)['0']; 

				$april[] = $category->getEventReport($category->id,'04',$year)['0']; 

				$mei[] = $category->getEventReport($category->id,'05',$year)['0']; 

				$juni[] = $category->getEventReport($category->id,'06',$year)['0'];

				$juli[] = $category->getEventReport($category->id,'07',$year)['0']; 

				$agustus[] = $category->getEventReport($category->id,'08',$year)['0']; 

				$september[] = $category->getEventReport($category->id,'09',$year)['0']; 

				$oktober[] = $category->getEventReport($category->id,'10',$year)['0']; 

				$november[] = $category->getEventReport($category->id,'11',$year)['0']; 

				$desember[] = $category->getEventReport($category->id,'12',$year)['0'];

			

			$html .= '<tr style="text-align:center">';

			$html .= '<td style="text-align:left">'.$category->title.'</td>';

			$html .= '<td>'.$category->getEventReport($category->id,'01',$year)['0'].'</td>';

			$html .= '<td>'.$category->getEventReport($category->id,'02',$year)['0'].'</td>';

			$html .= '<td>'.$category->getEventReport($category->id,'03',$year)['0'].'</td>';

			$html .= '<td>'.$category->getEventReport($category->id,'04',$year)['0'].'</td>';

			$html .= '<td>'.$category->getEventReport($category->id,'05',$year)['0'].'</td>';

			$html .= '<td>'.$category->getEventReport($category->id,'06',$year)['0'].'</td>';

			$html .= '<td>'.$category->getEventReport($category->id,'07',$year)['0'].'</td>';

			$html .= '<td>'.$category->getEventReport($category->id,'08',$year)['0'].'</td>';

			$html .= '<td>'.$category->getEventReport($category->id,'09',$year)['0'].'</td>';

			$html .= '<td>'.$category->getEventReport($category->id,'10',$year)['0'].'</td>';

			$html .= '<td>'.$category->getEventReport($category->id,'11',$year)['0'].'</td>';

			$html .= '<td>'.$category->getEventReport($category->id,'12',$year)['0'].'</td>';

			$html .= '</tr>';

			}

			$html .= '<tr style="text-align:center">';

			$html .= '<td>Total</td>';

			$html .= '<td>'.array_sum($januari).'</td>';

			$html .= '<td>'.array_sum($februari).'</td>';

			$html .= '<td>'.array_sum($maret).'</td>';

			$html .= '<td>'.array_sum($april).'</td>';

			$html .= '<td>'.array_sum($mei).'</td>';

			$html .= '<td>'.array_sum($juni).'</td>';

			$html .= '<td>'.array_sum($juli).'</td>';

			$html .= '<td>'.array_sum($agustus).'</td>';

			$html .= '<td>'.array_sum($september).'</td>';

			$html .= '<td>'.array_sum($oktober).'</td>';

			$html .= '<td>'.array_sum($november).'</td>';

			$html .= '<td>'.array_sum($desember).'</td>';

			$html .= '</tr>';

			$html .= '</table>';

			$pdf->writeHTML($html, true, false, false, false, '');

		

			$pdf->Output('Laporan_Jumlah_Agenda_Tahun_'.date('Y').'.pdf', 'I');	

		}

		if($_POST['export_type']=='word')

		{

			spl_autoload_unregister(array('YiiBase','autoload'));

		

			Yii::import('application.extensions.PHPWord',true);

		

			spl_autoload_register(array('YiiBase', 'autoload'));

		

			$PHPWord = new PHPWord();

		

			$PHPWord->addFontStyle('bold', array('bold'=>true));

			$PHPWord->addFontStyle('subjudul', array('name'=>'Calibri','bold'=>true, 'size'=>11));

			$PHPWord->addFontStyle('normal', array('name'=>'Calibri','bold'=>false, 'size'=>9));

			$PHPWord->addFontStyle('judul', array('bold'=>true, 'size'=>16,'align'=>'center','name'=>'Times New Roman'));

		

			$tableStyle = array(

				'borderSize'=>0, 

				'borderColor'=>'000000', 

				'cellMargin'=>80,

				'border'=>true

			);

					

			$textTableStyle = array(

				'borderSize'=>0, 

				'borderColor'=>'ffffff', 

				'cellMargin'=>80,

				'border'=>true

			);			

		

			$PHPWord->addTableStyle('tableStyle', $tableStyle);

			$PHPWord->addTableStyle('textTableStyle', $textTableStyle);

			

			$center = array('align'=>'center');

		

			$fontStyle = array('name'=>'Calibri','bold'=>false, 'size'=>9,'align'=>'both');

		

			$fontHeadTable = array('name'=>'Calibri','bold'=>true, 'size'=>9);

		

			$paraStyle = array('spaceAfter'=>'2','size'=>9,'align'=>'both');

			$paraStyleCenter = array('spaceAfter'=>'2','size'=>9,'align'=>'center');

			$headStyle = array('spaceAfter'=>'2','align'=>'center','bold'=>true);

		

			$section = $PHPWord->createSection();

		

			$section->addText('Laporan Jumlah Agenda Tahun '.date('Y').'','judul',$center);		

		

			$section->addTextBreak(1);		

		

			$table = $section->addTable('textTableStyle');

		

			$table->addRow();

			$table->addCell(3000)->addText("Agenda",$fontStyle,$paraStyle);

			$table->addCell(500)->addText("Jan",$fontStyle,$paraStyle);

			$table->addCell(500)->addText("Feb",$fontStyle,$paraStyle);

			$table->addCell(500)->addText("Mar",$fontStyle,$paraStyle);

			$table->addCell(500)->addText("Apr",$fontStyle,$paraStyle);

			$table->addCell(500)->addText("Mei",$fontStyle,$paraStyle);

			$table->addCell(500)->addText("Jun",$fontStyle,$paraStyle);

			$table->addCell(500)->addText("Jul",$fontStyle,$paraStyle);

			$table->addCell(500)->addText("Agu",$fontStyle,$paraStyle);

			$table->addCell(500)->addText("Sep",$fontStyle,$paraStyle);

			$table->addCell(500)->addText("Okt",$fontStyle,$paraStyle);

			$table->addCell(500)->addText("Nov",$fontStyle,$paraStyle);

			$table->addCell(500)->addText("Des",$fontStyle,$paraStyle);

			

			$categories = Event::model()->getCategory();

			$year = date('Y');

			foreach($categories as $category) 

			{

				$januari[] = $category->getEventReport($category->id,'01',$year)['0'];

				$februari[] = $category->getEventReport($category->id,'02',$year)['0'];

				$maret[] = $category->getEventReport($category->id,'03',$year)['0']; 

				$april[] = $category->getEventReport($category->id,'04',$year)['0']; 

				$mei[] = $category->getEventReport($category->id,'05',$year)['0']; 

				$juni[] = $category->getEventReport($category->id,'06',$year)['0'];

				$juli[] = $category->getEventReport($category->id,'07',$year)['0']; 

				$agustus[] = $category->getEventReport($category->id,'08',$year)['0']; 

				$september[] = $category->getEventReport($category->id,'09',$year)['0']; 

				$oktober[] = $category->getEventReport($category->id,'10',$year)['0']; 

				$november[] = $category->getEventReport($category->id,'11',$year)['0']; 

				$desember[] = $category->getEventReport($category->id,'12',$year)['0'];

				

				//ISI DATA

				$table->addRow();

				$table->addCell(3000)->addText($category->title,$fontStyle,$paraStyle);

				$table->addCell(500)->addText($category->getEventReport($category->id,'01',$year)['0'],$fontStyle,$paraStyle);

				$table->addCell(500)->addText($category->getEventReport($category->id,'02',$year)['0'],$fontStyle,$paraStyle);

				$table->addCell(500)->addText($category->getEventReport($category->id,'03',$year)['0'],$fontStyle,$paraStyle);

				$table->addCell(500)->addText($category->getEventReport($category->id,'04',$year)['0'],$fontStyle,$paraStyle);

				$table->addCell(500)->addText($category->getEventReport($category->id,'05',$year)['0'],$fontStyle,$paraStyle);

				$table->addCell(500)->addText($category->getEventReport($category->id,'06',$year)['0'],$fontStyle,$paraStyle);

				$table->addCell(500)->addText($category->getEventReport($category->id,'07',$year)['0'],$fontStyle,$paraStyle);

				$table->addCell(500)->addText($category->getEventReport($category->id,'08',$year)['0'],$fontStyle,$paraStyle);

				$table->addCell(500)->addText($category->getEventReport($category->id,'09',$year)['0'],$fontStyle,$paraStyle);

				$table->addCell(500)->addText($category->getEventReport($category->id,'10',$year)['0'],$fontStyle,$paraStyle);

				$table->addCell(500)->addText($category->getEventReport($category->id,'11',$year)['0'],$fontStyle,$paraStyle);

				$table->addCell(500)->addText($category->getEventReport($category->id,'12',$year)['0'],$fontStyle,$paraStyle);

			}

			//TOTAL

			$table->addRow();

			$table->addCell(3000)->addText('Total',$fontStyle,$paraStyle);

			$table->addCell(500)->addText(array_sum($januari),$fontStyle,$paraStyle);

			$table->addCell(500)->addText(array_sum($februari),$fontStyle,$paraStyle);

			$table->addCell(500)->addText(array_sum($maret),$fontStyle,$paraStyle);

			$table->addCell(500)->addText(array_sum($april),$fontStyle,$paraStyle);

			$table->addCell(500)->addText(array_sum($mei),$fontStyle,$paraStyle);

			$table->addCell(500)->addText(array_sum($juni),$fontStyle,$paraStyle);

			$table->addCell(500)->addText(array_sum($juli),$fontStyle,$paraStyle);

			$table->addCell(500)->addText(array_sum($agustus),$fontStyle,$paraStyle);

			$table->addCell(500)->addText(array_sum($september),$fontStyle,$paraStyle);

			$table->addCell(500)->addText(array_sum($oktober),$fontStyle,$paraStyle);

			$table->addCell(500)->addText(array_sum($november),$fontStyle,$paraStyle);

			$table->addCell(500)->addText(array_sum($desember),$fontStyle,$paraStyle);

		

			//EXPORT DATA

			$objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');

		

			$pathFile = Yii::app()->basePath.'/../exports/';

		

			$filename = 'Laporan_Jumlah_Agenda_Tahun_'.date('Y').'.docx';

		

			$objWriter->save($pathFile.$filename);

		

			$this->redirect(Yii::app()->request->baseUrl.'/exports/'.$filename);

		}

	}

	

	public function actionExportSkpdProccess()

	{

		if($_POST['export_type']=='pdf')

		{

			$pdf=Yii::createComponent('application.extensions.tcpdf.ETcPdf', 'L', 'cm', 'A4', true, 'UTF-8');

			// create new PDF document

			$pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

			// set document information

			$pdf->SetCreator(PDF_CREATOR);

			$pdf->SetAuthor('Pemerintah Kabupaten Serang');

			$pdf->SetTitle('Laporan Jumlah Agenda SKPD Tahun'.date('Y').'');

			$pdf->SetSubject('Laporan Jumlah Agenda SKPD');

			//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	

			//set auto page breaks

			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

			// add a page

			$pdf->AddPage();

			$pdf->SetFont('times', '', 10);

		

			$html  = '<h1 style="text-align:center;">Laporan Jumlah Agenda SKPD Tahun '.date('Y').'</h1>';

			$html .= '<table border="1">';

			$html .= '<tr style="text-align:center;">';

			$html .= '<td>Agenda</td>';

			$html .= '<td>Jan</td>';

			$html .= '<td>Feb</td>';

			$html .= '<td>Mar</td>';

			$html .= '<td>Apr</td>';

			$html .= '<td>Mei</td>';

			$html .= '<td>Jun</td>';

			$html .= '<td>Jul</td>';

			$html .= '<td>Agu</td>';

			$html .= '<td>Sep</td>';

			$html .= '<td>Okt</td>';

			$html .= '<td>Nov</td>';

			$html .= '<td>Des</td>';

			$html .= '</tr>';

			

			$categories = Event::model()->getSkpd();

			$year = date('Y');

			foreach($categories as $category) 

			{

				$januari[] = $category->getEventReport($category->id,'01',$year)['0'];

				$februari[] = $category->getEventReport($category->id,'02',$year)['0'];

				$maret[] = $category->getEventReport($category->id,'03',$year)['0']; 

				$april[] = $category->getEventReport($category->id,'04',$year)['0']; 

				$mei[] = $category->getEventReport($category->id,'05',$year)['0']; 

				$juni[] = $category->getEventReport($category->id,'06',$year)['0'];

				$juli[] = $category->getEventReport($category->id,'07',$year)['0']; 

				$agustus[] = $category->getEventReport($category->id,'08',$year)['0']; 

				$september[] = $category->getEventReport($category->id,'09',$year)['0']; 

				$oktober[] = $category->getEventReport($category->id,'10',$year)['0']; 

				$november[] = $category->getEventReport($category->id,'11',$year)['0']; 

				$desember[] = $category->getEventReport($category->id,'12',$year)['0'];

			

			$html .= '<tr style="text-align:center">';

			$html .= '<td style="text-align:left">'.$category->name.'</td>';

			$html .= '<td>'.$category->getEventReport($category->id,'01',$year)['0'].'</td>';

			$html .= '<td>'.$category->getEventReport($category->id,'02',$year)['0'].'</td>';

			$html .= '<td>'.$category->getEventReport($category->id,'03',$year)['0'].'</td>';

			$html .= '<td>'.$category->getEventReport($category->id,'04',$year)['0'].'</td>';

			$html .= '<td>'.$category->getEventReport($category->id,'05',$year)['0'].'</td>';

			$html .= '<td>'.$category->getEventReport($category->id,'06',$year)['0'].'</td>';

			$html .= '<td>'.$category->getEventReport($category->id,'07',$year)['0'].'</td>';

			$html .= '<td>'.$category->getEventReport($category->id,'08',$year)['0'].'</td>';

			$html .= '<td>'.$category->getEventReport($category->id,'09',$year)['0'].'</td>';

			$html .= '<td>'.$category->getEventReport($category->id,'10',$year)['0'].'</td>';

			$html .= '<td>'.$category->getEventReport($category->id,'11',$year)['0'].'</td>';

			$html .= '<td>'.$category->getEventReport($category->id,'12',$year)['0'].'</td>';

			$html .= '</tr>';

			}

			$html .= '<tr style="text-align:center">';

			$html .= '<td>Total</td>';

			$html .= '<td>'.array_sum($januari).'</td>';

			$html .= '<td>'.array_sum($februari).'</td>';

			$html .= '<td>'.array_sum($maret).'</td>';

			$html .= '<td>'.array_sum($april).'</td>';

			$html .= '<td>'.array_sum($mei).'</td>';

			$html .= '<td>'.array_sum($juni).'</td>';

			$html .= '<td>'.array_sum($juli).'</td>';

			$html .= '<td>'.array_sum($agustus).'</td>';

			$html .= '<td>'.array_sum($september).'</td>';

			$html .= '<td>'.array_sum($oktober).'</td>';

			$html .= '<td>'.array_sum($november).'</td>';

			$html .= '<td>'.array_sum($desember).'</td>';

			$html .= '</tr>';

			$html .= '</table>';

			$pdf->writeHTML($html, true, false, false, false, '');

		

			$pdf->Output('Laporan_Jumlah_Agenda_Skpd_Tahun_'.date('Y').'.pdf', 'I');	

		}

		if($_POST['export_type']=='word')

		{

			spl_autoload_unregister(array('YiiBase','autoload'));

		

			Yii::import('application.extensions.PHPWord',true);

		

			spl_autoload_register(array('YiiBase', 'autoload'));

		

			$PHPWord = new PHPWord();

		

			$PHPWord->addFontStyle('bold', array('bold'=>true));

			$PHPWord->addFontStyle('subjudul', array('name'=>'Calibri','bold'=>true, 'size'=>11));

			$PHPWord->addFontStyle('normal', array('name'=>'Calibri','bold'=>false, 'size'=>9));

			$PHPWord->addFontStyle('judul', array('bold'=>true, 'size'=>16,'align'=>'center','name'=>'Times New Roman'));

		

			$tableStyle = array(

				'borderSize'=>0, 

				'borderColor'=>'000000', 

				'cellMargin'=>80,

				'border'=>true

			);

					

			$textTableStyle = array(

				'borderSize'=>0, 

				'borderColor'=>'ffffff', 

				'cellMargin'=>80,

				'border'=>true

			);			

		

			$PHPWord->addTableStyle('tableStyle', $tableStyle);

			$PHPWord->addTableStyle('textTableStyle', $textTableStyle);

			

			$center = array('align'=>'center');

		

			$fontStyle = array('name'=>'Calibri','bold'=>false, 'size'=>9,'align'=>'both');

		

			$fontHeadTable = array('name'=>'Calibri','bold'=>true, 'size'=>9);

		

			$paraStyle = array('spaceAfter'=>'2','size'=>9,'align'=>'both');

			$paraStyleCenter = array('spaceAfter'=>'2','size'=>9,'align'=>'center');

			$headStyle = array('spaceAfter'=>'2','align'=>'center','bold'=>true);

		

			$section = $PHPWord->createSection();

		

			$section->addText('Laporan Jumlah Agenda Tahun '.date('Y').'','judul',$center);		

		

			$section->addTextBreak(1);		

		

			$table = $section->addTable('textTableStyle');

		

			$table->addRow();

			$table->addCell(3000)->addText("Agenda",$fontStyle,$paraStyle);

			$table->addCell(500)->addText("Jan",$fontStyle,$paraStyle);

			$table->addCell(500)->addText("Feb",$fontStyle,$paraStyle);

			$table->addCell(500)->addText("Mar",$fontStyle,$paraStyle);

			$table->addCell(500)->addText("Apr",$fontStyle,$paraStyle);

			$table->addCell(500)->addText("Mei",$fontStyle,$paraStyle);

			$table->addCell(500)->addText("Jun",$fontStyle,$paraStyle);

			$table->addCell(500)->addText("Jul",$fontStyle,$paraStyle);

			$table->addCell(500)->addText("Agu",$fontStyle,$paraStyle);

			$table->addCell(500)->addText("Sep",$fontStyle,$paraStyle);

			$table->addCell(500)->addText("Okt",$fontStyle,$paraStyle);

			$table->addCell(500)->addText("Nov",$fontStyle,$paraStyle);

			$table->addCell(500)->addText("Des",$fontStyle,$paraStyle);

			

			$categories = Event::model()->getSkpd();

			$year = date('Y');

			foreach($categories as $category) 

			{

				$januari[] = $category->getEventReport($category->id,'01',$year)['0'];

				$februari[] = $category->getEventReport($category->id,'02',$year)['0'];

				$maret[] = $category->getEventReport($category->id,'03',$year)['0']; 

				$april[] = $category->getEventReport($category->id,'04',$year)['0']; 

				$mei[] = $category->getEventReport($category->id,'05',$year)['0']; 

				$juni[] = $category->getEventReport($category->id,'06',$year)['0'];

				$juli[] = $category->getEventReport($category->id,'07',$year)['0']; 

				$agustus[] = $category->getEventReport($category->id,'08',$year)['0']; 

				$september[] = $category->getEventReport($category->id,'09',$year)['0']; 

				$oktober[] = $category->getEventReport($category->id,'10',$year)['0']; 

				$november[] = $category->getEventReport($category->id,'11',$year)['0']; 

				$desember[] = $category->getEventReport($category->id,'12',$year)['0'];

				

				//ISI DATA

				$table->addRow();

				$table->addCell(3000)->addText($category->name,$fontStyle,$paraStyle);

				$table->addCell(500)->addText($category->getEventReport($category->id,'01',$year)['0'],$fontStyle,$paraStyle);

				$table->addCell(500)->addText($category->getEventReport($category->id,'02',$year)['0'],$fontStyle,$paraStyle);

				$table->addCell(500)->addText($category->getEventReport($category->id,'03',$year)['0'],$fontStyle,$paraStyle);

				$table->addCell(500)->addText($category->getEventReport($category->id,'04',$year)['0'],$fontStyle,$paraStyle);

				$table->addCell(500)->addText($category->getEventReport($category->id,'05',$year)['0'],$fontStyle,$paraStyle);

				$table->addCell(500)->addText($category->getEventReport($category->id,'06',$year)['0'],$fontStyle,$paraStyle);

				$table->addCell(500)->addText($category->getEventReport($category->id,'07',$year)['0'],$fontStyle,$paraStyle);

				$table->addCell(500)->addText($category->getEventReport($category->id,'08',$year)['0'],$fontStyle,$paraStyle);

				$table->addCell(500)->addText($category->getEventReport($category->id,'09',$year)['0'],$fontStyle,$paraStyle);

				$table->addCell(500)->addText($category->getEventReport($category->id,'10',$year)['0'],$fontStyle,$paraStyle);

				$table->addCell(500)->addText($category->getEventReport($category->id,'11',$year)['0'],$fontStyle,$paraStyle);

				$table->addCell(500)->addText($category->getEventReport($category->id,'12',$year)['0'],$fontStyle,$paraStyle);

			}

			//TOTAL

			$table->addRow();

			$table->addCell(3000)->addText('Total',$fontStyle,$paraStyle);

			$table->addCell(500)->addText(array_sum($januari),$fontStyle,$paraStyle);

			$table->addCell(500)->addText(array_sum($februari),$fontStyle,$paraStyle);

			$table->addCell(500)->addText(array_sum($maret),$fontStyle,$paraStyle);

			$table->addCell(500)->addText(array_sum($april),$fontStyle,$paraStyle);

			$table->addCell(500)->addText(array_sum($mei),$fontStyle,$paraStyle);

			$table->addCell(500)->addText(array_sum($juni),$fontStyle,$paraStyle);

			$table->addCell(500)->addText(array_sum($juli),$fontStyle,$paraStyle);

			$table->addCell(500)->addText(array_sum($agustus),$fontStyle,$paraStyle);

			$table->addCell(500)->addText(array_sum($september),$fontStyle,$paraStyle);

			$table->addCell(500)->addText(array_sum($oktober),$fontStyle,$paraStyle);

			$table->addCell(500)->addText(array_sum($november),$fontStyle,$paraStyle);

			$table->addCell(500)->addText(array_sum($desember),$fontStyle,$paraStyle);

		

			//EXPORT DATA

			$objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');

		

			$pathFile = Yii::app()->basePath.'/../exports/';

		

			$filename = 'Laporan_Jumlah_Agenda_Skpd_Tahun_'.date('Y').'.docx';

		

			$objWriter->save($pathFile.$filename);

		

			$this->redirect(Yii::app()->request->baseUrl.'/exports/'.$filename);

		}

	}

	

	public function actionSitemap()

    {

		$event=Event::model()->findAll(array(

			'order'=>'id DESC',

		));

		header('Content-Type: application/xml');

			$this->renderPartial('../event/sitemap',array('event'=>$event));

    }

	

	public function actionRead($id)

	{

		$this->layout = '//layouts/column2';

		

		$this->render('read',array(

			'model'=>$this->loadModel($id),

		));

	}

	

	public function actionReport()

	{

		$this->layout = '//layouts/column2';

		

		$this->render('report');

	}

	

	public function actionCalendar()

	{

		$this->layout = '//layouts/column2';

		

		$this->render('kalender');

	}



	/**

	* Displays a particular model.

	* @param integer $id the ID of the model to be displayed

	*/

	public function actionView($id)

	{

		$this->render('view',array(

			'model'=>$this->loadModel($id),

		));

	}



	/**

	* Creates a new model.

	* If creation is successful, the browser will be redirected to the 'view' page.

	*/

	public function actionCreate()

	{

		$model=new Event;

		

		// Uncomment the following line if AJAX validation is needed

		// $this->performAjaxValidation($model);



		if(isset($_POST['Event']))

		{

			$model->attributes=$_POST['Event'];

			date_default_timezone_set('Asia/Jakarta');

			$model->created_time = date('Y-m-d H:i:s');

			

			$thumbnail = CUploadedFile::getInstance($model,'thumbnail');

			

			if($thumbnail!==null)

				$model->thumbnail = str_replace(' ','-','_'.$thumbnail->name);

			

			if($model->save())

			{

				if($thumbnail!==null)

				{

						$path = Yii::app()->theme->basePath.'/uploads/event/';

						$thumbnail->saveAs($path.$model->thumbnail);

						if($thumbnail->getSize() > 64000) {

							$thumbnail = Yii::app()->image->load($path.$model->thumbnail);

							$thumbnail->resize(800, 600);

							$thumbnail->save();

						}

					}	

				

				Yii::app()->user->setFlash('success','Data berhasil ditambahkan');

				$this->redirect(array('view','id'=>$model->id));

			}

		}



		$this->render('create',array(

			'model'=>$model,

		));

	}



	/**

* Updates a particular model.

* If update is successful, the browser will be redirected to the 'view' page.

* @param integer $id the ID of the model to be updated

*/

	public function actionUpdate($id)

	{

		$model=$this->loadModel($id);

		$oldFile = $model->thumbnail;



		// Uncomment the following line if AJAX validation is needed

		// $this->performAjaxValidation($model);



		if(isset($_POST['Event']))

		{

			$model->attributes=$_POST['Event'];

			

			$thumbnail = CUploadedFile::getInstance($model,'thumbnail');

			

			if($thumbnail!==null)

				$model->thumbnail = str_replace(' ','-','_'.$thumbnail->name);

			else

				$model->thumbnail = $oldFile;

			

			if($model->save())

			{

				

					

					if($thumbnail!==null)

					{

						$path = Yii::app()->theme->basePath.'/uploads/event/';

						$thumbnail->saveAs($path.$model->thumbnail);

						if(file_exists($path.$oldFile) AND $oldFile!='')

							unlink($path.$oldFile);

					}

				

				Yii::app()->user->setFlash('success','Data berhasil ditambahkan');

				$this->redirect(array('view','id'=>$model->id));

			}

		}



		$this->render('update',array(

			'model'=>$model,

		));

	}

	

	public function saveLang($model)

	{

		$langs = Lang::model()->findAllByAttributes(array("active"=>1));

			

		foreach($langs as $lang) 

		{

			$eventLang = $model->getEventLang($lang->code);			

			$eventLang->attributes = $_POST[$lang->code]['EventLang'];

			$eventLang->save();

		}

	}



/**

* Deletes a particular model.

* If deletion is successful, the browser will be redirected to the 'admin' page.

* @param integer $id the ID of the model to be deleted

*/

	public function actionDelete($id)

	{

		$model=$this->loadModel($id);


		if(Yii::app()->request->isPostRequest)
		{

			if($this->loadModel($id)->delete())
			{
				$path = Yii::app()->basePath.'/../uploads/event/'; 	
				
				if(file_exists($path.$model->thumbnail) AND $model->thumbnail != '')
					unlink($path.$model->thumbnail);			

				if(!isset($_GET['ajax']))
					$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));

			}
		}

		else

			throw new CHttpException(400,

                        'Invalid request. Please do not repeat this request again.');

	}



	/**

	* Lists all models.

	*/

	public function actionIndex($id=null)

	{

		$this->layout = '//layouts/column2';

		

		$eventCategory = '';

		

		if($id!=null)

			$eventCategory = EventCategory::model()->findByPk($id);

		

		if($id!=null)

			$dataProvider=new CActiveDataProvider('Event',array(

				'criteria'=>array(

					'condition'=>'event_category_id='.$id.'',

					'order'=>'created_time DESC',

				)

			));

		else

			$dataProvider=new CActiveDataProvider('Event',array(

				'criteria'=>array(

					'order'=>'created_time DESC',

				)

			));

		

		$this->render('index',array(

			'dataProvider'=>$dataProvider,

			'eventCategory'=>$eventCategory

		));

	}



	/**

	* Manages all models.

	*/

	public function actionAdmin()

	{

		$model=new Event('search');

		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['Event']))

			$model->attributes=$_GET['Event'];



		$this->render('admin',array(

			'model'=>$model,

		));

	}



/**

* Returns the data model based on the primary key given in the GET variable.

* If the data model is not found, an HTTP exception will be raised.

* @param integer the ID of the model to be loaded

*/

public function loadModel($id)

{

$model=Event::model()->findByPk($id);

if($model===null)

throw new CHttpException(404,'The requested page does not exist.');

return $model;

}



/**

* Performs the AJAX validation.

* @param CModel the model to be validated

*/

protected function performAjaxValidation($model)

{

if(isset($_POST['ajax']) && $_POST['ajax']==='event-form')

{

echo CActiveForm::validate($model);

Yii::app()->end();

}

}

}

