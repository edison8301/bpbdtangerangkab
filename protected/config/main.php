<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'bandunglan',
	'id'=>'bandunglan',
	
	'theme'=>'bpbdtangerangkab',

	// preloading 'log' component
	'preload'=>array('log','booster'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.extensions.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'bisabisa',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
			'generatorPaths'=>array(
                'bootstrap.gii',
            ), 
		),
		'jbackup'=>array(
            'path'=>Yii::app()->basePath.'/../backups/', //Directory where backups are saved
            'layout' => '//layouts/administrator/column2', //2-column layout to display the options
            'filter' => 'accessControl', //filter or filters to the controller
            'bootstrap' => true, //if you want the module use bootstrap components
            'download' => true, // if you want the option to download
            'restore' => false, // if you want the option to restore
            'database' => true, //whether to make backup of the database or not
            //directory to consider for backup, must be made array key => value array ($ alias => $ directory)
            'directoryBackup'=>array( 
				//array(''=>Yii::app()->basePath.'/../css/')
            ),
            //directory sebe not take into account when the backup
            'excludeDirectoryBackup'=>array(
                //array(''=>Yii::app()->basePath.'/../css/')
            ),
            //files sebe not take into account when the backup
            'excludeFileBackup'=>array(
                __DIR__.'/../../folder/folder1/cfile.png',
            ),
            //directory where the backup should be done default Yii::getPathOfAlias('webroot')
            'directoryRestoreBackup'=>__DIR__.'/../../' 
        ),
		
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			
		),
		
		'fusioncharts'=>array(
			'class'=>'ext.fusioncharts.fusionCharts'
		),
		'advancedFilters'=>array(
			'class'=>'ext.advancedFilters.AdvancedFilters',
		),
		// uncomment the following to enable URLs in path-format
		/*
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		*/
		/*'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),*/
		// uncomment the following to use a MySQL database
		
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=bpbdtangerangkab',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),

		'booster'=>array(
            'class' => 'ext.booster.components.Booster',
        ),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);