<?php
$this->breadcrumbs=array(
	'Link Categories'=>array('index'),
	$model->title,
);
?>

<h1>View Link Category</h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type'=>'striped bordered',
'attributes'=>array(
		'id',
		'title',
),
)); ?>
