<?php
$this->breadcrumbs=array(
	'Link Categories'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List LinkCategory','url'=>array('index')),
array('label'=>'Create LinkCategory','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('link-category-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Manage Link Categories</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Add',
		'icon'=>'plus',
		'context'=>'primary',
		'url'=>array('linkcategory/create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Link',
		'icon'=>'list',
		'context'=>'primary',
		'url'=>array('link/admin')
)); ?>

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'link-category-grid',
'type'=>'striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'title',
array(
'class'=>'booster.widgets.TbButtonColumn',
),
),
)); ?>
