<?php
$this->breadcrumbs=array(
	'Link Categories',
);

$this->menu=array(
array('label'=>'Create LinkCategory','url'=>array('create')),
array('label'=>'Manage LinkCategory','url'=>array('admin')),
);
?>

<h1>Link Categories</h1>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
