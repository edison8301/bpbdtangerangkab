<?php
$this->breadcrumbs=array(
	'Link Categories'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List LinkCategory','url'=>array('index')),
array('label'=>'Manage LinkCategory','url'=>array('admin')),
);
?>

<h1>Add Link Category</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>