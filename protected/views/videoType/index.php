<?php
$this->breadcrumbs=array(
	'Video Types',
);

$this->menu=array(
array('label'=>'Create VideoType','url'=>array('create')),
array('label'=>'Manage VideoType','url'=>array('admin')),
);
?>

<h1>Video Types</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
