<?php
$this->breadcrumbs=array(
	'Video Types'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List VideoType','url'=>array('index')),
array('label'=>'Manage VideoType','url'=>array('admin')),
);
?>

<h1>Create VideoType</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>