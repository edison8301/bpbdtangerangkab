<?php
$this->breadcrumbs=array(
	'Video Types'=>array('index'),
	$model->title,
);

$this->menu=array(
array('label'=>'List VideoType','url'=>array('index')),
array('label'=>'Create VideoType','url'=>array('create')),
array('label'=>'Update VideoType','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete VideoType','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage VideoType','url'=>array('admin')),
);
?>

<h1>View VideoType #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'title',
),
)); ?>
