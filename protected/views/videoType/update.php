<?php
$this->breadcrumbs=array(
	'Video Types'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List VideoType','url'=>array('index')),
	array('label'=>'Create VideoType','url'=>array('create')),
	array('label'=>'View VideoType','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage VideoType','url'=>array('admin')),
	);
	?>

	<h1>Update VideoType <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>