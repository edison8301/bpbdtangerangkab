<?php
$this->breadcrumbs=array(
	'User'=>array('index'),
	'Kelola',
);
?>
<h1>Kelola User</h1>
<?php $this->widget('booster.widgets.TbButton',array(		
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'plus',
		'label'=>'Tambah',
		'url'=>array('user/create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'user-grid',
		'dataProvider'=>$model->search(),
		'type'=>'striped bordered hover',
		'filter'=>$model,
		'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'headerHtmlOptions'=>array('width'=>'5%'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			'username',
			array(
				'header'=>'Role',
				'name'=>'role_id',
				'value'=>'$data->Role->nama',	
				'filter'=>CHtml::listData(Role::model()->findAll(),'id','nama'),
				'headerHtmlOptions'=>array('width'=>'20%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'headerHtmlOptions'=>array('width'=>'10%'),
			),
		),
)); ?>
