<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(

	'id'=>'user-form',

	'enableAjaxValidation'=>false,

)); ?>



<p class="help-block">Fields with <span class="required">*</span> are required.</p>



<?php echo $form->errorSummary($model); ?>



	<?php echo $form->textFieldGroup($model,'username',array('class'=>'span5','maxlength'=>255)); ?>



	<?php echo $form->passwordFieldGroup($model,'password',array('class'=>'span5','maxlength'=>255)); ?>



	<?php echo $form->dropDownListGroup($model,'role_id',array(
			'widgetOptions'=>array(
				'data'=>CHtml::listData(Role::model()->findAll(),'id','nama')
		)
	)); ?>
	

	<div class="form-actions well">

	<?php $this->widget('booster.widgets.TbButton', array(

			'buttonType'=>'submit',

			'context'=>'primary',

			'icon'=>'ok white',
			'label'=>'Simpan',

		)); ?>

	</div>



<?php $this->endWidget(); ?>





<script>

	function refreshUserRole()

	{

		$('.user_attribute').hide('slow');

		

		if($('#User_role_id').val()==2)

		{					

			$('.user_attribute_kategori_id').show('slow');

		}

		

			



	}

		

	$(document).ready(function() 

	{	

		$('.user_attribute').hide();

		

		if($('#User_role_id').val()==2)

		{					

			$('.user_attribute_kategori_id').show();

		}		

	});

		

	$('#User_role_id').change(function() {

			refreshUserRole();

	});

		

		

		

		

</script>