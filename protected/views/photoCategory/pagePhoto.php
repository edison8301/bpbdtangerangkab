<?php
/* @var $this SiteController */
$this->pageTitle=Yii::app()->name;
?>

<div class="site-index">
<div class="row-fluid" style="margin-bottom: 20px">
    <div class="span12 panel-bg" style="text-align:justify">
	<h2 class="list">Gallery Foto</h2>
		<ul class="gallery group 3-col">
			<li  class="item span3"><a href="<?php echo Yii::app()->request->baseUrl; ?>/uploads/photo/<?php echo $model->file; ?>" rel="prettyPhoto[gallery1]" title="<?php echo $model->title; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/photo/<?php echo $model->file; ?>" alt="<?php echo $model->title; ?>" /></a></li>
		</ul>
	</div>
</div>
</div>
