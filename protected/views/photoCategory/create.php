<?php
$this->breadcrumbs=array(
	'Photo Categories'=>array('admin'),
	'Create',
);

?>

<h1>Tambah Kategori Foto</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>