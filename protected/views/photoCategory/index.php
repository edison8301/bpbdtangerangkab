<?php
$this->breadcrumbs=array(
	'Photo Categories',
);

$this->menu=array(
array('label'=>'Create PhotoCategory','url'=>array('create')),
array('label'=>'Manage PhotoCategory','url'=>array('admin')),
);
?>

<h1>Photo Categories</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
