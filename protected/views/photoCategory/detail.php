<?php
$this->breadcrumbs=array(
	'Photos',
);

$this->menu=array(
array('label'=>'Create Photo','url'=>array('create')),
array('label'=>'Manage Photo','url'=>array('admin')),
);
?>

<div class="margin20" id="post-read">

	<h3 class="list-bottom"><?php print strtoupper($model->title); ?></h3>
	<hr>
	<h1>Photos</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
		'dataProvider'=>$dataProvider,
		'itemView'=>'_detail',
)); ?>

</div>