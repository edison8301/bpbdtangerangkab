<?php
$this->breadcrumbs=array(
	'Photo Categories'=>array('admin'),
	'Manage',
);
?>

<h1>Kelola Kategori Foto</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'plus white',
		'label'=>'Tambah Kategori',
		'url'=>array('photoCategory/create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'list white',
		'label'=>'Kelola Foto',
		'url'=>array('photo/admin')
)); ?>&nbsp;



<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'photo-category-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'title',
			array(
				'class'=>'booster.widgets.TbButtonColumn',
			),
		),
)); ?>
