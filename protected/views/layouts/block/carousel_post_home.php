<?php
	$postCategory = PostCategory::model()->findByPk($post_category_id);
?>
<h3 class="list-bottom"><?php print $block->title; ?></h3>			
<hr>						
<div id="block-<?php print $block->id; ?>" class="row-fluid" style="width:590px;height:150px;display:none">			
	<?php foreach(Post::model()->getLatestPostByCategoryId($post_category_id,10) as $model) { ?>				
	<div class="float-left" style="width:200px;height:250px">					
		<div class="news-img"><?php echo $model->getThumbnail(array('width'=>'180px')); ?></div>					
		<div class="tittle-news">
			<?php echo CHtml::link($model->title, array('/post/read&id='.$model->id));?>
		</div><br>				
	</div>											
	<?php } ?>						
</div>

<script>
	$(document).ready(function() {
		$('#block-<?php print $block->id; ?>').show();
	});
</script>

<?php $this->widget('ext.carouFredSel.ECarouFredSel',array(					
		'id' => 'carousel',					
		'target' => '#block-'.$block->id,					
		'config' => array(						
			'items'=>3,						
			'scroll'=>array(							
				'fx'=>'directscroll',							
				'items'=>2,							
				'pauseOnHover'=>true						
			),						
			'height'=>'150px',						
			'pagination'=>array(							
				'items'=>2						
			)					
		)			
)); ?>	