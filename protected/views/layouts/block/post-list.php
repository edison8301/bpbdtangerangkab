<?php $display_mode = $block->getBlockAttributeValueByKey('post_list_display_mode'); ?>
<?php $post_category_id = $block->getBlockAttributeValueByKey('post_list_post_category_id'); ?>
<?php $limit = $block->getBlockAttributeValueByKey('post_list_limit'); ?>

<div class="block">
<div class='block-title'>
	<span><?php print $block->title; ?></span>
</div>
<div class="block-content" >

<?php if($display_mode=="frontpage") { ?>	
	<?php $i=1; foreach(Post::model()->getLatestPostByCategoryId($post_category_id,$limit) as $model){ ?>
	<?php if($i==1) { ?>
	<div class="row" style="margin-bottom:10px">
		<div class="col-xs-12">
			<div style="border:1px solid #999999;padding:8px;background:#ffffff;-webkit-border-radius: 3px 3px 3px 3px;border-radius: 3px 3px 3px 3px;">
				<div style="margin-bottom:5px">	
					<?php print $model->getImage(array('class'=>'img-responsive')); ?>
				</div>
				<div style="font-weight:bold;margin-bottom:5px;">
					<?php echo CHtml::link($model->title, array('/post/read&id='.$model->id));?>
				</div>
				<div style="margin-bottom:10px;font-size:11px">
					<i class="glyphicon glyphicon-calendar"></i> <?php print Helper::getTanggalDibuat($model->time_created); ?>
				</div>				
				<div>
					<?php print $model->getExcerpt(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php } else { ?>
	<div class="post-list-item" style="margin-bottom:10px;">
	<div class="row" style="margin-bottom:5px">
		<div class="col-xs-12" style="font-weight:none">				
			<?php echo CHtml::link($model->title, array('/post/read&id='.$model->id));?>				
		</div>					
	</div>
	<div class='row' style="margin-bottom:5px;">
		<div class="col-xs-3" style="padding-right:0px">
			<div style="border:1px solid #666666;padding:3px;background:#ffffff;-webkit-border-radius: 3px 3px 3px 3px;border-radius: 3px 3px 3px 3px;">
				<?php echo $model->getImage(array('class'=>'img-responsive')); ?>
			</div>
		</div>
		<div class="col-xs-9">
			<div>
				<?php echo $model->getExcerpt();?>
				<?php echo CHtml::link('[Selanjutnya]', array('/post/read','id'=>$model->id));?></p>
			</div>
			<div style="margin-bottom:5px;font-size:11px"><i class="glyphicon glyphicon-calendar"></i> <?php print Helper::getTanggalDibuat($model->time_created); ?></div>				
		</div>
	</div>
	</div><!-- .post-list-item -->
	<?php } //endif ?>
	<?php $i++; } //foreach ?>


<?php } ?>


<?php if($display_mode=="title") { ?>
	<?php foreach(Post::model()->getLatestPostByCategoryId($post_category_id,$limit) as $model){ ?>
	<div class='row' style="margin-bottom:5px;">
		<div class="col-xs-12"><?php echo CHtml::link($model->title, array('/post/read&id='.$model->id));?></div><br>
	</div>					
	<?php } ?>
<?php } ?>

<div id="postlist" style="font-weight:normal; font-size: 13px;">
	<?php $i=1; foreach(Bencana::model()->findAll(array('order' => 'tanggal DESC')) as $bencana) {
		if ($i++ == 5) break;
	 ?>
	 <hr>
	<div><i class="glyphicon glyphicon-calendar"></i> <?php print Helper::getTanggalDibuat($bencana->tanggal); ?></div> 
	<div><b>Bencana : </b><p><?php echo $bencana->getBencanaJenis();?></p></div>
	<div><b>Lokasi  : </b><p><?php echo $bencana->lokasi;?></p></div>
	<div><b>Korban Materi  : </b><p>Rp.<?php echo $bencana->korban_materi;?></p></div>

	<?php } ?>
</div>




<?php if($display_mode=="title_image_excerpt") { ?>
	<?php foreach(Post::model()->getLatestPostByCategoryId($post_category_id,$limit) as $model){ ?>
	<div class="post-list-item" style="margin-bottom:10px;">
	<div class="row" style="margin-bottom:5px">
		<div class="col-xs-12" style="font-weight:none">				
			<?php echo CHtml::link($model->title, array('/post/read&id='.$model->id));?>				
		</div>					
	</div>
	<div class='row' style="margin-bottom:5px;">
		<div class="col-xs-3" style="padding-right:0px">
			<div style="border:1px solid #666666;padding:3px;background:#ffffff;-webkit-border-radius: 3px 3px 3px 3px;border-radius: 3px 3px 3px 3px;">
				<?php echo $model->getImage(array('class'=>'img-responsive')); ?>
			</div>
		</div>
		<div class="col-xs-9">
			<div>
				<?php echo $model->getExcerpt();?>
				<?php echo CHtml::link('[Selanjutnya]', array('/post/read','id'=>$model->id));?></p>
			</div>
			<div style="margin-bottom:5px;font-size:11px"><i class="glyphicon glyphicon-calendar"></i> <?php print Bantu::getTanggalDibuat($model->time_created); ?></div>				
		</div>
	</div>
	</div><!-- .post-list-item -->
	<?php } ?>
<?php } ?>


<div style="border-top:0px solid #eeeeee;padding-top:8px;text-align:right;font-size:11px">
	<?php print CHtml::link('<i class="glyphicon glyphicon-list"></i> Index '.$block->title,array('Bencana/index')); ?>
</div>

</div><!-- .block-content -->

</div><!-- .block -->

<script>
	$(document).ready(function() {
		$('#postlist').show();
	});
</script>

<?php $this->widget('ext.carouFredSel.ECarouFredSel',array(					
		'id' => 'carousel',					
		'target' => '#postlist',					
		'config' => array(						
			'items'=>7,
			'direction'=>'up',
			'scroll'=>array(							
				'fx'=>'directscroll',							
				'items'=>5,							
				'pauseOnHover'=>true						
			),						
			'height'=>'150px',						
			'pagination'=>array(							
				'items'=>2						
			)					
		)			
)); ?>	