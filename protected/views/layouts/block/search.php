<div class="block">
	<div class="block-title"><?php print $block->title; ?></div>

	<div class="block-content">
		<?php print CHtml::beginForm(array('site/pencarian'),'GET'); ?>
		<?php print CHtml::textField('q','',array('class'=>'form-control','style'=>'margin-bottom:10px')); ?>
		<div style="text-align:right">
		<?php $this->widget('booster.widgets.TbButton',array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'label'=>'Cari',
			'icon'=>'search',
			'htmlOptions'=>array('style'=>'margin-bottom:10px;text-align:right')
		)); ?>
		</div>
		<?php print CHtml::endForm(); ?>
	</div>
</div>