<div class="block event-list-recent">

	<div class='block-title'>

		<span><?php print $block->title; ?></span>

	</div>

	<div class="block-content" id="block-<?php print $block->id; ?>">

	<?php foreach(Event::model()->getLatestEvent(5) as $model){ ?>

	<div class='row' style="margin-bottom:20px;">

		<div class="col-xs-3" style="margin-right:0px;padding-right:0px;text-align:center">

			<?php print CHtml::image(Yii::app()->baseUrl.'/images/calendar.png','',array('class'=>'img-responsive','style'=>'margin-left:auto;margin-right:auto')); ?>

			<?php echo date('j M',strtotime($model->date)); ?>

		</div>

		<div class="col-xs-9"><?php echo CHtml::link($model->title, array('/event/read&id='.$model->id));?></div><br>

	</div>					

	<?php } ?>

	</div>

</div>



<?php $this->widget('ext.carouFredSel.ECarouFredSel',array(					

		'id' => 'carousel',					

		'target' => '#block-'.$block->id,					

		'config' => array(						

			'items'=>3,

			'direction'=>'up',

			'scroll'=>array(							

				'fx'=>'directscroll',							

				'items'=>2,							

				'pauseOnHover'=>true						

			),						

			'height'=>'150px',						

			'pagination'=>array(							

				'items'=>2						

			)					

		)			

)); ?>	

			