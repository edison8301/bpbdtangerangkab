<?php $display_mode = $block->getBlockAttributeValueByKey('post_list_display_mode'); ?>
<?php $post_category_id = $block->getBlockAttributeValueByKey('post_list_post_category_id'); ?>
<div class="block">
<div class='block-title'>
	<span><?php print $block->title; ?></span>
</div>
<div class="block-content" >
	<div class="row" style="margin-bottom:10px">
		<div class="col-xs-6">
			<?php $i=1; foreach(Post::model()->getLatestPostByCategoryId($post_category_id,5) as $model){ ?>
				<?php if($i==1) { ?>	
					<div style="margin-bottom:5px">	
						<?php print $model->getImage(array('class'=>'img-responsive')); ?>
					</div>
					<div style="font-weight:bold;margin-bottom:5px;">
						<?php echo CHtml::link($model->title, array('/post/read&id='.$model->id));?>
					</div>
					<div style="margin-bottom:10px;font-size:11px">
						<i class="glyphicon glyphicon-calendar"></i> <?php print Helper::getTanggalDibuat($model->time_created); ?>
					</div>				
					<div>
						<?php print $model->getExcerpt(); ?>
					</div>
				<?php } ?>
			<?php $i++; } ?>
		</div>
		<div class="col-xs-6" style="margin-bottom: 10px;">
			<?php $i=1; foreach(Post::model()->getLatestPostByCategoryId($post_category_id,5) as $model){ ?>
				<?php if($i!=1) { ?>
				<div class="row" style="margin-bottom:10px">	
					<div class="col-xs-4" style="padding:0px">
						<div>
							<?php echo $model->getImage(array('class'=>'img-responsive')); ?>
						</div>
					</div>			
					
					<div class="col-xs-8">
						<div style="font-weight:bold;margin-bottom:5px; font-size: 12px;">
							<?php echo CHtml::link($model->title, array('/post/read&id='.$model->id));?>
						</div>
						<div>
							<?php print $model->getExcerpt(50); ?>
						</div>
					</div>
				</div>
				<?php } ?>
				<?php $i++; } ?>
		</div>
	</div><!-- .row -->
	<div style="border-top:0px solid #eeeeee;padding-top:8px;text-align:right;font-size:11px">
		<?php print CHtml::link('<i class="glyphicon glyphicon-list"></i> Index '.$block->title,array('postCategory/read','id'=>$post_category_id)); ?>
	</div>
</div><!-- .block-content -->
</div><!-- .block -->