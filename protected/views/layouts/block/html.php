<?php $html = $block->getBlockAttributeValueByKey('html'); ?>
<div class="block link-list">
	<div class='block-title'>
		<span><?php print $block->title; ?></span>
	</div>

	<div class="block-content">
		<?php print $html; ?>
	</div>
</div>		