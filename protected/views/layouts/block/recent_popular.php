<h4 class='block-title' >INOVASI</h4>

<hr>

<?php $this->widget('bootstrap.widgets.TbTabs',array(
					'type' => 'tabs', // 'tabs' or 'pills'
					'tabs' => array(
						array(
							'label' => 'Terpopuler',
							'content' => $this->renderPartial('//layouts/block/_inovasi_terpopuler',array('kategori_id'=>1),$this),
							'active' => true
						),
						array(
							'label' => 'Terbaru',
							'content' => $this->renderPartial('//layouts/block/_inovasi_terbaru',array('kategori_id'=>2),$this),
						),
					),
			)); ?>


