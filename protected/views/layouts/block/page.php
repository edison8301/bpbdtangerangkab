<?php $page_id = $block->getBlockAttributeValueByKey('page_page_id'); ?>
<?php $page = Page::model()->findByPk($page_id); ?>
<div class="block link-list">
	<div class='block-title'>
		<span><?php print $block->title; ?></span>
	</div>

	<div class="block-content">
		<?php print $page->content; ?>
	</div>
</div>		