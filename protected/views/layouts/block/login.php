<?php if(!Yii::app()->user->isGuest) { ?>		
		<div class="block" id="login">			
			<div class="block-title">				
				<span>LOGOUT PEGAWAI</span>			
			</div>			
			<div class="block-content" style="padding-top:10px">				
				<?php $this->widget('booster.widgets.TbButton',array(						
						'buttonType'=>'link',						
						'label'=>'LOGOUT',						
						'icon'=>'off',						
						'url'=>array('site/logout')
				)); ?>			
			</div>		
		</div>
		<?php } else { ?>		
		<div class="block" id="login">			
			<div class="block-title">				
				<span>LOGIN PEGAWAI</span>			
			</div>			
			<div class="block-content">				
				<?php print CHtml::beginForm('http://simpeg.bandung.lan.go.id/v2/index.php?r=site/remoteLogin'); ?>				
				<?php print CHtml::textField('LoginForm[username]','',array('placeholder'=>'Username','class'=>'form-control','onfocus'=>'this.placeholder = ""','onblur'=>'this.placeholder = "Username"')); ?>				
				<?php print CHtml::passwordField('LoginForm[password]','',array('placeholder'=>'Password','class'=>'form-control','onfocus'=>'this.placeholder = ""','onblur'=>'this.placeholder = "Password"')); ?>				
				<?php print CHtml::hiddenField('returnUrl','http://bandung.lan.go.id/v2'); ?>				
				<?php $this->widget('booster.widgets.TbButton',array(						
						'buttonType'=>'submit',						
						'label'=>'LOGIN',						
						'icon'=>'lock',				
				)); ?>				
				<?php print CHtml::endForm(); ?>			
			</div>		
		</div>		
<?php } ?>	