<?php $text = $block->getBlockAttributeValueByKey('text'); ?>
<div class="block link-list">
	<div class='block-title'>
		<span><?php print $block->title; ?></span>
	</div>

	<div class="block-content">
		<?php print $text; ?>
	</div>
</div>		