<h4 class='block-title' >SITE MAP</h4>
<hr>
<div class="row-fluid" id="sitemap-content" style="display:none;">
	<?php $dataTree = Menu::model()->getDataTreeItem(); ?>
	<?php $this->widget('CTreeView',array(
			'data'=>$dataTree,
			'animated'=>'fast', //quick animation
			'collapsed'=>'true',//remember must giving quote for boolean value in here
			'htmlOptions'=>array(
				'class'=>'filetree',//there are some classes that ready to use
			),
	)); ?>
</div><script>	$(document).ready(function() {		$('#sitemap-content').show();	});</script>
<div>&nbsp;</div>