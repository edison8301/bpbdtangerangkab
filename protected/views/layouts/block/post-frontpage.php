<div class='block-title'><?php print $block->title; ?></div>

<?php if($display_thumbnail=="ya") { ?>
<div class="block-content">
	<?php foreach(Post::model()->getLatestPostByCategoryId($post_category_id) as $model){ ?>
	<div class='row' style="margin-bottom:10px;">
		<div class="col-xs-3"><?php echo $model->getImage(array('class'=>'img-responsive')); ?></div>
		<div class="col-xs-9"><?php echo CHtml::link($model->title, array('/post/read&id='.$model->id));?></div><br>
	</div>					
	<?php } ?>
</div>
<?php } else { ?>
<div class="block-content">
	<?php foreach(Post::model()->getLatestPostByCategoryId($post_category_id) as $model){ ?>
	<div class='row' style="margin-bottom:10px;">
		<div class="col-xs-12"><?php echo CHtml::link($model->title, array('/post/read&id='.$model->id));?></div><br>
	</div>					
	<?php } ?>
</div>
<?php } ?>
			
<div>&nbsp;</div>