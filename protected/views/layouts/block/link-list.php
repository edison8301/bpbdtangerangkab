<?php $link_category_id = $block->getBlockAttributeValueByKey('link_list_link_category_id'); ?>

<?php $display_mode = $block->getBlockAttributeValueByKey('link_list_display_mode'); ?>

<div class="block link-list">

	<div class='block-title'>

		<span><?php print $block->title; ?></span>

	</div>



	<div class="block-content">

		<?php if($display_mode == 'text') { ?>

		<ul class="text">

		<?php foreach(Link::model()->findAllByAttributes(array('link_category_id'=>$link_category_id)) as $model) { ?>

			<?php $htmlOptions = array(); ?>
			<?php if($model->new_tab == 1) $htmlOptions = array('target'=>'_blank'); ?>
			<li><?php echo CHtml::link($model->title, $model->url,$htmlOptions);?></li>					

		<?php } ?>

		</ul>

		<?php } ?>

		<?php if($display_mode == 'image') { ?>

		<ul class="image">

		<?php foreach(Link::model()->findAllByAttributes(array('link_category_id'=>$link_category_id)) as $model) { ?>

			<?php $htmlOptions = array(); ?>
			<?php if($model->new_tab == 1) $htmlOptions = array('target'=>'_blank'); ?>
			<li><?php echo CHtml::link($model->getImage(), $model->url,$htmlOptions);?></li>	

		<?php } ?>

		</ul>	

		<?php } ?>

	</div>

</div>		