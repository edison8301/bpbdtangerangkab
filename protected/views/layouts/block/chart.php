
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/vendors/fusioncharts/js/FusionCharts.js"); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/vendors/fusioncharts/js/themes/FusionCharts.theme.fint.js"); ?>
<script>
<?php 
  $tahun = date('Y');
?>
FusionCharts.ready(function(){
      var revenueChart = new FusionCharts({
        "type": "column3d",
        "renderAt": "grafik-bulan",
        "width": "550",
        "height": "400",
        "dataFormat": "json",
        "dataSource": {
          "chart": {
              "caption" : "Grafik Jumlah Kejadian Bencana Tahun <?php print $tahun; ?>",
              "xAxisName": "Bulan",
              "yAxisName": "Jumlah Kejadian",
              "theme": "fint"
           },
          "data": 
              [ <?php print Bencana::getChartDataByYear($tahun); ?> ]
        }
    });

    revenueChart.render();
})		
</script>
<div class="block">
  <div class='block-title'>
    <span><?php print $block->title; ?></span>
  </div>
  <div class="block-content" >
      <div id="tahun" class="box">      
          <div id="grafik-bulan"> FusionChart XT will load here! </div>
      </div>
  </div>
</div>
