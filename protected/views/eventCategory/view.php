<?php
$this->breadcrumbs=array(
	'Event Categories'=>array('index'),
	$model->title,
);
?>

<h1>View Event Category</h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type'=>'striped bordered',
'attributes'=>array(
		'id',
		'title',
),
)); ?>
