<?php
$this->breadcrumbs=array(
	'Event Categories'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);
?>

	<h1>Update Event Category</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>