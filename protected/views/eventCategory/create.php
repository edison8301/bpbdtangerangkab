<?php
$this->breadcrumbs=array(
	'Event Categories'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List EventCategory','url'=>array('index')),
array('label'=>'Manage EventCategory','url'=>array('admin')),
);
?>

<h1>Add Event Category</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>