<?php
$this->breadcrumbs=array(
	'Event Categories'=>array('index'),
	'Manage',
);
?>

<h1>Kelola Kategori Event</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah',
		'icon'=>'plus',
		'context'=>'primary',
		'url'=>array('eventCategory/create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Kelola Event',
		'icon'=>'list',
		'context'=>'primary',
		'url'=>array('event/admin')
)); ?>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'event-category-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'title',
			array(
				'class'=>'booster.widgets.TbButtonColumn',
			),
		),
)); ?>
