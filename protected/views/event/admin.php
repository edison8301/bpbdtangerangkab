<?php
$this->breadcrumbs=array(
	'Event'=>array('index'),
	'Manage',
);
?>

<h1>Kelola Event</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah',
		'icon'=>'plus',
		'context'=>'primary',
		'url'=>array('event/create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Kategori',
		'icon'=>'list',
		'context'=>'primary',
		'url'=>array('eventCategory/admin')
)); ?>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'event-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'ajaxUpdate'=>false,
		'filter'=>$model,
		'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'headerHtmlOptions'=>array('width'=>'5%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'title',
				'value'=>'$data->title',
				'headerHtmlOptions'=>array('width'=>'30%','style'=>'text-align:center'),
				'htmlOptions'=>array()
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'date',
				'value'=>'$data->date',
				'headerHtmlOptions'=>array('width'=>'15%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'time',
				'value'=>'$data->time',
				'headerHtmlOptions'=>array('width'=>'15%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'event_category_id',
				'value'=>'$data->getRelationField("EventCategory","title")',
				'filter'=>CHtml::listData(EventCategory::model()->findAll(),'id','title'),
				'headerHtmlOptions'=>array('width'=>'15%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'time_created',
				'value'=>'$data->time_created',
				'filter'=>CHtml::listData(EventCategory::model()->findAll(),'id','title'),
				'headerHtmlOptions'=>array('width'=>'15%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center')
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
			),
		),
)); ?>
