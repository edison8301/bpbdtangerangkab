<?php echo '<?xml version="1.0" encoding="UTF-8"?>' ?>

<urlset
        xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
                http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">

<?php foreach($event as $model): ?>
        <url>
        <loc><?php echo CHtml::encode($this->createAbsoluteUrl('event/view',array('id'=>$model->id))); ?></loc>
        <changefreq>weekly</changefreq>
        <priority>0.5</priority>
        </url>
<?php endforeach; ?>

</urlset>