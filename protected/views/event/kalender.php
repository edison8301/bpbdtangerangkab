<?php $this->breadcrumbs=array(
		'Kalender Kegiatan'
	); ?>

<div id="read">

	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif ?>
	
	<?php $this->widget('ext.EFullCalendar.EFullCalendar', array(
	'lang'=>Helper::getLang(),
	'themeCssFile'=>'cupertino/theme.css',
	'htmlOptions'=>array(
		'style'=>'width:100%'
	),
	'options'=>array(
		'header'=>array(
			'left'=>'prev,next',
			'center'=>'title',
			'right'=>'today,agendaWeek,month',
		),
	'lazyFetching'=>true,
	'events'=>Event::model()->getEventForCalendar(),
	)
	)); ?>
	
</div>

<div>&nbsp;</div>

<div style="font-family:PTSans">
<?php $events = Event::model()->getEventYearly(); ?>
	<?php if($events!=null) { ?>
	<?php foreach($events as $event) { ?>
		<ul>
			<li><?php print '('.date("j-M-Y",strtotime($event->date)).')'; ?> <?php print $event->getEventLang()->title; ?></li>
		</ul>
	<?php } ?>
<?php } else print "Tidak ada kegiatan" ?>
</div>