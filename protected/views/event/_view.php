<div class="item">
	<div class="row">
		<div class="col-lg-3 col-md-3">
			<?php print $data->getThumbnail(); ?>
		</div>
		<div class="col-lg-9 col-md-9">
			<h2><?php echo CHtml::link($data->title,array('event/read','id'=>$data->id)); ?></h2>
			<div class="event-date">Tanggal Kegiatan : <?php echo Helper::getCreatedDate($data->date); ?></div>
			<div class="event-place">Lokasi : <?php echo $data->place; ?></div>
			<div class="event-category"><?php echo $data->getRelationField("EventCategory","title"); ?></div>
		</div>
	</div>
</div>
