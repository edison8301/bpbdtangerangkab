<?php $this->breadcrumbs=array(
		$model->getRelationField('EventCategory','title')=>array('event/index','id'=>$model->event_category_id),
		$model->title
); ?>
	

<?php $this->pageTitle = $model->title; ?>

<div class="margin20" id="centerbar">

	<div class="block-title">
		<span><?php print $model->title; ?></span>
	</div>
	
	<div>&nbsp;</div>
	
	<table class="table">
	<tr>
		<th width="25%" style="text-align:right">Nama Kegiatan</th>
		<td><?php print $model->title; ?></td>
	</tr>
	<tr>
		<th style="text-align:right">Tanggal</th>
		<td><?php print Bantu::getTanggal($model->date); ?></td>
	</tr>
	<tr>
		<th style="text-align:right">Waktu</th>
		<td><?php print $model->time; ?></td>
	</tr>
	<tr>
		<th style="text-align:right">Tempat</th>
		<td><?php print $model->place; ?></td>
	</tr>
	<tr>
		<th style="text-align:right">Deskripsi</th>
		<td><?php print $model->description; ?></td>
	</tr>
	<tr>
	</table>
	
	<div>&nbsp;</div>
	
	<?php /*
	<div class="social-share">
		<?php $this->widget('ext.SocialShareButton.SocialShareButton', array(
			'style'=>'horizontal',
			'networks' => array('facebook','googleplus','twitter'),
			'data_via'=>'', //twitter username (for twitter only, if exists else leave empty)
		));?>
	</div>
	*/ ?>
	
	<div>&nbsp;</div>

</div>