<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'event-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'title',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->datePickerGroup($model,'date',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','autoclose'=>true),'htmlOptions'=>array('class'=>'span5')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>')); ?>

	<?php echo $form->textFieldGroup($model,'time',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

	<?php echo $form->textFieldGroup($model,'place',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->dropDownListGroup($model,'event_category_id',array('widgetOptions'=>array('data'=>CHtml::listData(EventCategory::model()->findAll(),'id','title'),'htmlOptions'=>array('class'=>'span5','empty'=>'-- Select Category --')))); ?>
	
	<?php echo $form->labelEx($model,'description'); ?>
	<?php $this->widget('ext.editMe.widgets.ExtEditMe', array(
			'model'=>$model,
			'attribute'=>'description',
	)); ?>
	<?php echo $form->error($model,'description'); ?> 
	
	<?php echo $form->labelEx($model,'thumbnail',array('label'=>'Thumbnail (Ukuran Maksimal 500 KB)')); ?>
	<?php if($model->thumbnail!='') { ?>
	<div>&nbsp;</div>
		<?php print $model->getThumbnail(); ?>
		<?php print CHtml::link('<i class="glyphicon glyphicon-remove"></i>',array('event/removeThumbnail','id'=>$model->id),array('onclick'=>'return confirm("Anda yakin akan menghapus thumbnail ?")')); ?>
	<div>&nbsp;</div>
	<?php } ?>
	<?php echo $form->fileField($model,'thumbnail'); ?>
	<?php echo $form->error($model,'thumbnail'); ?>
	
	<div>&nbsp;</div>	
	
	
	<div class="form-actions well">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>'Simpan',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
