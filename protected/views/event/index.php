<?php 
	if($eventCategory != null)
	{ 
		//$eventCategoryLang = $eventCategory->getEventCategoryLang();
		$title = $eventCategory->title;
	} else {
		$title = Text::model()->getText(32)->title;
	}
?>

<?php
if(isset($_GET['id']) AND $_GET['id']!=null)
	$this->breadcrumbs=array(
		Event::model()->getCategory($_GET['id'])->getRelationField('EventCategory','title'),
	);
else 
	$this->breadcrumbs=array(
		'Indeks Event',
	);
?>

<div class="margin20 index-content" id="video-index">
	
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif ?>
	
	<h1><?php print $title; ?></h1>

	<?php $this->widget('booster.widgets.TbListView',array(
		'dataProvider'=>$dataProvider,
		'itemView'=>'_view',
	)); ?>

</div>