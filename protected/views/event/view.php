<?php
$this->breadcrumbs=array(
	'Events'=>array('index'),
	$model->title,
);
?>

<h1>View Event</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Sunting',
		'icon'=>'pencil',
		'context'=>'primary',
		'url'=>array('event/update','id'=>$model->id)
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Kelola',
		'icon'=>'list',
		'context'=>'primary',
		'url'=>array('/event/admin')
)); ?>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			'title',
			'date',
			'time',
			'place',
			array(
				'label'=>'Event Category',
				'value'=>$model->getRelationField('EventCategory','title')
			),
			array(
				'label'=>'Description',
				'type'=>'raw',
				'value'=>$model->description
			),
			array(
				'label'=>'Thumbnail',
				'type'=>'raw',
				'value'=>$model->getThumbnail()
			),
			'created_time',
		),
)); ?>

<div>&nbsp;</div>

