<?php
$this->breadcrumbs=array(
	'Videos'=>array('index'),
	$model->title,
);
?>

<div class="margin20" id="centerbar">
	
	<div class="block-title">
		<span>Video</span>
	</div>

	<div style="font-weight:bold;margin-top:10px;font-size:15px">
		<?php print $model->title; ?>
	</div>

	<div style="margin-top:5px;padding-left:5px;font-size:11px">
			<i class="glyphicon glyphicon-calendar"></i> <?php print $this->getWaktuDibuat($model->time_created); ?>
	</div>
	
	<div>&nbsp;</div>

	<div>
		<?php print $model->getVideo('margin-left:auto;margin-right:auto'); ?>
	</div>
	
	<div>&nbsp;</div>
	
	<?php print $model->description; ?>
	
</div>