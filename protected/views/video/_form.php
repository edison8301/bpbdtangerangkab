<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(

	'id'=>'video-form',

	'enableAjaxValidation'=>false,

)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->hiddenField($model,'model'); ?>
	
	<?php echo $form->hiddenField($model,'model_id'); ?>
	
	<?php echo $form->textFieldGroup($model,'title',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->dropDownListGroup($model,'video_category_id',array(
			'widgetOptions'=>array(
				'data'=>CHtml::listData(VideoCategory::model()->findAll(array('order'=>'title ASC')),'id','title'),array('empty'=>'-- Pilih Kategori --')
			)
	)); ?>

	<?php echo $form->dropDownListGroup($model,'video_type_id',array(
			'widgetOptions'=>array(
				'data'=>CHtml::listData(VideoType::model()->findAll(),'id','title'),array('class'=>'video_type_id')
			)
	)); ?>

	<?php echo $form->textFieldGroup($model,'youtube_url',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->fileFieldGroup($model,'mp4_file',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->fileFieldGroup($model,'ogv_file',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->fileFieldGroup($model,'webm_file',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->ckEditorGroup($model,'description',array('class'=>'span5','maxlength'=>255)); ?>
	
	<div class="form-actions well">
		<?php $this->widget('booster.widgets.TbButton', array(
				'buttonType'=>'submit',
				'context'=>'primary',
				'icon'=>'ok white',
				'label'=>'Simpan',
		)); ?>
	</div>


<?php $this->endWidget(); ?>

