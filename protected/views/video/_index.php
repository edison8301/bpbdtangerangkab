<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<div class="row" style="margin-bottom:10px">
	<div class="col-xs-3" style="padding-right:0px;">
			<?php print $data->getThumbnail(array('class'=>'img-responsive')) ; ?>
	</div>
	<div class="col-xs-9">
		<h2 style="font-size:15px;margin:0px 0px 5px 0px;font-weight:bold">
			<?php echo CHtml::link($data->title, array('/video/detail','id'=>$data->id));?>
		</h2>
		<div style="margin-bottom:10px;padding-left:0px;font-size:12px">
				<i class="glyphicon glyphicon-calendar"></i> <?php print $this->getWaktuDibuat($data->time_created); ?>
		</div>
	</div>
</div>
	