<?php
$this->breadcrumbs=array(
	'Videos'=>array('admin'),
	'Manage',
);
?>

<h1>Kelola Video</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'plus white',
		'label'=>'Tambah Video',
		'url'=>array('video/create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'list white',
		'label'=>'Kelola Kategori',
		'url'=>array('videoCategory/admin')
)); ?>&nbsp;


<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'video-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'headerHtmlOptions'=>array('width'=>'5%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'class'=>'CDataColumn',
				'header'=>'Title',
				'name'=>'title',
				'headerHtmlOptions'=>array('width'=>'45%','style'=>'text-align:center'),
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'video_category_id',
				'value'=>'$data->getRelationField("video_category","title")',
				'filter'=>CHtml::listData(VideoCategory::model()->findAll(array('order'=>'title ASC')),'id','title'),
				'headerHtmlOptions'=>array('width'=>'20%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'time_created',
				'headerHtmlOptions'=>array('width'=>'15%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center')
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'headerHtmlOptions'=>array('width'=>'10%'),
			),
		),
)); ?>