<?php $this->breadcrumbs=array('Videos'=>array('admin'),	$model->title,);?>

<h1>Lihat Video</h1>

<?php $this->widget('booster.widgets.TbButton',array(				
		'buttonType'=>'link',				
		'context'=>'primary',				
		'icon'=>'pencil white',				
		'label'=>'Sunting',				
		'url'=>array('video/update','id'=>$model->id)
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(				
		'buttonType'=>'link',				
		'context'=>'primary',				
		'icon'=>'plus white',				
		'label'=>'Tambah',				
		'url'=>array('video/create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',				
		'context'=>'primary',				
		'icon'=>'list white',				
		'label'=>'Kelola',				
		'url'=>array('video/admin')
)); ?>&nbsp;

<div>&nbsp;</div>
<?php $this->widget('booster.widgets.TbDetailView',array(				
		'data'=>$model,				
		'type'=>'striped bordered',				
		'attributes'=>array(						
			'title',						
			array(								
				'label'=>'Category',
				'type'=>'raw',
				'value'=>$model->getRelationField("video_category","title")
			),
			array(				
				'label'=>'Type',
				'type'=>'raw',
				'value'=>$model->getRelationField("video_type","title")
				),
			array(				
				'label'=>'Video',
				'type'=>'raw',				
				'value'=>$model->getVideo()
			),										
			array(								
				'label'=>'Description',								
				'type'=>'raw',								
				'value'=>$model->description						
			),
			'time_created'				
		)
)); ?>