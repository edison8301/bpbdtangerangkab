<?php
$this->breadcrumbs=array(
	'Bencana Kecamatans'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List BencanaKecamatan','url'=>array('index')),
array('label'=>'Create BencanaKecamatan','url'=>array('create')),
array('label'=>'Update BencanaKecamatan','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete BencanaKecamatan','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage BencanaKecamatan','url'=>array('admin')),
);
?>

<h1>Lihat <b><?php echo $model->nama; ?></b></h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'pencil white',
		'label'=>'Sunting',
		'url'=>array('/BencanaKecamatan/update','id'=>$model->id)
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'plus white',
		'label'=>'Tambah',
		'url'=>array('/BencanaKecamatan/create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'list white',
		'label'=>'Kelola',
		'url'=>array('/BencanaKecamatan/admin')
)); ?>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type' => 'striped bordered',
'attributes'=>array(
		'id',
		'nama',
),
)); ?>
