<?php
$this->breadcrumbs=array(
	'Bencana Kecamatans'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List BencanaKecamatan','url'=>array('index')),
array('label'=>'Create BencanaKecamatan','url'=>array('create')),
);

?>

<h1>Kelola Bencana Kecamatan</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'plus white',
		'label'=>'Tambah',
		'url'=>array('/BencanaKecamatan/create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'list',
		'label'=>'Bencana',
		'url'=>array('/bencana/admin')
)); ?>&nbsp;

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'bencana-kecamatan-grid',
'type' => 'striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
		'nama',
array(
'class'=>'booster.widgets.TbButtonColumn',
'htmlOptions' => array('width' => '7%')
),
),
)); ?>
