<?php
$this->breadcrumbs=array(
	'Bencana Kecamatans'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List BencanaKecamatan','url'=>array('index')),
array('label'=>'Manage BencanaKecamatan','url'=>array('admin')),
);
?>

<h1>Input Bencana</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>