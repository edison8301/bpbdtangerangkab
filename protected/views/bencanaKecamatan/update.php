<?php
$this->breadcrumbs=array(
	'Bencana Kecamatans'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List BencanaKecamatan','url'=>array('index')),
	array('label'=>'Create BencanaKecamatan','url'=>array('create')),
	array('label'=>'View BencanaKecamatan','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage BencanaKecamatan','url'=>array('admin')),
	);
	?>

	<h1>Input Bencana <?php echo $model->nama; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>