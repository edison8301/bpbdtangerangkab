<?php
$this->breadcrumbs=array(
	'Pages'=>array('index'),
	$model->title,
);

?>

<h1>Lihat Laman</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Sunting',
		'context'=>'primary',
		'icon'=>'pencil white',
		'url'=>array('/page/update','id'=>$model->id)
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Baca',
		'context'=>'primary',
		'icon'=>'search white',
		'url'=>array('/page/read','id'=>$model->id),
		'htmlOptions'=>array('target'=>'_blank')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah',
		'context'=>'primary',
		'icon'=>'plus white',
		'url'=>array('/page/create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Kelola',
		'context'=>'primary',
		'icon'=>'list white',
		'url'=>array('/page/admin')
)); ?>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
	'data'=>$model,
	'type'=>'striped bordered',
	'attributes'=>array(
		'title',
		array(
			'label'=>'Content',
			'type'=>'raw',
			'value'=>$model->content
		),
		array(
			'label'=>'Image',
			'type'=>'raw',
			'value'=>$model->getImage()
		),
		'time_created',
	),
)); ?>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbTabs',array(
        'type' => 'tabs', // 'tabs' or 'pills'
        'tabs' => array(
            array(
                'label' => 'File',
                'content' => $this->renderPartial('_file',array('model'=>$model),true),
                'active' => true
            ),
			array(
                'label' => 'Photo',
                'content' => $this->renderPartial('_photo',array('model'=>$model),true),
            ),
			array(
                'label' => 'Video',
                'content' => $this->renderPartial('_video',array('model'=>$model),true),
            ),
        ),
    )
); ?>