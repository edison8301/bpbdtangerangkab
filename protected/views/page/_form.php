<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'page-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data')
)); ?>

	<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'title',array('class'=>'span5','maxlength'=>255)); ?>
	
	<?php echo $form->ckEditorGroup($model,'content',array('class'=>'span5','maxlength'=>255)); ?>

	<?php /* echo $form->labelEx($model,'content'); ?>
	<?php $this->widget('ext.editMe.widgets.ExtEditMe', array(
			'model'=>$model,
			'attribute'=>'content',
	)); */ ?>

	<?php echo $form->error($model,'content'); ?> 
	
	<div>&nbsp;</div>
	
	<?php print $form->labelEx($model,'image'); ?>
	
	<?php if(!empty($model->image)) { ?>
		<?php print CHtml::image(Yii::app()->request->baseUrl.'/uploads/page/'.$model->image); ?>
	<?php } ?>
	
	<?php echo $form->fileField($model,'image',array('class'=>'span5','maxlength'=>255)); ?>
	
	<?php if(!empty($model->image)) { ?>
	<?php $this->widget('booster.widgets.TbButton',array(
			'buttonType'=>'link',
			'context'=>'danger',
			'size'=>'mini',
			'icon'=>'remove white',
			'label'=>'',
			'url'=>array('page/removeCoverImage','id'=>$model->id)
	)); ?>
	<?php } ?>
	
	<?php print $form->error($model,'cover_image'); ?>
	
	<div>&nbsp;</div>
	
	<div class="form-actions well">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
