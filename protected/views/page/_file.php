<h3>File Unduhan</h3>
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah',
		'context'=>'primary',
		'icon'=>'plus white',
		'url'=>array('/download/create','model'=>'Page','model_id'=>$model->id)
)); ?>

<div>&nbsp;</div>

<table class='table'>
<tr>
	<th style="width:8%">No</th>
	<th>Judul</th>
	<th>Total Unduh</th>
	<th>Akses</th>
	<th style="text-align:center">Unduh</th>
	<th style="text-align:center">Action</th>
</tr>
<?php $i=1; foreach($model->getDataDownload() as $data) { ?>
<tr>
	<td><?php print $i; ?></td>
	<td><?php print $data->title; ?></td>
	<td><?php print $data->total_download; ?></td>
	<td><?php print $data->download_access->title; ?></td>
	<td style="text-align:center">
		<?php print CHtml::link('<i class="glyphicon glyphicon-download-alt"></i>',Yii::app()->request->baseUrl.'/uploads/download/'.$data->file,array('target'=>'_blank')); ?>
	</td>
	<td style="text-align:center">
		<?php print CHtml::link('<i class="glyphicon glyphicon-pencil"></i>',array('download/update','id'=>$data->id)); ?>
		<?php print CHtml::link('<i class="glyphicon glyphicon-trash"></i>',array('download/hapus','id'=>$data->id),array('onclick'=>'return confirm("Yakin akan menghapus data?")')); ?>
	</td>
</tr>
<?php $i++; } ?>
</table>

