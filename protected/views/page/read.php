<div class="margin20" id="centerbar">	
	
	<div class="block-title">
		<span><?php print $model->title; ?></span>
	</div>
	
	<div class="block-content">
	<div>&nbsp;</div>
	
	<?php if($model->image!='') { ?>
	<?php print CHtml::image(Yii::app()->request->baseUrl.'/uploads/page/'.$model->cover_image); ?>	
	<div>&nbsp;</div>
	<?php } ?>
	
	<?php print $model->content; ?>
	
	<div>&nbsp;</div>
	
	<?php if($model->countDataVideo() > 0) { ?>
	<div class="row-fluid">
		<div class="span12">
			<h4>Galeri Video</h4>
			<?php foreach($model->getDataVideo() as $video) { ?>
			<div style="margin-left:auto;margin-right:auto;text-align:center">
				<div style="text-align:center;font-weight:bold"><?php print $video->title; ?></div>
				<?php print $video->displayVideo(); ?>
				<div>&nbsp;</div>
			</div>
			<?php } ?>
	
			<div>&nbsp;</div>
	
		</div>
	</div>
	<?php } ?>
	
	<div class="row-fluid">
		<div class="span12">
			<?php if($model->countDataPhoto() > 0) { ?>
			<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/vendors/lightbox/js/lightbox.min.js"); ?>
			<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl."/vendors/lightbox/css/lightbox.css"); ?>

			<h4>Galeri Foto</h4>
	
			<?php foreach($model->getDataPhoto() as $photo) { ?>
			<div class="photo-list">
				<div class="photo-list-item">
					<?php print CHtml::link($photo->getImage(),Yii::app()->baseUrl."/uploads/photo/".$photo->file,array('data-lightbox'=>'image-'.$photo->id,'data-title'=>$photo->title)); ?>
				</div>
			</div>
			<?php } ?>
	
			<div>&nbsp;</div>
			<?php } ?>
		</div>
	</div>
	
	
	<?php if($model->countDataDownload() > 0) { ?>
	<div class="row-fluid">
		<div class="span12">
			<h4>Berkas Unduhan</h4>
			<table class='table'>
			<tr>
				<th style="width:8%">No</th>
				<th style="width:62%">Judul</th>
				<th style="width:10%;text-align:center">Total</th>
				<th style="width:10%;text-align:center">Akses</th>
				<th style="width:10%;text-align:center">Unduh</th>
			</tr>
			<?php $i=1; foreach($model->getDataDownload() as $data) { ?>
			<tr>
				<td><?php print $i; ?></td>
				<td><?php print $data->title; ?></td>
				<td style="text-align:center"><?php print $data->total_download; ?></td>
				<td style="text-align:center"><?php print $data->download_access->title; ?></td>
				<td style="text-align:center">
					<?php print CHtml::link('<i class="icon-download-alt"></i>',array('download/getFile','id'=>$data->id),array('target'=>'_blank')); ?>
				</td>
			</tr>
			<?php $i++; } ?>
			</table>
		</div>
	</div>
	
	<div>&nbsp;</div>
	
	<?php } ?>
	
	</div>

</div>

