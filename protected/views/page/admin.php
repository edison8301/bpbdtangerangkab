<?php
$this->breadcrumbs=array(
	'Pages'=>array('index'),
	'Manage',
);
?>



<h1>Kelola Page</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'plus',
		'label'=>'Tambah Page',
		'url'=>array('page/create')
)); ?>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'page-grid',
		'dataProvider'=>$model->search(),
		'type'=>'striped bordered',
		'ajaxUpdate'=>false,
		'filter'=>$model,
		'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'headerHtmlOptions'=>array('width'=>'5%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'title',
				'type'=>'raw',
				'value'=>'$data->title',
				'headerHtmlOptions'=>array('width'=>'60%','style'=>'text-align:center'),
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'time_created',
				'type'=>'raw',
				'value'=>'$data->time_created',
				'headerHtmlOptions'=>array('width'=>'25%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'headerHtmlOptions'=>array('width'=>'10%')
			),
		),
)); ?>

