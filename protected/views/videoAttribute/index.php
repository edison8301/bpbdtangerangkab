<?php
$this->breadcrumbs=array(
	'Video Attributes',
);

$this->menu=array(
array('label'=>'Create VideoAttribute','url'=>array('create')),
array('label'=>'Manage VideoAttribute','url'=>array('admin')),
);
?>

<h1>Video Attributes</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
