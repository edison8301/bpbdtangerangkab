<?php
$this->breadcrumbs=array(
	'Video Attributes'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List VideoAttribute','url'=>array('index')),
array('label'=>'Create VideoAttribute','url'=>array('create')),
array('label'=>'Update VideoAttribute','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete VideoAttribute','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage VideoAttribute','url'=>array('admin')),
);
?>

<h1>View VideoAttribute #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'video_id',
		'key',
		'value',
),
)); ?>
