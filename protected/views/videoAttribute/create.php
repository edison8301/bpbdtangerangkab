<?php
$this->breadcrumbs=array(
	'Video Attributes'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List VideoAttribute','url'=>array('index')),
array('label'=>'Manage VideoAttribute','url'=>array('admin')),
);
?>

<h1>Create VideoAttribute</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>