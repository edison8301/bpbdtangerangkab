<?php
$this->breadcrumbs=array(
	'Video Attributes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List VideoAttribute','url'=>array('index')),
	array('label'=>'Create VideoAttribute','url'=>array('create')),
	array('label'=>'View VideoAttribute','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage VideoAttribute','url'=>array('admin')),
	);
	?>

	<h1>Update VideoAttribute <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>