<?php
$this->breadcrumbs=array(
	'Bencana Jenises'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List BencanaJenis','url'=>array('index')),
	array('label'=>'Create BencanaJenis','url'=>array('create')),
	array('label'=>'View BencanaJenis','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage BencanaJenis','url'=>array('admin')),
	);
	?>

	<h1>Update Jenis <b><?php echo $model->nama; ?></b></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>