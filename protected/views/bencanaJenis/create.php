<?php
$this->breadcrumbs=array(
	'Bencana Jenises'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List BencanaJenis','url'=>array('index')),
array('label'=>'Manage BencanaJenis','url'=>array('admin')),
);
?>

<h1>Tambah Jenis Bencana</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>