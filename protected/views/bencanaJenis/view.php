<?php
$this->breadcrumbs=array(
	'Bencana Jenises'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List BencanaJenis','url'=>array('index')),
array('label'=>'Create BencanaJenis','url'=>array('create')),
array('label'=>'Update BencanaJenis','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete BencanaJenis','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage BencanaJenis','url'=>array('admin')),
);
?>

<h1>Lihat Jenis Bencana <b><?php echo $model->nama; ?></b></h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'pencil white',
		'label'=>'Sunting',
		'url'=>array('/BencanaJenis/update','id'=>$model->id)
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'plus white',
		'label'=>'Tambah',
		'url'=>array('/BencanaJenis/create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'list white',
		'label'=>'Kelola Block',
		'url'=>array('/BencanaJenis/admin')
)); ?>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type' => 'striped bordered',
'attributes'=>array(
		'id',
		'nama',
),
)); ?>
