<?php
$this->breadcrumbs=array(
	'Photos'=>array('index'),
	$model->title,
);

$this->menu=array(
array('label'=>'List Photo','url'=>array('index')),
array('label'=>'Create Photo','url'=>array('create')),
array('label'=>'Update Photo','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Photo','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Photo','url'=>array('admin')),
);
?>

<h1><?php echo $model->title; ?></h1>

<?php print CHtml::link(CHtml::image(Yii::app()->request->baseUrl."/uploads/photo/".$model->file),Yii::app()->request->baseUrl."/uploads/photo/".$model->file,array('class'=>'photo-read-image')); ?>

<?php $this->widget('application.extensions.fancybox.EFancyBox', array(
		'target'=>'.photo-read-image',
		'config'=>array(),
)); ?>