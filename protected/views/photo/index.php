<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/vendors/lightbox/js/lightbox.min.js"); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl."/vendors/lightbox/css/lightbox.css"); ?>


<div class="margin20" id="photo-index">
	
	<h3 class="list-bottom"><?php print strtoupper($title); ?></h3>

	
	<?php $this->widget('booster.widgets.TbListView',array(
			'dataProvider'=>$dataProvider,
			'itemView'=>'_view',
	)); ?>
	
</div>
