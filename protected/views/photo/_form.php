<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
		'id'=>'photo-form',
		'enableAjaxValidation'=>false,
		'htmlOptions'=>array('enctype'=>'multipart/form-data')
)); ?>


	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
	
	<?php echo $form->hiddenField($model,'model'); ?>
	
	<?php echo $form->hiddenField($model,'model_id'); ?>
	
	

	<?php echo $form->textFieldGroup($model,'title',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->filefieldGroup($model,'file'); ?> <?php if($model->file != ''){ echo '<img width="15%" src='.Yii::app()->baseUrl.'/uploads/photo/'.$model->file.'>'; };?>

	<?php echo $form->dropDownListGroup($model,'photo_category_id',array(
			'widgetOptions'=>array(
				'data'=>CHtml::listData(PhotoCategory::model()->findAll(array('order'=>'title ASC')),'id','title'),
				'htmlOptions'=>array('empty'=>'-- Pilih Kategori --')
			)
	)); ?>
	
	<?php echo $form->dropDownListGroup($model,'photo_album_id',array(
			'widgetOptions'=>array(
				'data'=>CHtml::listData(PhotoAlbum::model()->findAll(array('order'=>'title ASC')),'id','title'),
				'htmlOptions'=>array('empty'=>'-- Pilih Album --')
			)
	)); ?>

	<div class="form-actions well">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
	)); ?>
	</div>

<?php $this->endWidget(); ?>

