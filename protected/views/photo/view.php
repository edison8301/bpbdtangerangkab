<?php
$this->breadcrumbs=array(
	'Photos'=>array('index'),
	$model->title,
);
?>

<h1>Lihat Foto</h1><?php $this->widget('booster.widgets.TbButton',array(		'buttonType'=>'link',		'context'=>'primary',		'icon'=>'pencil white',		'label'=>'Sunting',		'url'=>array('photo/update','id'=>$model->id))); ?>&nbsp;<?php $this->widget('booster.widgets.TbButton',array(		'buttonType'=>'link',		'context'=>'primary',		'icon'=>'plus white',		'label'=>'Tambah',		'url'=>array('photo/create'))); ?>&nbsp;<?php $this->widget('booster.widgets.TbButton',array(		'buttonType'=>'link',		'context'=>'primary',		'icon'=>'list white',		'label'=>'Kelola',		'url'=>array('photo/admin'))); ?>&nbsp;
<div>&nbsp;</div>
<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			'title',
			array(
				'label'=>'Category',
				'type'=>'raw',
				'value'=>$model->getRelationField("photo_category","title")
			),
			array(
				'label'=>'File',
				'type'=>'raw',
				'value'=>$model->getImage()
			),
			array(
				'label'=>'Url',
				'type'=>'raw',
				'value'=>Yii::app()->baseUrl.'/uploads/photo/'.$model->file
			),
			'time_created',
		),
)); ?>