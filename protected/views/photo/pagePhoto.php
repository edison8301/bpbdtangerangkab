<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<div class="site-index">
<div class="row-fluid" style="margin-bottom: 20px">
    <div class="span8 panel-bg" style="text-align:justify">
	<h2 class="list">Gallery Foto</h2>
			<ul class="gallery group 3-col">
			 <?php foreach(Photo::model()->findAllByAttributes(array("photo_category_id"=>$model->id)) as $model){?>
				<li  class="item span3"><a href="<?php echo Yii::app()->request->baseUrl; ?>/uploads/photo/<?php echo $model->file; ?>" rel="prettyPhoto[gallery1]" title="<?php echo $model->title; ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/photo/<?php echo $model->file; ?>" alt="<?php echo $model->title; ?>" /></a></li>
			<?php } ?>
			</ul>
	</div>
<div class="span4 visitor-counter">
		<h3 class="list">Visitor Counter</h3>
		<hr>
		<div class="content">
		    <div class="visitor panel-bg">
		    	<h4>100.000<small>visit all</small></h4>
		    	<h4>1.000<small>visit today</small></h4>
		    </div>
		</div>

		<h3 class="list">Site Map</h3>
		<hr>
		<div class="content">
		    <span class="sitemap"></span>
		</div>
		<h3 class="list">E-Directory</h3>
		<div class="content">
		    <span class="visitor">
		    	
		</div>
    </div>
</div>
</div>
