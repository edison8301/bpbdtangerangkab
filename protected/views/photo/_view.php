<div class="photo-list">
	<div class="photo-list-item">
		<?php print CHtml::link($data->getImage(array('class'=>'img-responsive')),Yii::app()->baseUrl."/uploads/photo/".$data->file,array('data-lightbox'=>'image-'.$data->id,'data-title'=>$data->title)); ?>
	</div>
</div>