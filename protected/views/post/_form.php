<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'post-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data')
)); ?>

	<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

	<?php echo $form->errorSummary($model); ?>

	
	<?php echo $form->textFieldGroup($model,'title',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->dropDownListGroup($model,'post_category_id',array(
			'widgetOptions'=>array(
				'data'=>PostCategory::model()->getParentList()
			)
	)); ?>

	<?php echo $form->ckEditorGroup($model,'content',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->ckEditorGroup($model,'excerpt',array('class'=>'span5','maxlength'=>255)); ?>
	
	
	
	<?php print $form->labelEx($model,'image'); ?>
	<?php if(!empty($model->image)) print CHtml::image(Yii::app()->request->baseUrl.'/uploads/post/'.$model->thumbnail); ?>
	<?php echo $form->fileField($model,'image',array('class'=>'span5','maxlength'=>255)); ?>
	
	<?php if(!empty($model->image)) { ?>
	<?php $this->widget('booster.widgets.TbButton',array(
			'buttonType'=>'link',
			'context'=>'danger',
			'size'=>'extra_small',
			'icon'=>'remove',
			'label'=>'',
			'url'=>array('post/removeThumbnail','id'=>$model->id)
	)); ?>
	<?php } ?>
	
	<?php print $form->error($model,'thumbnail'); ?>		
	
	<div>&nbsp;</div>

	<?php echo $form->textFieldGroup($model,'tags',array('class'=>'span5','maxlength'=>255)); ?>
	
	<div class="form-actions well">
		<?php $this->widget('booster.widgets.TbButton', array(
				'buttonType'=>'submit',
				'context'=>'primary',
				'icon'=>'ok',
				'label'=>'Simpan',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
