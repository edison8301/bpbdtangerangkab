<h3>Galeri Foto</h3>
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah',
		'context'=>'primary',
		'icon'=>'plus white',
		'url'=>array('/photo/create','model'=>'Post','model_id'=>$model->id)
)); ?>

<div>&nbsp;</div>

<table class='table'>
<tr>
	<th width="8%">No</th>
	<th width="60%">Judul</th>
	<th width="20%" style="text-align:center">Foto</th>
	<th width="12%" style="text-align:center">Action</th>
</tr>
<?php $i=1; foreach($model->getDataPhoto() as $data) { ?>
<tr>
	<td><?php print $i; ?></td>
	<td><?php print $data->title; ?></td>
	<td style="text-align:center"><?php print $data->getImage(); ?></td>
	<td style="text-align:center">
		<?php print CHtml::link('<i class="glyphicon glyphicon-pencil"></i>',array('photo/update','id'=>$data->id)); ?>
		<?php print CHtml::link('<i class="glyphicon glyphicon-trash"></i>',array('photo/hapus','id'=>$data->id),array('onclick'=>'return confirm("Yakin akan menghapus data?")')); ?>
	</td>
</tr>
<?php $i++; } ?>
</table>
