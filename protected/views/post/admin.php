<?php $this->breadcrumbs=array(
	'Post'=>array('admin'),
	'Kelola',
); ?>

<h1>Kelola Post</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'plus',
		'label'=>'Tambah Post',
		'url'=>array('post/create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'list',
		'label'=>'Kelola Kategori',
		'url'=>array('postCategory/admin')
)); ?>&nbsp;



<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'post-grid',
		'dataProvider'=>$model->search(),
		'type'=>'striped bordered',
		'filter'=>$model,
		'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'headerHtmlOptions'=>array('width'=>'5%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'title',
				'type'=>'raw',
				'value'=>'$data->title',
				'headerHtmlOptions'=>array('width'=>'40%','style'=>'text-align:center')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'image',
				'type'=>'raw',
				'value'=>'$data->getImage(array("width"=>"180px"))',
				'headerHtmlOptions'=>array('width'=>'15%','style'=>'text-align:center')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'post_category_id',
				'type'=>'raw',
				'value'=>'$data->getRelationField("post_category","title")',
				'filter'=>CHtml::listData(PostCategory::model()->findAll(array('order'=>'title ASC')),'id','title'),
				'headerHtmlOptions'=>array('width'=>'20%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'time_created',
				'type'=>'raw',
				'value'=>'$data->getTimeCreated()',
				'headerHtmlOptions'=>array('width'=>'12%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center')
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'headerHtmlOptions'=>array('width'=>'8%')
			),
		),
)); ?>

