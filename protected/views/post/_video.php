<h3>Galeri Video</h3>
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah',
		'context'=>'primary',
		'icon'=>'plus white',
		'url'=>array('/video/create','model'=>'Post','model_id'=>$model->id)
)); ?>

<div>&nbsp;</div>

<table class='table'>
<tr>
	<th style="width:8%">No</th>
	<th width="30%">Judul</th>
	<th width="50%" style="text-align:center">Video</th>
	<th width="12%" style="text-align:center">Action</th>
</tr>
<?php $i=1; foreach($model->getDataVideo() as $data) { ?>
<tr>
	<td><?php print $i; ?></td>
	<td><?php print $data->title; ?></td>
	<td style="text-align:center"><?php print $data->displayVideo(); ?></td>
	<td style="text-align:center">
		<?php print CHtml::link('<i class="glyphicon glyphicon-pencil"></i>',array('video/update','id'=>$data->id)); ?>
		<?php print CHtml::link('<i class="glyphicon glyphicon-trash"></i>',array('video/hapus','id'=>$data->id),array('onclick'=>'return confirm("Yakin akan menghapus data?")')); ?>
	</td>
</tr>
<?php $i++; } ?>
</table>
