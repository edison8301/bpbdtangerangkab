<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'download-form',
	'enableAjaxValidation'=>false,
	'htmlOptions'=>array('enctype'=>'multipart/form-data')
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'title',array('class'=>'span5','maxlength'=>255)); ?>
		<?php echo $form->dropDownListRow($model,'download_access_id',CHtml::listData(DownloadAccess::model()->findAll(),'id','title')); ?>			
	<?php print $form->labelEx($model,'file'); ?>
	<?php echo $form->fileField($model,'file',array('class'=>'span5','maxlength'=>255)); ?>
	<?php print $form->error($model,'file'); ?>

	<?php echo $form->hiddenField($model,'model',array('value'=>isset($_GET['model']) ? $_GET['model'] : $model->model)); ?>
	<?php echo $form->hiddenField($model,'model_id',array('value'=>isset($_GET['model_id']) ? $_GET['model_id'] : $model->model_id)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'icon'=>'ok white',	
			'label'=>'Simpan',
		)); ?>
</div>

<?php $this->endWidget(); ?>
