<div class="margin20" id="inovasi-submit">
	<h3 class="list-bottom">UNDUH BERKAS MEMBER</h3>
	<hr>
	<h3>Unduh Berkas Member</h3>
	
	
	<p>Untuk dapat mengunduh berkas dengan tipe akses member, Anda harus login terlebih dahulu.</p>
	<p>Silahkan gunakan link di bawah ini untuk melakukan login.</p>
	<p>Jika Anda belum terdaftar, silahkan melakukan registrasi sebagai member baru terlebih dahulu.</p>
	
	<p>&nbsp;</p>
	
	<?php $this->widget('bootstrap.widgets.TbButton',array(
				'buttonType'=>'link',
				'type'=>'primary',
				'icon'=>'lock white',
				'label'=>'Login',
				'url'=>array('member/login')
	)); ?>
	
	<?php $this->widget('bootstrap.widgets.TbButton',array(
				'buttonType'=>'link',
				'type'=>'primary',
				'icon'=>'user white',
				'label'=>'Registrasi',
				'url'=>array('member/signup')
	)); ?>
	
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	

</div>