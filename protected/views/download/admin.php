<?php
$this->breadcrumbs=array(

	'Downloads'=>array('index'),

	'Manage',

);


$this->menu=array(

array('label'=>'List Download','url'=>array('index')),

array('label'=>'Create Download','url'=>array('create')),

);


?>



<h1>Kelola Download</h1>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'download-grid',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'type'=>'striped bordered',
		'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'headerHtmlOptions'=>array('width'=>'5%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			'title',
			'file',
			'model',
			'model_id',
			'total_download',
			array(
				'class'=>'booster.widgets.TbButtonColumn',
			),
		),
)); ?>

