<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
		'id'=>'block-form',
		'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

	<?php echo $form->errorSummary($model); ?>
	
	
	
	<?php echo $form->textFieldGroup($model,'title',array('class'=>'span5','maxlength'=>255)); ?>	
	
	<?php echo $form->dropDownListGroup($model,'block_position_id',array(
			'widgetOptions'=>array(
				'data'=>CHtml::listData(BlockPosition::model()->findAll(array('order'=>'id ASC')),'id','title')
			)
	)); ?>
	
	<?php echo $form->textFieldGroup($model,'order',array('class'=>'span1')); ?>
	
	<?php echo $form->dropDownListGroup($model,'block_status_id',array(
			'widgetOptions'=>array(
				'data'=>CHtml::listData(BlockStatus::model()->findAll(),'id','title')
			)
	)); ?>
	
	<?php echo $form->dropDownListGroup($model,'block_type_id',array(
			'widgetOptions'=>array(
				'data'=>BlockType::getList(),
				'htmlOptions'=>array('class'=>'block_type_id')
			)
	)); ?>
	
	<hr>

	<div class="form-group block_attribute text">
		<?php print CHtml::label('Teks',''); ?>
		<?php print CHtml::textArea('BlockAttribute[text]',$model->getBlockAttributeValueByKey('text'),array('class'=>'form-control')); ?>
	
		<div>&nbsp;</div>
		
		<?php print CHtml::label('Tampilkan Judul',''); ?>
		<?php print CHtml::dropDownList('BlockAttribute[text_display_title]',$model->getBlockAttributeValueByKey('text_display_title'),array('ya'=>'Ya','tidak'=>'Tidak'),array('class'=>'form-control')); ?>
	</div>

	<div class="form-group block_attribute html">
		<?php print CHtml::label('HTML',''); ?>
		<?php $this->widget('ext.editMe.widgets.ExtEditMe', array(
				'name'=>'BlockAttribute[html]',
				'value'=>$model->getBlockAttributeValueByKey('html')
		)); ?>
		
		<div>&nbsp;</div>
		
		<?php print CHtml::label('Tampilkan Judul',''); ?>
		<?php print CHtml::dropDownList('BlockAttribute[html_display_title]',$model->getBlockAttributeValueByKey('html_display_title'),array('ya'=>'Ya','tidak'=>'Tidak'),array('class'=>'form-control')); ?>
	
	</div>

	<div class="block_attribute carousel_post_home">
		<?php print CHtml::label('Pilih Kategori Post',''); ?>
		<?php print CHtml::dropDownList('BlockAttribute[carousel_post_home_post_category_id]',$model->getBlockAttributeValueByKey('carousel_post_home_post_category_id'),PostCategory::model()->getParentList(),array('class'=>'form-control')); ?>
	</div>
	
	<div class="block_attribute post_carousel">
		<?php print CHtml::label('Pilih Kategori Post',''); ?>
		<?php print CHtml::dropDownList('BlockAttribute[post_carousel_post_category_id]',$model->getBlockAttributeValueByKey('post_carousel_post_category_id'),PostCategory::model()->getParentList(),array('class'=>'form-control')); ?>
	</div>
	
	
	<div class="block_attribute post_list">
		<?php print CHtml::label('Pilih Kategori Post',''); ?>
		<?php print CHtml::dropDownList('BlockAttribute[post_list_post_category_id]',$model->getBlockAttributeValueByKey('post_list_post_category_id'),PostCategory::model()->getParentList(),array('class'=>'form-control')); ?>
		
		<div>&nbsp;</div>
		
		<?php print CHtml::label('Mode Tampilan',''); ?>
		<?php print CHtml::dropDownList('BlockAttribute[post_list_display_mode]',$model->getBlockAttributeValueByKey('post_list_display_mode'),array(
					'title'=>'Judul',
					'title_image'=>'Judul + Gambar',
					'title_image_excerpt'=>'Judul + Gambar + Text',
					'frontpage'=>'Frontpage'
				),array('class'=>'form-control')); ?>
		
		<div>&nbsp;</div>
		
		<?php print CHtml::label('Jumlah Tampil',''); ?>
		<?php print CHtml::textField('BlockAttribute[post_list_limit]',$model->getBlockAttributeValueByKey('post_list_limit'),array('class'=>'form-control')); ?>
		
	</div>
	
	<div class="block_attribute list_recent_event">
		<?php print CHtml::label('Pilih Kategori Event',''); ?>
		<?php print CHtml::dropDownList('BlockAttribute[list_recent_event_event_category_id]',$model->getBlockAttributeValueByKey('list_recent_event_event_category_id'),EventCategory::model()->getParentList(),array('class'=>'form-control','empty'=>'-- Semua Kategori --')); ?>
	</div>
	
	<div class="block_attribute link_list">
		<?php print CHtml::label('Pilih Kategori Link',''); ?>
		<?php print CHtml::dropDownList('BlockAttribute[link_list_link_category_id]',$model->getBlockAttributeValueByKey('link_list_link_category_id'),CHtml::listData(LinkCategory::model()->findAll(),'id','title'),array('class'=>'form-control')); ?>
		
		<div>&nbsp;</div>
		
		<?php print CHtml::label('Apa Yang Ditampilkan',''); ?>
		<?php print CHtml::dropDownList('BlockAttribute[link_list_display_mode]',$model->getBlockAttributeValueByKey('link_list_display_mode'),array('text'=>'Text','image'=>'Image'),array('class'=>'form-control')); ?>
	</div>
	
	<div class="block_attribute page">
		<?php print CHtml::label('Pilih Page',''); ?>
		<?php print CHtml::dropDownList('BlockAttribute[page_page_id]',$model->getBlockAttributeValueByKey('page_page_id'),CHtml::listData(Page::model()->findAll(),'id','title'),array('class'=>'form-control')); ?>
	</div>

	<script>
		function refreshBlockType()
		{
			$('.block_attribute').hide();
			var type = $('.block_type_id').val();
			$('.'+type).show();
		}
		
		$(document).ready(function() {
			refreshBlockType();
		});
		
		$('.block_type_id').change(function() {
			refreshBlockType();
		});
	</script>
	
	<div>&nbsp;</div>
	
	<div class="form-actions well">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>'Simpan',
		)); ?>
	</div>
	
<?php $this->endWidget(); ?>
