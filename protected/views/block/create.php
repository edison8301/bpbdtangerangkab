<?php
$this->breadcrumbs=array(
	'Blocks'=>array('admin'),
	'Create',
);

?>

<h1>Tambah Block</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>