<?php
$this->breadcrumbs=array(
	'Block'=>array('admin'),
	'Manage',
);

?>



<h1>Kelola Block</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'plus white',
		'label'=>'Tambah Block',
		'url'=>array('/block/create')
)); ?>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'block-grid',
		'dataProvider'=>$model->search(),
		'type'=>'striped bordered',
		'filter'=>$model,
		'ajaxUpdate'=>true,
		'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'headerHtmlOptions'=>array('width'=>'5%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'title',
				'type'=>'raw',
				'value'=>'$data->title',
				'headerHtmlOptions'=>array('width'=>'35%','style'=>'text-align:center')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'order',
				'type'=>'raw',
				'value'=>'$data->order',
				'headerHtmlOptions'=>array('width'=>'10%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),

			),
			array(
				'class'=>'CDataColumn',
				'name'=>'block_type_id',
				'type'=>'raw',
				'value'=>'$data->getRelationField("block_type","title")',
				'filter'=>CHtml::listData(BlockType::model()->findAll(array('order'=>'title ASC')),'id','title'),
				'headerHtmlOptions'=>array('width'=>'20%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'block_position_id',
				'type'=>'raw',
				'value'=>'$data->getRelationField("block_position","title")',
				'filter'=>CHtml::listData(BlockPosition::model()->findAll(array('order'=>'title ASC')),'id','title'),
				'headerHtmlOptions'=>array('width'=>'20%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'headerHtmlOptions'=>array('width'=>'10%','style'=>'text-align:center')
			),
		),
)); ?>