<?php
$this->breadcrumbs=array(
	'Block'=>array('admin'),
	$model->title,
);

?>

<h1>Lihat Block</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'pencil white',
		'label'=>'Sunting Block',
		'url'=>array('/block/update','id'=>$model->id)
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'plus white',
		'label'=>'Tambah Block',
		'url'=>array('/block/create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'list white',
		'label'=>'Kelola Block',
		'url'=>array('/block/admin')
)); ?>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			'title',
			'order',
			array(
				'label'=>'Position',
				'type'=>'raw',
				'value'=>$model->getRelationField("block_position","title")
			),
			array(
				'label'=>'Type',
				'type'=>'raw',
				'value'=>$model->getRelationField("block_type","title")
			),
			array(
				'label'=>'Active',
				'type'=>'raw',
				'value'=>$model->getRelationField("active","title")
			),
		),
)); ?>
