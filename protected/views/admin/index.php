<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/vendors/fusioncharts/js/FusionCharts.js"); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/vendors/fusioncharts/js/themes/FusionCharts.theme.fint.js"); ?>


<script>

<?php 
  $tahun = date('Y');              
  if(isset($_GET['tahun']))
    $tahun = $_GET['tahun'];
?>


FusionCharts.ready(function(){
      var revenueChart = new FusionCharts({
        "type": "column3d",
        "renderAt": "grafik-bulan",
        "width": "900",
        "height": "600",
        "dataFormat": "json",
        "dataSource": {
          "chart": {
              "caption" : "Grafik Jumlah Kejadian Bencana Tahun <?php print $tahun; ?>",
              "xAxisName": "Bulan",
              "yAxisName": "Jumlah Kejadian",
              "theme": "fint"
           },
          "data": 
              [ <?php print Bencana::getChartDataByYear($tahun); ?> ]
        }
    });

    revenueChart.render();
})
		
</script>

<h3> Tahun</h3>
  <div class="row">
      <div class="col-md-12">
        <?php print CHtml::beginForm('','GET',array('id'=>'form-tahun')); ?>
          <div class="form-group">
            <?php print CHtml::dropDownList('tahun',$tahun,array('2015'=>'Tahun 2015','2014'=>'Tahun 2014'),array('class'=>'form-control')); ?>
          </div>
        <?php print CHtml::endForm(); ?>
          <?php Yii::app()->clientScript->registerScript("submit-onchange",'
            $("#tahun").change(function() {
            $("#form-tahun").submit();
                    });
          '); 
        ?>
      </div>
  </div>
<?php $box = $this->beginWidget('booster.widgets.TbPanel', array(
  'title'=>'STATISTIK BENCANA',
  'context' => 'info',
  'headerIcon'=>'signal' ));
?>   
<div id="grafik-bulan"> FusionChart XT will load here! </div>
<?php $this->endWidget(); ?>


