<?php Yii::app()->ClientScript->registerScriptFile(Yii::app()->baseUrl."/vendors/fusioncharts/js/FusionCharts.js"); ?>
<?php Yii::app()->ClientScript->registerScriptFile(Yii::app()->baseUrl."/vendors/fusioncharts/js/themes/FusionCharts.theme.fint.js"); ?>

<script>
<?php
  $tahun = date('Y');
    if(isset($_GET['tahun']))
    $tahun = $_GET['tahun'];
?>



FusionCharts.ready(function(){
      var revenueChart = new FusionCharts({
        "type": "column3d",
        "renderAt": "grafik-jenis",
        "width": "900",
        "height": "600",
        "dataFormat": "json",
        "dataSource": {
          "chart": {
              "caption" : "Grafik Jumlah Jenis Kejadian Bencana Tahun <?php print $tahun ?>",
              "xAxisName": "Jenis Bencana",
              "yAxisName": "Jumlah Bencana",
              "theme": "fint"
           },
          "data":        
              [ <?php print Bencana:: getdataChartByJenis($tahun); ?> ]
             
           
        }
    });

    revenueChart.render();
})
		
</script>
<div> &nbsp</div> 

<?php $box = $this->beginWidget('booster.widgets.TbPanel', array(
  'title'=>'STATISTIK BENCANA',
  'context' => 'info',
  'headerIcon'=>'signal' )
  );
?>  
<div id="grafik-jenis"> FusionChart XT will load here! </div>
<?php $this->endWidget(); ?>
