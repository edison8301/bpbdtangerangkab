<h2>Background</h2>

<?php print CHTml::beginForm(array('theme/updateAttribute','id'=>1),'POST',array('enctype'=>'multipart/form-data')); ?>

<div class="form-group">
	<?php print CHtml::label('Background Color',''); ?>
	<?php print CHtml::textField('ThemeAttribute[background_color]',$model->getAttr('background_color'),array('class'=>'form-control')); ?>
</div>

<div class="form-group">
	<?php print CHtml::label('Background Image',''); ?>
	<?php if($model->getAttr('background_image')!='') { ?>
		<?php print CHtml::image(Yii::app()->request->baseUrl."/uploads/theme/".$model->getAttr('background_image')); ?>
	<?php }?>
	<?php print CHtml::fileField('ThemeAttribute[background_image]','',array('class'=>'form-control')); ?>

	<?php print CHtml::hiddenField('ThemeAttribute[upload_file_type]','background'); ?>

	<?php print CHtml::label('Clear Image',''); ?>
	<?php print CHtml::checkBox('clear_image','',array('class'=>'span1')); ?> Click to Clear Image
</div>

<div class="form-group">
	<?php print CHtml::label('Background Repeat',''); ?>
	<?php print CHtml::dropDownList('ThemeAttribute[background_repeat]',$model->getAttr('background_repeat'),array('no-repeat'=>'no-repeat',	'repeat'=>'repeat','repeat-x'=>'repeat-x','repeat-y')); ?>
</div>
<div class="form-group">
	<?php print CHtml::label('Background Position',''); ?>
	<?php print CHtml::dropDownList('ThemeAttribute[background_position]',$model->getAttr('background_position'),array('center top'=>'center top','center center'=>'center center','center bottom'=>'center bottom','left top'=>'left top','left center'=>'left center','left bottom'=>'left bottom','right top'=>'right top','right center'=>'right center','right bottom')); ?>
</div>

<div class="form-group">
	<?php print CHtml::label('Background Attachment',''); ?>
	<?php print CHtml::dropDownList('ThemeAttribute[background_attachment]',$model->getAttr('background_attachment'),array('scroll'=>'scroll','fixed'=>'fixed')); ?>
</div>

<div class="form-actions well">
	<?php $this->widget('booster.widgets.TbButton',array(
			'buttonType'=>'submit',
			'label'=>'Simpan',
			'icon'=>'ok white',
			'context'=>'primary'
	)); ?>
</div>

<?php print CHtml::endForm(); ?>