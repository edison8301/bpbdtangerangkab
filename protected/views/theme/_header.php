<h2>Header</h2>

<?php print CHTml::beginForm(array('theme/updateAttribute','id'=>1),'POST',array('enctype'=>'multipart/form-data')); ?>

<?php print CHtml::label('Header Height',''); ?>
<?php print CHtml::textField('ThemeAttribute[header_height]',$model->getAttr('header_height'),array('class'=>'span1')); ?>

<?php print CHtml::label('Header Background Color',''); ?>
<?php print CHtml::textField('ThemeAttribute[header_background_color]',$model->getAttr('header_background_color'),array('class'=>'span3')); ?>

<?php print CHtml::label('Header Background Image',''); ?>
<?php if($model->getAttr('header_background_image')!='') { ?>
	<?php print CHtml::image(Yii::app()->request->baseUrl."/uploads/theme/".$model->getAttr('header_background_image')); ?>
<?php }?>
<?php print CHtml::fileField('ThemeAttribute[header_background_image]',''); ?>

<?php print CHtml::hiddenField('ThemeAttribute[upload_file_type]','header'); ?>

<?php print CHtml::label('Clear Image',''); ?>
<?php print CHtml::checkBox('clear_image','',array('class'=>'span1')); ?> Click to Clear Image

<?php print CHtml::label('Header Background Repeat',''); ?>
<?php print CHtml::dropDownList('ThemeAttribute[header_background_repeat]',$model->getAttr('header_background_repeat'),array('no-repeat'=>'no-repeat',	'repeat'=>'repeat','repeat-x'=>'repeat-x','repeat-y'=>'repeat-y')); ?>

<?php print CHtml::label('Header Background Position',''); ?>
<?php print CHtml::dropDownList('ThemeAttribute[header_background_position]',$model->getAttr('header_background_position'),array('center top'=>'center top','center center'=>'center center','center bottom'=>'center bottom','left top'=>'left top','left center'=>'left center','left bottom'=>'left bottom','right top'=>'right top','right center'=>'right center','right bottom')); ?>

<?php print CHtml::label('Header Background Attachment',''); ?>
<?php print CHtml::dropDownList('ThemeAttribute[header_background_attachment]',$model->getAttr('header_background_attachment'),array('scroll'=>'scroll','fixed'=>'fixed')); ?>




<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton',array(
			'buttonType'=>'submit',
			'label'=>'Simpan',
			'icon'=>'ok white',
			'context'=>'primary'
	)); ?>
</div>

<?php print CHtml::endForm(); ?>