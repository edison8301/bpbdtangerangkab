<h2>Logo</h2>

<?php print CHTml::beginForm(array('theme/updateAttribute','id'=>1),'POST',array('enctype'=>'multipart/form-data')); ?>



<?php print CHtml::label('Margin Left',''); ?>
<?php print CHtml::textField('ThemeAttribute[logo_margin_left]',$model->getAttr('logo_margin_left'),array('class'=>'span1')); ?>

<?php print CHtml::label('Margin Top',''); ?>
<?php print CHtml::textField('ThemeAttribute[logo_margin_top]',$model->getAttr('logo_margin_top'),array('class'=>'span1')); ?>

<?php print CHtml::label('Margin Right',''); ?>
<?php print CHtml::textField('ThemeAttribute[logo_margin_right]',$model->getAttr('logo_margin_right'),array('class'=>'span1')); ?>

<?php print CHtml::label('Margin Bottom',''); ?>
<?php print CHtml::textField('ThemeAttribute[logo_margin_bottom]',$model->getAttr('logo_margin_bottom'),array('class'=>'span1')); ?>


<?php print CHtml::label('File Logo',''); ?>
<?php if($model->getAttr('logo_file')!='') { ?>
	<?php print CHtml::image(Yii::app()->request->baseUrl."/uploads/theme/".$model->getAttr('logo_file')); ?>
<?php }?>
<?php print CHtml::fileField('ThemeAttribute[logo_file]',''); ?>

<?php print CHtml::label('Clear Image',''); ?>
<?php print CHtml::checkBox('clear_image','',array('class'=>'span1')); ?> Click to Clear Image



<?php print CHtml::hiddenField('ThemeAttribute[upload_file_type]','logo'); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton',array('buttonType'=>'submit','label'=>'Simpan','icon'=>'ok white','context'=>'primary')); ?>
</div>

<?php print CHtml::endForm(); ?>