<?php
$this->breadcrumbs=array(
	'Themes'=>array('index'),
	'Manage',
);
	
?>

<h1>Kelola Theme</h1>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'theme-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'title',
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'template'=>'{view}'
			),
		),
)); ?>
