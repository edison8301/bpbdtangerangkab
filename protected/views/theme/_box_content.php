<h2>Box Content</h2>

<?php print CHTml::beginForm(array('theme/updateAttribute','id'=>1),'POST',array('enctype'=>'multipart/form-data')); ?>

<?php print CHtml::label('Background Color',''); ?>
<?php print CHtml::textField('ThemeAttribute[box_content_background_color]',$model->getAttr('box_content_background_color'),array('class'=>'form-control')); ?>

<?php print CHtml::label('Menu Background Image',''); ?>
<?php if($model->getAttr('box_content_background_image')!='') { ?>
	<?php print CHtml::image(Yii::app()->request->baseUrl."/uploads/theme/".$model->getAttr('box_content_background_image')); ?>
<?php }?>
<?php print CHtml::fileField('ThemeAttribute[box_content_background_image]',''); ?>

<?php print CHtml::hiddenField('ThemeAttribute[upload_file_type]','box_content'); ?>

<?php print CHtml::label('Clear Image',''); ?>
<?php print CHtml::checkBox('clear_image','',array('class'=>'span1')); ?> Click to Clear Image

<?php print CHtml::label('Menu Background Repeat',''); ?>
<?php print CHtml::dropDownList('ThemeAttribute[background_repeat]',$model->getAttr('box_content_background_repeat'),array('no-repeat'=>'no-repeat',	'repeat'=>'repeat','repeat-x'=>'repeat-x','repeat-y'=>'repeaty')); ?>

<?php print CHtml::label('Menu Background Position',''); ?>
<?php print CHtml::dropDownList('ThemeAttribute[background_position]',$model->getAttr('box_content_background_position'),array('center top'=>'center top','center center'=>'center center','center bottom'=>'center bottom','left top'=>'left top','left center'=>'left center','left bottom'=>'left bottom','right top'=>'right top','right center'=>'right center','right bottom')); ?>

<?php print CHtml::label('Menu Background Attachment',''); ?>
<?php print CHtml::dropDownList('ThemeAttribute[background_attachment]',$model->getAttr('box_content_background_attachment'),array('scroll'=>'scroll','fixed'=>'fixed')); ?>




<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton',array(
			'buttonType'=>'submit',
			'label'=>'Simpan',
			'icon'=>'ok white',
			'context'=>'primary'
	)); ?>
</div>

<?php print CHtml::endForm(); ?>