<h2>Main Menu</h2>

<?php print CHTml::beginForm(array('theme/updateAttribute','id'=>1),'POST',array('enctype'=>'multipart/form-data')); ?>

<?php print CHtml::label('Link Color',''); ?>
<?php print CHtml::textField('ThemeAttribute[mainmenu_link_color]',$model->getAttr('mainmenu_link_color'),array('class'=>'span3')); ?>

<?php print CHtml::label('Link Color Hover',''); ?>
<?php print CHtml::textField('ThemeAttribute[mainmenu_link_color_hover]',$model->getAttr('mainmenu_link_color_hover'),array('class'=>'span3')); ?>

<?php print CHtml::label('Link Background Color Hover',''); ?>
<?php print CHtml::textField('ThemeAttribute[mainmenu_link_background_color_hover]',$model->getAttr('mainmenu_link_background_color_hover'),array('class'=>'span3')); ?>




<hr>
<?php print CHtml::label('Background Color',''); ?>
<?php print CHtml::textField('ThemeAttribute[mainmenu_background_color]',$model->getAttr('mainmenu_background_color'),array('class'=>'span3')); ?>

<?php print CHtml::label('Mainmenu Background Image',''); ?>
<?php if($model->getAttr('mainmenu_background_image')!='') { ?>
	<?php print CHtml::image(Yii::app()->request->baseUrl."/uploads/theme/".$model->getAttr('mainmenu_background_image')); ?>
<?php }?>
<?php print CHtml::fileField('ThemeAttribute[mainmenu_background_image]',''); ?>

<?php print CHtml::hiddenField('ThemeAttribute[upload_file_type]','mainmenu'); ?>

<?php print CHtml::label('Clear Image',''); ?>
<?php print CHtml::checkBox('clear_image','',array('class'=>'span1')); ?> Click to Clear Image


<?php print CHtml::label('Background Repeat',''); ?>
<?php print CHtml::dropDownList('ThemeAttribute[mainmenu_background_repeat]',$model->getAttr('mainmenu_background_repeat'),array('no-repeat'=>'no-repeat',	'repeat'=>'repeat','repeat-x'=>'repeat-x','repeat-y'=>'repeat-y')); ?>

<?php print CHtml::label('Background Position',''); ?>
<?php print CHtml::dropDownList('ThemeAttribute[mainmenu_background_position]',$model->getAttr('mainmenu_background_position'),array('center top'=>'center top','center center'=>'center center','center bottom'=>'center bottom','left top'=>'left top','left center'=>'left center','left bottom'=>'left bottom','right top'=>'right top','right center'=>'right center','right bottom')); ?>

<?php print CHtml::label('Background Attachment',''); ?>
<?php print CHtml::dropDownList('ThemeAttribute[mainmenu_background_attachment]',$model->getAttr('mainmenu_background_attachment'),array('scroll'=>'scroll','fixed'=>'fixed')); ?>




<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton',array(
			'buttonType'=>'submit',
			'label'=>'Simpan',
			'icon'=>'ok white',
			'context'=>'primary'
	)); ?>
</div>

<?php print CHtml::endForm(); ?>