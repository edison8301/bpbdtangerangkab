<h2>Main Menu</h2>

<?php print CHTml::beginForm(array('theme/updateAttribute','id'=>1),'POST',array('enctype'=>'multipart/form-data')); ?>

<?php print CHtml::label('Font Color',''); ?>
<?php print CHtml::textField('ThemeAttribute[running_font_color]',$model->getAttr('running_font_color'),array('class'=>'span3')); ?>


<?php print CHtml::label('Background Color',''); ?>
<?php print CHtml::textField('ThemeAttribute[running_background_color]',$model->getAttr('running_background_color'),array('class'=>'span3')); ?>


<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton',array(
			'buttonType'=>'submit',
			'label'=>'Simpan',
			'icon'=>'ok white',
			'context'=>'primary'
	)); ?>
</div>

<?php print CHtml::endForm(); ?>