<?php
$this->breadcrumbs=array(
	'Themes'=>array('index'),
	$model->title,
);

$this->menu=array(
array('label'=>'List Theme','url'=>array('index')),
array('label'=>'Create Theme','url'=>array('create')),
array('label'=>'Update Theme','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Theme','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Theme','url'=>array('admin')),
);
?>

<h1>Lihat Tema</h1>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			'title',
		),
)); ?>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbTabs', array(
		'type'=>'tabs',
		'placement'=>'top',
		'tabs'=>array(
			//array('label'=>'Logo','content'=>$this->renderPartial('_logo',array('model'=>$model),true),'active'=>true),
			array('label'=>'Background','content'=>$this->renderPartial('_background',array('model'=>$model),true),'active'=>true),
			//array('label'=>'Header','content'=>$this->renderPartial('_header',array('model'=>$model),true)),
			//array('label'=>'Mainmenu','content'=>$this->renderPartial('_mainmenu',array('model'=>$model),true)),
			//array('label'=>'Running Text','content'=>$this->renderPartial('_running_text',array('model'=>$model),true)),
			//array('label'=>'Box Header','content'=>$this->renderPartial('_box_header',array('model'=>$model),true)),
			//array('label'=>'Box Content','content'=>$this->renderPartial('_box_content',array('model'=>$model),true)),
		
		),
	));
?>
