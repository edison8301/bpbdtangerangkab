<?php
$this->breadcrumbs=array(
	'Theme Attributes'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List ThemeAttribute','url'=>array('index')),
array('label'=>'Create ThemeAttribute','url'=>array('create')),
array('label'=>'Update ThemeAttribute','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete ThemeAttribute','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage ThemeAttribute','url'=>array('admin')),
);
?>

<h1>View ThemeAttribute #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'theme_id',
		'key',
		'value',
),
)); ?>
