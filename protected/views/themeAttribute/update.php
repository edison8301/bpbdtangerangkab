<?php
$this->breadcrumbs=array(
	'Theme Attributes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List ThemeAttribute','url'=>array('index')),
	array('label'=>'Create ThemeAttribute','url'=>array('create')),
	array('label'=>'View ThemeAttribute','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage ThemeAttribute','url'=>array('admin')),
	);
	?>

	<h1>Update ThemeAttribute <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>