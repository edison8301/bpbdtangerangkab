<?php
$this->breadcrumbs=array(
	'Theme Attributes'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List ThemeAttribute','url'=>array('index')),
array('label'=>'Manage ThemeAttribute','url'=>array('admin')),
);
?>

<h1>Create ThemeAttribute</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>