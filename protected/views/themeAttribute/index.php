<?php
$this->breadcrumbs=array(
	'Theme Attributes',
);

$this->menu=array(
array('label'=>'Create ThemeAttribute','url'=>array('create')),
array('label'=>'Manage ThemeAttribute','url'=>array('admin')),
);
?>

<h1>Theme Attributes</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
