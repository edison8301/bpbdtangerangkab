<?php
$this->breadcrumbs=array(
	'Menu Item Types',
);

$this->menu=array(
array('label'=>'Create MenuItemType','url'=>array('create')),
array('label'=>'Manage MenuItemType','url'=>array('admin')),
);
?>

<h1>Menu Item Types</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
