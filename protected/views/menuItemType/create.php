<?php
$this->breadcrumbs=array(
	'Menu Item Types'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List MenuItemType','url'=>array('index')),
array('label'=>'Manage MenuItemType','url'=>array('admin')),
);
?>

<h1>Create MenuItemType</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>