<?php
$this->breadcrumbs=array(
	'Menu Item Types'=>array('index'),
	$model->title,
);

$this->menu=array(
array('label'=>'List MenuItemType','url'=>array('index')),
array('label'=>'Create MenuItemType','url'=>array('create')),
array('label'=>'Update MenuItemType','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete MenuItemType','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage MenuItemType','url'=>array('admin')),
);
?>

<h1>View MenuItemType #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'title',
),
)); ?>
