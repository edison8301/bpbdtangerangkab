<?php
$this->breadcrumbs=array(
	'Menu Item Types'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List MenuItemType','url'=>array('index')),
	array('label'=>'Create MenuItemType','url'=>array('create')),
	array('label'=>'View MenuItemType','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage MenuItemType','url'=>array('admin')),
	);
	?>

	<h1>Update MenuItemType <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>