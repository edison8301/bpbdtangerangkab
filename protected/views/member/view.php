<?php
$this->breadcrumbs=array(
	'Member'=>array('admin'),
	$model->nama,
);
?>
<h1>Lihat Member</h1>
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Sunting',
		'context'=>'primary',
		'icon'=>'pencil white',
		'url'=>array('/member/update','id'=>$model->id)
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Kelola',
		'context'=>'primary',
		'icon'=>'list white',
		'url'=>array('/member/admin')
)); ?>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			'email',
			//'password',
			'nama',
			'alamat',
			'telepon',
			'nama_instansi',
			'alamat_instansi',
			'telepon_instansi',
			'login_terakhir',
			'waktu_dibuat',
		),
)); ?>

<h3>Inovasi Dikirim</h3>

<table class='table'>
<tr>
	<th>No</th>
	<th>Nama Inovasi</th>
	<th>Jenis Inovasi</th>
	<th>Kelompok Inovasi</th>
	<th>Pengirim</th>
	<th>Waktu Pengiriman</th>
	<th>Status</th>
	<th>Action</th>
</tr>
<?php $i=1; foreach($model->getDataInovasi() as $data) { ?>
<tr>
	<td><?php print $i; ?></td>
	<td><?php print CHtml::link($data->nama_inovasi,array('inovasi/view','id'=>$data->id)); ?></td>
	<td><?php print $data->jenis_inovasi->nama; ?></td>
	<td><?php print $data->kelompok_inovator->nama; ?></td>
	<td><?php print CHtml::link($data->member->nama,array('member/view','id'=>$data->member_id)); ?></td>
	<td><?php print $data->waktu_dibuat; ?></td>
	<td><?php print $data->status_inovasi->nama; ?></td>
	<td>
		<?php print CHtml::link("<i class=\"icon-eye-open\"></i>",array('inovasi/view','id'=>$data->id)); ?>
		<?php print CHtml::link("<i class=\"icon-pencil\"></i>",array('inovasi/update','id'=>$data->id)); ?>
		<?php print CHtml::link("<i class=\"icon-trash\"></i>",array('inovasi/hapus','id'=>$data->id),array('onclick'=>'return confirm("Yakin akan menghapus data?")')); ?>
	</td>
</tr>
<?php $i++; } ?>
</table>