<?php
$this->breadcrumbs=array(
	'Members'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Member','url'=>array('index')),
array('label'=>'Manage Member','url'=>array('admin')),
);
?>

<div class="margin20">
	
	<h3 class="list-bottom">Registrasi Member</h3>

	<hr>
	
	<h3>Registrasi Member</h3>
	
	<p>Silahkan gunakan form di bawah ini untuk melakukan registrasi member baru.</p>
	
	<p>Sudah terdaftar? Silahkan login  <?php print CHtml::link("di sini",array('member/login')); ?></p>
	
	<hr>
	
	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
			'id'=>'member-form',
			'enableAjaxValidation'=>false,
	)); ?>


	<?php echo $form->errorSummary($model); ?>
	
	<h4>Informasi Login</h4>
	<?php echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->passwordFieldRow($model,'password',array('class'=>'span5','maxlength'=>255)); ?>
	
	<hr>
	
	<h4>Data Member</h4>
	<?php echo $form->textFieldRow($model,'nama',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textAreaRow($model,'alamat',array('rows'=>6, 'cols'=>50, 'class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'telepon',array('class'=>'span5','maxlength'=>255)); ?>
	
	<hr>
	
	<h4>Data Instansi (Opsional)</h4>
	
	<?php echo $form->textFieldRow($model,'nama_instansi',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textAreaRow($model,'alamat_instansi',array('rows'=>6, 'cols'=>50, 'class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'telepon_instansi',array('class'=>'span5','maxlength'=>255)); ?>



<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'icon'=>'ok white',
			'label'=>'Submit',
		)); ?>
</div>

<?php $this->endWidget(); ?>

</div>