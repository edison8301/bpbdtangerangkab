<?php
$this->breadcrumbs=array(
	'Members'=>array('index'),
	'Manage',
);
?>

<h1>Kelola Member</h1>


<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'member-grid',
		'dataProvider'=>$model->search(),
		'type'=>'striped bordered',
		'filter'=>$model,
		'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'headerHtmlOptions'=>array('width'=>'5%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			'nama',
			'email',
			'telepon',
			'alamat',
			array(
				'class'=>'CDataColumn',
				'name'=>'aktif',
				'header'=>'Aktif',
				'type'=>'raw',
				'value'=>'$data->aktif ==1 ? "Ya" : "Tidak"',
				'filter'=>array('0'=>'Tidak','1'=>'Ya'),
			),
			'login_terakhir',
			array(
				'class'=>'booster.widgets.TbButtonColumn',
			),
		),
)); ?>
