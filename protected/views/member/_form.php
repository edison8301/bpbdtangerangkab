<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'member-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>		<?php echo $form->dropDownListRow($model,'aktif',array('1'=>'Ya','0'=>'Tidak')); ?>		<hr>		<h3>Info Login</h3>
	<?php echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>255)); ?>
	<?php echo $form->passwordFieldRow($model,'password',array('class'=>'span5','maxlength'=>255)); ?>
		<hr>		<h3>Data Member</h3>
	<?php echo $form->textFieldRow($model,'nama',array('class'=>'span5','maxlength'=>255)); ?>
	<?php echo $form->textAreaRow($model,'alamat',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>
	<?php echo $form->textFieldRow($model,'telepon',array('class'=>'span5','maxlength'=>255)); ?>
	<hr>		<h3>Data Instansi</h3>
	<?php echo $form->textFieldRow($model,'nama_instansi',array('class'=>'span5','maxlength'=>255)); ?>
	<?php echo $form->textAreaRow($model,'alamat_instansi',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>
	<?php echo $form->textFieldRow($model,'telepon_instansi',array('class'=>'span5','maxlength'=>255)); ?>


	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'icon'=>'ok white',
				'label'=>'Simpan',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
