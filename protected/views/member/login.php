<?php
$this->breadcrumbs=array(
	'Members'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Member','url'=>array('index')),
array('label'=>'Manage Member','url'=>array('admin')),
);
?>

<div class="margin20">
	
	<h3 class="list-bottom">Login Member</h3>

	<hr>
	
	<h3>Login Member</h3>
	
	<?php foreach(Yii::app()->user->getFlashes() as $key => $message) { ?>
        <div class="alert alert-<?php print $key; ?>"><?php print $message; ?></div>
    <?php } ?>
	
	<p>Silahkan gunakan form di bawah ini untuk melakukan login</p>
	
	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
			'id'=>'member-form',
			'enableAjaxValidation'=>false,
	)); ?>


	<?php echo $form->errorSummary($model); ?>
	
	<?php echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->passwordFieldRow($model,'password',array('class'=>'span5','maxlength'=>255)); ?>
	
	<?php if(CCaptcha::checkRequirements()): ?>
	<div>
		<?php echo $form->labelEx($model,'verifyCode'); ?>
		<div>
		<?php $this->widget('CCaptcha'); ?>
		<?php echo $form->textField($model,'verifyCode'); ?>
		</div>
		<div class="hint">Mohon ketik karakter seperti ditunjukkan pada gambar di atas
		<br/>Karakter tidak case-sensitive.</div>
		<?php echo $form->error($model,'verifyCode'); ?>
	</div>
	<?php endif; ?>
	
	<div>&nbsp;</div>
	
	<p>Belum terdaftar? Silahkan registrasi <?php print CHtml::link("di sini",array('member/signup')); ?></p>
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'icon'=>'lock white',
			'label'=>'Login',
		)); ?>&nbsp;
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'link',
			'type'=>'primary',
			'icon'=>'envelope white',
			'label'=>'E-Direktori',
			'url'=>array('inovasi/edirektori')
		)); ?>
	</div>

<?php $this->endWidget(); ?>

</div>