<h1>Daftar Bencana Terakhir</h1>

<?php $model = new Bencana; ?>

	<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'bencana-grid',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'type' => 'striped bordered',
		'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'headerHtmlOptions'=>array('width'=>'5%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'header' => 'Tanggal',
				'name' => 'tanggal',
				'value' => 'Helper::tanggal($data->tanggal)',
			),
			array(
				'header' => 'Bencana',
				'name' => 'id_bencana_jenis',
				'value' => '$data->getBencanaJenis()',
				'filter' => CHtml::ListData(BencanaJenis::model()->findAll(array('order' => ' nama ASC')), 'id', 'nama'),
			),
			array(
				'header' => 'Lokasi Bencana',
				'name' => 'lokasi',
				'value' => '$data->lokasi'),
			'penyebab'
		),
)); ?>