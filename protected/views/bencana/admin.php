<?php
$this->breadcrumbs=array(
	'Bencanas'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Bencana','url'=>array('index')),
array('label'=>'Create Bencana','url'=>array('create')),
);

?>

<h1>Kelola Bencana</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'plus white',
		'label'=>'Tambah',
		'url'=>array('/bencana/create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'list',
		'label'=>'Desa',
		'url'=>array('/bencanaDesa/admin')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'list',
		'label'=>'Kecamatan',
		'url'=>array('/bencanaKecamatan/admin')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'list',
		'label'=>'Jenis',
		'url'=>array('/bencanaJenis/admin')
)); ?>&nbsp;


<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'bencana-grid',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'type' => 'striped bordered',
		'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'headerHtmlOptions'=>array('width'=>'5%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			'tanggal',
			array(
				'header' => 'Jenis  Bencana',
				'name' => 'id_bencana_jenis',
				'value' => '$data->getBencanaJenis()',
				'filter' => CHtml::ListData(BencanaJenis::model()->findAll(array('order' => 'nama ASC')), 'id', 'nama')
			),
			'lokasi',
			array(
				'header' => 'Desa',
				'name' => 'id_bencana_desa',
				'value' => '$data->getBencanaDesa()',
				'filter' => CHtml::ListData(BencanaDesa::model()->findAll(array('order' => 'nama ASC')), 'id', 'nama')
			),
			array(
				'header' => 'Kecamatan',
				'name' => 'id_bencana_kecamatan',
				'value' => '$data->getBencanaKecamatan()',
				'filter' => CHtml::ListData(BencanaKecamatan::model()->findAll(array('order' => 'nama ASC')), 'id', 'nama')
				),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'htmlOptions' => array('width' => '7%')
			),
		),
)); ?>
