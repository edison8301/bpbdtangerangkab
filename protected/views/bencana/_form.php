<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'bencana-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

		<?php echo $form->datePickerGroup($model,'tanggal',array('widgetOptions'=>array('options'=>array('format'=>'yyyy-mm-dd','autoclose'=>true),'htmlOptions'=>array('class'=>'span5')), 'prepend'=>'<i class="glyphicon glyphicon-calendar"></i>')); ?>

		<?php echo $form->dropDownListGroup($model,'id_bencana_jenis',array(
			'widgetOptions'=>array(
				'data'=>CHtml::listData(BencanaJenis::model()->findAll(array('order'=>'id ASC')),'id','nama')
			)
	)); ?>

	<?php echo $form->textFieldGroup($model,'lokasi',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

			<?php echo $form->dropDownListGroup($model,'id_bencana_desa',array(
			'widgetOptions'=>array(
				'data'=>CHtml::listData(BencanaDesa::model()->findAll(array('order'=>'id ASC')),'id','nama')
			)
	)); ?>

			<?php echo $form->dropDownListGroup($model,'id_bencana_kecamatan',array(
			'widgetOptions'=>array(
				'data'=>CHtml::listData(BencanaKecamatan::model()->findAll(array('order'=>'id ASC')),'id','nama')
			)
	)); ?>

	<?php echo $form->textFieldGroup($model,'nama_korban',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->textFieldGroup($model,'penyebab',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->textFieldGroup($model,'korban_materi',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

	<?php echo $form->textFieldGroup($model,'korban_kk',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

	<?php echo $form->textFieldGroup($model,'korban_jiwa',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

	<?php echo $form->textFieldGroup($model,'korban_meninggal',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

	<?php echo $form->textFieldGroup($model,'korban_luka',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

	<?php echo $form->textAreaGroup($model,'keterangan', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>6, 'cols'=>50, 'class'=>'span8')))); ?>


<div class="form-actions well">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'icon' => 'ok',
			'context'=>'primary',
			'label'=> 'Simpan',
		)); ?>
</div>

<?php $this->endWidget(); ?>
