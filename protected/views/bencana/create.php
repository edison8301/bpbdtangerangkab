<?php
$this->breadcrumbs=array(
	'Bencanas'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Bencana','url'=>array('index')),
array('label'=>'Manage Bencana','url'=>array('admin')),
);
?>

<h1>Input Data Bencana</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>