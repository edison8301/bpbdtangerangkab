<?php
$this->breadcrumbs=array(
	'Bencanas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Bencana','url'=>array('index')),
	array('label'=>'Create Bencana','url'=>array('create')),
	array('label'=>'View Bencana','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Bencana','url'=>array('admin')),
	);
	?>

	<h1>Update Bencana <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>