<?php
$this->breadcrumbs=array(
	'Bencanas'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Bencana','url'=>array('index')),
array('label'=>'Create Bencana','url'=>array('create')),
array('label'=>'Update Bencana','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Bencana','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Bencana','url'=>array('admin')),
);
?>

<h1>Lihat Bencana ID: <b><?php echo $model->id; ?></b></h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'pencil white',
		'label'=>'Sunting',
		'url'=>array('/bencana/update','id'=>$model->id)
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'plus white',
		'label'=>'Tambah',
		'url'=>array('/bencana/create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'list white',
		'label'=>'Kelola',
		'url'=>array('/bencana/admin')
)); ?>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type' => 'striped bordered',
'attributes'=>array(
		'id',
		'tanggal',
		array(
			'label' => 'Jenis Bencana',
			'type' => 'raw',
			'value' => $model->getBencanaJenis()),
		'lokasi',
		array(
			'label' => 'Bencana Desa',
			'type' => 'raw',
			'value' => $model->getBencanaDesa()),
		array(
			'label' => 'Bencana Kecamatan',
			'type' => 'raw',
			'value' => $model->getBencanaKecamatan()),
		'nama_korban',
		'penyebab',
		'korban_materi',
		'korban_kk',
		'korban_jiwa',
		'korban_meninggal',
		'korban_luka',
		'keterangan',
		'waktu_dibuat',
),
)); ?>
