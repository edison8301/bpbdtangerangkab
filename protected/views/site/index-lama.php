<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;

Yii::app()->clientScript->registerScript('tabs','
	$(document).ready(function() {
		$( "#tabs" ).tabs();
	});
');

?>

<!-- start slideshow -->
<section id="slide-panel">
	<div class="slider-bootstrap"><!-- start slider -->
		<div id="myCarousel" class="carousel slide" style="min-height:360px;">
			<!--Carousel indicator-->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" ></li>
			</ol>
			
			<!-- Carousel items -->
			<div class="carousel-inner">	      
				<?php $j=1; foreach(Slide::model()->findAll(array('order'=>'id DESC', 'limit'=>'10')) as $data) { ?> 
				<div class="<?php echo $j==1 ? "active" : "";?> item" style="min-height:360px;">
					<img src="<?php echo Yii::app()->baseUrl.'/uploads/slide/'.$data->file;?>" width="1000" height="360" />
					
					<div class="carousel-caption">
						<h3><?php echo $data->title;?></h3>
						<p class="lead"><?php echo $data->title;?></p>
					</div>
				</div>
				<?php $j++; }?>
				      
			</div>
			<!-- Carousel nav -->
			<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
			<a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
		</div>
	</div>
</section>
<script>
    $('.carousel').carousel({
		interval: 5000,
		pause:"hover",
    })
</script>
<!-- /slider -->
<!-- end slideshow -->

<!-- start content site -->
<div class='margin20' id="site-index">
	<div class="row-fluid" style="margin-bottom: 20px">
		<div class="span8">
			<?php /*
			<h3 class="list">Inovasi Terbaru</h3>
			<hr>
			
			<div id='inovasi-terbaru'>
			<?php $this->widget('bootstrap.widgets.TbTabs',array(
					'type' => 'tabs', // 'tabs' or 'pills'
					'tabs' => array(
						array(
							'label' => 'Pemerintahan',
							'content' => $this->renderPartial('_tab',array('kategori_id'=>1),$this),
							'active' => true
						),
						array(
							'label' => 'Ekonomi',
							'content' => $this->renderPartial('_tab',array('kategori_id'=>2),$this),
						),
						array(
							'label' => 'Sosial Budaya',
							'content' => $this->renderPartial('_tab',array('kategori_id'=>3),$this),
						),
						array(
							'label' => 'Teknologi',
							'content' => $this->renderPartial('_tab',array('kategori_id'=>4),$this),
						),
						array(
							'label' => 'Kemasyarakatan',
							'content' => $this->renderPartial('_tab',array('kategori_id'=>5),$this),
						),
					),
			)); ?>	
			</div>
			*/ ?>
			
			<h3 class="list-bottom">BERITA</h3>
			<hr>
			
			<div id="berita" class="row-fluid" style="width:590px">
			
			<?php foreach(Post::model()->findAll() as $model){ ?>
                       			
				<div class="float-left" style="width:200px;height:220px">
					<div class="news-img"><?php echo $model->getThumbnail(array('width'=>'180px')); ?></div>
					<div class="tittle-news"><?php echo CHtml::link($model->title, array('/post/read&id='.$model->id));?></div><br>
				</div>
								
			<?php } ?>
			<?php $this->widget('ext.carouFredSel.ECarouFredSel', array(
					'id' => 'carousel',
					'target' => '#berita',
					'config' => array(
						'items'=>3,
						'prev'=>array(
							'button'=>'.next'
						),
						'scroll'=>array(
							'fx'=>'directscroll',
							'items'=>2,
							'pauseOnHover'=>true
						),
						'pagination'=>array(
							'items'=>2
						)
					)
			)); ?>			
			</div>
			
			<h3 class="list-bottom">ARTIKEL</h3>
			<hr>
			
			<div id="artikel" class="row-fluid" style="width:590px;height:150px">
			<?php foreach(Post::model()->findAll() as $model){ ?>
                       			
				<div class="float-left" style="width:200px;height:220px">
					<div class="news-img"><?php echo $model->getThumbnail(array('width'=>'180px')); ?></div>
					<div class="tittle-news"><?php echo CHtml::link($model->title, array('/post/read&id='.$model->id));?></div><br>
				</div>
								
			<?php } ?>
			<?php $this->widget('ext.carouFredSel.ECarouFredSel', array(
					'id' => 'carousel',
					'target' => '#artikel',
					'config' => array(
						'items'=>3,
						'scroll'=>array(
							'fx'=>'directscroll',
							'items'=>2,
							'pauseOnHover'=>true
						),
						'height'=>'150px',
						'pagination'=>array(
							'items'=>2
						)
					)
			)); ?>			
			</div>
			
			<h3 class="list-bottom">AGENDA</h3>
			<hr>
			
			<div id="agenda" class="row-fluid" style="width:590px;height:150px">
			<?php foreach(Post::model()->findAll() as $model){ ?>
                       			
				<div class="float-left" style="width:200px;height:220px">
					<div class="news-img"><?php echo $model->getThumbnail(array('width'=>'180px')); ?></div>
					<div class="tittle-news"><?php echo CHtml::link($model->title, array('/post/read&id='.$model->id));?></div><br>
				</div>
								
			<?php } ?>
			<?php $this->widget('ext.carouFredSel.ECarouFredSel', array(
					'id' => 'carousel',
					'target' => '#agenda',
					'config' => array(
						'items'=>3,
						'scroll'=>array(
							'fx'=>'directscroll',
							'items'=>2,
							'pauseOnHover'=>true
						),
						'height'=>'150px',
						'pagination'=>array(
							'items'=>2
						)
					)
			)); ?>			
			</div>
			
			<h3 class="list-bottom">POLICY BRIEF</h3>
			<hr>
			
			<div id="policy-brief" class="row-fluid" style="width:590px;height:150px">
			<?php foreach(Post::model()->findAll() as $model){ ?>
                       			
				<div class="float-left" style="width:200px;height:220px">
					<div class="news-img"><?php echo $model->getThumbnail(array('width'=>'180px')); ?></div>
					<div class="tittle-news"><?php echo CHtml::link($model->title, array('/post/read&id='.$model->id));?></div><br>
				</div>
								
			<?php } ?>
			<?php $this->widget('ext.carouFredSel.ECarouFredSel', array(
					'id' => 'carousel',
					'target' => '#policy-brief',
					'config' => array(
						'items'=>3,
						'scroll'=>array(
							'fx'=>'directscroll',
							'items'=>2,
							'pauseOnHover'=>true
						),
						'height'=>'150px',
						'pagination'=>array(
							'items'=>2
						)
					)
			)); ?>			
			</div>
			
		</div>
	
		<div class="span4">
			
			<?php $this->renderPartial('//layouts/block/pencarian'); ?>
			
			<?php $this->renderPartial('//layouts/block/e-directory'); ?>
				
			<?php $this->renderPartial('//layouts/block/sitemap'); ?>
			
			<?php $this->renderPartial('//layouts/block/inovasi'); ?>
			
			<?php $this->renderPartial('//layouts/block/forum'); ?>
			
			<?php $this->renderPartial('//layouts/block/visitor-counter'); ?>

		</div>
	</div>
	
	<div class="row-fluid">
		<div class='span4'>
			<h4 class='block-title'>BERITA</h4>
			<hr>
			<div id="e-book">
				<?php foreach(Post::model()->getLatestPostByCategoryId() as $model){ ?>
				<div class='row-fluid' style="margin-bottom:10px;">
					<div class="span3"><?php echo $model->getThumbnail(); ?></div>
					<div class="span9"><?php echo CHtml::link($model->title, array('/post/read&id='.$model->id));?></div><br>
				</div>					
				<?php } ?>
			</div>
			
			<div>&nbsp;</div>
			
		</div>
		<div class='span4'>
			<h4 class='block-title'>ARTIKEL</h4>
			<hr>
			<div id="jurnal">
				<?php foreach(Post::model()->getLatestPostByCategoryId() as $model){ ?>       			
				<div class="row-fluid" style="margin-bottom:10px;">
					<div class="span3"><?php echo $model->getThumbnail(); ?></div>
					<div class="span9"><?php echo CHtml::link($model->title, array('/post/read&id='.$model->id));?></div><br>
				</div>
				<?php } ?>
			</div>
			
			<div>&nbsp;</div>
			
		</div>
		<div class='span4'>
			<h4 class='block-title'>AGENDA</h4>
				<hr>
				<div id="karya-tulis">
				<?php foreach(Post::model()->getLatestPostByCategoryId() as $model){ ?>
				<div class="row-fluid" style="margin-bottom:10px">
					<div class="span3"><?php echo $model->getThumbnail(); ?></div>
					<div class="span9"><?php echo CHtml::link($model->title, array('/post/read&id='.$model->id));?></div><br>
				</div>
								
				<?php } ?>
					
				</div>
				
				<div>&nbsp;</div>
		</div>
		
	</div>
	
	<div class="row-fluid">
		<div class='span4'>
			<h4 class='block-title'>E-BOOK</h4>
			<hr>
			<div id="e-book">
				<?php foreach(Post::model()->getLatestPostByCategoryId() as $model){ ?>
				<div class='row-fluid' style="margin-bottom:10px;">
					<div class="span3"><?php echo $model->getThumbnail(); ?></div>
					<div class="span9"><?php echo CHtml::link($model->title, array('/post/read&id='.$model->id));?></div><br>
				</div>					
				<?php } ?>
			</div>
			<div>&nbsp;</div>
		</div>
		<div class='span4'>
			<h4 class='block-title'>JURNAL</h4>
			<hr>
			<div id="jurnal">
				<?php foreach(Post::model()->getLatestPostByCategoryId() as $model){ ?>       			
				<div class="row-fluid" style="margin-bottom:10px;">
					<div class="span3"><?php echo $model->getThumbnail(); ?></div>
					<div class="span9"><?php echo CHtml::link($model->title, array('/post/read&id='.$model->id));?></div><br>
				</div>
				<?php } ?>
				<div>&nbsp;</div>
			</div>
		</div>
		<div class='span4'>
			<h4 class='block-title'>KARYA TULIS</h4>
				<hr>
				<div id="karya-tulis">
				<?php foreach(Post::model()->getLatestPostByCategoryId() as $model){ ?>
				<div class="row-fluid" style="margin-bottom:10px">
					<div class="span3"><?php echo $model->getThumbnail(); ?></div>
					<div class="span9"><?php echo CHtml::link($model->title, array('/post/read&id='.$model->id));?></div><br>
				</div>
				<?php } ?>
				<div>&nbsp;</div>	
				</div>
		</div>
		
	</div>


</div>
