<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle=Yii::app()->name . ' - Contact Us';
$this->breadcrumbs=array(
	'Contact',
);

$menuItem = MenuItem::model()->findByPk($_GET['id']);

?>

<div id="centerbar" class="margin20">

	<div class="block-title">
		<span>Hubungi Kami</span>
	</div>

	<div>&nbsp;</div>
	
<?php if(Yii::app()->user->hasFlash('contact')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>

<?php else: ?>

<?php if($menuItem!==null) { ?>
<?php print $menuItem->getMenuAttributeValueByKey('form_contact_html'); ?>
<?php } ?>

<?php /*
<iframe style="border:2px solid #ccc;padding:5px" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7933.444647456471!2d106.82735863396933!3d-6.167925902216178!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f5d0f93e22ab%3A0xb068468c95b3ffee!2sLembaga+Administrasi+Negara!5e0!3m2!1sid!2s!4v1403618266199" width="550" height="400" frameborder="0" style="border:0"></iframe>
*/ ?>

<div>&nbsp;</div>

<div>
Silahkan gunakan form di bawah ini untuk menghubungi kami.
</div>

<div>&nbsp;</div>

<div class="form">

<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'=>'contact-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<?php /*
	<p class="note">Fields with <span class="required">*</span> are required.</p>
	*/ ?>
	
	<?php echo $form->errorSummary($model); ?>
	
	<?php echo $form->textFieldGroup($model,'name'); ?>

	<?php echo $form->textFieldGroup($model,'email'); ?>

	<?php echo $form->textFieldGroup($model,'subject',array('size'=>60,'maxlength'=>128)); ?>
	
	<?php echo $form->textAreaGroup($model,'body',array('rows'=>6, 'cols'=>50)); ?>

	<?php if(CCaptcha::checkRequirements()): ?>
	<div>
		<?php echo $form->labelEx($model,'verifyCode'); ?>
		<div>
		<?php $this->widget('CCaptcha'); ?>
		<?php echo $form->textField($model,'verifyCode'); ?>
		</div>
		<div class="hint">Please enter the letters as they are shown in the image above.
		<br/>Letters are not case-sensitive.</div>
		<?php echo $form->error($model,'verifyCode'); ?>
	</div>
	<?php endif; ?>

	<div>&nbsp;</div>
	
	<div class="action-forms well">
	<?php $this->widget('booster.widgets.TbButton',array(
			'buttonType'=>'submit',
			'icon'=>'ok',
			'label'=>'Kirim Pesan',
			'context'=>'primary'
	)); ?>
	</div>
	

	<?php $this->endWidget(); ?>
	
	<div>&nbsp;</div>
	
</div><!-- form -->

<?php endif; ?>

</div>