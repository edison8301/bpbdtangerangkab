<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;

Yii::app()->clientScript->registerScript('tabs','
	$(document).ready(function() {
		$( "#tabs" ).tabs();
	});
');

?>

<!-- start slideshow -->
<section id="slide-panel">
	<div class="slider-bootstrap"><!-- start slider -->
		<div id="myCarousel" class="carousel slide" style="min-height:360px;">
			<!--Carousel indicator-->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" ></li>
			</ol>
			<!-- Carousel items -->
			<div class="carousel-inner">	      
				<?php $j=1; foreach(Slide::model()->findAll(array('order'=>'id DESC', 'limit'=>'10')) as $data) { ?> 
				<div class="<?php echo $j==1 ? "active" : "";?> item" style="min-height:360px;">
					<img src="<?php echo Yii::app()->baseUrl.'/uploads/slide/'.$data->file;?>" width="1000" height="360" />
					<div class="carousel-caption">
						<h3><?php echo $data->title;?></h3>
						<p class="lead">&nbsp;</p>
					</div>
				</div>
				<?php $j++; }?>
			</div>
			<!-- Carousel nav -->
			<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
			<a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
		</div>
	</div>
</section>
<script>
    $('.carousel').carousel({
		interval: 5000,
		pause:"hover",
    })
</script>
<!-- /slider -->
<!-- end slideshow -->

<!-- start content site -->
<div class='margin20' id="site-index">
	<div class="row-fluid" style="margin-bottom: 20px">
		<div class="span8">
			
			
			<?php foreach(Block::model()->findAllByAttributes(array('block_position_id'=>'home_center'),array('order'=>'t.order ASC')) as $block) { ?>
				<?php print $block->getBlockDisplay(); ?>
			<?php } ?>
			
			
			<?php //$this->renderPartial('//layouts/block/statistik'); ?>
			
			
			
		</div>
	
		<div class="span4">
			
			<?php foreach(Block::model()->findAllByAttributes(array('block_position_id'=>'rightbar'),array('order'=>'t.order ASC')) as $block) { ?>
				<?php print $block->getBlockDisplay(); ?>
			<?php } ?>	

		</div>
	</div>
	
	<div class="row-fluid">
		<div class='span4'>
			<?php foreach(Block::model()->findAllByAttributes(array('block_position_id'=>'home_bottom_1_left'),array('order'=>'t.order ASC')) as $block) { ?>
				<?php print $block->getBlockDisplay(); ?>
			<?php } ?>			
		</div>
		<div class='span4'>
			<?php foreach(Block::model()->findAllByAttributes(array('block_position_id'=>'home_bottom_1_center'),array('order'=>'t.order ASC')) as $block) { ?>
				<?php print $block->getBlockDisplay(); ?>
			<?php } ?>
			
		</div>
		<div class='span4'>
			<?php foreach(Block::model()->findAllByAttributes(array('block_position_id'=>'home_bottom_1_right'),array('order'=>'t.order ASC')) as $block) { ?>
				<?php print $block->getBlockDisplay(); ?>
			<?php } ?>
		</div>
		
	</div>
	
	<div class="row-fluid">
		<div class='span4'>
			<?php foreach(Block::model()->findAllByAttributes(array('block_position_id'=>'home_bottom_2_left'),array('order'=>'t.order ASC')) as $block) { ?>
				<?php print $block->getBlockDisplay(); ?>
			<?php } ?>
		</div>
		<div class='span4'>
			<?php foreach(Block::model()->findAllByAttributes(array('block_position_id'=>'home_bottom_2_center'),array('order'=>'t.order ASC')) as $block) { ?>
				<?php print $block->getBlockDisplay(); ?>
			<?php } ?>
		</div>
		<div class='span4'> 
			<?php foreach(Block::model()->findAllByAttributes(array('block_position_id'=>'home_bottom_2_right'),array('order'=>'t.order ASC')) as $block) { ?>
				<?php print $block->getBlockDisplay(); ?>
			<?php } ?>
		</div>
		
	</div>


</div>
