<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<div class="site-index">
<div class="row-fluid" style="margin-bottom: 20px">
    <div class="span8 panel-bg">
	<h3 class="list">Berita</h3>
	<hr>
	<p style="text-align: justify">
		<div class="horizontal-post">
			 <?php foreach(Post::model()->findAll() as $model){?>
			<div class="span4 news table">
					<div class="news-img"><img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/post/<?php echo $model->thumbnail; ?>"></div>
					<span class="tittle"><?php echo $model->title;?></span><br>
					<div class= "news-text" style="color:white"><?php echo substr($model->content,0,100).'......';?></div>
					<div class="other"><?php echo CHtml::link('Read More', array('/site/postPage&id='.$model->id));?></div>
			</div>
			<?php } ?>	
			</div>
	</div>
	
	<div class="span4 visitor-counter">
		<h3 class="list">Visitor Counter</h3>
		<hr>
		<div class="content">
		    <div class="visitor panel-bg">
		    	<h4>100.000<small>visit all</small></h4>
		    	<h4>1.000<small>visit today</small></h4>
		    </div>
		</div>

		<h3 class="list">Site Map</h3>
		<hr>
		<div class="content">
		    <span class="sitemap"></span>
		</div>
		<h3 class="list">E-Directory</h3>
		<div class="content">
		    <span class="visitor">
		    	
		</div>
    </div>
</div>
</div>