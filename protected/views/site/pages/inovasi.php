<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<div class="site-index">
<div class="row-fluid" style="margin-bottom: 20px">
    <div class="span8 panel-bg">
	<h3 class="list">Inovasi</h3>
	<hr>
	<p style="text-align: justify">
	<script>
		  $(function() {
		    $( "#tabs" ).tabs();
		  });
  	</script>
  	<div id="tabs">
	  <ul>
	  	 <?php foreach(Inovasi::model()->findAll() as $model){?>
	    <li><a href="#<?php echo $model->kategori->nama;?>"><?php echo $model->kategori->nama;?></a></li>
	   	 <?php } ?>
	  </ul>
	  <?php foreach(Inovasi::model()->findAll() as $model){?>
	  <div class="tab-content">
	  	<div id="<?php echo $model->kategori->nama;?>">
	    	<p><h4><?php echo CHtml::link($model->nama_inovasi, array('/site/postPage&id='.$model->id));?></h4></p>
	  	</div>
	  </div>
	   <?php } ?>
	</div>
	
	<div class="span4 visitor-counter">
		<h3 class="list">Visitor Counter</h3>
		<hr>
		<div class="content">
		    <div class="visitor panel-bg">
		    	<h4>100.000<small>visit all</small></h4>
		    	<h4>1.000<small>visit today</small></h4>
		    </div>
		</div>

		<h3 class="list">Site Map</h3>
		<hr>
		<div class="content">
		    <span class="sitemap"></span>
		</div>
		<h3 class="list">E-Directory</h3>
		<div class="content">
		    <span class="visitor">
		    	
		</div>
    </div>
</div>
</div>