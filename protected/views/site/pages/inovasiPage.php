<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<div class="site-index">
<div class="row-fluid" style="margin-bottom: 20px">
    <div class="span8 panel-bg" style="text-align:justify">
	<h3 class="list">Inovasi</h3>
	<hr>
		<p class="tittle-inovasi"><?php echo $model->nama_inovasi;?></p>
		<p>Penggagas : <i><?php echo $model->penggagas;?></i></p>
		<p>Kelompok Inovator : <i><?php echo $model->inovator->nama;?></i></p>
		<hr>
		<span class="title">Deskripsi :</span>
		<p><?php echo $model->deskripsi;?></p>
		<span class="title">Faktor Pendorong :</span>
		<p><?php echo $model->faktor_pendorong;?></p>
		<span class="title">Tahapan Proses :</span>
		<p><?php echo $model->tahapan_proses;?></p>
		<span class="title">Output :</span>
		<p><?php echo $model->output;?></p>
	</div>
	
	<div class="span4 visitor-counter">
		<h3 class="list">Visitor Counter</h3>
		<hr>
		<div class="content">
		    <div class="visitor panel-bg">
		    	<h4>100.000<small>visit all</small></h4>
		    	<h4>1.000<small>visit today</small></h4>
		    </div>
		</div>

		<h3 class="list">Site Map</h3>
		<hr>
		<div class="content">
		    <span class="sitemap"></span>
		</div>
		<h3 class="list">E-Directory</h3>
		<div class="content">
		    <span class="visitor">
		    	
		</div>
    </div>
</div>
</div>