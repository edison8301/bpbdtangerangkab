<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<div class="site-index">
<div class="row-fluid" style="margin-bottom: 20px">
    <div class="span8 panel-bg" style="text-align:justify">
	<h3 class="list">Berita</h3>
	<hr>
		<div class="news-img-read"><img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/post/<?php echo $model->thumbnail; ?>"></div>
		<span class="tittle"><?php echo $model->title;?></span>
		<p class="tgl"><?php echo date("j F, Y", strtotime("$model->date_created")); // gives 201101$model->date_created;?></p>
		<p><?php echo $model->content;?></p>
	</div>
	
	<div class="span4 visitor-counter">
		<h3 class="list">Visitor Counter</h3>
		<hr>
		<div class="content">
		    <div class="visitor panel-bg">
		    	<h4>100.000<small>visit all</small></h4>
		    	<h4>1.000<small>visit today</small></h4>
		    </div>
		</div>

		<h3 class="list">Site Map</h3>
		<hr>
		<div class="content">
		    <span class="sitemap"></span>
		</div>
		<h3 class="list">E-Directory</h3>
		<div class="content">
		    <span class="visitor">
		    	
		</div>
    </div>
</div>
</div>