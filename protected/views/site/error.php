<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>

<div class="margin20" id='site-error'>
	
	<h3 class='list-bottom'>ERROR <?php echo $code; ?></h3>
	
	<hr>
	
	<h3>Error <?php echo $code; ?></h3>
	<div class="error">
	<?php echo CHtml::encode($message); ?>
	</div>
</div>