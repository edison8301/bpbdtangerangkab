<?php $model = Kategori::model()->findByPk($kategori_id); ?>
<div id="inovasi<?php print $kategori_id; ?>" class="site-index-inovasi-box">
<?php foreach(Inovasi::model()->findAllByAttributes(array('kategori_id'=>$kategori_id)) as $data) { ?>
<div class="site-index-inovasi">
	<div class="row-fluid">
		<div class="span12">
			<h3 class="tittle-inovasi"><?php echo CHtml::link($data->nama_inovasi, array('/inovasi/read','id'=>$data->id));?></h3>
			<div class='post-read-created-time'>
				<?php print Bantu::getWaktuDibuat($data->tanggal_inovasi); ?>
			</div>
		</div>
	</div>
	<div class="row-fluid">
	<div class="span4">
		<?php if($data->gambar_ilustrasi!='') print $data->getGambarIlustrasi(); else print CHtml::image(Yii::app()->request->baseUrl.'/images/logo-lan.png',''); ?>
	</div>
	<div class="span8">
		<?php echo substr($data->deskripsi,0,300).'...';?>
		<?php echo CHtml::link('Selanjutnya', array('/post/inovasi','id'=>$data->id));?>
	</div>
	</div>
</div>
<hr>
<?php } ?>
</div>

<?php //if($kategori_id == 1) { ?>
<?php $this->widget('ext.carouFredSel.ECarouFredSel', array(
					'id' => 'carousel'.$kategori_id,
					'target' => '#inovasi'.$kategori_id,
					'config' => array(
						'items'=>1,
						'scroll'=>array(
							'fx'=>'fade',
							'items'=>1,
							'pauseOnHover'=>true
						),
						'pagination'=>array(
							'items'=>1
						)
					)
)); ?>		
<?php //} ?>


			
			
			