<script type="text/javascript">

function openKCFinder(field) {

    window.KCFinder = {

        callBack: function(url) {

            field.value = url;

            window.KCFinder = null;

        }

    };

    window.open('<?php print Yii::app()->request->baseUrl; ?>/vendors/kcfinder/browse.php?type=files&dir=files/public', 'kcfinder_textbox',

        'status=0, toolbar=0, location=0, menubar=0, directories=0, ' +

        'resizable=1, scrollbars=0, width=800, height=600'

    );

}

</script>



<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
		'id'=>'slide-form',
		'enableAjaxValidation'=>false,
		'htmlOptions'=>array('enctype'=>'multipart/form-data')
)); ?>

	<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'title',array('class'=>'span5','maxlength'=>255)); ?>	

	<?php echo $form->labelEx($model,'file',array('label'=>'File (Ukuran 960px x 300px, Maksimal 200 KB)')); ?>

	<?php if($model->file!='') { ?>

	<?php print $model->getImage(); ?>

	<?php } ?>

	<?php echo $form->fileField($model,'file'); ?>

	<?php echo $form->error($model,'file'); ?>
	
	<div>&nbsp;</div>
	
	<div class="form-actions well">
		<?php $this->widget('booster.widgets.TbButton', array(
				'buttonType'=>'submit',
				'context'=>'primary',
				'icon'=>'ok',
				'label'=>'Save',
		)); ?>&nbsp;
	</div>

<?php $this->endWidget(); ?>
