<?php
$this->breadcrumbs=array(
	'Slide'=>array('admin'),
	'Kelola',
);
?>

<h1>Kelola Slide</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'plus',
		'label'=>'Tambah',
		'url'=>array('slide/create')
)); ?>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'slide-grid',
		'dataProvider'=>$model->search(),
		'type'=>'striped bordered',
		'filter'=>$model,
		'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'headerHtmlOptions'=>array('width'=>'5%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'class'=>'CDataColumn',
				'header'=>'title',
				'name'=>'title',
				'type'=>'raw',
				'value'=>'$data->title',
				'headerHtmlOptions'=>array('width'=>'35%')
			),
			array(
				'class'=>'CDataColumn',
				'header'=>'file',
				'type'=>'raw',
				'value'=>'$data->getImage()',
				'headerHtmlOptions'=>array('width'=>'40%')
			),
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'headerHtmlOptions'=>array('width'=>'10%')
			),
		),
)); ?>

