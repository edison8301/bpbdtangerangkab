<?php
$this->breadcrumbs=array(
	'Menus'=>array('index'),
	'Manage',
);



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('menu-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Kelola Menu</h1>

<?php /* $this->widget('bootstrap.widgets.TbButtonGroup', array(
    'type'=>'primary', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
    'buttons'=>array(
		array('label'=>'Pilihan Menu','items'=>array(
			array('label'=>'Tambah Menu','icon'=>'plus','url'=>array('/menu/create')),
		)),
    ),
)); */ ?>

<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'link','type'=>'primary','icon'=>'plus white','label'=>'Tambah Menu','url'=>array('menu/create'))); ?>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'menu-grid',
	'dataProvider'=>$model->search(),
	'type'=>'striped bordered',
	'filter'=>$model,
	'columns'=>array(
		'title',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
