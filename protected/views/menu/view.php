<?php
$this->breadcrumbs=array(
	'Menu'=>array('admin'),
	$model->title,
);
?>



<h1>Kelola Menu</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'plus white',
		'label'=>'Tambah Menu Item',
		'url'=>array('menuItem/create',
		'menu_id'=>$model->id)
)); ?>

<div>&nbsp;</div>

<table class="table table-hover">
<thead>
<tr>
	<th width="50%">Title</th>
	<th width="20%" style="text-align:left">Order</th>
	<th width="20%">Type</th>
	<th width="10%">Action</th>

</tr>
</thead>
<?php $level = 0; foreach($model->getDataMenuItem() as $data) { ?>

<?php $this->renderPartial('_menu_item',array('data'=>$data,'model'=>$model,'level'=>$level)); ?>

<?php } ?>

</table>