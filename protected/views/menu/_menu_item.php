<tr>
	<td width="50%" style="padding-left:<?php print $level*30; ?>px">
		
		<?php $this->widget('booster.widgets.TbButtonGroup', array(
				'context'=>'primary',
				'size'=>'extra_small',
				'buttons'=>array(
					array('label'=>'','items'=>array(
						array('label'=>'Sunting','icon'=>'pencil','url'=>array('/menuItem/update','id'=>$data->id)),
						array('label'=>'Tambah Submenu','icon'=>'pencil','url'=>array('/menuItem/create','parent_id'=>$data->id,'menu_id'=>$model->id)),
					)),
				),
		)); ?>
		<?php print $data->title; ?>
		</td>
	<td style="padding-left:<?php print 8+$level*30; ?>px">
		<?php $this->widget('booster.widgets.TbLabel',array(	
				'context' => 'success',
				'label' => $data->order,
		)); ?>
	</td>
	<td width="20%">
		<?php $this->widget('booster.widgets.TbLabel',array(	
				'context' => 'warning',
				'label' => $data->getRelationField("menu_item_type","title"),
		)); ?>
	<td width="10%">
		<?php print CHtml::link('<i class="glyphicon glyphicon-plus"></i>',array('/menuItem/create','parent_id'=>$data->id,'menu_id'=>$model->id),array('data-toggle'=>'tooltip','title'=>'Tambah Submenu')); ?>
		<?php print CHtml::link('<i class="glyphicon glyphicon-pencil"></i>',array('menuItem/update','id'=>$data->id),array('data-toggle'=>'tooltip','title'=>'Sunting')); ?>
		<?php print CHtml::link('<i class="glyphicon glyphicon-trash"></i>','#',array('submit'=>array('menuItem/delete','id'=>$data->id),'params'=>array('returnUrl'=>$this->createUrl('menu/view',array('id'=>$model->id))),'confirm'=>'Yakin akan menghapus data?','data-toggle'=>'tooltip','title'=>'Hapus')); ?>
	</td>
</tr>

<?php $level++; foreach($data->getDataMenuItemSub() as $subdata) { ?>

<?php $this->renderPartial('_menu_item',array('model'=>$model,'data'=>$subdata,'level'=>$level)); ?>

<?php } ?>