<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'menu-attribute-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Kolom dengan <span class="required">*</span> harus diisi.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'menu_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'key',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'value',array('class'=>'span5','maxlength'=>255)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
