<?php
$this->breadcrumbs=array(
	'Menu Attributes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MenuAttribute','url'=>array('index')),
	array('label'=>'Create MenuAttribute','url'=>array('create')),
	array('label'=>'View MenuAttribute','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage MenuAttribute','url'=>array('admin')),
);
?>

<h1>Sunting MenuAttribute</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>