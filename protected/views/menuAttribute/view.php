<?php
$this->breadcrumbs=array(
	'Menu Attributes'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MenuAttribute','url'=>array('index')),
	array('label'=>'Create MenuAttribute','url'=>array('create')),
	array('label'=>'Update MenuAttribute','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete MenuAttribute','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MenuAttribute','url'=>array('admin')),
);
?>

<h1>Lihat MenuAttribute #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'type'=>'striped bordered',
	'attributes'=>array(
		'id',
		'menu_id',
		'key',
		'value',
	),
)); ?>
