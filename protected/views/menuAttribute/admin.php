<?php
$this->breadcrumbs=array(
	'Menu Attributes'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List MenuAttribute','url'=>array('index')),
	array('label'=>'Create MenuAttribute','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('menu-attribute-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Atur Menu Attributes</h1>



<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'menu-attribute-grid',
	'dataProvider'=>$model->search(),
	'type'=>'striped bordered',
	'filter'=>$model,
	'columns'=>array(
		'id',
		'menu_id',
		'key',
		'value',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
