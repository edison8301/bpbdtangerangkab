<?php
$this->breadcrumbs=array(
	'Menu Attributes',
);

$this->menu=array(
	array('label'=>'Create MenuAttribute','url'=>array('create')),
	array('label'=>'Manage MenuAttribute','url'=>array('admin')),
);
?>

<h1>Menu Attributes</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
