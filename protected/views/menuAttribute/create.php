<?php
$this->breadcrumbs=array(
	'Menu Attributes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MenuAttribute','url'=>array('index')),
	array('label'=>'Manage MenuAttribute','url'=>array('admin')),
);
?>

<h1>Tambah MenuAttribute</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>