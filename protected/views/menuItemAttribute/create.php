<?php
$this->breadcrumbs=array(
	'Menu Item Attributes'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List MenuItemAttribute','url'=>array('index')),
array('label'=>'Manage MenuItemAttribute','url'=>array('admin')),
);
?>

<h1>Create MenuItemAttribute</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>