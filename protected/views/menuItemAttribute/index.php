<?php
$this->breadcrumbs=array(
	'Menu Item Attributes',
);

$this->menu=array(
array('label'=>'Create MenuItemAttribute','url'=>array('create')),
array('label'=>'Manage MenuItemAttribute','url'=>array('admin')),
);
?>

<h1>Menu Item Attributes</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
