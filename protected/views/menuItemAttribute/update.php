<?php
$this->breadcrumbs=array(
	'Menu Item Attributes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List MenuItemAttribute','url'=>array('index')),
	array('label'=>'Create MenuItemAttribute','url'=>array('create')),
	array('label'=>'View MenuItemAttribute','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage MenuItemAttribute','url'=>array('admin')),
	);
	?>

	<h1>Update MenuItemAttribute <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>