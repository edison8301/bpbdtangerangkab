<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'menu-item-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php $menu_id = isset($_GET['menu_id']) ? $_GET['menu_id'] : $model->menu_id; ?>
	
	<?php echo $form->hiddenField($model,'menu_id',array('value'=>$menu_id)); ?>

	<?php echo $form->textFieldGroup($model,'title',array('class'=>'span5','maxlength'=>255)); ?>
	
	<?php echo $form->dropDownListGroup($model,'parent_id',array(
			'widgetOptions'=>array(
				'data'=>Menu::model()->getParentList($menu_id),
				'htmlOptions'=>array('empty'=>'-- Select Parent --')
			),
	)); ?>
	
	<?php echo $form->textFieldGroup($model,'order',array('class'=>'span1')); ?>
	
	<?php echo $form->dropDownListGroup($model,'menu_item_type_id',array(
			'widgetOptions'=>array(
				'data'=>MenuItemType::getList(),
				'htmlOptions'=>array('class'=>'menu_item_type_id','empty'=>'-- Select Type --')
			),
	)); ?>
	
	<div class="form-group menu_attribute link">
		<?php print CHtml::label('URL',''); ?>
		<?php print CHtml::textField('MenuAttribute[url]',$model->getMenuAttributeValueByKey('url'),array('class'=>'form-control')); ?>
	</div>
	
	<div class="form-group menu_attribute page">
		<?php print CHtml::label('Pilih Page',''); ?>
		<?php print CHtml::dropDownList('MenuAttribute[page_id]',$model->getMenuAttributeValueByKey('page_id'),CHtml::listData(Page::model()->findAll(),'id','title'),array('class'=>'form-control')); ?>
	</div>
	
	<div class="form-group menu_attribute post">
		<?php print CHtml::label('Pilih Kategori Artikel',''); ?>
		<?php print CHtml::dropDownList('MenuAttribute[post_category_id]',$model->getMenuAttributeValueByKey('post_category_id'),CHtml::listData(PostCategory::model()->findAll(),'id','title'),array('class'=>'form-control')); ?>
	</div>
	
	<div class="form-group menu_attribute photo">
		<?php print CHtml::label('Pilih Foto',''); ?>
		<?php print CHtml::dropDownList('MenuAttribute[photo_id]',$model->getMenuAttributeValueByKey('photo_id'),CHtml::listData(Photo::model()->findAll(),'id','title'),array('class'=>'form-control')); ?>
	</div>
	
	<div class="form-group menu_attribute contact">
		<?php print CHtml::label('Email Tujuan',''); ?>
		<?php print CHtml::textField('MenuAttribute[form_contact_email]',$model->getMenuAttributeValueByKey('form_contact_email'),array('class'=>'form-control')); ?>
		
		<div>&nbsp;</div>
		
		<?php print CHtml::label('Teks',''); ?>
		<?php $this->widget('ext.editMe.widgets.ExtEditMe', array(
				'name'=>'MenuAttribute[form_contact_html]',
				'value'=>$model->getMenuAttributeValueByKey('form_contact_html')
		)); ?>
		
		
	</div>
	
	<script>
		function refreshMenuItemType()
		{
			$('.menu_attribute').hide();
			var type = $('.menu_item_type_id').val();
			$('.'+type).show();
		}
		
		$(document).ready(function() {
			refreshMenuItemType();
		});
		
		$('.menu_item_type_id').change(function() {
			refreshMenuItemType();
		});
	</script>
	

<div class="form-actions well">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok white',
			'label'=>'Simpan',
		)); ?>
</div>

<?php $this->endWidget(); ?>
