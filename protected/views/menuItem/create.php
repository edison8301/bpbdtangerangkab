<?php
$this->breadcrumbs=array(
	'Menu Items'=>array('index'),
	'Create',
);

$this->menu=array(
	//array('label'=>'List MenuItem','url'=>array('index')),
	//array('label'=>'Manage MenuItem','url'=>array('admin')),
);
?>

<h1>Tambah Menu Item</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>