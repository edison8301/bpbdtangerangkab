<?php
$this->breadcrumbs=array(
	'Validasis'=>array('index'),
	$model->id,
);

$this->menu=array(
	//array('label'=>'List Validasi','url'=>array('index')),
	array('label'=>'Tambah Validasi','url'=>array('create')),
	array('label'=>'Sunting Validasi','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Validasi','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola Validasi','url'=>array('admin')),
);
?>

<h1>Lihat Validasi</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			'nama',
			array(
				'label'=>'Katgori',
				'value'=>$model->KategoriValidasi->nama
			),	
		),
)); ?>
