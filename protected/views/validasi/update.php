<?php
$this->breadcrumbs=array(
	'Validasis'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Validasi','url'=>array('index')),
	array('label'=>'Create Validasi','url'=>array('create')),
	array('label'=>'View Validasi','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Validasi','url'=>array('admin')),
	);
	?>

	<h1>Update Validasi <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>