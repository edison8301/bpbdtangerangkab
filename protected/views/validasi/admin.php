<?php
$this->breadcrumbs=array(
	'Validasi'=>array('admin'),
	'Kelola',
);

$this->menu=array(
	//array('label'=>'List Validasi','url'=>array('index')),
	//array('label'=>'Tambah Validasi','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('validasi-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Kelola Validasi</h1>

<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'link','type'=>'primary','icon'=>'plus white','label'=>'Tambah Validasi','url'=>array('validasi/create'))); ?>&nbsp;

<?php $this->widget('bootstrap.widgets.TbGridView',array(
		'id'=>'validasi-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'nama',
			array(
				'header'=>'Kategori',
				'name'=>'kategori_validasi_id',
				'value'=>'$data->KategoriValidasi->nama',
				'filter'=>CHtml::listData(KategoriValidasi::model()->findAll(),'id','nama')
			),
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
			),
		),
)); ?>
