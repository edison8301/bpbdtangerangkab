<?php
$this->breadcrumbs=array(
	'Validasis'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Validasi','url'=>array('index')),
array('label'=>'Manage Validasi','url'=>array('admin')),
);
?>

<h1>Tambah Validasi</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>