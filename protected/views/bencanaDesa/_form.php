<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'bencana-desa-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

		<?php echo $form->dropDownListGroup($model,'id_bencana_kecamatan',array(
			'widgetOptions'=>array(
				'data'=>CHtml::listData(BencanaKecamatan::model()->findAll(array('order'=>'id ASC')),'id','nama')
			)
	)); ?>

	<?php echo $form->textFieldGroup($model,'nama',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

<div class="form-actions well">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon' => 'ok',
			'label'=>'Simpan',
		)); ?>
</div>

<?php $this->endWidget(); ?>
