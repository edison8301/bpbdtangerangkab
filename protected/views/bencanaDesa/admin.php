<?php
$this->breadcrumbs=array(
	'Bencana Desas'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List BencanaDesa','url'=>array('index')),
array('label'=>'Create BencanaDesa','url'=>array('create')),
);

?>

<h1>Kelola Desa</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'plus white',
		'label'=>'Tambah',
		'url'=>array('/bencanadesa/create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'list',
		'label'=>'Bencana',
		'url'=>array('/bencana/admin')
)); ?>&nbsp;

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'bencana-desa-grid',
'type' => 'striped bordered',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
		array(
			'header' => 'Bencana Kecamatan',
			'name' => 'id_bencana_kecamatan',
			'value' => '$data->getBencanaKecamatan()',
			'filter' => CHtml::ListData(BencanaKecamatan::model()->findAll(array('order' => 'nama ASC')), 'id', 'nama')
			),
		'nama',
array(
'class'=>'booster.widgets.TbButtonColumn',
'htmlOptions' => array('width' => '7%')
),
),
)); ?>
