<?php
$this->breadcrumbs=array(
	'Bencana Desas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List BencanaDesa','url'=>array('index')),
	array('label'=>'Create BencanaDesa','url'=>array('create')),
	array('label'=>'View BencanaDesa','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage BencanaDesa','url'=>array('admin')),
	);
	?>

	<h1>Update Bencana Desa <b><?php echo $model->nama; ?></b></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>