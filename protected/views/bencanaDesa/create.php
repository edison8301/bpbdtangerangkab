<?php
$this->breadcrumbs=array(
	'Bencana Desas'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List BencanaDesa','url'=>array('index')),
array('label'=>'Manage BencanaDesa','url'=>array('admin')),
);
?>

<h1>Input Bencana Desa</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>