<?php
$this->breadcrumbs=array(
	'Bencana Desas'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List BencanaDesa','url'=>array('index')),
array('label'=>'Create BencanaDesa','url'=>array('create')),
array('label'=>'Update BencanaDesa','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete BencanaDesa','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage BencanaDesa','url'=>array('admin')),
);
?>

<h1>Lihat Bencana <b><?php echo $model->nama; ?></b></h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'pencil white',
		'label'=>'Sunting',
		'url'=>array('/BencanaDesa/update','id'=>$model->id)
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'plus white',
		'label'=>'Tambah',
		'url'=>array('/BencanaDesa/create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'list white',
		'label'=>'Kelola',
		'url'=>array('/BencanaDesa/admin')
)); ?>

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type' => 'striped bordered',
'attributes'=>array(
		'id',
		array(
			'label' => 'Bencana Kecamatan',
			'type' => 'raw',
			'value' => $model->getBencanaKecamatan()),
		'nama',
),
)); ?>
