<?php
$this->breadcrumbs=array(
	'Role'=>array('admin'),
	$model->nama,
);
?>

<h1>Lihat Role</h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			'id',
			'nama',
		),
)); ?>
