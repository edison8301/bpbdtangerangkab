<?php
$this->breadcrumbs=array(
	'Role'=>array('index'),
	'Kelola',
);

$this->menu=array(
	//array('label'=>'List Role','url'=>array('index')),
	//array('label'=>'Buat Role','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('role-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Kelola Role</h1>

<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'link','type'=>'primary','icon'=>'plus white','label'=>'Tambah Role','url'=>array('role/create'))); ?>&nbsp;


<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'role-grid',
'dataProvider'=>$model->search(),
'type'=>'striped bordered hover',
'filter'=>$model,
'columns'=>array(
		//'id',
		array(
			'header'=>'No',
			'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
			'htmlOptions'=>array(
				'style'=>'text-align:center',
				'width'=>'30px',
		      ),
		),
		'nama',
		array(
				'header'=>'Akses',
				'type'=>'raw',
				'value'=>'CHtml::link("<i class=\"icon-lock\"></i>",array("role/access","id"=>$data->id))'
			),
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
