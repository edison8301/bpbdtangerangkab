<?php
$this->breadcrumbs=array(
	'Roles'=>array('index'),
	$model->id,
);


?>

<h1>Hak Akses Role <?php echo $model->nama; ?></h1>

<?php print CHtml::beginForm(); ?>

<?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'submit','type'=>'primary','label'=>'Simpan','icon'=>'ok white')); ?>

<div>&nbsp;</div>


<table class="table table-striped table-bordered">
<thead>
<tr>
	<th>No</th>
	<th>Nama Akses</th>
	<th>Sub Akses</th>
	<th>Pilih Akses</th>
</tr>
</thead>
<?php $i=1; foreach(Akses::model()->findAll(array('order'=>'nama_controller, nama_action ASC')) as $akses) { ?>
<tr>
	<td><?php print $i; ?></td>
	<td><?php print $akses->nama_controller; ?></td>
	<td><?php print $akses->nama_action; ?></td>
	<td><?php print CHtml::checkBox('RoleAkses['.$akses->id.']',$akses->cekAkses($_GET['id'])); ?></td>
</tr>
<?php $i++; } ?>
</table>

<?php print CHtml::endForm(); ?>