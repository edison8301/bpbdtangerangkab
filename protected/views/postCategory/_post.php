<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

	<div class="row">
		<div class="col-xs-12">
			<h2 style="font-size:15px;line-height:20px;margin:0px 0px 5px 0px;font-weight:bold"><?php echo CHtml::link($data->title, array('/post/read','id'=>$data->id));?></h2>
			<div style="margin-bottom:10px;padding-left:0px;font-size:12px">
				<i class="glyphicon glyphicon-calendar"></i> <?php print $this->getWaktuDibuat($data->created_time); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-3" style="padding-right:0px">
			<div style="border:1px solid #333333;padding:3px;background:#ffffff;-webkit-border-radius: 3px 3px 3px 3px;border-radius: 3px 3px 3px 3px;">
				<?php print $data->getThumbnail(array('class'=>'img-responsive')); ?>
			</div>
		</div>
		<div class="col-xs-9">
			<?php echo $data->getExcerpt(300);?>&nbsp;
			<?php echo CHtml::link('[Selanjutnya]', array('/post/read','id'=>$data->id));?></p>
		</div>
	</div>
	
	<div style="margin-bottom:20px">&nbsp;</div>

	