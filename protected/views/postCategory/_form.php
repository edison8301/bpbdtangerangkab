<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'post-category-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->dropDownListGroup($model,'parent_id',array(
			'widgetOptions'=>array(
				'data'=>PostCategory::model()->getParentList(),
				'htmlOptions'=>array('empty'=>'-- Select Parent --')
			)
	)); ?>

	<?php echo $form->textFieldGroup($model,'title',array('class'=>'span5','maxlength'=>255)); ?>

	<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon'=>'ok',
			'label'=>'Simpan',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
