<?php
$this->breadcrumbs=array(
	'Posts',
);

$this->menu=array(
	array('label'=>'Create Post','url'=>array('create')),
	array('label'=>'Manage Post','url'=>array('admin')),
);
?>

<div class="margin20" id="centerbar">

<div class="block-title">
	<span><?php print strtoupper($model->title); ?></span>
</div>


<?php $this->widget('booster.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_read',
)); ?>

</div>