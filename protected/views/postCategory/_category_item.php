<tr>
	<td style="padding-left:<?php print $level*30; ?>px">
		<?php $this->widget('booster.widgets.TbButtonGroup', array(
				'context'=>'primary',
				'size'=>'extra_small',
				'buttons'=>array(
					array('label'=>'','items'=>array(
						array('label'=>'Sunting','icon'=>'pencil','url'=>array('/postCategory/update','id'=>$data->id)),
						array('label'=>'Tambah Subkategori','icon'=>'plus','url'=>array('/postCategory/create','parent_id'=>$data->id)),										)),
				),
		)); ?>
		<?php print $data->title; ?>	</td>	<td style="text-align:center">		<?php $this->widget('booster.widgets.TbLabel',array(				'context' => 'success',				'label' => $data->countPost(),		)); ?>	</td>
	<td style="text-align:center">
		<?php $this->widget('booster.widgets.TbLabel',array(
				'context' => 'info',
				'label' => $data->countTotalPost(),
		)); ?>
	</td>
	<td style="text-align:center">
		<?php print CHtml::link('<i class="glyphicon glyphicon-plus"></i>',array('postCategory/create','parent_id'=>$data->id),array('data-toggle'=>'tooltip','title'=>'Tambah Subkategori')); ?>				<?php print CHtml::link('<i class="glyphicon glyphicon-pencil"></i>',array('postCategory/update','id'=>$data->id),array('data-toggle'=>'tooltip','title'=>'Sunting')); ?>
		<?php print CHtml::link('<i class="glyphicon glyphicon-trash"></i>','#',array(				'submit'=>array('postCategory/delete','id'=>$data->id),				'params'=>array('returnUrl'=>$this->createUrl('postCategory/admin')),'confirm'=>'Yakin akan menghapus data?',				'data-toggle'=>'tooltip',				'title'=>'Hapus'		)); ?>
	</td>
	
</tr>
<?php $level++; foreach($data->getDataSubCategory() as $subdata) { ?>
<?php $this->renderPartial('_category_item',array('model'=>$model,'data'=>$subdata,'level'=>$level)); ?>
<?php } ?>