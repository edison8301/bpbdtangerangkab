<?php $block = new Block; ?>
<?php $post_category_id = $block->getBlockAttributeValueByKey('post_list_post_category_id'); ?>

<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>
<?php $i=1; foreach(Post::model()->getLatestPostByCategoryId($post_category_id,1) as $model){ ?>
	<div class="row">
		<div class="col-xs-12">
			<h2 style="font-size:16px;margin:0px 0px 5px 0px;font-weight:bold"><?php echo CHtml::link($data->Post->title, array('/post/read','id'=>$data->post_id));?></h2>
			<div style="margin-bottom:10px;padding-left:0px;font-size:12px">
				<i class="glyphicon glyphicon-calendar"></i> <?php print Bantu::getTanggalDibuat($data->Post->time_created); ?>
			</div>
		</div>
		<div class="col-xs-3" style="padding-right:0px">
			<div style="border:1px solid #333333;padding:3px;background:#ffffff;-webkit-border-radius: 3px 3px 3px 3px;border-radius: 3px 3px 3px 3px;">
				<?php print $data->Post->getImage(array('class'=>'img-responsive')); ?>
			</div>
		</div>
		<div class="col-xs-9">
			<?php echo substr($data->Post->content,0,300).'...';?>
			<?php echo CHtml::link('[Selanjutnya]', array('/post/read','id'=>$data->post_id));?>

		</div>
	</div>
	
	<div>&nbsp;</div>
<?php } ?>
