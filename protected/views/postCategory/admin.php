<?php

$this->breadcrumbs=array(

	'Menu'=>array('admin'),

	$model->title=>'#',

);



?>



<h1>Kelola Kategori Post</h1>


<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'plus white',
		'label'=>'Tambah Kategori',
		'url'=>array('postCategory/create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'list white',
		'label'=>'Kelola Post',
		'url'=>array('post/admin')
)); ?>



<div>&nbsp;</div>



<table class="table">

<tr>

	<th width="70%">Title</th>
	<th width="10%" style="text-align:center">Jumlah Post</th>
	<th width="10%" style="text-align:center">Total Post</th>
	<th width="10%" style="text-align:center">Action</th>

</tr>

<?php $level = 0; foreach($model->getDataCategory() as $data) { ?>

<?php $this->renderPartial('_category_item',array('data'=>$data,'model'=>$model,'level'=>$level)); ?>

<?php } ?>

</table>