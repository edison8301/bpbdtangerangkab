<?php
$this->breadcrumbs=array(
	'Role Akses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List RoleAkses','url'=>array('index')),
	array('label'=>'Create RoleAkses','url'=>array('create')),
	array('label'=>'View RoleAkses','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage RoleAkses','url'=>array('admin')),
	);
	?>

	<h1>Update RoleAkses <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>