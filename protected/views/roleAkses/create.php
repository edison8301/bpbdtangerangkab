<?php
$this->breadcrumbs=array(
	'Role Akses'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List RoleAkses','url'=>array('index')),
array('label'=>'Manage RoleAkses','url'=>array('admin')),
);
?>

<h1>Create RoleAkses</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>