<?php
$this->breadcrumbs=array(
	'Role Akses'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List RoleAkses','url'=>array('index')),
array('label'=>'Create RoleAkses','url'=>array('create')),
array('label'=>'Update RoleAkses','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete RoleAkses','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage RoleAkses','url'=>array('admin')),
);
?>

<h1>View RoleAkses #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'role_id',
		'akses_id',
		'status',
),
)); ?>
