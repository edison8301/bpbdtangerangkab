<?php
$this->breadcrumbs=array(
	'Photo Albums'=>array('index'),
	'Manage',
);
?>

<h1>Kelola Album Foto</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'plus white',
		'label'=>'Tambah Album',
		'url'=>array('photoAlbum/create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'list white',
		'label'=>'Kelola Foto',
		'url'=>array('photo/admin')
)); ?>&nbsp;

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'photo-album-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'headerHtmlOptions'=>array('width'=>'5%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			'title',
			'time_created',
			array(
				'class'=>'booster.widgets.TbButtonColumn',
			),
		),
)); ?>
