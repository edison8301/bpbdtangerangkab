<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(

	'id'=>'photo-album-form',

	'enableAjaxValidation'=>false,

)); ?>


	<p class="help-block">Fields with <span class="required">*</span> are required.</p>



	<?php echo $form->errorSummary($model); ?>


	<?php echo $form->textFieldGroup($model,'title',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>


	<?php echo $form->ckEditorGroup($model,'description', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>6, 'cols'=>50, 'class'=>'span8')))); ?>

	<?php echo $form->textFieldGroup($model,'time_created',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>


	<div class="form-actions well">

	<?php $this->widget('booster.widgets.TbButton', array(

			'buttonType'=>'submit',

			'context'=>'primary',

			'icon'=>'ok',

			'label'=>'Simpan',

		)); ?>
	</div>



<?php $this->endWidget(); ?>
