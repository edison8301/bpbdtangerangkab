<?php
$this->breadcrumbs=array(
	'Photo Albums'=>array('index'),
	$model->title,
);


?>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl."/vendors/lightbox/js/lightbox.min.js"); ?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl."/vendors/lightbox/css/lightbox.css"); ?>


<div id="centerbar">
	<div class="block-title">
		<span>Galeri Foto</span>
	</div>

	<div style="font-weight:bold;margin-top:10px;font-size:15px">
		<?php print $model->title; ?>
	</div>

	<div style="margin-top:5px;padding-left:5px;font-size:11px">
			<i class="glyphicon glyphicon-calendar"></i> <?php print $this->getWaktuDibuat($model->time_created); ?>
	</div>
	

	<div>&nbsp;</div>

	<div class="row">
	<div class="col-xs-12">
		<?php $i=1; foreach($model->getDataPhoto() as $data) { ?>
		<div class="photo-list" style="border:1px solid #999">
			<div class="photo-list-item">
				<?php print CHtml::link($data->getImage(array('class'=>'img-responsive')),Yii::app()->baseUrl."/uploads/photo/".$data->file,array('data-lightbox'=>'image-'.$data->id,'data-title'=>$data->title)); ?>
			</div>
		</div>
		<?php $i++; } ?>
	</div>
</div>

<div>&nbsp;</div>

<?php print $model->description; ?>

</div>