<?php
$this->breadcrumbs=array(
	'Photo Albums'=>array('index'),
	$model->title,
);
?>

<h1>Lihat Album Foto</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'pencil white',
		'label'=>'Sunting',
		'url'=>array('update','id'=>$model->id)
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'plus white',
		'label'=>'Tambah',
		'url'=>array('create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'list white',
		'label'=>'Kelola',
		'url'=>array('admin')
)); ?>&nbsp;

<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			'title',
			array(
				'label'=>'Description',
				'type'=>'raw',
				'value'=>$model->description
			),
			'time_created',
		),
)); ?>

<h2>Foto</h2>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'plus',
		'label'=>'Tambah',
		'url'=>array('photo/create','photo_album_id'=>$model->id,'model'=>'photoAlbum','model_id'=>$model->id)
)); ?>&nbsp;

<div>&nbsp;</div>

<table class="table">
<tr>
	<th>No</th>
	<th>Foto</th>
	<th>&nbsp;</th>
</tr>
<?php $i=1; foreach($model->getDataPhoto() as $data) { ?>
<tr>
	<td><?php print $i; ?></td>
	<td><?php print $data->getImage(); ?></td>
	<td>&nbsp;</td>
</tr>
<?php $i++; } ?>
</table>