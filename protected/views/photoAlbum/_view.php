<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<div class="row" style="margin-bottom:10px">
	<div class="col-xs-3" style="padding-right:0px;">
		<div style="border:1px solid #333333;padding:3px;background:#ffffff;-webkit-border-radius: 3px 3px 3px 3px;border-radius: 3px 3px 3px 3px;">
			<?php print $data->getFirstImage(array('class'=>'img-responsive')) ; ?>
		</div>
	</div>
	<div class="col-xs-9">
		<h2 style="font-size:15px;margin:0px 0px 5px 0px;font-weight:bold">
			<?php echo CHtml::link($data->title, array('/photoAlbum/detail','id'=>$data->id));?>
		</h2>
		<div style="margin-bottom:10px;padding-left:0px;font-size:12px">
				<i class="glyphicon glyphicon-calendar"></i> <?php print $this->getWaktuDibuat($data->time_created); ?>
		</div>
	</div>
</div>
	