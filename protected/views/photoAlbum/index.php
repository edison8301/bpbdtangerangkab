<?php
$this->breadcrumbs=array(
	'Photo Albums',
);

$this->menu=array(
array('label'=>'Create PhotoAlbum','url'=>array('create')),
array('label'=>'Manage PhotoAlbum','url'=>array('admin')),
);
?>

<div class="margin20" id="centerbar">
	
	<div class="block-title">
		<span>Galeri Foto</span>
	</div>

	<?php $this->widget('booster.widgets.TbListView',array(
		'dataProvider'=>$dataProvider,
		'itemView'=>'_view',
	)); ?>

</div>
