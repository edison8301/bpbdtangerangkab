<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(

	'id'=>'link-form',

	'enableAjaxValidation'=>false,

	'htmlOptions'=>array('enctype'=>'multipart/form-data')

)); ?>



<p class="help-block">Fields with <span class="required">*</span> are required.</p>



<?php echo $form->errorSummary($model); ?>



	<?php echo $form->textFieldGroup($model,'title',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	

	<?php echo $form->textFieldGroup($model,'url',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	

	<?php echo $form->dropDownListGroup($model,'link_category_id',array('widgetOptions'=>array('data'=>CHtml::listData(LinkCategory::model()->findAll(),'id','title'),'htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>



	<?php echo $form->textFieldGroup($model,'order',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

	<?php echo $form->checkBoxGroup($model,'new_tab',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

	<?php echo $form->labelEx($model,'image',array('label'=>'Gambar Maksimal 200 KB')); ?>

	<?php if($model->image!='') { ?>

	<?php print CHTml::image(Yii::app()->request->baseUrl.'/uploads/link/'.$model->image."","",array("class"=>"")); ?>

	<?php } ?>

	<?php echo $form->fileField($model,'image'); ?>

	<?php echo $form->error($model,'image'); ?>

	

	<div>&nbsp;</div>

	

	<div class="form-actions well">

	<?php $this->widget('booster.widgets.TbButton', array(

			'buttonType'=>'submit',

			'context'=>'primary',

			'icon'=>'ok',

			'label'=>$model->isNewRecord ? 'Add' : 'Update',

		)); ?>

	</div>



<?php $this->endWidget(); ?>

