<?php
$this->breadcrumbs=array(
	'Links'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);?>

	<h1>Update Link</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>