<?php $this->breadcrumbs=array(
	'Link'=>array('admin'),
	'Kelola',
); ?>

<h1>Kelola Link</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Add Link',
		'icon'=>'plus',
		'context'=>'primary',
		'url'=>array('link/create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Category',
		'icon'=>'list',
		'context'=>'primary',
		'url'=>array('linkCategory/admin')
)); ?>

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'link-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'ajaxUpdate'=>false,
		'filter'=>$model,
		'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'headerHtmlOptions'=>array('width'=>'5%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center'),
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'title',
				'value'=>'$data->title',
				'headerHtmlOptions'=>array('width'=>'30%','style'=>'text-align:center'),
				'htmlOptions'=>array()
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'order',
				'value'=>'$data->order',
				'headerHtmlOptions'=>array('width'=>'5%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'url',
				'value'=>'$data->url',
				'headerHtmlOptions'=>array('width'=>'15%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'image',
				'type'=>'raw',
				'value'=>'Link::model()->getBackendThumbnail($data->image,"")',
				'headerHtmlOptions'=>array('width'=>'20%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center')
			),
			array(
				'class'=>'CDataColumn',
				'name'=>'link_category_id',
				'value'=>'$data->getRelationField("LinkCategory","title")',
				'filter'=>CHtml::listData(LinkCategory::model()->findAll(),'id','title'),
				'headerHtmlOptions'=>array('width'=>'15%','style'=>'text-align:center'),
				'htmlOptions'=>array('style'=>'text-align:center')
			),
			
			array(
				'class'=>'booster.widgets.TbButtonColumn',
				'headerHtmlOptions'=>array('width'=>'10%')
			),
		),
)); ?>
