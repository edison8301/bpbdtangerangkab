<?php
$this->breadcrumbs=array(
	'Links'=>array('index'),
	$model->title,
);?>

<h1>View Link</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Update',
		'icon'=>'pencil',
		'context'=>'primary',
		'url'=>array('link/update','id'=>$model->id)
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Add',
		'context'=>'primary',
		'icon'=>'plus white',
		'url'=>array('/link/create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Manage',
		'context'=>'primary',
		'icon'=>'list white',
		'url'=>array('/link/admin')
)); ?>
<div>&nbsp;</div>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'type'=>'striped bordered',
'attributes'=>array(
		'title',
		'url',
		array(
			'label'=>'Category',
			'type'=>'raw',
			'value'=>$model->getRelationField("LinkCategory","title")
		),
		array(
			'label'=>'Image',
			'type'=>'raw',
			'value'=>$model->getImage()
		),
		'order',
		'time_created',
),
)); ?>
