<?php
$this->breadcrumbs=array(
	'User Attributes',
);

$this->menu=array(
array('label'=>'Create UserAttribute','url'=>array('create')),
array('label'=>'Manage UserAttribute','url'=>array('admin')),
);
?>

<h1>User Attributes</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
