<?php
$this->breadcrumbs=array(
	'User Attributes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List UserAttribute','url'=>array('index')),
	array('label'=>'Create UserAttribute','url'=>array('create')),
	array('label'=>'View UserAttribute','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage UserAttribute','url'=>array('admin')),
	);
	?>

	<h1>Update UserAttribute <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>