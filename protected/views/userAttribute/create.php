<?php
$this->breadcrumbs=array(
	'User Attributes'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List UserAttribute','url'=>array('index')),
array('label'=>'Manage UserAttribute','url'=>array('admin')),
);
?>

<h1>Create UserAttribute</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>