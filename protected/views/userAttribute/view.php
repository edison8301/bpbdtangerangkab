<?php
$this->breadcrumbs=array(
	'User Attributes'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List UserAttribute','url'=>array('index')),
array('label'=>'Create UserAttribute','url'=>array('create')),
array('label'=>'Update UserAttribute','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete UserAttribute','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage UserAttribute','url'=>array('admin')),
);
?>

<h1>View UserAttribute #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'user_id',
		'key',
		'value',
),
)); ?>
