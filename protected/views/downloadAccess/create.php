<?php
$this->breadcrumbs=array(
	'Download Accesses'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List DownloadAccess','url'=>array('index')),
array('label'=>'Manage DownloadAccess','url'=>array('admin')),
);
?>

<h1>Create DownloadAccess</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>