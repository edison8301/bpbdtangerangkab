<?php
$this->breadcrumbs=array(
	'Download Accesses',
);

$this->menu=array(
array('label'=>'Create DownloadAccess','url'=>array('create')),
array('label'=>'Manage DownloadAccess','url'=>array('admin')),
);
?>

<h1>Download Accesses</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
