<?php
$this->breadcrumbs=array(
	'Download Accesses'=>array('index'),
	$model->title,
);

$this->menu=array(
array('label'=>'List DownloadAccess','url'=>array('index')),
array('label'=>'Create DownloadAccess','url'=>array('create')),
array('label'=>'Update DownloadAccess','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete DownloadAccess','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage DownloadAccess','url'=>array('admin')),
);
?>

<h1>View DownloadAccess #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'title',
),
)); ?>
