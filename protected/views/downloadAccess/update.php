<?php
$this->breadcrumbs=array(
	'Download Accesses'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List DownloadAccess','url'=>array('index')),
	array('label'=>'Create DownloadAccess','url'=>array('create')),
	array('label'=>'View DownloadAccess','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage DownloadAccess','url'=>array('admin')),
	);
	?>

	<h1>Update DownloadAccess <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>