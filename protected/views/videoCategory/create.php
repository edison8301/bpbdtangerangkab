<?php
$this->breadcrumbs=array(
	'Video Categories'=>array('admin'),
	'Create',
);
?>

<h1>Tambah Kategori Video</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>