<?php
$this->breadcrumbs=array(
	'Video Categories'=>array('admin'),
	'Manage',
);

?>

<h1>Kelola Kategori Video</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'plus white',
		'label'=>'Tambah Kategori',
		'url'=>array('videoCategory/create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'context'=>'primary',
		'icon'=>'list white',
		'label'=>'Kelola Video',
		'url'=>array('video/admin')
)); ?>&nbsp;

<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'video-category-grid',
		'type'=>'striped bordered',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
			'title',
			array(
				'class'=>'booster.widgets.TbButtonColumn',
			),
		),
)); ?>
