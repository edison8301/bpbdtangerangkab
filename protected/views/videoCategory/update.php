<?php
$this->breadcrumbs=array(
	'Video Categories'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List VideoCategory','url'=>array('index')),
	array('label'=>'Create VideoCategory','url'=>array('create')),
	array('label'=>'View VideoCategory','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage VideoCategory','url'=>array('admin')),
	);
	?>

	<h1>Sunting Kategori Video</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>