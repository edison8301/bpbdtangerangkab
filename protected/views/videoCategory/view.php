<?php
$this->breadcrumbs=array(
	'Video Categories'=>array('index'),
	$model->title,
);

?>

<h1>Lihat Kategori Video</h1>

<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Sunting',
		'context'=>'primary',
		'icon'=>'pencil white',
		'url'=>array('update','id'=>$model->id)
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Tambah',
		'context'=>'primary',
		'icon'=>'plus white',
		'url'=>array('create')
)); ?>&nbsp;
<?php $this->widget('booster.widgets.TbButton',array(
		'buttonType'=>'link',
		'label'=>'Kelola',
		'context'=>'primary',
		'icon'=>'list white',
		'url'=>array('admin')
)); ?>

<div>&nbsp;</div>


<?php $this->widget('booster.widgets.TbDetailView',array(
		'data'=>$model,
		'type'=>'striped bordered',
		'attributes'=>array(
			'id',
			'title',
		),
)); ?>
