<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/admin/main'); ?>
<div class="caption">
        <h1 class="page-title"><?php print $this->pageTitle; ?></h1>
</div>
<div class="box-breadcumbs">
        <?php if(isset($this->breadcrumbs)):
                if ( Yii::app()->controller->route !== 'site/index' )
                    $this->breadcrumbs = array_merge(array (Yii::t('zii','<i class="icon-home"></i>')=>Yii::app()->homeUrl.'?r=site/index'), $this->breadcrumbs);
                    
                    $this->widget('zii.widgets.CBreadcrumbs', array(
                            'links'=>$this->breadcrumbs,
                            'homeLink'=>false,
                            'encodeLabel'=>false,
                            'tagName'=>'ul',
                            'separator'=>'',
                            'activeLinkTemplate'=>'<li><a href="{url}">{label}</a> <span class="divider">/</span></li>',
                            'inactiveLinkTemplate'=>'<li><span>{label}</span></li>',
                            'htmlOptions'=>array ('class'=>'breadcrumb')
                    ));
                    //<!-- breadcrumbs -->
        endif?>
        <?php
                foreach(Yii::app()->user->getFlashes() as $key => $message) {
                        echo '<div class="alert alert-' . $key . '">';
                        echo '<button type="button" class="close" data-dismiss="alert">�</button>';
                        print $message;
                        print "</div>\n";
                        
                        
                }
        ?>
</div>
<div  id="content">
        <?php echo $content; ?>
</div>
<?php $this->endContent(); ?>