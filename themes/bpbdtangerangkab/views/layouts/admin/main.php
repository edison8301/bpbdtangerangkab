<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
		
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admin/admin.css" />
	
</head>

<body>


<?php $this->widget('booster.widgets.TbNavbar',array(
        'brand' => '',
        'fixed' => false,
    	'fluid' => true,
        'items' => array(
            array(
                'class' => 'booster.widgets.TbMenu',
            	'type' => 'navbar',
                'items' => array(
                    array('label' => 'Home', 'url' => array('site/index'), 'icon'=>'home'),
                    array('label' => 'Admin', 'url' => array('admin/index'), 'icon'=>'wrench'),
                    array('label' => 'Logout', 'url' => array('site/logout'), 'icon'=>'off','linkOptions'=>array('class'=>'pull-right')),
                )
            )
        )
    )
); ?>

<div style="padding:0px 15px;">
	<div class="row">
		<div class="col-xs-2">
			<?php $this->widget('booster.widgets.TbMenu', array(
					'type'=>'list',
					'stacked'=>true,
					'items'=>array(
						array('label'=>'Dashboard','icon'=>'wrench','url'=>array('/admin/index')),
						array('label'=>'Bencana','icon'=>'bullhorn','url'=>array('/bencana/admin','id'=>1)),
						array('label'=>'Laporan','icon'=>'wrench','url'=>array('/bencana/report')),
						array('label'=>'Menu','icon'=>'list','url'=>array('/menu/view','id'=>1)),
						array('label'=>'Page','icon'=>'file','url'=>array('/page/admin')),
						array('label'=>'Post','icon'=>'book','url'=>array('/post/admin')),
						array('label'=>'Photo','icon'=>'picture','url'=>array('/photo/admin')),
						array('label'=>'Video','icon'=>'play','url'=>array('/video/admin')),
						array('label'=>'Download','icon'=>'hdd','url'=>array('/download/admin')),
						array('label'=>'Block','icon'=>'th-large','url'=>array('/block/admin')),																		
						array('label'=>'Link','icon'=>'link','url'=>array('/link/admin')),						
						array('label'=>'Event','icon'=>'calendar','url'=>array('/event/admin')),
						array('label'=>'Slide','icon'=>'play-circle','url'=>array('/slide/admin')),
						'---',
						array('label'=>'Member','icon'=>'user white','url'=>array('/member/admin')),
						array('label'=>'User','icon'=>'user white','url'=>array('/user/admin')),
						array('label'=>'Backup','icon'=>'hdd white','url'=>array('/jbackup')),
						array('label'=>'Setting','icon'=>'cog','url'=>array('/setting/admin')),
						array('label'=>'Logout','icon'=>'off','url'=>array('/site/logout')),
					),
			)); ?>			
		</div>
		
		<div class="col-xs-10">
			<?php echo $content; ?>
		</div>
	</div>
	<!--Footer Panel -->
	<div class="clear"></div>
	
	<div id="footer">	
		Copyright &copy <?php echo date('Y');?> PKP2A I LAN
	</div><!-- footer -->
	
</div><!--page-->

</body>
</html>
