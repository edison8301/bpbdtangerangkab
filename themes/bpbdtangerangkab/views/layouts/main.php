<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/fonts.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/flaticon.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css" />
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<?php if(!Yii::app()->user->isGuest) { ?>
<div id="adminnav">
<?php $this->widget('booster.widgets.TbNavbar',array(
        'brand' => '',
        'fixed' => false,	
    	'fluid' => true,
        'items' => array(
            array(
                'class' => 'booster.widgets.TbMenu',
            	'type' => 'navbar',
                'items' => array(
                    array('label' => 'Home', 'url' => array('site/index'), 'icon'=>'home'),
                    array('label' => 'Admin', 'url' => array('admin/index'), 'icon'=>'wrench'),
                    array('label' => '('.Yii::app()->user->id.') Logout', 'url' => array('site/logout'), 'icon'=>'off'),
                )
            )
        )
    )
); ?>
</div>
<?php } ?>
<div id="wrapper">
	
	<div class="container" id="page">
		
		<div class="row" id="header">
			<div id="logo">
				<img class="glow" src="<?php print Yii::app()->theme->baseUrl; ?>/images/logo.png" class="img-responsive">
			</div>
		</div><!-- header -->
		<div class="row" id="mainnav">
				<?php $this->widget('booster.widgets.TbNavbar',array(
						'brand' => '',
						'fixed' => false,
						'fluid' => true,
						'htmlOptions' => array(),
						'items' => array(
							array(
								'class' => 'booster.widgets.TbMenu',
								'type' => 'navbar',
								'items' => Menu::model()->getMenuItemById(1)
							)
						)
				));?>
		</div>
		<div id="content">
			<?php echo $content; ?>
		</div>
	</div><!-- page -->
	<div id="bottom" class="container">
		<div class="row">
		<div class="col-md-3">
			<?php foreach(Block::findAllByPosition('bottom_1') as $block) { ?>				
			<?php print $block->getBlockDisplay(); ?>		
			<?php } ?>
		</div>
		<div class="col-md-6">
			<?php foreach(Block::findAllByPosition('bottom_2') as $block) { ?>				
			<?php print $block->getBlockDisplay(); ?>		
			<?php } ?>
		</div>
		<?php /*
		<div class="col-md-3">
			<?php foreach(Block::model()->findAllByAttributes(array('block_position_id'=>'bottom_3','active_id'=>1),array('order'=>'t.order ASC')) as $block) { ?>				
			<?php print $block->getBlockDisplay(); ?>		
			<?php } ?>
		</div>
		*/ ?>
		<div class="col-md-3">
			<?php foreach(Block::findAllByPosition('bottom_3') as $block) { ?>			
			<?php print $block->getBlockDisplay(); ?>		
			<?php } ?>
		</div>
		</div>
		<div class="row" id="copyright">
			<div class="col-md-12">
				<div>
					Copyright &copy; <?php echo date('Y'); ?> BPBD Kabupaten Tangerang
				</div>
			</div>
		</div>
	</div>
	<?php /*
	<div id="footer">
		<div class="container">
			<div id="copyright">
			Copyright &copy; <?php echo date('Y'); ?> PKP2A I LAN

			
			</div>
		</div>
	</div><!-- footer -->
	*/ ?>
</div>
</body>
</html>
